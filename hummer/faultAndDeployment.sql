PGDMP                         w            security_server    11.2    11.2 "   �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                        0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    143743    security_server    DATABASE     �   CREATE DATABASE security_server WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_India.1252' LC_CTYPE = 'English_India.1252';
    DROP DATABASE security_server;
             postgres    false                       1255    143744    delete_previous_prop()    FUNCTION     �   CREATE FUNCTION public.delete_previous_prop() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
delete from bmsconfig where ip = new.ip and name = new.name;
RETURN new;
END;
$$;
 -   DROP FUNCTION public.delete_previous_prop();
       public       postgres    false            "           1255    143745    insert_config_data()    FUNCTION     &  CREATE FUNCTION public.insert_config_data() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE
result record;
BEGIN
      select * from config_data where config_data.device_id=new.device_id into result;
      insert into config_data_history(ip_add, floor, room, calibration, start_freq, stop_freq, threshold, mask_offset, use_mask, start_time, stop_time, cable_length, preamp_type, gsm_dl, wcdma_dl, wifi_band, lte_dl, band_start1, band_stop1, band_en1, band_start2, band_stop2, band_en2, band_start3, band_stop3, band_en3, band_start4, band_stop4, band_en4, band_start5, band_stop5, band_en5, device_id, country_code)
values(result.ip_add,result.floor,result.room,result.calibration,result.start_freq,result.stop_freq,result.threshold,result.mask_offset,result.use_mask,result.start_time,
result.stop_time,result.cable_length,result.preamp_type,result.gsm_dl,result.wcdma_dl,result.wifi_band,result.lte_dl,
             result.band_start1, result.band_stop1, result.band_en1, result.band_start2, result.band_stop2,
             result.band_en2, result.band_start3, result.band_stop3, result.band_en3,
             result.band_start4, result.band_stop4, result.band_en4, result.band_start5,
             result.band_stop5, result.band_en5, result.device_id, result.country_code);
      RETURN new;

    END;

$$;
 +   DROP FUNCTION public.insert_config_data();
       public       postgres    false            #           1255    143746    move_alarm_to_history_table()    FUNCTION     �  CREATE FUNCTION public.move_alarm_to_history_table() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN

insert into alarms_history select * from alarms where id = new.id;
if new.severity == 0 THEN
delete from alarms where mangedoject_id = new.mangedoject_id and component_id = new.component_id and event_id = new.event_id;
ELSE
delete from alarms where mangedoject_id = new.mangedoject_id and component_id = new.component_id and event_id = new.event_id and id != new.id;
END IF;
RETURN new;
END;
$$;
 4   DROP FUNCTION public.move_alarm_to_history_table();
       public       postgres    false            $           1255    143747 $   move_alarm_to_history_table_on_ack()    FUNCTION     �   CREATE FUNCTION public.move_alarm_to_history_table_on_ack() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
insert into alarms_history select * from alarms where id = new.id;
delete from alarms where ip = new.ip;
RETURN new;
END;
$$;
 ;   DROP FUNCTION public.move_alarm_to_history_table_on_ack();
       public       postgres    false            %           1255    143748 '   move_alarm_to_history_table_on_update()    FUNCTION       CREATE FUNCTION public.move_alarm_to_history_table_on_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
insert into alarms_history select * from alarms where id = new.id;
if new.severity == 0 THEN
delete from alarms where id = new.id;
END IF;
RETURN new;
END;
$$;
 >   DROP FUNCTION public.move_alarm_to_history_table_on_update();
       public       postgres    false            &           1255    143749 "   move_bms_status_to_history_table()    FUNCTION     �   CREATE FUNCTION public.move_bms_status_to_history_table() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
insert into bmsstatus_history select * from bmsstatus where ip = new.ip;
delete from bmsstatus where ip = new.ip;
RETURN new;
END;
$$;
 9   DROP FUNCTION public.move_bms_status_to_history_table();
       public       postgres    false            '           1255    143750 
   move_gps()    FUNCTION     �   CREATE FUNCTION public.move_gps() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
 IF NEW.lat <> OLD.lat OR NEW.lon <> OLD.lon THEN
 INSERT INTO gps_data(lat,lon)
 VALUES(OLD.lat::numeric, OLD.lon::numeric);
 END IF;
 
 RETURN NEW;
END;
$$;
 !   DROP FUNCTION public.move_gps();
       public       postgres    false                       1255    143751 %   move_jmdevice_data_to_history_table()    FUNCTION       CREATE FUNCTION public.move_jmdevice_data_to_history_table() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN

insert into jmdevicedata_history select * from jmdevice_data where id = new.id;
delete from jmdevice_data where deviceid = new.deviceid and id!=new.id;
RETURN new;
END;
$$;
 <   DROP FUNCTION public.move_jmdevice_data_to_history_table();
       public       postgres    false                       1255    143752    update_peak_info()    FUNCTION       CREATE FUNCTION public.update_peak_info() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

	DECLARE
	result record;
	BEGIN
		  update peak_info set currentconfigid= (SELECT id FROM config_data_history ORDER BY ID DESC LIMIT 1) where currentconfigid=-1;
		  RETURN new;

		END;

	$$;
 )   DROP FUNCTION public.update_peak_info();
       public       postgres    false            �            1259    143753 
   alarm_info    TABLE     �  CREATE TABLE public.alarm_info (
    id integer NOT NULL,
    type character varying,
    trigger character varying,
    power character varying,
    frequency character varying,
    "time" character varying,
    angle character varying,
    range numeric,
    duration bigint,
    sample bigint,
    lat numeric,
    lon numeric,
    ip_addr character varying,
    alarm boolean,
    transid character varying,
    cueid character varying,
    emitterpower integer
);
    DROP TABLE public.alarm_info;
       public         postgres    false            �            1259    143759    alarm_info_id_seq    SEQUENCE     �   CREATE SEQUENCE public.alarm_info_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.alarm_info_id_seq;
       public       postgres    false    196                       0    0    alarm_info_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.alarm_info_id_seq OWNED BY public.alarm_info.id;
            public       postgres    false    197            �            1259    143761    alarms    TABLE     �  CREATE TABLE public.alarms (
    id integer NOT NULL,
    ip character varying,
    component_id character varying,
    component_type numeric,
    mangedoject_id character varying,
    managedobject_type numeric,
    event_id character varying,
    event_type numeric,
    severity numeric,
    event_desctiption text,
    generation_time numeric,
    status numeric DEFAULT 1,
    logtime timestamp without time zone DEFAULT now()
);
    DROP TABLE public.alarms;
       public         postgres    false            �            1259    143769    component_type    TABLE     �   CREATE TABLE public.component_type (
    id integer NOT NULL,
    component character varying,
    component_type int4range,
    status boolean DEFAULT true
);
 "   DROP TABLE public.component_type;
       public         postgres    false            �            1259    143776 
   event_type    TABLE     �   CREATE TABLE public.event_type (
    id integer NOT NULL,
    event_node character varying,
    event_type int4range,
    status boolean DEFAULT true
);
    DROP TABLE public.event_type;
       public         postgres    false            �            1259    143783    managedobject_type    TABLE     �   CREATE TABLE public.managedobject_type (
    id integer NOT NULL,
    managed_object character varying,
    managed_object_id numeric,
    status boolean DEFAULT true
);
 &   DROP TABLE public.managedobject_type;
       public         postgres    false            �            1259    143790    severity    TABLE     �   CREATE TABLE public.severity (
    id integer NOT NULL,
    severity character varying,
    severity_id numeric,
    status boolean DEFAULT true
);
    DROP TABLE public.severity;
       public         postgres    false            �            1259    143797    alarms_data    VIEW     �  CREATE VIEW public.alarms_data AS
 SELECT alarms.id,
    alarms.ip,
    alarms.component_id,
    alarms.component_type,
    alarms.mangedoject_id,
    alarms.managedobject_type,
    alarms.event_id,
    alarms.event_type,
    alarms.severity,
    alarms.event_desctiption,
    alarms.generation_time,
    alarms.status,
    alarms.logtime,
    event_type.event_node,
    component_type.component,
    managedobject_type.managed_object,
    severity.severity AS severity_type
   FROM ((((public.alarms
     LEFT JOIN public.event_type ON (((alarms.event_type)::integer <@ event_type.event_type)))
     LEFT JOIN public.component_type ON (((alarms.component_type)::integer <@ component_type.component_type)))
     LEFT JOIN public.managedobject_type ON ((alarms.managedobject_type = managedobject_type.managed_object_id)))
     LEFT JOIN public.severity ON ((alarms.severity = severity.severity_id)));
    DROP VIEW public.alarms_data;
       public       postgres    false    202    201    201    200    200    199    199    198    198    198    198    198    198    198    198    198    198    198    198    198    202            �            1259    143802    alarms_history    TABLE     �  CREATE TABLE public.alarms_history (
    id integer,
    ip character varying,
    component_id numeric,
    componen_type character varying,
    mangedoject_id numeric,
    managedobject_type character varying,
    event_id numeric,
    event_type character varying,
    severity numeric,
    event_desctiption text,
    generation_time timestamp without time zone,
    acknowledged boolean,
    logtime timestamp without time zone
);
 "   DROP TABLE public.alarms_history;
       public         postgres    false            �            1259    143808    alarms_id_seq    SEQUENCE     �   CREATE SEQUENCE public.alarms_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.alarms_id_seq;
       public       postgres    false    198                       0    0    alarms_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.alarms_id_seq OWNED BY public.alarms.id;
            public       postgres    false    205            �            1259    143810 	   audit_log    TABLE     �   CREATE TABLE public.audit_log (
    id integer NOT NULL,
    log_type character varying,
    logtime character varying,
    description text
);
    DROP TABLE public.audit_log;
       public         postgres    false            �            1259    143816    audit_log_id_seq    SEQUENCE     �   CREATE SEQUENCE public.audit_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.audit_log_id_seq;
       public       postgres    false    206                       0    0    audit_log_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.audit_log_id_seq OWNED BY public.audit_log.id;
            public       postgres    false    207            �            1259    143818    bms_info    TABLE     �   CREATE TABLE public.bms_info (
    id integer NOT NULL,
    on_btn character varying,
    reset_btn character varying,
    pr_btn character varying
);
    DROP TABLE public.bms_info;
       public         postgres    false            �            1259    143824    bms_info_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bms_info_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.bms_info_id_seq;
       public       postgres    false    208                       0    0    bms_info_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.bms_info_id_seq OWNED BY public.bms_info.id;
            public       postgres    false    209            �            1259    143826 	   bmsconfig    TABLE     �   CREATE TABLE public.bmsconfig (
    id integer NOT NULL,
    ip character varying,
    name character varying,
    tag character varying,
    value character varying
);
    DROP TABLE public.bmsconfig;
       public         postgres    false            �            1259    143832    bmsconfig_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bmsconfig_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.bmsconfig_id_seq;
       public       postgres    false    210                       0    0    bmsconfig_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.bmsconfig_id_seq OWNED BY public.bmsconfig.id;
            public       postgres    false    211            �            1259    143834 	   bmsstatus    TABLE       CREATE TABLE public.bmsstatus (
    id integer NOT NULL,
    ip character varying,
    bcv1 numeric,
    bcv2 numeric,
    bcv3 numeric,
    bcv4 numeric,
    bcv5 numeric,
    bcv6 numeric,
    bcv7 numeric,
    bcv8 numeric,
    bcv9 numeric,
    bcv10 numeric,
    bcv11 numeric,
    bcv12 numeric,
    bcv13 numeric,
    bcv14 numeric,
    btv numeric,
    tbc numeric,
    soc numeric,
    btemp numeric,
    alarmword numeric,
    logtime timestamp without time zone DEFAULT now(),
    generationtime numeric
);
    DROP TABLE public.bmsstatus;
       public         postgres    false            �            1259    143841    bmsstatus_history    TABLE     �  CREATE TABLE public.bmsstatus_history (
    id integer,
    ip character varying,
    bcv1 numeric,
    bcv2 numeric,
    bcv3 numeric,
    bcv4 numeric,
    bcv5 numeric,
    bcv6 numeric,
    bcv7 numeric,
    bcv8 numeric,
    bcv9 numeric,
    bcv10 numeric,
    bcv11 numeric,
    bcv12 numeric,
    bcv13 numeric,
    bcv14 numeric,
    btv numeric,
    tbc numeric,
    soc numeric,
    btemp numeric,
    alarmword numeric,
    logtime timestamp without time zone,
    generationtime numeric
);
 %   DROP TABLE public.bmsstatus_history;
       public         postgres    false            �            1259    143847    bmsstatus_id_seq    SEQUENCE     �   CREATE SEQUENCE public.bmsstatus_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.bmsstatus_id_seq;
       public       postgres    false    212                       0    0    bmsstatus_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.bmsstatus_id_seq OWNED BY public.bmsstatus.id;
            public       postgres    false    214            �            1259    143849 
   color_code    TABLE     6  CREATE TABLE public.color_code (
    id integer NOT NULL,
    startfreq integer,
    stopfreq integer,
    networktype character varying,
    color character varying,
    depl character varying,
    epr character varying,
    modulation character varying,
    visible boolean,
    profile character varying
);
    DROP TABLE public.color_code;
       public         postgres    false            �            1259    143855    color_code_id_seq    SEQUENCE     �   CREATE SEQUENCE public.color_code_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.color_code_id_seq;
       public       postgres    false    215                       0    0    color_code_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.color_code_id_seq OWNED BY public.color_code.id;
            public       postgres    false    216            �            1259    143857    commands    TABLE     �   CREATE TABLE public.commands (
    id integer NOT NULL,
    node_id numeric,
    tag character varying,
    cmd character varying,
    status boolean DEFAULT true
);
    DROP TABLE public.commands;
       public         postgres    false            �            1259    143864    commands_id_seq    SEQUENCE     �   CREATE SEQUENCE public.commands_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.commands_id_seq;
       public       postgres    false    217            	           0    0    commands_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.commands_id_seq OWNED BY public.commands.id;
            public       postgres    false    218            �            1259    143866    component_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.component_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.component_type_id_seq;
       public       postgres    false    199            
           0    0    component_type_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.component_type_id_seq OWNED BY public.component_type.id;
            public       postgres    false    219            �            1259    143868    config_data    TABLE     �  CREATE TABLE public.config_data (
    ip_add character varying NOT NULL,
    floor character varying,
    room character varying NOT NULL,
    calibration character varying,
    start_freq character varying,
    stop_freq character varying,
    threshold character varying,
    mask_offset character varying,
    use_mask character varying,
    start_time character varying,
    stop_time character varying,
    cable_length character varying,
    preamp_type character varying,
    gsm_dl character varying,
    wcdma_dl character varying,
    wifi_band character varying,
    lte_dl character varying,
    band_start1 character varying,
    band_stop1 character varying,
    band_en1 character varying,
    band_start2 character varying,
    band_stop2 character varying,
    band_en2 character varying,
    band_start3 character varying,
    band_stop3 character varying,
    band_en3 character varying,
    band_start4 character varying,
    band_stop4 character varying,
    band_en4 character varying,
    band_start5 character varying,
    band_stop5 character varying,
    band_en5 character varying,
    device_id bigint,
    country_code character varying,
    distance character varying,
    tx_power character varying,
    fade_margin character varying
);
    DROP TABLE public.config_data;
       public         postgres    false            �            1259    143874    config_data_history    TABLE       CREATE TABLE public.config_data_history (
    id integer NOT NULL,
    ip_add character varying NOT NULL,
    floor character varying,
    room character varying,
    calibration character varying,
    start_freq character varying,
    stop_freq character varying,
    threshold character varying,
    mask_offset character varying,
    use_mask character varying,
    start_time character varying,
    stop_time character varying,
    cable_length character varying,
    preamp_type character varying,
    gsm_dl character varying,
    wcdma_dl character varying,
    wifi_band character varying,
    lte_dl character varying,
    band_start1 character varying,
    band_stop1 character varying,
    band_en1 character varying,
    band_start2 character varying,
    band_stop2 character varying,
    band_en2 character varying,
    band_start3 character varying,
    band_stop3 character varying,
    band_en3 character varying,
    band_start4 character varying,
    band_stop4 character varying,
    band_en4 character varying,
    band_start5 character varying,
    band_stop5 character varying,
    band_en5 character varying,
    device_id bigint,
    country_code character varying,
    fade_margin character varying,
    tx_power character varying,
    distance character varying
);
 '   DROP TABLE public.config_data_history;
       public         postgres    false            �            1259    143880    config_data_history_id_seq    SEQUENCE     �   CREATE SEQUENCE public.config_data_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public.config_data_history_id_seq;
       public       postgres    false    221                       0    0    config_data_history_id_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public.config_data_history_id_seq OWNED BY public.config_data_history.id;
            public       postgres    false    222            �            1259    143882    config_profile    TABLE     �  CREATE TABLE public.config_profile (
    ip_add character varying NOT NULL,
    floor character varying,
    room character varying NOT NULL,
    calibration character varying,
    start_freq character varying,
    stop_freq character varying,
    threshold character varying,
    mask_offset character varying,
    use_mask character varying,
    start_time character varying,
    stop_time character varying,
    cable_length character varying,
    preamp_type character varying,
    gsm_dl character varying,
    wcdma_dl character varying,
    wifi_band character varying,
    lte_dl character varying,
    band_start1 character varying,
    band_stop1 character varying,
    band_en1 character varying,
    band_start2 character varying,
    band_stop2 character varying,
    band_en2 character varying,
    band_start3 character varying,
    band_stop3 character varying,
    band_en3 character varying,
    band_start4 character varying,
    band_stop4 character varying,
    band_en4 character varying,
    band_start5 character varying,
    band_stop5 character varying,
    band_en5 character varying,
    device_id bigint,
    country_code character varying,
    profile character varying,
    id integer NOT NULL
);
 "   DROP TABLE public.config_profile;
       public         postgres    false            �            1259    143888    config_profile_id_seq    SEQUENCE     �   CREATE SEQUENCE public.config_profile_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.config_profile_id_seq;
       public       postgres    false    223                       0    0    config_profile_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.config_profile_id_seq OWNED BY public.config_profile.id;
            public       postgres    false    224            �            1259    143890    databasechangelog    TABLE     Y  CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);
 %   DROP TABLE public.databasechangelog;
       public         postgres    false            �            1259    143896    databasechangeloglock    TABLE     �   CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);
 )   DROP TABLE public.databasechangeloglock;
       public         postgres    false            �            1259    143899    device_info    TABLE     )  CREATE TABLE public.device_info (
    id integer NOT NULL,
    ip_add character varying,
    name character varying,
    color character varying,
    lat character varying,
    lon character varying,
    is_active character varying,
    state character varying,
    starttime character varying
);
    DROP TABLE public.device_info;
       public         postgres    false            �            1259    143905    device_info_id_seq    SEQUENCE     {   CREATE SEQUENCE public.device_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.device_info_id_seq;
       public       postgres    false    227                       0    0    device_info_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.device_info_id_seq OWNED BY public.device_info.id;
            public       postgres    false    228            �            1259    143907    devices    TABLE     {   CREATE TABLE public.devices (
    id integer NOT NULL,
    node_name character varying,
    status boolean DEFAULT true
);
    DROP TABLE public.devices;
       public         postgres    false            �            1259    143914    devices_id_seq    SEQUENCE     �   CREATE SEQUENCE public.devices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.devices_id_seq;
       public       postgres    false    229                       0    0    devices_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.devices_id_seq OWNED BY public.devices.id;
            public       postgres    false    230            �            1259    143916    event_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.event_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.event_type_id_seq;
       public       postgres    false    200                       0    0    event_type_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.event_type_id_seq OWNED BY public.event_type.id;
            public       postgres    false    231            �            1259    143918    frequencyconfiguration_data    TABLE     �   CREATE TABLE public.frequencyconfiguration_data (
    id integer NOT NULL,
    frequency numeric,
    bandwidth numeric,
    freqtype character varying,
    profile character varying
);
 /   DROP TABLE public.frequencyconfiguration_data;
       public         postgres    false            �            1259    143924 "   frequencyconfiguration_data_id_seq    SEQUENCE     �   CREATE SEQUENCE public.frequencyconfiguration_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 9   DROP SEQUENCE public.frequencyconfiguration_data_id_seq;
       public       postgres    false    232                       0    0 "   frequencyconfiguration_data_id_seq    SEQUENCE OWNED BY     i   ALTER SEQUENCE public.frequencyconfiguration_data_id_seq OWNED BY public.frequencyconfiguration_data.id;
            public       postgres    false    233            �            1259    143926    gps_data    TABLE     �   CREATE TABLE public.gps_data (
    id integer NOT NULL,
    lat numeric,
    lon numeric,
    inserttime timestamp without time zone DEFAULT now()
);
    DROP TABLE public.gps_data;
       public         postgres    false            �            1259    143933    gps_data_id_seq    SEQUENCE     �   CREATE SEQUENCE public.gps_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.gps_data_id_seq;
       public       postgres    false    234                       0    0    gps_data_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.gps_data_id_seq OWNED BY public.gps_data.id;
            public       postgres    false    235            �            1259    143935    jmdata    TABLE     	  CREATE TABLE public.jmdata (
    ip_add character varying,
    "time" character varying NOT NULL,
    angle character varying,
    lat double precision,
    lon double precision,
    jmpacketdatetime character varying,
    created_at timestamp without time zone
);
    DROP TABLE public.jmdata;
       public         postgres    false            �            1259    143941    jmdevice_data    TABLE     �   CREATE TABLE public.jmdevice_data (
    id integer NOT NULL,
    deviceid numeric NOT NULL,
    "time" timestamp without time zone DEFAULT now(),
    roll character varying,
    tilt character varying,
    pan character varying
);
 !   DROP TABLE public.jmdevice_data;
       public         postgres    false            �            1259    143948    jmdevicedata_history    TABLE     �   CREATE TABLE public.jmdevicedata_history (
    id integer NOT NULL,
    deviceid numeric,
    "time" timestamp without time zone DEFAULT now(),
    roll character varying,
    tilt character varying,
    pan character varying
);
 (   DROP TABLE public.jmdevicedata_history;
       public         postgres    false            �            1259    143955    jmdevice_data_history_id_seq    SEQUENCE     �   CREATE SEQUENCE public.jmdevice_data_history_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public.jmdevice_data_history_id_seq;
       public       postgres    false    238                       0    0    jmdevice_data_history_id_seq    SEQUENCE OWNED BY     \   ALTER SEQUENCE public.jmdevice_data_history_id_seq OWNED BY public.jmdevicedata_history.id;
            public       postgres    false    239            �            1259    143957    jmdevice_data_id_seq    SEQUENCE     �   CREATE SEQUENCE public.jmdevice_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.jmdevice_data_id_seq;
       public       postgres    false    237                       0    0    jmdevice_data_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.jmdevice_data_id_seq OWNED BY public.jmdevice_data.id;
            public       postgres    false    240            �            1259    143959    jmnode_mapping    TABLE     q   CREATE TABLE public.jmnode_mapping (
    deviceid numeric NOT NULL,
    antennaid numeric,
    sector numeric
);
 "   DROP TABLE public.jmnode_mapping;
       public         postgres    false            �            1259    143965    jmnode_mapping_id_seq    SEQUENCE     �   CREATE SEQUENCE public.jmnode_mapping_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.jmnode_mapping_id_seq;
       public       postgres    false            �            1259    143967 
   led_status    TABLE     }   CREATE TABLE public.led_status (
    ip_add character varying NOT NULL,
    led_on integer,
    device_id bigint NOT NULL
);
    DROP TABLE public.led_status;
       public         postgres    false            �            1259    143973    managedobject_type_id_seq    SEQUENCE     �   CREATE SEQUENCE public.managedobject_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.managedobject_type_id_seq;
       public       postgres    false    201                       0    0    managedobject_type_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.managedobject_type_id_seq OWNED BY public.managedobject_type.id;
            public       postgres    false    244            �            1259    143975    min_threshold    TABLE     �   CREATE TABLE public.min_threshold (
    id integer NOT NULL,
    startfreq numeric,
    stopfreq numeric,
    power numeric,
    attenuation numeric
);
 !   DROP TABLE public.min_threshold;
       public         postgres    false            �            1259    143981    min_threshold_id_seq    SEQUENCE     �   CREATE SEQUENCE public.min_threshold_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.min_threshold_id_seq;
       public       postgres    false    245                       0    0    min_threshold_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.min_threshold_id_seq OWNED BY public.min_threshold.id;
            public       postgres    false    246            �            1259    143983 	   node_info    TABLE     �   CREATE TABLE public.node_info (
    id integer NOT NULL,
    ip_add character varying,
    type character varying,
    status character varying
);
    DROP TABLE public.node_info;
       public         postgres    false            �            1259    143989    node_info_id_seq    SEQUENCE     �   CREATE SEQUENCE public.node_info_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.node_info_id_seq;
       public       postgres    false    247                       0    0    node_info_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.node_info_id_seq OWNED BY public.node_info.id;
            public       postgres    false    248            �            1259    143991    ptz_info    TABLE     �   CREATE TABLE public.ptz_info (
    device_ip character varying NOT NULL,
    ptz_ip character varying,
    ptz_offset character varying,
    name character varying,
    sector_offset character varying
);
    DROP TABLE public.ptz_info;
       public         postgres    false            �            1259    143997 
   nodes_info    VIEW       CREATE VIEW public.nodes_info AS
 SELECT device_info.ip_add,
    device_info.name
   FROM public.device_info
UNION
 SELECT node_info.ip_add,
    node_info.type AS name
   FROM public.node_info
UNION
 SELECT ptz_info.ptz_ip AS ip_add,
    ptz_info.name
   FROM public.ptz_info;
    DROP VIEW public.nodes_info;
       public       postgres    false    227    227    247    247    249    249            �            1259    144001 	   peak_info    TABLE       CREATE TABLE public.peak_info (
    ip_add character varying,
    power character varying,
    frequency character varying,
    "time" character varying,
    device_id bigint,
    id integer NOT NULL,
    angle character varying,
    currentconfigid bigint,
    antennaid character varying,
    alarm boolean,
    jmdevice_id bigint,
    type character varying,
    sector character varying,
    inserttime timestamp without time zone DEFAULT now(),
    technology character varying,
    lat numeric,
    lon numeric
);
    DROP TABLE public.peak_info;
       public         postgres    false            �            1259    144008    peak_info_id_seq    SEQUENCE     y   CREATE SEQUENCE public.peak_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.peak_info_id_seq;
       public       postgres    false    251                       0    0    peak_info_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.peak_info_id_seq OWNED BY public.peak_info.id;
            public       postgres    false    252            �            1259    144010    peak_jm_view    VIEW     �  CREATE VIEW public.peak_jm_view AS
 SELECT peak_info.id,
    peak_info.ip_add,
    peak_info.power,
    peak_info.frequency,
    peak_info."time",
    peak_info.device_id,
    peak_info.angle,
    peak_info.antennaid,
    peak_info.alarm,
    peak_info.jmdevice_id,
    peak_info.type,
    peak_info.sector,
    peak_info.inserttime,
    peak_info.lat,
    peak_info.lon
   FROM public.peak_info;
    DROP VIEW public.peak_jm_view;
       public       postgres    false    251    251    251    251    251    251    251    251    251    251    251    251    251    251    251            �            1259    144014 	   peak_view    VIEW     �  CREATE VIEW public.peak_view AS
 SELECT peak_info.ip_add,
    peak_info.power,
    peak_info.frequency,
    peak_info."time",
    peak_info.device_id,
    peak_info.id,
    peak_info.angle,
    peak_info.currentconfigid,
    peak_info.antennaid,
    peak_info.alarm,
    peak_info.jmdevice_id,
    peak_info.type,
    peak_info.sector,
    peak_info.inserttime
   FROM public.peak_info
  ORDER BY peak_info.id;
    DROP VIEW public.peak_view;
       public       postgres    false    251    251    251    251    251    251    251    251    251    251    251    251    251    251            �            1259    144018    priority    TABLE     l   CREATE TABLE public.priority (
    id integer NOT NULL,
    priority integer,
    name character varying
);
    DROP TABLE public.priority;
       public         postgres    false                        1259    144024    priority_id_seq    SEQUENCE     �   CREATE SEQUENCE public.priority_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.priority_id_seq;
       public       postgres    false    255                       0    0    priority_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.priority_id_seq OWNED BY public.priority.id;
            public       postgres    false    256                       1259    144026    ptz_position_data    TABLE     �   CREATE TABLE public.ptz_position_data (
    id integer NOT NULL,
    ptz_ip character varying,
    pan_position real,
    tilt_position real,
    date_time character varying,
    system_time character varying
);
 %   DROP TABLE public.ptz_position_data;
       public         postgres    false                       1259    144032    ptz_position_data_id_seq    SEQUENCE     �   CREATE SEQUENCE public.ptz_position_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.ptz_position_data_id_seq;
       public       postgres    false    257                       0    0    ptz_position_data_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.ptz_position_data_id_seq OWNED BY public.ptz_position_data.id;
            public       postgres    false    258                       1259    144034    role    TABLE     �   CREATE TABLE public.role (
    id integer NOT NULL,
    role character varying(255),
    active integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
    DROP TABLE public.role;
       public         postgres    false                       1259    144037    role_id_seq    SEQUENCE     t   CREATE SEQUENCE public.role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.role_id_seq;
       public       postgres    false    259                       0    0    role_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.role_id_seq OWNED BY public.role.id;
            public       postgres    false    260                       1259    144272    sensor_config    TABLE     �  CREATE TABLE public.sensor_config (
    sensornum integer NOT NULL,
    sensorenabled character varying,
    sensorip character varying,
    sensorport integer NOT NULL,
    rotatingsensor character varying,
    omcip character varying,
    rfswitchip character varying,
    angle double precision,
    beamwidth double precision,
    gsmserverip character varying,
    usestatus character varying,
    sector character varying
);
 !   DROP TABLE public.sensor_config;
       public         postgres    false                       1259    144039    severity_id_seq    SEQUENCE     �   CREATE SEQUENCE public.severity_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.severity_id_seq;
       public       postgres    false    202                       0    0    severity_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.severity_id_seq OWNED BY public.severity.id;
            public       postgres    false    261                       1259    144041    switch_conf    TABLE     �   CREATE TABLE public.switch_conf (
    id integer NOT NULL,
    sid integer,
    pathid integer,
    switchid integer,
    val character varying,
    seq integer
);
    DROP TABLE public.switch_conf;
       public         postgres    false                       1259    144047    switch_conf_id_seq    SEQUENCE     �   CREATE SEQUENCE public.switch_conf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.switch_conf_id_seq;
       public       postgres    false    262                       0    0    switch_conf_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.switch_conf_id_seq OWNED BY public.switch_conf.id;
            public       postgres    false    263                       1259    144049    systemconfiguration_data    TABLE     �   CREATE TABLE public.systemconfiguration_data (
    id integer NOT NULL,
    ugs integer,
    tmdas integer,
    timeout integer,
    validity integer,
    selftimeout numeric,
    auto integer,
    selfvalidity integer,
    profile character varying
);
 ,   DROP TABLE public.systemconfiguration_data;
       public         postgres    false            	           1259    144055    systemconfiguration_data_id_seq    SEQUENCE     �   CREATE SEQUENCE public.systemconfiguration_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 6   DROP SEQUENCE public.systemconfiguration_data_id_seq;
       public       postgres    false    264                       0    0    systemconfiguration_data_id_seq    SEQUENCE OWNED BY     c   ALTER SEQUENCE public.systemconfiguration_data_id_seq OWNED BY public.systemconfiguration_data.id;
            public       postgres    false    265            
           1259    144057    task_priority    TABLE     �   CREATE TABLE public.task_priority (
    ip_add character varying,
    priority character varying NOT NULL,
    device_name character varying
);
 !   DROP TABLE public.task_priority;
       public         postgres    false                       1259    144063    trigger_data    TABLE     ?  CREATE TABLE public.trigger_data (
    ip_add character varying NOT NULL,
    unit_no character varying,
    product_name character varying,
    model character varying,
    mfg_date character varying,
    client character varying,
    location character varying,
    country character varying,
    device_id bigint
);
     DROP TABLE public.trigger_data;
       public         postgres    false                       1259    144069 	   user_role    TABLE     �   CREATE TABLE public.user_role (
    id integer NOT NULL,
    user_id bigint,
    role_id bigint,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
    DROP TABLE public.user_role;
       public         postgres    false                       1259    144072    user_role_id_seq    SEQUENCE     y   CREATE SEQUENCE public.user_role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.user_role_id_seq;
       public       postgres    false    268                       0    0    user_role_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.user_role_id_seq OWNED BY public.user_role.id;
            public       postgres    false    269                       1259    144074    user_setting    TABLE     �   CREATE TABLE public.user_setting (
    id integer NOT NULL,
    type character varying,
    mode character varying,
    gps character varying,
    gps_accuracy numeric
);
     DROP TABLE public.user_setting;
       public         postgres    false                       1259    144080    user_setting_id_seq    SEQUENCE     �   CREATE SEQUENCE public.user_setting_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public.user_setting_id_seq;
       public       postgres    false    270                       0    0    user_setting_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.user_setting_id_seq OWNED BY public.user_setting.id;
            public       postgres    false    271                       1259    144082    users    TABLE       CREATE TABLE public.users (
    id integer NOT NULL,
    active integer,
    email character varying(255),
    last_name character varying(255),
    password character varying(255),
    first_name character varying(255),
    mobile character varying(255),
    username character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
    DROP TABLE public.users;
       public         postgres    false                       1259    144088    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    272                        0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       postgres    false    273            �           2604    144090    alarm_info id    DEFAULT     n   ALTER TABLE ONLY public.alarm_info ALTER COLUMN id SET DEFAULT nextval('public.alarm_info_id_seq'::regclass);
 <   ALTER TABLE public.alarm_info ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    197    196            �           2604    144091 	   alarms id    DEFAULT     f   ALTER TABLE ONLY public.alarms ALTER COLUMN id SET DEFAULT nextval('public.alarms_id_seq'::regclass);
 8   ALTER TABLE public.alarms ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    205    198            �           2604    144092    audit_log id    DEFAULT     l   ALTER TABLE ONLY public.audit_log ALTER COLUMN id SET DEFAULT nextval('public.audit_log_id_seq'::regclass);
 ;   ALTER TABLE public.audit_log ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    207    206            �           2604    144093    bms_info id    DEFAULT     j   ALTER TABLE ONLY public.bms_info ALTER COLUMN id SET DEFAULT nextval('public.bms_info_id_seq'::regclass);
 :   ALTER TABLE public.bms_info ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    209    208            �           2604    144094    bmsconfig id    DEFAULT     l   ALTER TABLE ONLY public.bmsconfig ALTER COLUMN id SET DEFAULT nextval('public.bmsconfig_id_seq'::regclass);
 ;   ALTER TABLE public.bmsconfig ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    211    210            �           2604    144095    bmsstatus id    DEFAULT     l   ALTER TABLE ONLY public.bmsstatus ALTER COLUMN id SET DEFAULT nextval('public.bmsstatus_id_seq'::regclass);
 ;   ALTER TABLE public.bmsstatus ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    214    212            �           2604    144096    color_code id    DEFAULT     n   ALTER TABLE ONLY public.color_code ALTER COLUMN id SET DEFAULT nextval('public.color_code_id_seq'::regclass);
 <   ALTER TABLE public.color_code ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    216    215            �           2604    144097    commands id    DEFAULT     j   ALTER TABLE ONLY public.commands ALTER COLUMN id SET DEFAULT nextval('public.commands_id_seq'::regclass);
 :   ALTER TABLE public.commands ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    218    217            �           2604    144098    component_type id    DEFAULT     v   ALTER TABLE ONLY public.component_type ALTER COLUMN id SET DEFAULT nextval('public.component_type_id_seq'::regclass);
 @   ALTER TABLE public.component_type ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    219    199            �           2604    144099    config_data_history id    DEFAULT     �   ALTER TABLE ONLY public.config_data_history ALTER COLUMN id SET DEFAULT nextval('public.config_data_history_id_seq'::regclass);
 E   ALTER TABLE public.config_data_history ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    222    221            �           2604    144100    config_profile id    DEFAULT     v   ALTER TABLE ONLY public.config_profile ALTER COLUMN id SET DEFAULT nextval('public.config_profile_id_seq'::regclass);
 @   ALTER TABLE public.config_profile ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    224    223            �           2604    144101    device_info id    DEFAULT     p   ALTER TABLE ONLY public.device_info ALTER COLUMN id SET DEFAULT nextval('public.device_info_id_seq'::regclass);
 =   ALTER TABLE public.device_info ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    228    227            �           2604    144102 
   devices id    DEFAULT     h   ALTER TABLE ONLY public.devices ALTER COLUMN id SET DEFAULT nextval('public.devices_id_seq'::regclass);
 9   ALTER TABLE public.devices ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    230    229            �           2604    144103    event_type id    DEFAULT     n   ALTER TABLE ONLY public.event_type ALTER COLUMN id SET DEFAULT nextval('public.event_type_id_seq'::regclass);
 <   ALTER TABLE public.event_type ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    231    200            �           2604    144104    frequencyconfiguration_data id    DEFAULT     �   ALTER TABLE ONLY public.frequencyconfiguration_data ALTER COLUMN id SET DEFAULT nextval('public.frequencyconfiguration_data_id_seq'::regclass);
 M   ALTER TABLE public.frequencyconfiguration_data ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    233    232            �           2604    144105    gps_data id    DEFAULT     j   ALTER TABLE ONLY public.gps_data ALTER COLUMN id SET DEFAULT nextval('public.gps_data_id_seq'::regclass);
 :   ALTER TABLE public.gps_data ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    235    234            �           2604    144106    jmdevice_data id    DEFAULT     t   ALTER TABLE ONLY public.jmdevice_data ALTER COLUMN id SET DEFAULT nextval('public.jmdevice_data_id_seq'::regclass);
 ?   ALTER TABLE public.jmdevice_data ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    240    237            �           2604    144107    jmdevicedata_history id    DEFAULT     �   ALTER TABLE ONLY public.jmdevicedata_history ALTER COLUMN id SET DEFAULT nextval('public.jmdevice_data_history_id_seq'::regclass);
 F   ALTER TABLE public.jmdevicedata_history ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    239    238            �           2604    144108    managedobject_type id    DEFAULT     ~   ALTER TABLE ONLY public.managedobject_type ALTER COLUMN id SET DEFAULT nextval('public.managedobject_type_id_seq'::regclass);
 D   ALTER TABLE public.managedobject_type ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    244    201            �           2604    144109    min_threshold id    DEFAULT     t   ALTER TABLE ONLY public.min_threshold ALTER COLUMN id SET DEFAULT nextval('public.min_threshold_id_seq'::regclass);
 ?   ALTER TABLE public.min_threshold ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    246    245            �           2604    144110    node_info id    DEFAULT     l   ALTER TABLE ONLY public.node_info ALTER COLUMN id SET DEFAULT nextval('public.node_info_id_seq'::regclass);
 ;   ALTER TABLE public.node_info ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    248    247            �           2604    144111    peak_info id    DEFAULT     l   ALTER TABLE ONLY public.peak_info ALTER COLUMN id SET DEFAULT nextval('public.peak_info_id_seq'::regclass);
 ;   ALTER TABLE public.peak_info ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    252    251            �           2604    144112    priority id    DEFAULT     j   ALTER TABLE ONLY public.priority ALTER COLUMN id SET DEFAULT nextval('public.priority_id_seq'::regclass);
 :   ALTER TABLE public.priority ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    256    255            �           2604    144113    ptz_position_data id    DEFAULT     |   ALTER TABLE ONLY public.ptz_position_data ALTER COLUMN id SET DEFAULT nextval('public.ptz_position_data_id_seq'::regclass);
 C   ALTER TABLE public.ptz_position_data ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    258    257            �           2604    144114    role id    DEFAULT     b   ALTER TABLE ONLY public.role ALTER COLUMN id SET DEFAULT nextval('public.role_id_seq'::regclass);
 6   ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    260    259            �           2604    144115    severity id    DEFAULT     j   ALTER TABLE ONLY public.severity ALTER COLUMN id SET DEFAULT nextval('public.severity_id_seq'::regclass);
 :   ALTER TABLE public.severity ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    261    202            �           2604    144116    switch_conf id    DEFAULT     p   ALTER TABLE ONLY public.switch_conf ALTER COLUMN id SET DEFAULT nextval('public.switch_conf_id_seq'::regclass);
 =   ALTER TABLE public.switch_conf ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    263    262            �           2604    144117    systemconfiguration_data id    DEFAULT     �   ALTER TABLE ONLY public.systemconfiguration_data ALTER COLUMN id SET DEFAULT nextval('public.systemconfiguration_data_id_seq'::regclass);
 J   ALTER TABLE public.systemconfiguration_data ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    265    264            �           2604    144118    user_role id    DEFAULT     l   ALTER TABLE ONLY public.user_role ALTER COLUMN id SET DEFAULT nextval('public.user_role_id_seq'::regclass);
 ;   ALTER TABLE public.user_role ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    269    268            �           2604    144119    user_setting id    DEFAULT     r   ALTER TABLE ONLY public.user_setting ALTER COLUMN id SET DEFAULT nextval('public.user_setting_id_seq'::regclass);
 >   ALTER TABLE public.user_setting ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    271    270            �           2604    144120    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    273    272            �          0    143753 
   alarm_info 
   TABLE DATA               �   COPY public.alarm_info (id, type, trigger, power, frequency, "time", angle, range, duration, sample, lat, lon, ip_addr, alarm, transid, cueid, emitterpower) FROM stdin;
    public       postgres    false    196   �w      �          0    143761    alarms 
   TABLE DATA               �   COPY public.alarms (id, ip, component_id, component_type, mangedoject_id, managedobject_type, event_id, event_type, severity, event_desctiption, generation_time, status, logtime) FROM stdin;
    public       postgres    false    198   �w      �          0    143802    alarms_history 
   TABLE DATA               �   COPY public.alarms_history (id, ip, component_id, componen_type, mangedoject_id, managedobject_type, event_id, event_type, severity, event_desctiption, generation_time, acknowledged, logtime) FROM stdin;
    public       postgres    false    204   �w      �          0    143810 	   audit_log 
   TABLE DATA               G   COPY public.audit_log (id, log_type, logtime, description) FROM stdin;
    public       postgres    false    206   �w      �          0    143818    bms_info 
   TABLE DATA               A   COPY public.bms_info (id, on_btn, reset_btn, pr_btn) FROM stdin;
    public       postgres    false    208   ,|      �          0    143826 	   bmsconfig 
   TABLE DATA               =   COPY public.bmsconfig (id, ip, name, tag, value) FROM stdin;
    public       postgres    false    210   I|      �          0    143834 	   bmsstatus 
   TABLE DATA               �   COPY public.bmsstatus (id, ip, bcv1, bcv2, bcv3, bcv4, bcv5, bcv6, bcv7, bcv8, bcv9, bcv10, bcv11, bcv12, bcv13, bcv14, btv, tbc, soc, btemp, alarmword, logtime, generationtime) FROM stdin;
    public       postgres    false    212   &}      �          0    143841    bmsstatus_history 
   TABLE DATA               �   COPY public.bmsstatus_history (id, ip, bcv1, bcv2, bcv3, bcv4, bcv5, bcv6, bcv7, bcv8, bcv9, bcv10, bcv11, bcv12, bcv13, bcv14, btv, tbc, soc, btemp, alarmword, logtime, generationtime) FROM stdin;
    public       postgres    false    213   C}      �          0    143849 
   color_code 
   TABLE DATA               z   COPY public.color_code (id, startfreq, stopfreq, networktype, color, depl, epr, modulation, visible, profile) FROM stdin;
    public       postgres    false    215   `}      �          0    143857    commands 
   TABLE DATA               A   COPY public.commands (id, node_id, tag, cmd, status) FROM stdin;
    public       postgres    false    217   �}      �          0    143769    component_type 
   TABLE DATA               O   COPY public.component_type (id, component, component_type, status) FROM stdin;
    public       postgres    false    199   �~      �          0    143868    config_data 
   TABLE DATA               �  COPY public.config_data (ip_add, floor, room, calibration, start_freq, stop_freq, threshold, mask_offset, use_mask, start_time, stop_time, cable_length, preamp_type, gsm_dl, wcdma_dl, wifi_band, lte_dl, band_start1, band_stop1, band_en1, band_start2, band_stop2, band_en2, band_start3, band_stop3, band_en3, band_start4, band_stop4, band_en4, band_start5, band_stop5, band_en5, device_id, country_code, distance, tx_power, fade_margin) FROM stdin;
    public       postgres    false    220   �~      �          0    143874    config_data_history 
   TABLE DATA               �  COPY public.config_data_history (id, ip_add, floor, room, calibration, start_freq, stop_freq, threshold, mask_offset, use_mask, start_time, stop_time, cable_length, preamp_type, gsm_dl, wcdma_dl, wifi_band, lte_dl, band_start1, band_stop1, band_en1, band_start2, band_stop2, band_en2, band_start3, band_stop3, band_en3, band_start4, band_stop4, band_en4, band_start5, band_stop5, band_en5, device_id, country_code, fade_margin, tx_power, distance) FROM stdin;
    public       postgres    false    221         �          0    143882    config_profile 
   TABLE DATA               �  COPY public.config_profile (ip_add, floor, room, calibration, start_freq, stop_freq, threshold, mask_offset, use_mask, start_time, stop_time, cable_length, preamp_type, gsm_dl, wcdma_dl, wifi_band, lte_dl, band_start1, band_stop1, band_en1, band_start2, band_stop2, band_en2, band_start3, band_stop3, band_en3, band_start4, band_stop4, band_en4, band_start5, band_stop5, band_en5, device_id, country_code, profile, id) FROM stdin;
    public       postgres    false    223   �      �          0    143890    databasechangelog 
   TABLE DATA               �   COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
    public       postgres    false    225   �      �          0    143896    databasechangeloglock 
   TABLE DATA               R   COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
    public       postgres    false    226   ^�      �          0    143899    device_info 
   TABLE DATA               e   COPY public.device_info (id, ip_add, name, color, lat, lon, is_active, state, starttime) FROM stdin;
    public       postgres    false    227   ��      �          0    143907    devices 
   TABLE DATA               8   COPY public.devices (id, node_name, status) FROM stdin;
    public       postgres    false    229   ߁      �          0    143776 
   event_type 
   TABLE DATA               H   COPY public.event_type (id, event_node, event_type, status) FROM stdin;
    public       postgres    false    200   @�      �          0    143918    frequencyconfiguration_data 
   TABLE DATA               b   COPY public.frequencyconfiguration_data (id, frequency, bandwidth, freqtype, profile) FROM stdin;
    public       postgres    false    232   ]�      �          0    143926    gps_data 
   TABLE DATA               <   COPY public.gps_data (id, lat, lon, inserttime) FROM stdin;
    public       postgres    false    234   z�      �          0    143935    jmdata 
   TABLE DATA               _   COPY public.jmdata (ip_add, "time", angle, lat, lon, jmpacketdatetime, created_at) FROM stdin;
    public       postgres    false    236   ��      �          0    143941    jmdevice_data 
   TABLE DATA               N   COPY public.jmdevice_data (id, deviceid, "time", roll, tilt, pan) FROM stdin;
    public       postgres    false    237   ��      �          0    143948    jmdevicedata_history 
   TABLE DATA               U   COPY public.jmdevicedata_history (id, deviceid, "time", roll, tilt, pan) FROM stdin;
    public       postgres    false    238   �      �          0    143959    jmnode_mapping 
   TABLE DATA               E   COPY public.jmnode_mapping (deviceid, antennaid, sector) FROM stdin;
    public       postgres    false    241   ��      �          0    143967 
   led_status 
   TABLE DATA               ?   COPY public.led_status (ip_add, led_on, device_id) FROM stdin;
    public       postgres    false    243   և      �          0    143783    managedobject_type 
   TABLE DATA               [   COPY public.managedobject_type (id, managed_object, managed_object_id, status) FROM stdin;
    public       postgres    false    201   �      �          0    143975    min_threshold 
   TABLE DATA               T   COPY public.min_threshold (id, startfreq, stopfreq, power, attenuation) FROM stdin;
    public       postgres    false    245   #�      �          0    143983 	   node_info 
   TABLE DATA               =   COPY public.node_info (id, ip_add, type, status) FROM stdin;
    public       postgres    false    247   i�      �          0    144001 	   peak_info 
   TABLE DATA               �   COPY public.peak_info (ip_add, power, frequency, "time", device_id, id, angle, currentconfigid, antennaid, alarm, jmdevice_id, type, sector, inserttime, technology, lat, lon) FROM stdin;
    public       postgres    false    251   ��      �          0    144018    priority 
   TABLE DATA               6   COPY public.priority (id, priority, name) FROM stdin;
    public       postgres    false    255   �      �          0    143991    ptz_info 
   TABLE DATA               V   COPY public.ptz_info (device_ip, ptz_ip, ptz_offset, name, sector_offset) FROM stdin;
    public       postgres    false    249   ]�      �          0    144026    ptz_position_data 
   TABLE DATA               l   COPY public.ptz_position_data (id, ptz_ip, pan_position, tilt_position, date_time, system_time) FROM stdin;
    public       postgres    false    257   z�      �          0    144034    role 
   TABLE DATA               H   COPY public.role (id, role, active, created_at, updated_at) FROM stdin;
    public       postgres    false    259   ��      �          0    144272    sensor_config 
   TABLE DATA               �   COPY public.sensor_config (sensornum, sensorenabled, sensorip, sensorport, rotatingsensor, omcip, rfswitchip, angle, beamwidth, gsmserverip, usestatus, sector) FROM stdin;
    public       postgres    false    274   �      �          0    143790    severity 
   TABLE DATA               E   COPY public.severity (id, severity, severity_id, status) FROM stdin;
    public       postgres    false    202   ��      �          0    144041    switch_conf 
   TABLE DATA               J   COPY public.switch_conf (id, sid, pathid, switchid, val, seq) FROM stdin;
    public       postgres    false    262   ��      �          0    144049    systemconfiguration_data 
   TABLE DATA                  COPY public.systemconfiguration_data (id, ugs, tmdas, timeout, validity, selftimeout, auto, selfvalidity, profile) FROM stdin;
    public       postgres    false    264   �      �          0    144057    task_priority 
   TABLE DATA               F   COPY public.task_priority (ip_add, priority, device_name) FROM stdin;
    public       postgres    false    266   �      �          0    144063    trigger_data 
   TABLE DATA               |   COPY public.trigger_data (ip_add, unit_no, product_name, model, mfg_date, client, location, country, device_id) FROM stdin;
    public       postgres    false    267   .�      �          0    144069 	   user_role 
   TABLE DATA               Q   COPY public.user_role (id, user_id, role_id, created_at, updated_at) FROM stdin;
    public       postgres    false    268   ��      �          0    144074    user_setting 
   TABLE DATA               I   COPY public.user_setting (id, type, mode, gps, gps_accuracy) FROM stdin;
    public       postgres    false    270   ��      �          0    144082    users 
   TABLE DATA               }   COPY public.users (id, active, email, last_name, password, first_name, mobile, username, created_at, updated_at) FROM stdin;
    public       postgres    false    272   �      !           0    0    alarm_info_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.alarm_info_id_seq', 216583, true);
            public       postgres    false    197            "           0    0    alarms_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.alarms_id_seq', 1, false);
            public       postgres    false    205            #           0    0    audit_log_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.audit_log_id_seq', 1586, true);
            public       postgres    false    207            $           0    0    bms_info_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.bms_info_id_seq', 1, false);
            public       postgres    false    209            %           0    0    bmsconfig_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.bmsconfig_id_seq', 66, true);
            public       postgres    false    211            &           0    0    bmsstatus_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.bmsstatus_id_seq', 1, false);
            public       postgres    false    214            '           0    0    color_code_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.color_code_id_seq', 17, true);
            public       postgres    false    216            (           0    0    commands_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.commands_id_seq', 17, true);
            public       postgres    false    218            )           0    0    component_type_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.component_type_id_seq', 1, false);
            public       postgres    false    219            *           0    0    config_data_history_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.config_data_history_id_seq', 581, true);
            public       postgres    false    222            +           0    0    config_profile_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.config_profile_id_seq', 1, false);
            public       postgres    false    224            ,           0    0    device_info_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.device_info_id_seq', 77, true);
            public       postgres    false    228            -           0    0    devices_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.devices_id_seq', 1, false);
            public       postgres    false    230            .           0    0    event_type_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.event_type_id_seq', 1, false);
            public       postgres    false    231            /           0    0 "   frequencyconfiguration_data_id_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.frequencyconfiguration_data_id_seq', 5, true);
            public       postgres    false    233            0           0    0    gps_data_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.gps_data_id_seq', 2, true);
            public       postgres    false    235            1           0    0    jmdevice_data_history_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public.jmdevice_data_history_id_seq', 16, true);
            public       postgres    false    239            2           0    0    jmdevice_data_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.jmdevice_data_id_seq', 12, true);
            public       postgres    false    240            3           0    0    jmnode_mapping_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.jmnode_mapping_id_seq', 6, true);
            public       postgres    false    242            4           0    0    managedobject_type_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.managedobject_type_id_seq', 1, false);
            public       postgres    false    244            5           0    0    min_threshold_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.min_threshold_id_seq', 1, false);
            public       postgres    false    246            6           0    0    node_info_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.node_info_id_seq', 29, true);
            public       postgres    false    248            7           0    0    peak_info_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.peak_info_id_seq', 116861008, true);
            public       postgres    false    252            8           0    0    priority_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.priority_id_seq', 1, false);
            public       postgres    false    256            9           0    0    ptz_position_data_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.ptz_position_data_id_seq', 37432, true);
            public       postgres    false    258            :           0    0    role_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.role_id_seq', 3, true);
            public       postgres    false    260            ;           0    0    severity_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.severity_id_seq', 1, false);
            public       postgres    false    261            <           0    0    switch_conf_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.switch_conf_id_seq', 1, false);
            public       postgres    false    263            =           0    0    systemconfiguration_data_id_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public.systemconfiguration_data_id_seq', 6, true);
            public       postgres    false    265            >           0    0    user_role_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.user_role_id_seq', 3, true);
            public       postgres    false    269            ?           0    0    user_setting_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.user_setting_id_seq', 2, true);
            public       postgres    false    271            @           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 5, true);
            public       postgres    false    273            �           2606    144122    alarm_info alarm_info_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.alarm_info
    ADD CONSTRAINT alarm_info_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.alarm_info DROP CONSTRAINT alarm_info_pkey;
       public         postgres    false    196            �           2606    144124    alarms alarms_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.alarms
    ADD CONSTRAINT alarms_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.alarms DROP CONSTRAINT alarms_pkey;
       public         postgres    false    198            �           2606    144126    bmsconfig bmsconfig_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.bmsconfig
    ADD CONSTRAINT bmsconfig_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.bmsconfig DROP CONSTRAINT bmsconfig_pkey;
       public         postgres    false    210            �           2606    144128    bmsstatus bmsstatus_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.bmsstatus
    ADD CONSTRAINT bmsstatus_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.bmsstatus DROP CONSTRAINT bmsstatus_pkey;
       public         postgres    false    212            �           2606    144130    color_code color_code_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.color_code
    ADD CONSTRAINT color_code_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.color_code DROP CONSTRAINT color_code_pkey;
       public         postgres    false    215            �           2606    144132    commands commands_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.commands
    ADD CONSTRAINT commands_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.commands DROP CONSTRAINT commands_pkey;
       public         postgres    false    217            �           2606    144134 "   component_type component_type_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.component_type
    ADD CONSTRAINT component_type_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.component_type DROP CONSTRAINT component_type_pkey;
       public         postgres    false    199            �           2606    144136 ,   config_data_history config_data_history_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.config_data_history
    ADD CONSTRAINT config_data_history_pkey PRIMARY KEY (id);
 V   ALTER TABLE ONLY public.config_data_history DROP CONSTRAINT config_data_history_pkey;
       public         postgres    false    221            �           2606    144138    config_data config_data_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.config_data
    ADD CONSTRAINT config_data_pkey PRIMARY KEY (ip_add, room);
 F   ALTER TABLE ONLY public.config_data DROP CONSTRAINT config_data_pkey;
       public         postgres    false    220    220            �           2606    144140 "   config_profile config_profile_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.config_profile
    ADD CONSTRAINT config_profile_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.config_profile DROP CONSTRAINT config_profile_pkey;
       public         postgres    false    223            �           2606    144142    device_info device_id_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.device_info
    ADD CONSTRAINT device_id_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.device_info DROP CONSTRAINT device_id_pkey;
       public         postgres    false    227            �           2606    144144    devices devices_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.devices DROP CONSTRAINT devices_pkey;
       public         postgres    false    229            �           2606    144146    event_type event_type_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.event_type
    ADD CONSTRAINT event_type_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.event_type DROP CONSTRAINT event_type_pkey;
       public         postgres    false    200            �           2606    144148 <   frequencyconfiguration_data frequencyconfiguration_data_pkey 
   CONSTRAINT     z   ALTER TABLE ONLY public.frequencyconfiguration_data
    ADD CONSTRAINT frequencyconfiguration_data_pkey PRIMARY KEY (id);
 f   ALTER TABLE ONLY public.frequencyconfiguration_data DROP CONSTRAINT frequencyconfiguration_data_pkey;
       public         postgres    false    232            �           2606    144150    gps_data gps_data_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.gps_data
    ADD CONSTRAINT gps_data_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.gps_data DROP CONSTRAINT gps_data_pkey;
       public         postgres    false    234            �           2606    144152    jmdata jmdata_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.jmdata
    ADD CONSTRAINT jmdata_pkey PRIMARY KEY ("time");
 <   ALTER TABLE ONLY public.jmdata DROP CONSTRAINT jmdata_pkey;
       public         postgres    false    236            �           2606    144154 /   jmdevicedata_history jmdevice_data_history_pkey 
   CONSTRAINT     m   ALTER TABLE ONLY public.jmdevicedata_history
    ADD CONSTRAINT jmdevice_data_history_pkey PRIMARY KEY (id);
 Y   ALTER TABLE ONLY public.jmdevicedata_history DROP CONSTRAINT jmdevice_data_history_pkey;
       public         postgres    false    238            �           2606    144156     jmdevice_data jmdevice_data_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.jmdevice_data
    ADD CONSTRAINT jmdevice_data_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.jmdevice_data DROP CONSTRAINT jmdevice_data_pkey;
       public         postgres    false    237            �           2606    144158 "   jmnode_mapping jmnode_mapping_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.jmnode_mapping
    ADD CONSTRAINT jmnode_mapping_pkey PRIMARY KEY (deviceid);
 L   ALTER TABLE ONLY public.jmnode_mapping DROP CONSTRAINT jmnode_mapping_pkey;
       public         postgres    false    241                       2606    144160    led_status led_status_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.led_status
    ADD CONSTRAINT led_status_pkey PRIMARY KEY (ip_add);
 D   ALTER TABLE ONLY public.led_status DROP CONSTRAINT led_status_pkey;
       public         postgres    false    243            �           2606    144162    audit_log log_audit_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.audit_log
    ADD CONSTRAINT log_audit_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.audit_log DROP CONSTRAINT log_audit_pkey;
       public         postgres    false    206            �           2606    144164 *   managedobject_type managedobject_type_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public.managedobject_type
    ADD CONSTRAINT managedobject_type_pkey PRIMARY KEY (id);
 T   ALTER TABLE ONLY public.managedobject_type DROP CONSTRAINT managedobject_type_pkey;
       public         postgres    false    201                       2606    144166     min_threshold min_threshold_pkey 
   CONSTRAINT     ^   ALTER TABLE ONLY public.min_threshold
    ADD CONSTRAINT min_threshold_pkey PRIMARY KEY (id);
 J   ALTER TABLE ONLY public.min_threshold DROP CONSTRAINT min_threshold_pkey;
       public         postgres    false    245                       2606    144168    node_info node_info_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.node_info
    ADD CONSTRAINT node_info_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.node_info DROP CONSTRAINT node_info_pkey;
       public         postgres    false    247                       2606    144170    peak_info peak_info_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.peak_info
    ADD CONSTRAINT peak_info_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.peak_info DROP CONSTRAINT peak_info_pkey;
       public         postgres    false    251            �           2606    144172 .   databasechangeloglock pk_databasechangeloglock 
   CONSTRAINT     l   ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.databasechangeloglock DROP CONSTRAINT pk_databasechangeloglock;
       public         postgres    false    226                       2606    144174    priority priority_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.priority
    ADD CONSTRAINT priority_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.priority DROP CONSTRAINT priority_pkey;
       public         postgres    false    255                       2606    144176    ptz_info ptz_info_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.ptz_info
    ADD CONSTRAINT ptz_info_pkey PRIMARY KEY (device_ip);
 @   ALTER TABLE ONLY public.ptz_info DROP CONSTRAINT ptz_info_pkey;
       public         postgres    false    249                       2606    144178 (   ptz_position_data ptz_position_data_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public.ptz_position_data
    ADD CONSTRAINT ptz_position_data_pkey PRIMARY KEY (id);
 R   ALTER TABLE ONLY public.ptz_position_data DROP CONSTRAINT ptz_position_data_pkey;
       public         postgres    false    257                       2606    144180    role role_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.role DROP CONSTRAINT role_pkey;
       public         postgres    false    259            #           2606    144279 !   sensor_config sensor_config_pkey1 
   CONSTRAINT     r   ALTER TABLE ONLY public.sensor_config
    ADD CONSTRAINT sensor_config_pkey1 PRIMARY KEY (sensornum, sensorport);
 K   ALTER TABLE ONLY public.sensor_config DROP CONSTRAINT sensor_config_pkey1;
       public         postgres    false    274    274            �           2606    144182    severity severity_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.severity
    ADD CONSTRAINT severity_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.severity DROP CONSTRAINT severity_pkey;
       public         postgres    false    202                       2606    144184    switch_conf switch_conf_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.switch_conf
    ADD CONSTRAINT switch_conf_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.switch_conf DROP CONSTRAINT switch_conf_pkey;
       public         postgres    false    262                       2606    144186 6   systemconfiguration_data systemconfiguration_data_pkey 
   CONSTRAINT     t   ALTER TABLE ONLY public.systemconfiguration_data
    ADD CONSTRAINT systemconfiguration_data_pkey PRIMARY KEY (id);
 `   ALTER TABLE ONLY public.systemconfiguration_data DROP CONSTRAINT systemconfiguration_data_pkey;
       public         postgres    false    264                       2606    144188     task_priority task_priority_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public.task_priority
    ADD CONSTRAINT task_priority_pkey PRIMARY KEY (priority);
 J   ALTER TABLE ONLY public.task_priority DROP CONSTRAINT task_priority_pkey;
       public         postgres    false    266                       2606    144190    trigger_data trigger_data_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.trigger_data
    ADD CONSTRAINT trigger_data_pkey PRIMARY KEY (ip_add);
 H   ALTER TABLE ONLY public.trigger_data DROP CONSTRAINT trigger_data_pkey;
       public         postgres    false    267                       2606    144192    user_role user_role_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.user_role DROP CONSTRAINT user_role_pkey;
       public         postgres    false    268                       2606    144194    user_setting user_setting_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.user_setting
    ADD CONSTRAINT user_setting_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.user_setting DROP CONSTRAINT user_setting_pkey;
       public         postgres    false    270            !           2606    144196    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         postgres    false    272            �           1259    144197    fki_config_fkey    INDEX     L   CREATE INDEX fki_config_fkey ON public.config_data USING btree (device_id);
 #   DROP INDEX public.fki_config_fkey;
       public         postgres    false    220            �           1259    144198    fki_led_fkey    INDEX     H   CREATE INDEX fki_led_fkey ON public.led_status USING btree (device_id);
     DROP INDEX public.fki_led_fkey;
       public         postgres    false    243                       1259    144199    fki_peak_fkey    INDEX     H   CREATE INDEX fki_peak_fkey ON public.peak_info USING btree (device_id);
 !   DROP INDEX public.fki_peak_fkey;
       public         postgres    false    251                       1259    144200    fki_trigger_fkey    INDEX     N   CREATE INDEX fki_trigger_fkey ON public.trigger_data USING btree (device_id);
 $   DROP INDEX public.fki_trigger_fkey;
       public         postgres    false    267            	           1259    144201    peak_info_currentconfigid_idx    INDEX     ^   CREATE INDEX peak_info_currentconfigid_idx ON public.peak_info USING btree (currentconfigid);
 1   DROP INDEX public.peak_info_currentconfigid_idx;
       public         postgres    false    251                       1259    144202    peak_info_time    INDEX     J   CREATE INDEX peak_info_time ON public.peak_info USING btree (inserttime);
 "   DROP INDEX public.peak_info_time;
       public         postgres    false    251            /           2620    144203 .   config_data insert_config_data_history_trigger    TRIGGER     �   CREATE TRIGGER insert_config_data_history_trigger AFTER INSERT ON public.config_data FOR EACH ROW EXECUTE PROCEDURE public.insert_config_data();
 G   DROP TRIGGER insert_config_data_history_trigger ON public.config_data;
       public       postgres    false    290    220            0           2620    144204 /   config_data insert_config_data_history_trigger1    TRIGGER     �   CREATE TRIGGER insert_config_data_history_trigger1 AFTER UPDATE ON public.config_data FOR EACH ROW EXECUTE PROCEDURE public.insert_config_data();
 H   DROP TRIGGER insert_config_data_history_trigger1 ON public.config_data;
       public       postgres    false    290    220            +           2620    144205    alarms move_alarm_to_history    TRIGGER     �   CREATE TRIGGER move_alarm_to_history AFTER INSERT ON public.alarms FOR EACH ROW EXECUTE PROCEDURE public.move_alarm_to_history_table();
 5   DROP TRIGGER move_alarm_to_history ON public.alarms;
       public       postgres    false    291    198            ,           2620    144206 &   alarms move_alarm_to_history_on_update    TRIGGER     �   CREATE TRIGGER move_alarm_to_history_on_update AFTER UPDATE ON public.alarms FOR EACH ROW EXECUTE PROCEDURE public.move_alarm_to_history_table_on_update();
 ?   DROP TRIGGER move_alarm_to_history_on_update ON public.alarms;
       public       postgres    false    293    198            .           2620    144207 $   bmsstatus move_bms_status_to_history    TRIGGER     �   CREATE TRIGGER move_bms_status_to_history BEFORE INSERT ON public.bmsstatus FOR EACH ROW EXECUTE PROCEDURE public.move_bms_status_to_history_table();
 =   DROP TRIGGER move_bms_status_to_history ON public.bmsstatus;
       public       postgres    false    294    212            2           2620    144208 +   jmdevice_data move_jmdevice_data_to_history    TRIGGER     �   CREATE TRIGGER move_jmdevice_data_to_history AFTER INSERT ON public.jmdevice_data FOR EACH ROW EXECUTE PROCEDURE public.move_jmdevice_data_to_history_table();
 D   DROP TRIGGER move_jmdevice_data_to_history ON public.jmdevice_data;
       public       postgres    false    237    276            1           2620    144209    device_info movegps    TRIGGER     m   CREATE TRIGGER movegps BEFORE UPDATE ON public.device_info FOR EACH ROW EXECUTE PROCEDURE public.move_gps();
 ,   DROP TRIGGER movegps ON public.device_info;
       public       postgres    false    295    227            -           2620    144210    bmsconfig unique_prop_bms    TRIGGER        CREATE TRIGGER unique_prop_bms BEFORE INSERT ON public.bmsconfig FOR EACH ROW EXECUTE PROCEDURE public.delete_previous_prop();
 2   DROP TRIGGER unique_prop_bms ON public.bmsconfig;
       public       postgres    false    210    275            3           2620    144211 "   peak_info update_peak_info_trigger    TRIGGER     �   CREATE TRIGGER update_peak_info_trigger AFTER INSERT ON public.peak_info FOR EACH ROW EXECUTE PROCEDURE public.update_peak_info();

ALTER TABLE public.peak_info DISABLE TRIGGER update_peak_info_trigger;
 ;   DROP TRIGGER update_peak_info_trigger ON public.peak_info;
       public       postgres    false    277    251            $           2606    144212 &   config_data config_data_device_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.config_data
    ADD CONSTRAINT config_data_device_id_fkey FOREIGN KEY (device_id) REFERENCES public.device_info(id);
 P   ALTER TABLE ONLY public.config_data DROP CONSTRAINT config_data_device_id_fkey;
       public       postgres    false    227    220    3056            )           2606    144217 %   user_role fka68196081fvovjhkek5m97n3y    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT fka68196081fvovjhkek5m97n3y FOREIGN KEY (role_id) REFERENCES public.role(id) ON UPDATE CASCADE ON DELETE CASCADE;
 O   ALTER TABLE ONLY public.user_role DROP CONSTRAINT fka68196081fvovjhkek5m97n3y;
       public       postgres    false    268    259    3090            *           2606    144222 %   user_role fkj345gk1bovqvfame88rcx7yyx    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT fkj345gk1bovqvfame88rcx7yyx FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;
 O   ALTER TABLE ONLY public.user_role DROP CONSTRAINT fkj345gk1bovqvfame88rcx7yyx;
       public       postgres    false    3105    272    268            %           2606    144227 $   led_status led_status_device_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.led_status
    ADD CONSTRAINT led_status_device_id_fkey FOREIGN KEY (device_id) REFERENCES public.device_info(id);
 N   ALTER TABLE ONLY public.led_status DROP CONSTRAINT led_status_device_id_fkey;
       public       postgres    false    3056    227    243            &           2606    144232 "   peak_info peak_info_device_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.peak_info
    ADD CONSTRAINT peak_info_device_id_fkey FOREIGN KEY (device_id) REFERENCES public.device_info(id);
 L   ALTER TABLE ONLY public.peak_info DROP CONSTRAINT peak_info_device_id_fkey;
       public       postgres    false    251    3056    227            '           2606    144237 $   peak_info peak_info_jmdevice_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.peak_info
    ADD CONSTRAINT peak_info_jmdevice_id_fkey FOREIGN KEY (jmdevice_id) REFERENCES public.jmdevicedata_history(id);
 N   ALTER TABLE ONLY public.peak_info DROP CONSTRAINT peak_info_jmdevice_id_fkey;
       public       postgres    false    251    3068    238            (           2606    144242 (   trigger_data trigger_data_device_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.trigger_data
    ADD CONSTRAINT trigger_data_device_id_fkey FOREIGN KEY (device_id) REFERENCES public.device_info(id);
 R   ALTER TABLE ONLY public.trigger_data DROP CONSTRAINT trigger_data_device_id_fkey;
       public       postgres    false    3056    227    267            �      x������ � �      �      x������ � �      �      x������ � �      �   ,  x��Y�n�6=;_��AR�D�f�Ik`7	b��Xh#�`K�,�ߗCY�$�\{��"�~�4|o8CQA������]2{~ևÄZ\y�dB�DQ>>RɗvS7��頻dQ���vp�z�k�̞V�ܭ��jq�m�4��,��~��u�'�e �)1�
��s�|8�uwQt9���K�9ve_�M2-���>�����5�"d�H>�ٺl۝��~ٗ]��v�oE��!�z����k���un.:]����K-��b�8Pr1����@/��/��-6��������[}
_��Q�ϯ������������0�DF��_g��l x6�X'��3�T>�H��v_6U�m�|m���Qٽ%�?,͍j�?@�%���l��6��Gw]]i�wG��o����M	��K�=���c�~���w�=��?�m]���u�޾$�gtJNw��
Д�r��%Q�0�#�$L��bi�z�(�&����,0̥���,`.�,	�\j1�0�2�~2�a4���29$��o1Ұb�0�s�2ŽdR�c䊄-S�	��O�-Y�8�V�	>S��6/(��-w[p�&x�Q(f�S���`�0�`�7J5��g��,��To�D�2qL�#S,�9S�2�WE�S=(:_8B��b&�8��[az-p�S+��UC�"�Vp�����;]��d
4��(G��iE،�OYN+�<̊�i��A鍒�	l�NJ��N��M���_���A���E��5�0�<^u�����|���x?��A��+�ɥ~4���x��9��0�Fy�+"&08�ȜX ��B�9��H�ф���0�`2Ά�qкeخi1m2���;�s�q;[|yz�q���$Lg�;F�S�sl����jN�,F�Q��cւ����,`��a|��kZ q��Y����f��,8�i����cֵ�ۙ��Ƭg�p%0�UK{"�O����.*��i�$VL�,���Zw���>J`�u�����8��"$ad�T�%ֺKx�ƙ%V��A���ӫ��]�S      �      x������ � �      �   �   x�m�K��0D����W��fH�0$$$����A�$c��W���p��������H���c���Ӻ�BP���zn�u[�K�⬦��ľ%j�\��X*��V��J�F�R��Z68:��x�J���on��8!Sc^�Y�G�Cyw�Mh\�S�`��d,`�˝"Z�9�{��xX0�ho7d-b)��ɮ�״�G�u���D	~" �;ė�      �      x������ � �      �      x������ � �      �   }   x��5�4474 �����
���i`�il�� �i�)�i��9%\������Ŗ:
��!� B�1LR��X��X!�)���7�[G�" D�;��:"�j�i�i
4!����a�9��1z\\\ �)      �   �   x�E��
�0����Y�4ӛ.��Uh��З���$�HV�����#y�띶@�"�+9[#b�����㘁5 �}��ȶ6���d��b���sn��;� �.��xP�6�Qr�˶��1_��(�      �      x������ � �      �   Y   x���A
�0���ʪi����Q��K�BvEG!
�!$�4A���#��I��7�#��y�Y��DV�l��k�/�ZԔ~�Na���9�      �   l   x����	� �sܥ�ſ�%:����T饂��B�#I3	X �PVOb�%�M�\M{�{߿G��գ�K6xZ~n�u�V�[a��+έ(t�U�`-�U䷻*;�N�^�      �      x������ � �      �   �  x��Mk�0���_�c��ѷ]z���C�B����~y�Dޘ���wBB۔ҽuA!�蝇�^�L
PBR`2Nl�]�/Wٻ96���{�k�uv����P/���v���m�X��b��	�`rZ�3P���sC�L�O��ͦ����u�iVR[
Z�����a�'5g�՘m������H���,�~1Pf�9��9$#�|�������+�YUj�GQL)����W>����ԽrL��q�!��$�/;"d4�Fc��FÄRA+��޾Ar������&\Λ|���tj��񒊣D���.Rǣ�B"h�*��͑��/�ʮq�j�n���f{��ϔ��9����9_�k��>�[�$��ð��FL�x���>f��k��RQ�o��b�k�)uÇ_����9[����Y�]1�L~ K�{�      �      x�3�L��"�=... U�      �   L   x�37�44�340�320�37��T�L5BNC#0NK�)N�t���4��7��720�T04�20�2������ 6�      �   Q   x�3�t���q��,�2�ts�q��2M9�#�}�,3N'�` m������q����� eƜ� e1z\\\ #�_      �      x������ � �      �      x������ � �      �      x������ � �      �   V  x���[r#1E��U��B�,"+���1J��M	����	P�b�C�-�[��?b� �C�������zȽ���xʭ�[����S��O���a���%���`R?�a�2�|&kf�`60Σ{��I�b��BU-Գ��kǫ�\�'�!�|0������HAG^f֎�Ca�U�2��v�o�K*���ٕXL̊|��I6�N�q�z�p�}&�w's&O�����.�bO��}���5��v��{E���U3�{� � V�Mf��������^�<2�+���G���ͣ2v�?����L�Gv�*Eg�+Ɠ��(Jv�a]���|Ө�0Q{2�%_Mf=������N^Q����r�#�q=y�(��m�Ʈ�ꝓI0M-)AN��3��^^!��C����3��g��U+��9��c��[�49S����2
��7�-�C�YL�A7�ވ%���:��òw7����Z/H� �'#�kv2u-�!���`N��yzuM���QH��Woe�k���#���_'3]��yqvSB��������ۢ�{o^M�sw��Q����,;�#f�[������`V�cp^�>B܈.=�L�'>+7�l�=�����xuIϖ��d쓽}a���+̺c❷l�1����V�(�/w��L-3����r$%�e�*\B�}*��$��Fb��.�I�dj��)���d:bn��P���y��0T-R]2��m�3u��M���<��Av�g��i*�(ݡ�Qny���/*Gl�Feum�CSl�mJԯ�<�7�/�C�c�Ij2״�%Pt;.��=�6�[�ɕ�L#�r����r3�� �`��DFAH^��b!j$��r3i�O�cor�Q��\B�u���EE�2�8{���T�W�f�[T�(�Ӝ�����"�r=V�߄1�.o�R��F�/a씲*���$���}�L"kRJ��1'����F+{{�]{xV���n�wp!�	���\.�@>��-��~!6W�r��,[V���ۡ�T�I�r�������E3J���f����ܑ��}�����<~1[�B� �W�I��ݎ�av�uƻ�@����,��[/�=P���p���D�      �      x������ � �      �   v   x��ѱ�@Dј����n.���:\p�<�}SԦ��>ZG��s&�<�:7\��B�rߦ[b-���	�Z�w�vW�?�]��A�Sl�b�G�G�G�G���5�7�u�      �   &   x��A�@��wfR��9�8fG��ĸl����>�IW;      �       x�34�340�320�37�4�47����� 5��      �      x������ � �      �   6   x�%ȹ  �:���8�a�9��L ���Si,tU��+���Wg���F�      �      x������ � �      �      x��]��,�m\�S��A��]� A�_v�&�:��#R���#�Y�Q� �s�5k$�DQ,j���=ܖ���!�����۶��m���\������'�}O%m{�/>�?����?���?�������_����߆����5mew??������/ �t ��� Bx����a0 �ss�~�V@y�����h �$�+�7��J�n� �> ���% � @>��7������(��3p.�2��~Zc?N�i?������,������E�����������{�@i	�c�5�r��
��_~:��Kp�?ſ���]�ZA��(�wڗ����_i��������BRv��7��%���0 XA����q6 �$
�G�����e����1���m���<B�d32 r[�� d �L��z�@hD,�Y�PUh d��4��@��  0��N8�+ 3r ��,�>�0ȝ ��� И���3��e3^������_�d #m"����iH�4e�,/a���g# �l���k~о!#��)��An ����%Ɉe	#���my `	#�ƈ@� X}%D�y�%�X��< �`� v ��SJ�͎4 �m��6�'�0c��,a�4a��ʰay�F1)�>��7�@nH��C3�z.ä ��ie���!)>��0Lh��S� &��h<��Ie�Pڿ�Ճ� Æ�Q�0:���e\R24��\��w2.�Z�d����$��p�y b?�in��2pۧTx�s>�	���k9L6e��t�
>l?}���Wi	�30�c ��%t�X�[��?�Û�xWw���w	`	F��T���_,[��Q���t@+�p�/h߀Y��oG��D����}Ex�tAއ��<�&�����Ql�۞~��/�];�o7@�Of�,@̴���B�n�]����n����m������׿��tCWQs�
g(�����f�Jj�V��v3_2 �v�0 `�K�,rk�a�����ҨR[���`��O�F�IfH��0s�g@B2L.5 "�휙7?��å>" d%6����+�O��԰���s���Ga��]�n	M�g���� yڭ��X;K�<� �[A��Z�A� ��'��l�M ��'-9���� w
�)?�, /Y����x�S��!^ߐ�?e�TR��G ���R�G��W�ó�  C�V j��/�W�Р���(�0� 0Th��(�/y�3LH؟�~�{�	�nbh��A�~��`h�l�C�� ���/M��Ã��mb_ca	���1�,�Veߐ5 .|ʂ7,�5Fa���?��;�9��7�xlKX���(qq�U���)�yO\ �����pp�Ҿ]���wz8ؾ!i�.O����|g��p���ϏH��Sz��ӻ�V�؎�7 �� c �!B3��xI�vy
g����.O�쫑�#�]���+��@�}Us�b����_+l� �`ק\ X�s�B�"�ް�T.8��� �ev���� �r�X��F�4��/W�]���I +(�-��m�i_R�]��=' �K(�%��@r�]��r�2 *�]��د�T@ 9ѮW� *�C��r�^�O �,��"|)�߯�^� 1�1��D�]���I�ݐ�|I&&T�$� Ä���vW� �	�@P���ͮY� ���A>��r�K'1<"_0dh�qm8�4�xmjW�p��� 0\hp���p�ٗ��]��@����cPh�0p,�1z���:�
��sD�\�]�B�д�'�Fv�
3Y?,�����]��pA_�u�a��3�Z�b�*����dH/�����];q���._a �s��p��A��],��M�"���+\��WU\|J�7Sp�I�E��+����ڴ�!��q �!F+��ϔ�;Dh���ҕ�}��'�� P0�h��o�o��v5{����"f&{t�<���W��pvZ�Q�>�._� �n�`1z�~���W�$�R�]�B�a�o��ly���c��x�/��.a�Ak�w����)-��Ws�	.��]�B��������u,̂L���; ����D�ؐ���S� �}I�y��)�
1u2`%�tFh� �
�SN��䢭�mOITX:Ro��sOÉ��GJ=k	O�}E֞���nK�AzJ�b5��(C����)v��������k�nC�Vx����pM-�� �"�Y~okR��0�c�+|&T2�[=%S1�H�TY��)��?0������hźd'��5�����ͫl����+F��5��W��W�a�P@��)��}�7=��%W�����\�yJ�b���`�@GOiU�ۥ/5���?�BK�>]�VpӤ�*V���������]�:T��~��T�>{��P��l�Uw*z���+�
i G�0}DdO�Wh ��r�T%b�@n�$��OW =��R}��c%%c���J)� Ð�(�����VJ�B �견P��)%�����0D���+<��)U��a��Q�^9Q�c%m1�E�$�� >e�vԫ8+4�����X���
`;m�TG�T%n�1V���Bq���<qrUA��)�@�����!o���̭B?5Oi\ ���
�SJ�9Ǩ�ߊ#r��v|�D�^M����#�����r??��*�yF�b��J� ��f j�����fF���Jo 2d\@��ݱ�g.�he�ݾ�m�_S���P�B�(\�Q��>\�3
zT:�(�?eśӛ��
�s��4@j�����0�t���}��Fر�g�.v Q{d `+��V�c7�h]����}I��̅���do V���6x���	�SB���)�� ,��p����< v��ck�Ֆ�m������~F�b��B�@�BF�b0����H��\��e7�����<F�b���wl���˷0lH�����Ӿ���TIE �r�(SD �!Q�SʎE�� �R���o�!)��0�TP��q@�}|�))~�������\��+:<�3��_�,a���@�a�Ѹ�@^Ѻ6%2��h]�j�W K�1M�	�xF���T��p�$h���� 0b��6rzԸ�|_3 '�3�h�_�����}���'�9ݼ��v���<� �3�h���ڜ7�'�����>C�V�Q�������0!����/�>C�V��Z���F�b����W������if_�F�B̿z�������~u�P�_��F�b�_������O��H�6M�� �X��;�F�b��MQ��@95�%o ��,q��dQ7~�_@0�b+�#��}�
�ӷ]��1݆b*x(������C'���Ỵ��t C��i��/{H"F��M����	|��D��R�C�Q`�+vWȓԑ��.#]a\AM5�0-��[=I��HW�0Kb߬��W�2�[����Lb� ����0 t6�a,ܦ�� �'�} ���H*��]��  S�l�"l��{�Q�03��A�/O*�x�n_olܿd�)Z�*yBN�W�,FR\Av��h�qMQھ`ZL�R��R�)�M3R��{������R�) GVF�b7U���KVf+�	���b����U��')�m(#P�����G�i���d3�Hƌ4����ޑh�
���0#�i��7 .0qA8�0�;��Շ�j���-�J �;��13�ȫ4}X��%0*f[�j��
Z��G����v��畈�lJ�BL��@��%�8�����}�*�֬u�l�e�O�*Ҿ%s��	�͔J��s�t6%R)��N!)������a�S#%R1H��yxlMxZ�T*���l���&��ҩH���L�(��y:��f��L%T��4���e�B�T�i^�%�_P�%V�@LJ�J�bv�2�N��?PZ�+��������@IU��K.2���V�`�W �r㧙��[4�U1����v��k%U!v    Ǥ=M�>�`.a���(�
���j0PB¾֒�ۗ��S!�jo����@	U� fU�d܁Ҩ�g��2nl-���{K�P�v��պ	4�p��T����,N��+�2*�L���IDF�b���V��>���}-��*���X��:�VL�2������W�����n_b�0������b���/�yÊGCF�"�{Щ���cd),���fŨ��p�|����(��6�Ac�)��v��`�*����y�'nl4F�B,ͬ=�x�Q�9<jx��Wi�Cf����'c ��"#L!| kL�쯠Ǘ2dt�q|C�_".(�o�Z��ް<�~+u�(����4 ���X�C�,���.#�Z��اd�gšZ+2j;���/u "f��\�`��ġ\(2r� ��  �� b���ѡ�ٗ�ɑѫ�}[�:2Պ���Cn��
j���:�N"�XaV��@�����HV�߯寚}y���W~?�bFF����jO�}�?F���W�?@�.2Jf������$؂2��G܇��W�/��1�}7��
3�*����Q���q����n�&�e�82����٘R}�n��0�> �j��� �> �< cH�(S ���A��:��T����0�N�T��w�&�ٗW��ѧ��U:J�9|�)2�f �P�ٗ�F�������S�е��Q����\8��3DF��	�4#K�P[�hDd$)�
P�B�ȈQ
��fX��)2j�oD��_���K^ ˑQ��@�I���p�Q�0ш�	ߐ��p`�ñ��G9��,���uߐp��8ʕ7 8�!A+��N�c����Q�����+G7�ݼ\����?écء�82zä����h����g�]��wC r%0�;�r��W�#F� ��}):})bk��hP�Zn�cC��HP�	P����Q,2�oy .�d�f@�<���2
�}���:"�@�O@<'`H��y�Q�|e<� �>���Ԉ���� +8pR��kGd�'_���[1�9���뫏oH�K(0�G��p �k���[�}�o��g�l�� ��FFhb��f��~�ј0�2p�������;ډ4a��1a@]�	���} z� �0� x}J� "2���o	��Ąq uf���W0��(L�ػ޷hT��Ąq@u
�FFb�ljR�<w� �����)#0a�_݂�]��L�8��� �)���0 �z
(�b�%���f��
<z+kY��q(#0av��g?*!m��!Fa�Ke�ٶ[�]�+�d���p�΄���wS##5�8� �f.��	?N��͉y z�n�'͉ݾ~2h�!5�hMO�����ĨL ��� ���R��S� .��.�GFaGVf�%v�:6��x31�f��1w IXA���Q�/�^��0�����&�/���}Y��a	�3�E�_����n�J�+(pr6X��9ɷ�gi	������"������Db4$!>w���ǻ�X� 'FD ��̀Ⱥ�ĈIh mE��![�Q�yF&5dݾdFOb��'v�N���K�wX#�W�vh��'w � %��z�Ėo N4�n�� R$����ub$%��Qq�|Yb$%�	8���"�O������
Lq�`	�u�ݾ�U	�/k'Clk�Y	�H���o ���|"��
�oD2�f ��Q����ѕ�ty��)|��`���������"T��f_�"���n�G��M���l�xN� ������� C���;q����(J�jz�m������I�av*A[���I��E஦��Z#*�ρ���n��)�/�3u9(�K�����ꁴ@���HJ�?�� 80������T$FS�̀z$/�091������׉є0ۀڡ�}]p�=��}��U]b4%f ��`�45�)�{��$�}Ҥ���>Q�us|DXA�)MN�#��$�NA�Z��mhԬ��� d 8������ĨI� z��@a@Р@���V&FNb0y�+��21rf�vaQܘ9�@�/�� <h���N�/�FNb�p��F��"�;�'a@�}�9	c_;�5�&`N�<;���9��Δ�(�;�K�9	3�ځ�ه�bFN�� QS�9ɗ�CR�Q�0�b�	3jf(ZQR�9�W�7 +�HL+������J�.�?���cb�$�7�z��d�f�  `8�
 �Ѣ T�&FPb0#A�FPb���M5����=����!	xx�'1�;��׳w 0�g@/k��<�(J�5�4EA ��$FRb �n�ٗ���HJ�	�ٗ�HJ�*܇I	3��\����q@�5�n � �(J ��\J��tr/`�%������Pa<[��R���&FW�x�z"�X���I����/�Č��+�X�Q�|�	��ibT$f���+��	��K��$��?B̽���}�W^h�Zć^#%���=�af,]���C	�Twu0Z���!�рѓp��zኊA������7��c�Yft%��/�-:3��/�:���Ɍ��f����(z$D��I���1l���gFdbg���VeG������F����M���SW ���E�-a/���M��2k%e1C����@Ћ
� �`E_�r���02�3a<@�
z �/�O@��F�h
$p3#0����c���C�]E�,����x���7 -�Q�r�}�С�/jaS�k�aCb �d "�C����G,�ʌ����XEl̑�	��*D Z�3�7�_A������,8���ˌ��>�6b���(MP�~}��_4<$�+�%,X�\n�Rf�&����͌҄���*��M�P��� � A_���bL�M�	�ٗq #4av�8� �nBf�&�>�p�슔�����\qA�D�J��?�U]����#�$� �٥&�8)+�0� �O�`�A�;yXY��_{�+M jC���C!�҄��%,O�rؕ&�����A�҄�ַ �y� �n�}y���Jf �Y�1��Zef�Ԅ�A%_< ���ٰ��*�  	k�Ԅ#�q��/%��.5�\P%�sv�	c����v�	g�<�����M�D�+�u&_�,��v�	g|?�76v�	7�������
T�9�o��3���}�=Tw�������ڇ(̮1�����f����������>J�t�+�v�	3 �����A������<�(�v�	g_eD�X�]s��/J~�/W�]s�������0�پR�q����]rr1_O�gs Q�]w�-�4~d���v�ɀ��n�f�1����.@a"j-cO ���
�" y`W�pt0� ߀ �.@�c�k��O�߯��qd���q�C�?*�?>�J����2nf? �)#~�.>��ٮ?�6$5D���'��G��La�F�<�]~���_�>���}�\=�D<"��'���C�����f���O C�`8�<�CR����.?�<@MD<���'��x ���'��s���Gf)����P�p	��'� �q7� ��u'� ��L90A3�lם0��]x����/�u')O���O =Q��0�Td�af����T��@���.A��?�|U ���Z�O���iM"��*� ��E�HɮKa|�h#��/uO��FV��,R- S�v9
)%5R*�	XB�erZ� hr�O��E��7�J�t�+Q����#�
��bW�00	�*�g����*u�r�.v
c_{e�@P!���P}�W��]|���"�f_F�Ů=��o���?A��߯��6���-�����~X�w�]p��~m�5��YH��M�����g���;���?Ů7��}�?v�	c_�u�0���o�D��a�W��1�� v�	���3%l�Z�b�0u� ~,v�	3 gi��5�w v�I������H��<  k  
4x��z�c ��@3�t�7�S�% �� ����  Âf A{Pe ��A��l
dd��p �l
  C��Z �!B3�C��OA� &�A�
����u&n��[�]g���  XB��*T} ,!�S��M��f)v�	��d�"LS'�!�4a��M�,"�m6 `�R"SB�Є�����s"�ЄY��^ �3ͣ��1���a���nS��;��Fhb��'%w�ZW�ɗ~�̊Fg��~�L�$�����km��柡�o�?vm+���l_U��`��L�߯��
U����O��	CqFd¬?5)�p�3"�/�?$���W~��L���Z���˓#2a�OM�gP�Fd��͘�aD&�}��3�#2���/��0���_m�F�;�G FSb���=��y��ϓhv
 HH1�����1�[�ժ�XX�$�@�Zd�% 䆋���6+���nrA��7
�/��������zӾ��  ������H���`C3����ۗl�HJ��uqю�
�(��� ��%q7d%vȧ��*u��#)�@��s��P3�FQ��fUM�:�FUb����r)�6$��
�*a�P�I�}8�0�����ه3	�*a�!�P�ۇ�_��GfXw 81�fTp0�f��6��G�TĨJ��츧����@a�%4��1�Kr#3q�q%�}swKb��&ed&f�q?�������ȨL��'��ݾ���}��4���.1"f���{QFd�-�@#Z�o�j����\�ј0��M���\���0@��K��,�O�����٨��!�(����: +(p"ts���0
��԰�R3�(H���!S�(H
*`��(H�P���W%����Mp�����(H�4 2�����@5�	UF@BN��#��	`N���oTi����쉟��K��FaƕQ����(_������L#�����l�ד��M�8�����hov 0H�����b��}}eD$ �t2��;cc 	�VFMB��bUMeT%̚Pc��WFU��WO���ReT%_�/@TFU��W��QQeT%v���qƠ�Q�0�_�Q�_Q�W�%���0{�̾�FS��~�2e��������IE�}G�Gχq�����2�z#�9b��ae�%�D��Q���ѕ��j���=ԏ{�;|��2�ƾJ�RՕQ���뚊n_!#*���g���"d�_#�!0���	7����2���_����� ��v U���`Bf\-#@P `�"�X��g��qC�7 +Hp��ٗ匕Q�� ��l �#����@�/_�����>zM�/�FP�,�!����c���񀤮�+�48)_�xy_E�w���
�Z>��݄�HJ�	P9�ቔ������G
��2��\UFRb�v�RV��*��hJ� &�+�>�M	3��6*�)���#a�15�hJ�����;}TFTb���a� Á�����Vg�Q���O(j����� �/q�dd%���J� � �Iov���2�ƾVK�/w����+�P�l #+a~��n���U��_�W@���ʨJ� ��P;s�X���v��i8b��I�]�j��o ��`=ߕ:aĂFPb�>h< @�`��"z�P����څ����ѓ��7̪���2�;�r��>x�(J� &��f?�}����[�q2��N"!��VFQ�x��a�g��a�$v ��z7�� d�@P�W ���Ǒ+�%,8�̗�`��C~�b�#'����z|U�2rf�ՄP�+jFM��WO��UFD�p���oHA[e�#���}�0�����X�K	F<¬?���7 
c�#_�F<�D�3���H���V�O7gQ��l@Uie�#�}-	���2�ƾ�?ݾF;b�?���9+�1�?�eǕ��$@F;b� �;��� F:b����j��F����'`�h �� jQx e��x��
vx-�hG��E��\� ��Mu˯��i��?W�7FDb	wTp۽Pҵ�����b�Xҁx	d-��h�7v�E����^.�?Z���������M}���8)]І���
V���i�s폖��S�(�oH�_!ſ?~����H?      �   B   x���	� ��L1L!duQ�`�u佀���d̈h��	��:2nS}��&
���\$p-�      �      x������ � �      �      x������ � �      �   g   x�3�tL����4�420��5��54W04�22�26�3���%�e�Z�Z�hh�khR``hej�gja�K�˘3�� ��^K]�23�2cC �37�!����� 9�$�      �   t   x�3�t�KL�IM�4�4�34��3�3�4���G0�44�340�320�p� ����M8#S�9�L�g
4,��<\���rc1ј�M�	�a�eD�q��h����� �'@�      �      x������ � �      �   #  x�=�[rC1C��b:��^��u�4so&>`@q�~�EԁK�'!��$��m�!-0Ӷr�>�S�m³N|J.:�)�W�bd"���"���lV�$�6�2Y��ISsHی,�E�^W�db�Uw��f�+U�'�+~�AN-�\�{"���j]�B�ֻ�86�5z�$��_j5���8�"��P�d�)Ru2��l=_�މ�Y�Bq�(/p�()��C�l�:U����
�Yj5���1��Aq"�4�Z>?i�g��M��qk�����Gw�K��,�J�yB���?�G\e      �      x�3�4�4�4B����X�S����� G�m      �      x������ � �      �   D   x�34�340�320�37�4�����	r��4��"#C�ĤdN�� wG?��!��9W� ���      �   #   x�3�4�4��".# ��6�4�@�1z\\\ ���      �   -   x�3���+IM/J,IM�,.I,���K,��LI/(�44������ ��      �   4  x�u��O�0��s�+<�JקJ99'�9�/�&`/c%l̿^�&�蒞~�o?y�Ա�ܗ9NK��p d���Ü|��R1�i�Ĭ����0��viS}�^Wk�6p�B���]e�npS�SQ�
T����sl�8�+�S�Q�������,9��Fv>mn��H�X|�"+���x<$<^�ܳ�%��߮S�@�
��ŸEtlh�߅J�c%hX3��w���byN��v��j��˕��T/�7e��b�1�p?t�t&Ǽj��Cf�21ТO�!�|���e�b�=,�8�+�XQ�O�4��     