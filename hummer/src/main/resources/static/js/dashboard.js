
var graphDataForCurrentSelectedNode=[];
var xData = [];
var index = 25;
var dashborad = {};
var myLineChart = "";
var towerMarkerLayer = null;
var pathLayer = null;
var antennaId=51;
var sectorId = "5";
polylines = [];
alarmPolylines = [];
polyLines={};
mark=[];
getRealTimeData=true;
currentNodeLat='';
currentNodeLon=''
nodesInfo={};
maximizeFrequency=false;
var maskingScreenData=[];
var colorStripsData=[];
var startFreq;
var stopFreq;
var threshold;
var current_ip_addr;
var current_device_id;
var eventCount=0;
var currentTime='';
var workerFor = null;
var workerFor1 = null;
var defaultProfile = 'default';
var graphFlag = 0;

var numberOfSensors = null;
var config1 = {};
frequencyAngleCount={};
frequencyFilter=0;
var system_default_threshold=-85;
var system_default_threshold1=-95;
var system_default_threshold2=-110;
latLonData={};
ptzAngleOffset=0;
ptzRollOffset=0;
frequencySelected=false;
ptzOperationStarted=false;

var frequencyFilterVal = 3000;
var bandWidthFilter = 6000;
var startFrequencyFilter = 0;
var stopFrequnecyFilter = 6000;

var max_power = 0;
var min_power = -120;
var min_threshold = -120;
//var oprmode='';
var systemType = '';
var manualOverride='';
var stopInProgress = 0;

var usedDataset;

/***************************************************Custom Chart********************/

	
Chart.defaults.derivedline = Chart.defaults.scatter;

//controller.scatter.js

var custom = Chart.controllers.scatter.extend({
    draw: function(ease) {
        // Call super method first
        Chart.controllers.scatter.prototype.draw.call(this, ease);

        // Now we can do some custom drawing for this dataset. Here we'll draw a red box around the first point in each dataset
        var meta = this.getMeta();
//       console.log(meta);
        /*var ctx1 = this.chart.chart.ctx;
        ctx1.beginPath();
ctx1.moveTo(parseInt($("#start_freq").val()),parseInt($("#threshold").val()));
ctx1.lineTo(pt0._xScale.right,pt0._view.x,parseInt($("#threshold").val()));

        ctx1.strokeStyle="blue";
        ctx1.stroke();*/


        var points = meta.data;
        
        for(var i in points)
        {
            var pt0 = points[i];
            var radius = pt0._view.radius;
            //console.log(pt0);
            var ctx = this.chart.chart.ctx;
            //console.log(ctx);
            ctx.save();
            //ctx.strokeStyle = 'red';
            ctx.lineWidth = 1;
           //console.log(pt0);
            // ctx.strokeRect(pt0._view.x - radius, pt0._view.y - radius, 2 * radius, 2 * radius);
            ctx.beginPath();
            ctx.moveTo(pt0._view.x,pt0._view.y);
            ctx.lineTo(pt0._view.x,pt0._yScale.bottom);

            //ctx.strokeStyle="red";
            ctx.stroke();



        }

    }
});

Chart.pluginService.register({
	beforeDraw: function (chart, easing) {
		if (chart.config.options.chartArea && chart.config.options.chartArea.backgroundColor) {
			var ctx = chart.chart.ctx;
			var chartArea = chart.chartArea;

			ctx.save();
			ctx.fillStyle = chart.config.options.chartArea.backgroundColor;
			ctx.fillRect(chartArea.left, chartArea.top, chartArea.right - chartArea.left, chartArea.bottom - chartArea.top);
			ctx.restore();
			
		}

	}
});


Chart.controllers.derivedline = custom;


var integratedDataset = [{
                label: 'Sector 1',
                backgroundColor: colorToHex(167, 147, 141),
                borderColor: colorToHex(167, 147, 141),
                //data: [{x: 2400,y: -90}, {x: 1300,y: -20},{x:1800,y:-39}],
                data: [],
                fill: false
                //,showLine:true
            },
            {
				label: 'Sector 2',
				borderColor: colorToHex(21,151,155),
				backgroundColor: colorToHex(21,151,155),
				data: [],
				fill:false
			},
			{
				label: 'Sector 3',
				borderColor: colorToHex(255,195,0),
				backgroundColor: colorToHex(255,195,0),
				data: [],
				fill:false
			}/*,
			{
				label: '',
				borderColor: colorToHex(204,201,201),
				backgroundColor: colorToHex(204,201,201),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(249,237,237),
				backgroundColor: colorToHex(249,237,237),
				data: [],
				fill:false
			}*/];

var standaloneDataset = [{
                label: 'STRU',
                backgroundColor: colorToHex(179, 255, 217),
                borderColor: colorToHex(179, 255, 217),
                //data: [{x: 2400,y: -90}, {x: 1300,y: -20},{x:1800,y:-39}],
                data: [],
                fill: false
                //,showLine:true
            }/*,
            {
				label: 'Sector 2',
				borderColor: colorToHex(21,151,155),
				backgroundColor: colorToHex(21,151,155),
				data: [],
				fill:false
			},
			{
				label: 'Sector 3',
				borderColor: colorToHex(255,195,0),
				backgroundColor: colorToHex(255,195,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(204,201,201),
				backgroundColor: colorToHex(204,201,201),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(249,237,237),
				backgroundColor: colorToHex(249,237,237),
				data: [],
				fill:false
			}*/];


var config1;
	
function initializeConfig1(){
	config1 = {
        type: 'derivedline',
        data: {
            //labels: ['0', '500', '1000', '1500', '2000', '2500', '3000'],
            datasets: usedDataset/*[{
                label: 'Sector 1',
                backgroundColor: colorToHex(167, 147, 141),
                borderColor: colorToHex(167, 147, 141),
                //data: [{x: 2400,y: -90}, {x: 1300,y: -20},{x:1800,y:-39}],
                data: [],
                fill: false
                //,showLine:true
            },
            {
				label: 'Sector 2',
				borderColor: colorToHex(21,151,155),
				backgroundColor: colorToHex(21,151,155),
				data: [],
				fill:false
			},
			{
				label: 'Sector 3',
				borderColor: colorToHex(255,195,0),
				backgroundColor: colorToHex(255,195,0),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(204,201,201),
				backgroundColor: colorToHex(204,201,201),
				data: [],
				fill:false
			},
			{
				label: '',
				borderColor: colorToHex(249,237,237),
				backgroundColor: colorToHex(249,237,237),
				data: [],
				fill:false
			}]*/
        },
        options: {
            responsive: true,
            onClick: updateChartFrequency,
			maintainAspectRatio: false,
			layout: {
	            padding: {
	                left: 0,
	                right: 10,
	                top: -40,
	                bottom: 0
	            }
	        },
            title: {
                display: true,
                //text: 'Freq(MHz) vs Power(dBm)'
                text: ''
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Freq(MHz)'
                    },
                ticks: {
                    min: 0,
                    max: 3000,
                    stepSize: 500
                }
                }],
                yAxes: [{
                    display: true,
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Power(dBm)'
                    },
                ticks: {
                    min: -120,
                    max: 0,
                    stepSize: 20
                }
                }]
            }
        }
    };
}	
var config2 = {
        data: {
			datasets: [{
				label: '',
				borderColor: colorToHex(255,0,0),
				backgroundColor: colorToHex(255,0,0),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(255,128,0),
				backgroundColor: colorToHex(255,128,0),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(255,165,0),
				backgroundColor: colorToHex(255,165,0),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(255,206,0),
				backgroundColor: colorToHex(255,206,0),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(255,255,0),
				backgroundColor: colorToHex(255,255,0),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(184,255,0),
				backgroundColor: colorToHex(184,255,0),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(0,255,0),
				backgroundColor: colorToHex(0,255,0),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(0,208,0),
				backgroundColor: colorToHex(0,208,0),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(0,196,196),
				backgroundColor: colorToHex(0,196,196),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(0,148,255),
				backgroundColor: colorToHex(0,148,255),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(80,80,255),
				backgroundColor: colorToHex(80,80,255),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(0,38,255),
				backgroundColor: colorToHex(0,38,255),
				data: [],
				fill:false,
				pointStyle: 'rect'
			},
			{
				label: '',
				borderColor: colorToHex(142,63,255),
				backgroundColor: colorToHex(142,63,255),
				data: [],
				fill:false,
				pointStyle: 'rect'
			}]
		},
        options: {
	        legend: {
	            display: false,
	        },
			maintainAspectRatio: false,
	        layout: {
	            padding: {
	                left: 30,
	                right: 10,
	                top: -30,
	                bottom: 0
	            }
	        },
        	animation: false,
			responsive: true,
			title: {
				display: true,
				text: ''
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Freq(MHz)'
					},
				ticks: {
					display:false,
					min: 0,
					max: 3000,
					stepSize: 300
				},
				gridLines:{
					drawOnChartArea: false,
					color: "#101010",
					lineWidth:-1
				}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Capture'
					},
				ticks: {
					display: false,
					min: -25,
					max: 0.1,
					stepSize: 5	
				},
				gridLines:{
					drawOnChartArea: false,
					color: "#101010",
					lineWidth:0
				}
				}]
			},
			chartArea: {
				backgroundColor: 'rgba(10, 10, 9, 1)'
			}
		}
    };


function updateChartFrequency(evt){
	
	 var activePoint = myLineChart.getElementAtEvent(evt);
	 const clickedElementIndex = activePoint[0]._index;
	   const label = myLineChart.data.labels[clickedElementIndex];
	   
	   // get value by index
	   const value = myLineChart.data.datasets[0].data[clickedElementIndex];
	   //console.log(clickedElementIndex, label, value)
	   var idx = activePoint[0]['_index'];
	   maximizeAngle(value.x);
	   
}

function maximizeAngle(frequency, maxAngle){
	 //maxAngle=0;
	 var temp_angle='';
	 maxAngleValue=0;
	 $("#def_freq").val(frequency);
	 //commented on 14-06-19
	  /*for(angle in frequencyAngleCount){
		   if(frequencyAngleCount[angle]>maxAngleValue){
			  maxAngle=angle;
			  maxAngleValue=frequencyAngleCount[angle];
		   }
	   }*/
	   /*if($("#ptz_angle_offset").val()!=""){
	   		ptzAngleOffset=parseFloat($("#ptz_angle_offset").val());
	   }*/
	   temp_angle=ptzAngleOffset;
	   if(ptzAngleOffset<0){
	      temp_angle=360+ptzAngleOffset;
	   }
	  if(maxAngle<temp_angle){
	 	 temp_angle=ptzAngleOffset;
		 }
	   if(maxAngle!=0){
	   	 $("#start_angle").val(maxAngle-30-temp_angle-ptzRollOffset);
	   	 $("#end_angle").val(parseInt(maxAngle)+30-temp_angle-ptzRollOffset);
	   	 $("#current_frequency_df").val(frequency);
	   	 $("#current_angle_df").val(parseInt(maxAngle));
	   //maximizeFrequency=true;
	   	 frequencyFilter=frequency
	   
	   }
	   
	   if(systemType=="standalone"){			//previous changes systemType!="integrated"
			$("#start_angle").val(0);
			$("#end_angle").val(0);
	   }
	   
	   removePolylines();
	   createLineForTheTower(currentNodeLat,currentNodeLon,2000,0+ptzAngleOffset,"",'black');
	   createLineForTheTower(currentNodeLat,currentNodeLon,2000,180+ptzAngleOffset,"",'black');
	   createSemiCircle();
	   if(maxAngle!=0){
		   createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseInt(maxAngle)-30+ptzAngleOffset,"",'black');
		   //createLineForTheTower(currentNodeLat,currentNodeLon,2000,90,$("#current_node_ip").val(),'black');
		      
		   createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseFloat(maxAngle)+30+ptzAngleOffset,"",'black');
		   frequencySelected=true;
		}
}




function updateFrequencyCone(){
	/* maxAngle=0;
	   maxAngleValue=0;
	   for(angle in frequencyAngleCount){
		   if(frequencyAngleCount[angle]>maxAngleValue){
			   
			  maxAngle=angle;
			  maxAngleValue=frequencyAngleCount[angle];
		   }
	   }
	    if($("#ptz_angle_offset").val()!=""){
	   		ptzAngleOffset=parseFloat($("#ptz_angle_offset").val());
	   }
	    removePolylines();
	    createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseInt(maxAngle)-30+ptzAngleOffset,$("#current_node_ip").val(),'black');
	    createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseFloat(maxAngle)+30+ptzAngleOffset,$("#current_node_ip").val(),'black');
	*/
}
/***************************************************Custom Chart********************/

var config = {
        type: 'scatter',
        data: {
            //labels: ['0', '500', '1000', '1500', '2000', '2500', '3000'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: '#ff6384',
                borderColor: '#ff6384',
                //data: [{x: 2400,y: -90}, {x: 1300,y: -20},{x:1800,y:-39}],
                data: [],
                fill: false,
                showLine:true
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Freq(MHz) vs Power(dBm)'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Freq(MHz)'
                    },
                ticks: {
                    min: 0,
                    max: 3000,
                    stepSize: 500
                }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Power(dBm)'
                    },
                ticks: {
                    min: -120,
                    max: 0,
                    stepSize: 20
                }
                }]
            }
        }
    };




//Not In use
/*window.onload = function() {
    var ctx = document.getElementById('canvas').getContext('2d');
    //alert("11");
    myLineChart = new Chart(ctx, config1);
    //setTimeout(function(){ updateGraphRealTime({x:2800,y:-20}) }, 3000);
};*/

/*var updateGraphRealTime = function(data)
{
    var oldData = myLineChart.data.datasets[0].data;
    var newData = oldData.shift();
    console.log(oldData);
    oldData.push(data);
    console.log(oldData);
    myLineChart.data.datasets[0].data = oldData;
    myLineChart.update();
}*/


var events = function()
{
	getNodeConfigurationData(1);
    /*$("#nodes_box").on("change","input",function()
    {
    	
        if($(this).is(":checked"))
        {
            clearData();
            getNodeConfigurationData($(this).val());
           
//            getPeakDataOfNode($(this).val());
            getLeadStatus($(this).val());
            currentNodeLat=nodesInfo[$(this).val()]['lat'];
            currentNodeLon=nodesInfo[$(this).val()]['lon'];
            
            getDeviceInfo($(this).val());

            $(".selectednode").each(function(){
                $(this).removeClass("selectednode");
            });
            $(this).parent().parent().addClass("selectednode");

        }
    });

    $("#node_color").change(function(){
        $("#node_color").css("background",$("#node_color").val());
        $("#node_color").css("color",$("#node_color").val());
    });*/

}


function getCordinatesForAngle(angle){
	lat="";
	lon="";
	if(angle==0){
		lat=28.5492602;
		lon=79.1790986;
	}
	
	else if(angle==60){
		
	}
}

var createNodesList = function(data)
{
    var html = "";
    for(var i in data)
    {
        html +='<li data-lat="'+data[i].lat+'" data-lon="'+data[i].lon+'" data-color="'+data[i].color+'" data-time="'+data[i].starttime+'" ><span class="color-symbol" style="background:'+data[i].color+' !important;"></span><span><input type="radio" name="nodes" value="'+data[i].id+'" data-id="'+data[i].id+'" data-ip="'+data[i].ip_add+'"></span>&nbsp;<span>'+data[i].name+'</span>&nbsp;-&nbsp;'+data[i].ip_add+'&nbsp;&nbsp;&nbsp;&nbsp;<span style="float:right"><a href="#"  onclick=deleteNode('+data[i].id+')><i class="fa fa-times"></i></a></span></li>'
        nodeInfoData={"lat":data[i].lat,"lon":data[i].lon};
        nodesInfo[data[i].id]=nodeInfoData;
    }
    
    //console.log(nodesInfo);
    $("#nodes_box").html("");
    $("#nodes_box").append(html);
}

function deleteNode(nodeId){
	
	 swal({
	      title: "Are you sure?",
	      text: "Are You Sure You Want To Delete Node!!",
	      icon: "warning",
	      buttons: [
	        'No, cancel it!',
	        'Yes, I am sure!'
	      ],
	      dangerMode: true,
	    }).then(function(isConfirm) {
	      if (isConfirm) {
	    	  $.ajax({
			        url:serviceUrls.deleteNode+"/"+nodeId,
			        
			        dataType:"json",
			        type:"post",
			        success:function(data)
			        {
			        	  swal("Success","Node Data Has Been Deleted","success");	
			        	  responseAlert(data,true);
			        },
			 		error:function(data){
			 			  responseAlert(data,false);
			 		}
			    });
	      } else {
	        swal("Cancelled", "Node Data Has Not Been Deleted", "error");
	      }
	    })
	
//	swal({
//		  title: "Are you sure?",
//		  text: "Node Data will be deleted",
//		  type: "warning",
//		  showCancelButton: true,
//		  confirmButtonClass: "btn-danger",
//		  confirmButtonText: "Yes, delete it!",
//		  cancelButtonText: "No, cancel plx!",
//		  closeOnConfirm: false,
//		  closeOnCancel: false
//		},
//		function(isConfirm) {
//		  if (isConfirm) {
//			  $.ajax({
//			        url:serviceUrls.deleteNode+"/"+nodeId,
//			//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.getNodeConfig+"/"+nodeId,
//			        dataType:"json",
//			        type:"post",
//			        success:function(data)
//			        {
//			        	  swal("Success","Node Data Has Been Deleted","success");	
//			        	  responseAlert(data,true);
//			        },
//			 		error:function(data){
//			 			  responseAlert(data,false);
//			 		}
//			    });
//		  } else {
//		    swal("Cancelled", "Delete Node Operation Cancelled :)", "error");
//		  }
//		});
//	
	
}
var getNodesData = function()
{
    $.ajax({
        url:serviceUrls.getNodesData,
//url:"http://10.100.207.117:9000/security_server/api/getDevicesList",
        dataType:"json",
        success:function(data)
        {
            
            //createNodesList(data);
            currentNodeLat=data[0].lat;
            currentNodeLon=data[0].lon;
			current_ip_addr = data[0].ip_add;
			getPtzInfo();
			current_device_id = data[0].id;
			setNodePositionOverMap();
			createLineForTheSPS(currentNodeLat,currentNodeLon,2000,0+ptzAngleOffset+ptzRollOffset,"",'blue');
			createLineForTheSPS(currentNodeLat,currentNodeLon,2000,60+ptzAngleOffset+ptzRollOffset,"",'blue');
			createLineForTheSPS(currentNodeLat,currentNodeLon,2000,120+ptzAngleOffset+ptzRollOffset,"",'blue');
			createLineForTheSPS(currentNodeLat,currentNodeLon,2000,180+ptzAngleOffset+ptzRollOffset,"",'blue');
			$("#location").html(data[0].lat+", "+data[0].lon);
			//createSemiCircle();
            //$($("#nodes_box li input")[0]).trigger("click");
        }
    })
}

var getNodeConfigurationData = function(nodeId)
{
	
	
    $.ajax({
        url:serviceUrls.getNodeConfigByRoom,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.getNodeConfig+"/"+nodeId,
        dataType:"json",
        success:function(data)
        {
            //$("#current_node").val(nodeId);
            
//            console.log(data);
            
            //setupConfigurationInForm(data);
			setupStatusData(data);
			startFreq = data.start_freq;
			stopFreq = data.stop_freq;
			threshold = data.threshold;
			
			min_threshold = parseFloat(data.threshold);
			min_power = min_threshold;
			
			//changed here
			//graphXAxis();
        }
    });
}

var getPeakDataOfNode = function(nodeId)
{
    $.ajax({
        url:serviceUrls.peakData+"/"+nodeId,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.peakData+"/"+nodeId,
        dataType:"json",
        success:function(data)
        {
           
            updatePeekTable(data);
            createOrUpdateGraph(data);

        }
    });
}

var getLeadStatus= function(nodeId)
{
    $.ajax({
        url:serviceUrls.ledStatus+"/"+nodeId,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.ledStatus+"/"+nodeId,
        dataType:"json",
        success:function(data)
        {
           
           
           
        }
    });
}

var updateLedStatus = function()
{
    if(data.status == 0)
    {
        $("#led_status").css("background-position","-4490px -512px")
    }
    else
    {
        $("#led_status").css("background-position","-4544px -512px;")
    }
}

var setupConfigurationInForm = function(data)
{
	
    $("#start_freq").val(data.start_freq);
    $("#stop_freq").val(data.stop_freq);
    $("#threshold").val(data.threshold);
    $("#mask_offset").val(data.mask_offset);
    $("#cable_length").val(data.cable_length);
    $("#preamp_type").val(data.preamp_type);

    $("#config_start_time").val(data.start_time);
    $("#config_stop_time").val(data.stop_time);
    $("#country_code").val(data.country_code);
    $("#current_node_ip").val(data.ip_add);
    
    data.gsm_dl == 0?$("#gsm_dl").prop("checked",false):$("#gsm_dl").prop("checked",true);
    data.wcdma_dl == 0?$("#wcdma_dl").prop("checked",false):$("#wcdma_dl").prop("checked",true);
    data.wifi_band == 0?$("#wifi_band").prop("checked",false):$("#wifi").prop("checked",true);
    data.use_mask == 0?$("#use_mask").prop("checked",false):$("#use_mask").prop("checked",true);
    data.lte_dl == 0?$("#lte_dl").prop("checked",false):$("#lte_dl").prop("checked",true);
    data.calibration == 0?$("#calibrate").prop("checked",false):$("#calibration").prop("checked",true);

    $("#start_1").val(data.band_start1);
    $("#start_2").val(data.band_start2);
    $("#start_3").val(data.band_start3);
    $("#start_4").val(data.band_start4);
    $("#start_5").val(data.band_start5);

    $("#stop_1").val(data.band_stop1);
    $("#stop_2").val(data.band_stop2);
    $("#stop_3").val(data.band_stop3);
    $("#stop_4").val(data.band_stop4);
    $("#stop_5").val(data.band_stop5);

    data.band_en1 == 0?$("#start_stop_check_1").prop("checked",false):$("#start_stop_check_1").prop("checked",true);
    data.band_en2 == 0?$("#start_stop_check_2").prop("checked",false):$("#start_stop_check_2").prop("checked",true);
    data.band_en3 == 0?$("#start_stop_check_3").prop("checked",false):$("#start_stop_check_3").prop("checked",true);
    data.band_en4 == 0?$("#start_stop_check_4").prop("checked",false):$("#start_stop_check_4").prop("checked",true);
    data.band_en5 == 0?$("#start_stop_check_5").prop("checked",false):$("#start_stop_check_5").prop("checked",true);


    setupStatusData(data);
    graphXAxis();
    getPtzInfo();
    

}





var updateConfig = function()
{
	if( parseInt($("#start_freq").val())<0 || parseInt($("#start_freq").val())>6000 || $("#start_freq").val()==''){
        swal("Error","Start Frequency should be between given range","error");
        return;
    }
	
	if( parseInt($("#stop_freq").val())<0 || parseInt($("#stop_freq").val())>6000 || $("#stop_freq").val()==''){
        swal("Error","Stop Frequency should be between given range","error");
        return;
    }
	
	if( parseInt($("#start_freq").val())>=parseInt($("#stop_freq").val())){
        swal("Error","Start Freq Should be Less Than Stop Freq","error");
        return;
    }
	
	if( parseInt($("#threshold").val())<-100 || parseInt($("#threshold").val())>-10 || $("#threshold").val()==''){
        swal("Error","Threshold should be between given range","error");
        return;
    }
	
	if( parseInt($("#mask_offset").val())<-10 || parseInt($("#mask_offset").val())>20 || $("#mask_offset").val()==''){
        swal("Error","Mask Offset should be between given range","error");
        return;
    }
	
	if( parseInt($("#cable_length").val())<0 || parseInt($("#cable_length").val())>100 || $("#cable_length").val()==''){
        swal("Error","Cable Length should be between given range","error");
        return;
    }
	
	if( parseInt($("#preamp_type").val())<0 || parseInt($("#preamp_type").val())>9 || $("#preamp_type").val()==''){
        swal("Error","Preamp Type should be between given range","error");
        return;
    }
	
	if( $("#country_code").val()==''){
        swal("Error","Country code cannot be blank","error");
        return;
    }
	
    var configData =
    {
              "floor": "1",
              "room": "1",
              "calibration": $("#calibrate").is(":checked")?"1":"0",
              "start_freq": $("#start_freq").val(),
              "stop_freq": $("#stop_freq").val(),
              "threshold": $("#threshold").val(),
              "mask_offset": $("#mask_offset").val(),
              "use_mask": $("#use_mask").is(":checked")?"1":"0",


              "start_time":  $("#config_start_time").val(),
              "stop_time": $("#config_stop_time").val(),
              "country_code":$("#country_code").val(),

              "cable_length": $("#cable_length").val(),
              "preamp_type": $("#preamp_type").val(),



              "gsm_dl": $("#gsm_dl").is(":checked")?"1":"0",
              "wcdma_dl": $("#wcdma_dl").is(":checked")?"1":"0",
              "wifi_band": $("#wifi").is(":checked")?"1":"0",
              "lte_dl": $("#lte").is(":checked")?"1":"0",
              "band_start1": $("#start_1").val(),
              "band_stop1": $("#stop_1").val(),
              "band_en1": $("#start_stop_check_1").is(":checked")?"1":"0",
              "band_start2": $("#start_2").val(),
              "band_stop2": $("#stop_2").val(),
              "band_en2": $("#start_stop_check_2").is(":checked")?"1":"0",
              "band_start3": $("#start_3").val(),
              "band_stop3": $("#stop_3").val(),
              "band_en3": $("#start_stop_check_3").is(":checked")?"1":"0",
              "band_start4": $("#start_4").val(),
              "band_stop4": $("#stop_4").val(),
              "band_en4": $("#start_stop_check_4").is(":checked")?"1":"0",
              "band_start5": $("#start_5").val(),
              "band_stop5": $("#stop_5").val(),
              "band_en5": $("#start_stop_check_5").is(":checked")?"1":"0"
            }

    $.ajax({
        url:serviceUrls.updateConfig+"/"+$("#current_node").val(),
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.updateConfig+"/"+$("#current_node").val(),
        dataType:"json",
        type:"post",
        data:configData,
        success:function(data)
        {
            swal("Success","Configuration Data Has Been Saved","success");
            responseAlert(data,false);
            //setupConfigurationInForm(data);
            setupStatusData(configData);
            graphXAxis();
        },
        error:function(data){
        	responseAlert(data,false)
        }
    });
}


var setupStatusData = function(data)
{
	
	//console.log(data)
    $(".status_bar tr td").removeClass();
    /*$(".status_bar tr td").each(function(){
            $(this).removeClass("ON");
            $(this).removeClass("OFF");
    })*/
    $("#Opr_mode").parent().addClass("Operation Mode");
	if(data.ref_level=='l1'){
    $("#Opr_mode").html("Default");
    }if(data.ref_level=='l2'){
    $("#Opr_mode").html("Normal");
    }if(data.ref_level=='l3'){
    $("#Opr_mode").html("High Sensitivity");
    }
	$("#start_freq_status").parent().addClass("START_TIME");
    $("#start_freq_status").html(data.start_freq);
    $("#stop_freq_status").parent().addClass("START_TIME");
	$("#stop_freq_status").html(data.stop_freq);
    $("#threshold_status").parent().addClass("START_TIME");
    if (data.threshold==system_default_threshold)
	  $("#threshold_status").html("System Default");
	   else if (data.threshold==system_default_threshold1)
	  $("#threshold_status").html("System Default");
	  else if (data.threshold==system_default_threshold2)
	  $("#threshold_status").html("System Default");
	else  
		$("#threshold_status").html(data.threshold);
	
	//$("#threshold_status").html(data.threshold);

    $("#gsm_status").html(data.gsm_dl     == 0?"OFF":"ON");
    $("#gsm_status").parent().addClass(data.gsm_dl     == 0?"OFF":"ON");

    $("#lte_status").html(data.lte_dl     == 0?"OFF":"ON");
    $("#lte_status").parent().addClass(data.lte_dl     == 0?"OFF":"ON");

    $("#wifi_status").html(data.wifi_band     == 0?"OFF":"ON");
    $("#wifi_status").parent().addClass(data.wifi_band     == 0?"OFF":"ON");

    $("#wcdma_status").html(data.wcdma_dl == 0?"OFF":"ON");

    $("#wcdma_status").parent().addClass(data.wcdma_dl     == 0?"OFF":"ON");

    //$("#sweep_status").html("Unknown");
    //$("#mask_status").html(data.use_mask     == 0?"OFF":"ON");
    //$("#mask_status").parent().addClass(data.use_mask == 0?"OFF":"ON");
	$("#ptz_status").parent().addClass("START_TIME");
	$("#start_status").parent().addClass("START_TIME");
	$("#location").parent().addClass("START_TIME");
    //$("#start_status").html($(".selectednode").data("time"));

    getNodeStartTimeAndStatus()


}

var getNodeStartTimeAndStatus = function ()
{
    $.ajax({
url:serviceUrls.getNodeDetails+"/"+$("#current_node").val(),
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.getNodeDetails+"/"+$("#current_node").val(),
        success:function(data)
        {
            setStatusOfNode(data.state,data.is_active);
            setNodePositionOverMap(data);
        }
    });

    //$("#node_status").html("<p>Not reachable</p>");
}

var checkNodeRechablity = function(isActive,state)
{
    if(isActive == "true")
    {
        if(state == "DOWN")
            state = "reachable";
    }
    else
    {
        state = "not reachable";
    }
    return state;
}

var setStatusOfNode  = function(text,isActive)
{

    if(isActive != null)
    {
        text = checkNodeRechablity(isActive,text);
    }

    switch(text.toLowerCase())
    {
        case "not reachable":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("not_reachable");
        break;
        case "reachable":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("reachable");
        break;
        case "running":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("reachable");
        break;
        case "Scanning":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("Scanning");
        break;
        case "monitoring":
            $("#node_status").html("<p>"+text+"</p>");
            $("#node_status").removeClass();
            $("#node_status").addClass("monitor");
        break;

    }

}

var setNodePositionOverMap = function()
{
	
	  
      addTower([currentNodeLat,currentNodeLon],'images/tower.png');
//    createLineForTheTower(data.lat,data.lon,100,0,data.ip_add);
}

var setAlarmPositionOverMap = function(angle, distance, opac, pan)
{
	  lineEndPoint = calulateLatLongAtGivenAngleAndDistance(currentNodeLat, currentNodeLon, angle, distance, "abc");
      return addAlarmTower(lineEndPoint, 'images/alarm.png', opac, pan);

}

/****************web socket section*************/


var connectSocket = function()
{

    var stompClient = null;

	xData = [];
    var socket = new SockJS(endPoint.socket);

    stompClient = Stomp.over(socket);
	stompClient.debug = null;
    stompClient.connect({}, function (frame)
	{
		//console.log('Connected: ' + frame);
		stompClient.subscribe(topics.status, function (data) {
			var statusData = jQuery.parseJSON(data.body);
			//console.log(statusData);
			updatedeviceStatus(statusData.id,statusData.ip_add,statusData.state,statusData.is_active)
		});
		
		stompClient.subscribe(topics.latlon, function (data) {
            console.log(data);
        	var latLon = jQuery.parseJSON(data.body);
			createLatLon(latLon);

        });

		
		stompClient.subscribe(topics.ptzoperation, function (data) {
			maximizeFrequency=true;
			$("#start_def_button").attr('disabled',true);

			//swal("Success","Maximize frequency has been set","success");
			//$("#def_modal").modal('toggle'); 
			//$("#def_modal").modal('hide');
		});
		
		/*stompClient.subscribe(topics.peak, function (data) {

			var statusData = jQuery.parseJSON(data.body);
			if(getRealTimeData==true){
				updatePeekTable(statusData);
				createOrUpdateGraph(statusData);
			}
		});*/
		
		/*stompClient.subscribe(topics.alarm, function (data) {

			var alarmData = jQuery.parseJSON(data.body);
			if(getRealTimeData==true){
				updateAlarmTable(alarmData);
			}
		});
		
		stompClient.subscribe(topics.eventcue, function (data) {

			var cueData = jQuery.parseJSON(data.body);
			if(getRealTimeData==true){
				updateEventCueTable(cueData);
			}
		});*/
		
		stompClient.subscribe(topics.freqAuto, function (data) {
			var freqData = jQuery.parseJSON(data.body);
			if(getRealTimeData==true){
				updateMaximizeFrquency(freqData);
			}
		});
		
		stompClient.subscribe(topics.nodeStatus, function (data) {
			var nodeData = jQuery.parseJSON(data.body);
//          console.log(statusData);
			createDeviceStatusRow(nodeData);
		});

		stompClient.subscribe(topics.led, function (data) {

		//callback for socket
		//console.log(data.body);
		});    

		stompClient.subscribe(topics.nodeAngle, function (data) {
			var angleData = jQuery.parseJSON(data.body);
			updateDirectionOfNode(angleData);
		});
		
		stompClient.subscribe(topics.jmData, function (data) {
			var jmData = jQuery.parseJSON(data.body);
//          console.log(statusData);
			createJmDataRow(jmData);
		});
		
		stompClient.subscribe(topics.fault, function (data) {

			//callback for socket
			//var alarmData = data.body.split(",");
			//updateFaultTable(alarmData);
			//$("#fault_status").parent().addClass("blinking");
			$("#new_alarm").parent().addClass("blinking");
			$("#canvas").parent().addClass("blinking");
			$("#sysovrload").parent().addClass("blinking");
		});
		
		stompClient.subscribe(topics.bmsstatus, function (data) 
		{
			var bmsstatus = data.body.split(",");
			console.log(bmsstatus);
			//updateStatusBMS(bmsstatus);
		});
		
		stompClient.subscribe(topics.currentevent, function (data) 
		{
			var status = data.body;
			console.log(status);
			currentStatus(status);
		});
		stompClient.subscribe(topics.currentmode, function (data) 
		{
			var status = data.body;
			console.log(status);
			currentMode(status);
			
		});
		
		stompClient.subscribe(topics.DbStatus, function (data) 
		{
			swal({
        	     title: "Success",
        	     text: data.body,
        	     type: "success",
        	     timer: 2000,
        	     buttons:false,
        	     className: "right-bottom"
        	     });
			
		});

	});

}

var workerExample = function()
{
	workerFor = new Worker('js/woker.js');
	
	// listen to message event of worker
	workerFor.addEventListener('message', function(event) 
	{
	    //console.log(event);
	    var statusData = jQuery.parseJSON(event.data);
		if(statusData.length == 0){
			//convertToData([],false);
			//clearGraph();
			graphFlag++;
			/*if(oprmode!="4")
			{
			
				if(graphFlag>5){
					convertToData([],false);
					clearGraph();
				}
			}*/
				if(graphFlag<35)
					workerFor.postMessage("Macro!");
				else
			    	workerFor.postMessage("listen!");
		   	
		}
		else{
			var status = jQuery.parseJSON(statusData.data);
		//	if(oprmode!="4"){
				updatePeekTable(status);
			//}
			createOrUpdateGraph(status);
			graphFlag=0;
			workerFor.postMessage("Marco!");
		}
	    //do your chart work here
	    
	});
	
	// listen to error event of worker
	workerFor.addEventListener('error', function(event) {
	    console.error('error received from workerFor => ', event);
	});
	
	workerFor.postMessage("Marco!");
}

var workerExample1 = function()
{
	workerFor1 = new Worker('js/woker1.js');
	
	// listen to message event of worker
	workerFor1.addEventListener('message', function(event) 
	{
	    //console.log(event);
		var recievedata = jQuery.parseJSON(event.data);
		if(recievedata.type=="alarm"){
			var al = jQuery.parseJSON(recievedata.data);
			if(getRealTimeData==true){
				updateAlarmTable(al);
			}
		}
		else{
			var cueData = jQuery.parseJSON(recievedata.data);
			if(getRealTimeData==true){
				updateEventCueTable(cueData);
			}
		}
	    
	});
	
	// listen to error event of worker
	workerFor1.addEventListener('error', function(event) {
	    console.error('error received from workerFor => ', event);
	});
	
	//workerFor1.postMessage("Marco!");
}


var currentStatus = function(data){
	
	if(data!='Idle')
		$("#maximize_button").attr('disabled',true);
	
	//if(opr_mode =="4")
	
	$("#current_status").html(data);
	$("#current_status").parent().addClass("START_TIME");
	
}
var currentMode = function(data){	
	
	$("#Opr_mode").html(data);
	
}

function getCurrentEvent(){
	$.ajax({
	url:serviceUrls.getCurrentEvent,
	type:"post",
	success:function(data)
	{
		$("#current_status").html(data);
		$("#current_status").parent().addClass("START_TIME");
		currentStatus(data);
	}
	});
}

// fn to disable maximize button on dashboard 
var disableMaximize = function(){
	$.ajax({
		 url:serviceUrls.getSystemConfiguration+"/"+defaultProfile,
         type:"post",
         success:function(data)
         {	
        	 manualOverride=data.ugs;
        	if (data.ugs==0 && systemType!= "standalone")
        		$("#maximize_button").attr('disabled',true);
         }
	
	});

}

/****************web socket section*************/


var updatedeviceStatus = function(id,ip,state,isActive)
{
    if($('input[name=nodes]:checked').data("id") == id)
    {
        setStatusOfNode(state,isActive);
    }
}

var addNode = function()
{

    if(validateAddNodeForm())
    {
    	
        var nodeData =
        {
                "name":$("#node_name").val(),
                "ip":$("#node_ip").val(),
                "color":$("#node_color").val(),
                "lat":$("#node_lat").val(),
                "lon":$("#node_lon").val(),
                "id":$("#node_id").val()
        }

        $.ajax({
            url:serviceUrls.addNode,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
            type:"post",
            data:nodeData,
            success:function(data)
            {
                responseAlert(data,true);
               
            }

        });
    }



}

var getColorCodeList = function()
{
    $.ajax({
        url:serviceUrls.getColorCodeList+"/"+defaultProfile,
        dataType:"json",
        success:function(data)
        {
           colorStripsData = data;
        }
    });
}

var startDef = function()
{

	removePolylines();
    if(validateDefForm())
    {
        var defData =
        {
                "startAngle":$("#start_angle").val(),
                "stopAngle":$("#end_angle").val(),
				"bandwidth":$("#frequency_offset").val(),
                "tilt_angle":$("#tilt_angle").val(),
                "ip_add":current_ip_addr,//$("#current_node_ip").val()
				"frequency":$("#def_freq").val(),
				/*"switch":$("#switch").val()*/
				"switch":"9"
        }
		
		$("#current_frequency_df").val($("#def_freq").val());
        
        /*if(parseInt($("#start_angle").val())+parseInt(ptzAngleOffset)<0){
        	alert("PTZ Operation Not Allowed Check Offset");
        	return;
        } */
		
		if( parseInt($("#start_angle").val())<0 || parseInt($("#start_angle").val())>180 || parseInt($("#end_angle").val())<0
				|| parseInt($("#end_angle").val())>180){
        	swal("Success","Start Angle and End Angle should be within 0-180","error");
        	return;
        }
		
        if( parseInt($("#start_angle").val())>parseInt($("#end_angle").val())){
        	swal("Success","Start Angle Should be Less Than End Angle","error");
        	return;
        }
		
		var switch_val = $("#switch").val()
		if(switch_val == "select")
		{
			swal("Error","Please select values from the dropdown for Switch","error");
			return;
		}
		
		var start = parseFloat($("#start_angle").val())+ptzAngleOffset+parseFloat(ptzRollOffset);
		var end = parseFloat($("#end_angle").val())+ptzAngleOffset+parseFloat(ptzRollOffset);
        
         createLineForTheTower(currentNodeLat,currentNodeLon,2000, start, $("#current_node_ip").val(),'black');
         createLineForTheTower(currentNodeLat,currentNodeLon,2000, end, $("#current_node_ip").val(),'black');
		
        $.ajax({
            url:serviceUrls.startDef,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
            type:"post",
            data:defData,
            success:function(data)
            {
            
            	
            	maximizeFrequency=true;
            	ptzOperationStarted=true;
            	//$("#def_modal").modal().hide();
                responseAlert(data,false);
				$("#maximize_button").attr('disabled',true);
                $("#start_def_button").attr('disabled',true);
                $("#def_modal").modal('hide');
                //createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseInt($("#start_angle").val())-30+ptzAngleOffset,$("#current_node_ip").val(),'black');
         	   //createLineForTheTower(currentNodeLat,currentNodeLon,2000,90,$("#current_node_ip").val(),'black');
         	   
         	   
         	  // createLineForTheTower(currentNodeLat,currentNodeLon,2000,parseFloat($("#end_angle").val())+30+ptzAngleOffset,$("#current_node_ip").val(),'black');
                //fetchLatLonSignals();
            },
			error:function(data)
			{
				data = JSON.parse(data.responseText);
				responseAlert(data,false);
			}
        });
    }
}

var updateMaximizeFrquency = function(freqData)
{

	removePolylines();
	var event_name
	var frequency;
	var start_angle;
	var end_angle;
	
	for(var i in freqData)
    {
		event_name = freqData.eventName;
		frequency = parseFloat(freqData.frequency);
		start_angle = parseFloat(freqData.startAngle);
		end_angle = parseFloat(freqData.stopAngle); 
	}
	//createLineForTheTower(currentNodeLat,currentNodeLon,2000,start_angle+ptzAngleOffset,$("#current_node_ip").val(),'black');
	//createLineForTheTower(currentNodeLat,currentNodeLon,2000,end_angle+ptzAngleOffset,$("#current_node_ip").val(),'black');
	
	createLineForTheTower(currentNodeLat,currentNodeLon,2000,start_angle,$("#current_node_ip").val(),'black');
	createLineForTheTower(currentNodeLat,currentNodeLon,2000,end_angle,$("#current_node_ip").val(),'black');
	
	frequencyFilter = frequency;
	maximizeFrequency=true;
	ptzOperationStarted=true;
	//$("#def_modal").modal().hide();
	$("#start_def_button").attr('disabled',true);
		
}

function stopDef(){
	if(stopInProgress == 1)
		swal({
     	     title: "Warning",
     	     text: "Stop already in progress,Please wait",
     	     type: "info",
     	     timer: 5000,
     	     buttons:false,
     	     className: "right-bottom"
     	     });
	stopInProgress = 1;
	
	var defData =
    {
			"ip_add":current_ip_addr
    }
	 $.ajax({
         url:serviceUrls.stopDef,
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
         type:"post",
         data:defData,
         success:function(data)
         {
        	
        	// setTimeout(() => {  stopInProgress = 0 ; }, 5000);
        	 maximizeFrequency=false;
             //responseAlert(data,false);
             $("#start_def_button").attr('disabled',false);
             if(manualOverride != 0){
             $("#maximize_button").attr('disabled',false);
             } 
			 
         },
         error:function(data){
        	 
        	// setTimeout(() => {  stopInProgress = 0 ; }, 5000);
         }

         

     });
	setTimeout(() => {  stopInProgress = 0 ; }, 5000);
}

function returnMyLink(cellValue, options, rowdata) 
{  	
	//maximizeAngle(rowdata.frequency, rowdata.angle);
		return '<a style="color: blue;text-decoration: underline;cursor: pointer;" href="javascript:maximizeAngle('+rowdata.frequency+','+parseFloat(rowdata.angle)+');">'+cellValue+'</a>';
		

}

var createOldPeakDataGrid = function()
{
	var today = new Date();
	var date = today.getDate()<10?'0'+today.getDate():today.getDate();
	var month = today.getMonth()<9?'0'+(today.getMonth()+1):(today.getMonth()+1);
	
	var startDate = today.getFullYear()+'-'+month+'-'+date+' '+'00:00:00';
	var endDate = today.getFullYear()+'-'+month+'-'+date+' '+'23:59:59';

	
	$.ajax({
		url:serviceUrls.oldPeakData+"/"+startDate+"/"+endDate+"/"+systemType,
        dataType:"json",
        success:function(data)
        {	           
        	oldPeakData=data;
        	$("#oldPeakDataDiv").jqGrid("clearGridData", true)
    		jQuery("#oldPeakDataDiv").jqGrid('setGridParam', {  datatype: 'local',  data:oldPeakData  }).trigger("reloadGrid");
        	
        	
        	$('#frequencyValue').css('display','none');
        	$('#frequencyTab').css('display','none');
        	$('#bandwidthValue').css('display','none');
        	$('#bandwidthTab').css('display','none');
        	$('#currPeakDiv').css('display','none');
        	$('#showFrequencyFilter').css('display','none');
        	$('#oldPeakDiv').css('display','block');
        	$('#lastOtRecords').css('display','block');
        },
        error:function(data){
        	swal("Error","Old Peak Data Cannot Be Loaded","error");
        }

     });
}
	
	
	var deployPeakDataGrid = function(deployOldPeakData) {
        $("#oldPeakDataDiv").jqGrid({
                data : deployOldPeakData,
                datatype : "local",
                type : 'POST',
                
                colNames : ['Date', 'Time', 'Freq(MHz)', 'Direction','Rx Level','Sector','Type','Emitter Type', 'Latitude', 'Longitude'],          				
				colModel : [ {
                        name : 'inserttime',
                        index : 'inserttime',
						width : 120,
						formatter: "date",
						formatoptions: { srcformat: "ISO8601Long", newformat: "m-d-Y" },
						editable: false
                },{
                        name : 'time',
                        index : 'time',
                        width : 100,
                        formatter: "date",
                        formatoptions: { srcformat: "ISO8601Long", newformat: "H:i:s" },
                        editable: false
                                                
                },{
                    name : 'frequency',
                    index : 'frequency',
                    width : 100,
                    formatter: returnMyLink,
                    editable: false,
                    sorttype:'number'
                    
                },{
                        name : 'angle',
                        index : 'angle',               
						width : 80,
						editable: false,
						sorttype:'number'
						
                },{
                    name : 'power',
                    index : 'power',
					width : 100,
					editable: false,
					sorttype:'number'
			
                },{
                    name : 'sector',
                    index : 'sector',
					width : 70,
					editable: false,
					sorttype:'number'
                },{
                    name : 'type',
                    index : 'type',
					width : 100,
					editable: false
					
                },{
                    name : 'technology',
                    index : 'technology',
					width : 100,
					editable: false
					
                },{
                    name : 'lat',
                    index : 'lat',
					width : 80,
					editable: false
					
                },{
                    name : 'lon',
                    index : 'lon',
					width : 80,
					editable: false
					
                }],
                
                loadOnce : true,
                toppager: false,
                viewrecords: true,
                height: 'auto',
                rowNum : 5000,
                rownumbers: true,
                /*pager : '#oldPeakDataPager',*/
                editurl: serviceUrls.editSensorConfig,
                gridview : true
                /*caption : 'Old Peak Data',*/
                
        });
        
        
        
        /*$('#oldPeakDataDiv').navGrid('#oldPeakDataPager', { edit: false, add: false, del: false, search: false, refresh: false, view: false, position: "left", cloneToTop: false, sortable: true },*/
        $('#oldPeakDataDiv').navGrid('#oldPeakDataPager', { del: false, add: false, edit: false, search: false},
        		
        		   
                {
                    height: 'auto',
                    width: 'auto',
/*                    editCaption: "Old Peak Data",*/
                    recreateForm: true,
                    closeAfterEdit: true,
                    errorTextFormat: function (data) {
                        return 'Error: ' + data.responseText
                    }
                
                });
        jQuery("#oldPeakDataDiv").jqGrid('filterToolbar',{stringResult: true,searchOnEnter : true});
}


function graphView(){
	
	var value = $("#show_graph").text();
	var graph = document.getElementById("spectrogram");
	if(value == "Show"){
		graph.style.display = "block";
		$("#show_graph").text("Hide");
	}
	else{
		graph.style.display = "none";
		$("#show_graph").text("Show");
	}
	
}



var responseAlert = function(data,reload)
{
	
    if(data.result == "success")
    {
       
        if(reload)
            location.reload();
    }
    else
    {
        swal("Something Went Wrong","Fail : "+data.message,"error");
        if(reload)
            location.reload();
    }

}

var createOrUpdateGraph = function(data)
{
	var appendFlag = false;
    for(var i in data)
    {
        //if($('input[name=nodes]:checked').data("id") == data[i].device_id.id)
        //{
            if(i == 0 )//&& data[i].time != $(".status_table tbody tr:first td:last").text())
            {
				if(data[i].time.split(" ")[1] == currentTime){
					appendFlag = true;
				}
				
				currentTime = data[i].time.split(" ")[1];
				//clear graph
                graphDataForCurrentSelectedNode = [];
            }
            if(data[i].frequency!="NaN"){
      		    if(data[i].antennaid!=antennaId)
					graphDataForCurrentSelectedNode[data[i].frequency]=data[i];
				//old code
				//graphDataForCurrentSelectedNode[data[i].frequency]=data[i].power;
			}
        //}
    }
    //myLineChart.data.datasets[0].data = convertToGraphData(graphDataForCurrentSelectedNode);
    //myLineChart.data.datasets[0].borderColor = ($('input[name=nodes]:checked').parent().parent().data("color"));
    //myLineChart.data.datasets[0].backgroundColor = ($('input[name=nodes]:checked').parent().parent().data("color"));
    //myScatterChart.data.datasets[0].data = convertToData(graphDataForCurrentSelectedNode);
    convertToData(graphDataForCurrentSelectedNode, appendFlag);
	myScatterChart.update();
    myLineChart.update();
}

var createOrUpdateOfflineGraph = function(data){
    graphDataForCurrentSelectedNode = [];
    for(var i in data)
    {

            if(i == 0 && data[i].time != $(".status_table tbody tr:first td:last").text())
            {

                graphDataForCurrentSelectedNode = [];
                //graphDataForTime = [];
            }
graphDataForCurrentSelectedNode[data[i].frequency]=data[i].power;

    }
    //myLineChart.data.datasets[0].data = convertToGraphData(graphDataForCurrentSelectedNode);
    //myLineChart.data.datasets[0].borderColor = ($('input[name=nodes]:checked').parent().parent().data("color"));
    //myLineChart.data.datasets[0].backgroundColor = ($('input[name=nodes]:checked').parent().parent().data("color"));
    //myScatterChart.data.datasets[0].data = convertToData(graphDataForCurrentSelectedNode);
    convertToData(graphDataForCurrentSelectedNode);
	myScatterChart.update();
    myLineChart.update();
}

var graphXAxis =  function( )
{



    var maxVal  = parseInt((stopFreq == 0)?3000:(stopFreq));
    var minVal  = parseInt(startFreq);
    
    //var threshold  = parseInt($("#threshold").val());

    myLineChart.options = {
        responsive: true,
		layout: {
			padding: {
				left: 0,
				right: 10,
				top: -40,
				bottom: 0
			}
		},
        onClick: updateChartFrequency,
        title: {
            display: true,
            text: ''
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Freq(MHz)'
                },
            ticks: {
                min: minVal,
                max: maxVal,
                stepSize: getStepSize(maxVal-minVal),
                xPadding: 6
            }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Power(dBm)'
                },
            ticks: {
                min: min_power,
                max: max_power,
                stepSize: Math.round((max_power - min_power)/10)
            }
            }]
        },
        annotation: {
            annotations: colorOutputData()
        }
    };
	
	myScatterChart.options={
        legend: {
            display: false,
        },
        layout: {
            padding: {
                left: 30,
                right: 10,
                top: -30,
                bottom: 0
            }
        },
    	animation: false,
		responsive: true,
		maintainAspectRatio: false,
		title: {
			display: true,
			text: ''
		},
		tooltips: {
			mode: 'index',
			intersect: false,
		},
		hover: {
			mode: 'nearest',
			intersect: true
		},
		scales: {
			xAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Freq(MHz)'
				},
			ticks: {
				display:false,
				min: minVal,
				max: maxVal,
				stepSize: getStepSize(maxVal-minVal)
			},
			gridLines:{
				drawOnChartArea: false,
				color: "#101010",
				lineWidth:-1
			}
			}],
			yAxes: [{
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Capture'
				},
			ticks: {
				display: false,
				min: 0,
				max: 25,
				stepSize: 5
			},
			gridLines:{
				drawOnChartArea: false,
				color: "#101010",
				lineWidth:0
			}
			}]
		},
		chartArea: {
			backgroundColor: 'rgba(10, 10, 9, 1)'
		}
	};
    myLineChart.update();
	myScatterChart.update();

}

var colorOutputData =function()
{
	var len = colorStripsData.length;
	var output = [];
	if(len>5)
		len=5;
	for(var i=0;i<len;i++){
		if(colorStripsData[i].visible){
			output.push({
				type: 'box',
				drawTime: 'beforeDatasetsDraw',
				xScaleID: 'x-axis-1',
				xMin: colorStripsData[i].startfreq,
				xMax: colorStripsData[i].stopfreq,
				backgroundColor: colorStripsData[i].color

			})
		}
	}
	return output;
	
}


var createLatLon = function(data)
{
	 console.log(data);
	 offset = data.ptzoffset;
	 ptzAngleOffset = parseFloat(data.ptzoffset);

	 if(ptzAngleOffset<0)
			$("#ptz_status").html(ptzAngleOffset+360);
		else
			$("#ptz_status").html(ptzAngleOffset);
	 
	 $("#location").html(data.lat+", "+data.lon);
	 
	 currentNodeLat = data.lat;
	 currentNodeLon = data.lon;
	 
	 removeTowerPolylines();
	 
	 towerMarkerLayer.clearLayers();
	 
	 setNodePositionOverMap();
	 createLineForTheSPS(currentNodeLat,currentNodeLon,2000,0+ptzAngleOffset+ptzRollOffset,"",'blue');
	 createLineForTheSPS(currentNodeLat,currentNodeLon,2000,60+ptzAngleOffset+ptzRollOffset,"",'blue');
	 createLineForTheSPS(currentNodeLat,currentNodeLon,2000,120+ptzAngleOffset+ptzRollOffset,"",'blue');
	 createLineForTheSPS(currentNodeLat,currentNodeLon,2000,180+ptzAngleOffset+ptzRollOffset,"",'blue');
} 

var addDataToMaskingGridTable = function (data) 
{
	maskingScreenData = data;
    $("#freq_grid").jqGrid({
    	data: data,
        datatype : "local",
        type : 'POST',
        colNames : ['Id','Frequency','Bandwidth', 'Type'],
        colModel: [{
            name : 'id',
            index : 'id',
            width : 10
        },{
            name : 'frequency',
            index : 'frequency',
            width : 15,
            editable : true
        },{
            name : 'bandwidth',
            index : 'bandwidth',
            width : 15,
            editable : true
        },{
			name : 'freqtype',
			index : 'freqtype',
			width: 15,
            editable : true
        }],
        pager : '#freq_pager',
        rowNum : 5,
        width : 520,
		height: 'auto',
		loadonce : true,
        sortname : 'id',
        sortorder : 'asc',
        viewrecords : true,
        gridview : true,
		caption: "Frequency Configuration"
    });
    jQuery("#freq_grid").jqGrid('navGrid', '#freq_pager', {
        edit: true,
        add: false,
        del: true,
        search: true
    });
}


var addDataToGridTable = function (data) 
{
	colorStripsData = data;
    $("#grid").jqGrid({
    	data: data,
        datatype : "local",
        type : 'POST',
        colNames : ['Id','Start Frequency','Stop Frequency', 'Network Type','Color'],
        colModel: [{
            name : 'id',
            index : 'id',
            width : 90
        },{
            name : 'startfreq',
            index : 'startfreq',
            width : 100,
            editable : true
        },{
            name : 'stopfreq',
            index : 'stopfreq',
            width : 90,
            editable : true
        },{
			name : 'networktype',
			index : 'networktype',
			width: 90,
            editable : true
        },{
            name : 'color',
            index :'color',
            width : 100,
            editable : true
        }],
        pager : '#pager',
        rowNum : 5,
        width : 770,
		height: 'auto',
		loadonce : true,
        sortname : 'id',
        sortorder : 'asc',
        viewrecords : true,
        gridview : true,
		caption: "Color Bands"
    });
    jQuery("#grid").jqGrid('navGrid', '#pager', {
        edit: true,
        add: false,
        del: true,
        search: true
    });
}

/*var lastSelection;

function editRow(id) 
{
    if (id && id !== lastSelection) 
    {
        var grid = $("#grid");
        grid.jqGrid('restoreRow',lastSelection);
        grid.jqGrid('editRow',id, {keys: true} );
        lastSelection = id;
    }
}*/

var checkNodeStatus = function(){
	$.ajax({
        url:serviceUrls.checkNodeStatus,
        dataType:"json",
        success:function(data)
        {
           createDeviceStatusRow(data);
        }
    });
}

var createDeviceStatusRow =function(data)
{
	
	$("#sl_table tbody").html("");
	var total = data.length;
	var up = 0;
	
	var down = 0;
	var adminstate=["DOWN","LOCK","UNLOCK"];
	var groupNodeCount=0;
	
	var row="";
	var addedRow="";
	for(var i=0;i<data.length;i++)
	{	
		var colorStaus={};
		colorStaus = colorAndStatusForStatusCodeRest(data[i].status);
		
	    if(data[i].type=="STRU"){
			row+="<tr style='background:"+colorStaus.color.backgroundColor+";color:"+colorStaus.color.fontColor+"'>";           //onclick='showJMData()' 
			row+="<td>"+data[i].type+"</td>"+
				"<td>"+data[i].ip_add+"</td>"+
				"<td>"+data[i].status+"</td></tr>";
		}
		else if(data[i].type!="FINLEY" && data[i].type!="FALCON" && data[i].type!="OXFAM"){
			row+="<tr style='background:"+colorStaus.color.backgroundColor+";color:"+colorStaus.color.fontColor+"'>";
			row+="<td>"+data[i].type+"</td>"+
				"<td>"+data[i].ip_add+"</td>"+
				"<td>"+data[i].status+"</td></tr>";
		}
		
	}

	$("#sl_table tbody").append(row);
	
}

var getJmData = function(){
	$('#jmbtn').prop('disabled', true);
	$.ajax({
        url:serviceUrls.getJmData,
        dataType:"json",
        success:function(data)
        {
			createJmDataRow(data);
			$('#jmbtn').prop('disabled', false);
        }
    });
}

var showJMData = function(){
	$("#jm_data").modal('show');
}

var createJmDataRow = function(data)
{
	$("#jm_data_table tbody").html("");
	
	var row="";
	for(var i=0;i<data.length;i++)
	{	
		row+="<tr>";
		row+="<td>"+data[i].deviceid+"</td>"+
			"<td>"+new Date(data[i].time).toLocaleString()+"</td>"+
			"<td>"+data[i].roll+"</td>"+
			"<td>"+data[i].tilt+"</td></tr>";
		
	}

	$("#jm_data_table tbody").append(row);
	
}

var colorAndStatusForStatusCodeRest = function(code)
{
	var color = {};
	color.fontColor = "black";
	color.backgroundColor = "white";
	var statusCount={};
	statusCount.run=0;
	statusCount.wait=0;
	statusCount.reachable=0;
	statusCount.down=0;
	
	var status={};
	
	switch(code.toLowerCase())
	{
		case "not reachable":
            color.backgroundColor = COLOR['nor'];
			statusCount.reachable++;
			break;
        case "reachable":
            color.backgroundColor = COLOR['run'];
			statusCount.run++; 
			break;
        case "running":
            color.backgroundColor = COLOR['run'];
			statusCount.run++; 
			break;
      
        case "scanning":
            color.backgroundColor = COLOR['run'];
			statusCount.run++; 
			break;
        case "monitoring":
            color.backgroundColor = COLOR['run'];
			statusCount.run++; 
			break;
		case "down":
            color.backgroundColor = COLOR['down'];
			statusCount.down++;
			break;
	
	}	
	status.color =color;
	status.count =statusCount;
	return status;
}

var COLOR = {
             	'run':"rgb(198,227,159)",
             	'wait':"rgb(251,201,142)",
             	'nor':"rgb(255,229,130)",
             	'down':"rgb(240,141,144)"
			};
			
var getStepSize = function(maxValue)
{
    return (maxValue/1000)*100
}


var updateDirectionOfNode = function(data)
{
    line = polylines[data.ip_add];
    if(line != undefined)
    {
        var path =  line.getLatLngs();
        lineEndPoint = calulateLatLongAtGivenAngleAndDistance(path[0].lat,path[0].lng,data.angle,100);
        path[1] = lineEndPoint;
        line.setLatLngs(path);
        $("#angle_map").html("");
        $("#angle_map").html(data.angle+"&deg;");
    }
}

var updateAlarmTable = function(data)
{
	//for(var i in data){
		if(data.type=='centroid'){
			//draw centroid on map for alarm
			var angle = parseFloat(data.angle);
			var marker = setAlarmPositionOverMap(data.angle, data.range, 1, false);
			marker.bindPopup("<table class='table e_tab'><thead><tr><th>Type</th><th>Value</th></tr></thead><tbody><tr><td>Frequency</td><td>"+data.frequency+"</td></tr><tr><td>Direction</td><td>"+data.angle+"</td></tr><tr><td>Range</td><td>"+data.range+"</td></tr><tr><td>Rx Level</td><td>"+data.power+"</td></tr></tbody></table>");//('<strong>Hummer</strong><br>'+data[i].angle+'');
			marker.id = mark.length;
			mark.push(marker);
			$("#bl_table tbody").prepend("<tr onclick=openPopup("+marker.id+")><td>"+data.time.split(" ")[0]+"</td><td>"+data.time.split(" ")[1]+"</td><td>"+data.trigger+"</td><td>"+data.frequency+"</td><td>"+data.angle+"</td><td>"+data.range+"</td><td>"+data.power+"</td><td>"+data.emitterpower+"</td><td>"+data.duration+"</td><td>"+data.sample+"</td><td>"+data.bandwidth+"</td><td>"+data.cueid+"</td>");
			//resizeTable("bl_table");
		}
		else{
			var power = data.power;
			var color=getColorCode(power);
			var angle = parseFloat(data.angle);
            createLineForTheAlarm(currentNodeLat, currentNodeLon, data.range, angle, "", color);
            
		}
	//}
}

var updateMaximizeEvent = function(){
	$.ajax({
        url:serviceUrls.maximizeData,
        dataType:"json",
        success:function(data)
        {
            for(var i in data){
				if(i==0){
					removePolylines();
					angle = parseFloat(data[i].angle)-ptzAngleOffset-parseFloat(ptzRollOffset);
					if(angle<=60){
						start_angle=0;
						end_angle=60;
					}
					else if(angle<=120){
						start_angle=60;
						end_angle=120;
					}
					else{
						start_angle=120;
						end_angle=180;
					}
					createLineForTheTower(currentNodeLat,currentNodeLon,2000,start_angle+ptzAngleOffset+parseFloat(ptzRollOffset), '', 'black');
					createLineForTheTower(currentNodeLat,currentNodeLon,2000,end_angle+ptzAngleOffset+parseFloat(ptzRollOffset), '', 'black');
					
					maximizeFrequency=true;
					ptzOperationStarted=true;
					
					power = data[i].power;
					color=getColorCode(power);
					createLineForTheAlarm(currentNodeLat, currentNodeLon, data[i].range, parseFloat(data[i].angle), "", color);
					$("#maximize_button").attr('disabled',true);
					$("#start_def_button").attr('disabled',true);
				}
				else{
					power = data[i].power;
					color=getColorCode(power);
					createLineForTheAlarm(currentNodeLat, currentNodeLon, data[i].range, parseFloat(data[i].angle), "", color);
				}
			}
        }
    });
}

var updateEventCueTable = function(data){
	
	//for(var i in data){
		$("#cue_table tbody").prepend("<tr><td>"+data.eventDate.split(" ")[1]+"</td><td>"+data.nodeType+"</td><td>"+data.eventType+"</td><td>"+data.eventData);
		resizeTable("cue_table");
	//}
}

var updatePeekTable = function(data)
{
	var today = new Date();
	var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
	//$("#peek_table_date_time").text(date);
	dbm=-1000;
	frequencyValue=0;
	currentAngle=0;
	if(data.length>0)
		frequencyAngleCount={};
    for(var i in data)
    {
    	currentAngle=data[i].angle;
//        if(true)//(data[i].device_id.id == $('input[name=nodes]:checked').data("id"))
//        {
            /*if($(".status_table tbody tr").length >=200)
            {
                $(".status_table tbody tr:last").remove();
            }*/
			
            //if(i == 0)
            //{
                //$("#el_table tbody").html("");
            //}
			var peakFrequency = parseFloat(data[i].frequency);
            /*if(maximizeFrequency==true){
            	
            	frequencyRange=parseFloat($("#frequency_offset").val());
            	console.log("Frequency range is "+frequencyRange);
				//alert(frequencyFilter+" "+frequencyRange+" " +data[i].frequency);
            	//sajal//if( data[i].antennaid==antennaId && ((parseFloat(data[i].frequency)>=parseFloat(frequencyFilter)-parseFloat(frequencyRange))&&(parseFloat(data[i].frequency)<=parseFloat(frequencyFilter)+parseFloat(frequencyRange)))){
					
					//alert(frequencyFilter+" "+frequencyRange+" " +data[i].frequency+" in");
					/*if(dbm<data[i].power){
						dbm=data[i].power;
						
						frequencyValue=parseFloat(data[i].frequency);
						console.log("Inside maximizeFrequency frequency is"+frequencyValue);
					}*/
            	
            		/*$("#current_angle_df").val(parseInt(data[i].angle)+parseInt(ptzAngleOffset));
            		colorCode=colorToHex('255','255','0');
            		 //createLineForTheTower(currentNodeLat,currentNodeLon,100,data[i].angle-30,0,data[i].ip_add,colorCode);
            		
            		powerValue=Math.pow(10,parseInt(data[i].power)/20);
            		distance=powerValue-maximizeFrequency-Math.pow(10,parseInt(27.55/20));
            		
            		exp = (27.55 - (20 * Math.log10(frequencyFilter)) + (30-data[i].power)) / 20.0;
            		distance=Math.pow(10.0, exp);
            		console.log("distance is "+distance+" and angle is "+data[i].angle+"frequency is "+frequencyFilter+" and power is "+ data[i].power);
            		color=getColorCode(data[i].power)
            		
            		if(data[i].angle!=$("#start_angle").val() && data[i].angle!=$("#end_angle").val()){
            			createLineForTheTower(currentNodeLat,currentNodeLon,distance,parseInt(parseInt(data[i].angle)+parseInt(ptzAngleOffset)),$("#current_node_ip").val(),color);
            		}*/
            		
            	//}
				/*if(data[i].antennaid!=antennaId && peakFrequency>=startFrequencyFilter && peakFrequency<=stopFrequnecyFilter){
            	 //$("#current_angle_df").val(data[i].angle);
					if(eventCount>500){
						$("#el_table tbody tr:last").remove();
						eventCount--;
					}
					$("#el_table tbody").prepend("<tr><td>"+data[i].time.split(" ")[0]+"</td><td>"+data[i].time.split(" ")[1]+"</td><td><a href='javascript:maximizeAngle("+data[i].frequency+","+parseFloat(data[i].angle)+");'>"+data[i].frequency+"</td><td>"+data[i].angle+"</td><td>"+data[i].power+"</td><td>"+data[i].sector+"</td><td>"+data[i].type+"</td><td>"+data[i].technology+"</td></tr>");//<td><button type='button' onclick=maximizeAngle("+data[i].frequency+")  class='btn btn-primary btn-sm'>Maximize</button></td></tr>");
					//resizeTable("el_table");
					eventCount++;
				}
				else{
					//console.log("HELLO    "+data[i].angle);
					$("#current_angle_df").val(data[i].angle);
				}
            }
            else{*/
			if(sectorId == "5" || data[i].sector == sectorId)
				if(data[i].antennaid!=antennaId && peakFrequency>=startFrequencyFilter && peakFrequency<=stopFrequnecyFilter){
					if(eventCount>500){
						$("#el_table tbody tr:last").remove();
						eventCount--;
					}
					$("#el_table tbody").prepend("<tr><td>"+data[i].time.split(" ")[0]+"</td><td>"+data[i].time.split(" ")[1]+"</td><td><a href='javascript:maximizeAngle("+data[i].frequency+","+parseFloat(data[i].angle)+");'>"+data[i].frequency+"</td><td>"+data[i].angle+"</td><td>"+data[i].power+"</td><td>"+data[i].sector+"</td><td>"+data[i].type+"</td><td>"+data[i].technology+"</td></tr>");//<td><button type='button' onclick=maximizeAngle("+data[i].frequency+")  class='btn btn-primary btn-sm'>Maximize</button></td></tr>");
					//resizeTable("el_table");
					eventCount++;
				}
//            	 createLineForTheTower(currentNodeLat,currentNodeLon,100,data[i].angle-30,0,data[i].ip_add);
//            	 createLineForTheTower(currentNodeLat,currentNodeLon,100,data[i].angle+30,0,data[i].ip_add);
//            }
            
            
           //sajal changed on 14-06-19
            /*if(data[i].angle in frequencyAngleCount){
            	frequencyAngleCount[data[i].angle]=frequencyAngleCount[data[i].angle]+1;
            }
            else{
            	frequencyAngleCount[data[i].angle]=1;
            }*/
//        }

    }
	
	/*if(maximizeFrequency==true && dbm!=-1000){
		console.log("Maximum power is found"+dbm);
		$("#current_angle_df").val(parseFloat(currentAngle)+parseFloat(ptzAngleOffset));
		exp = (27.55 - (20 * Math.log10(frequencyValue)) + (30-dbm)) / 20.0;
        distance=Math.pow(10.0, exp);
		console.log("distance is "+distance+" and angle is "+currentAngle+"frequency is "+frequencyValue+" and power is "+ dbm);
        color=getColorCode(dbm);
		//removeSelectedPolylines();
		if(currentAngle!=$("#start_angle").val() && currentAngle!=$("#end_angle").val()){
            			createLineForTheTower(currentNodeLat,currentNodeLon,distance,parseFloat(parseFloat(currentAngle)+parseFloat(ptzAngleOffset)),$("#current_node_ip").val(),color);
            		}
		
	}*/
    //done by sajal changed on 14-06-19
       /*maxAngle=0;
	   maxAngleValue=0;
	   //console.log(frequencyAngleCount);
	   for(angle in frequencyAngleCount){
		   if(frequencyAngleCount[angle]>maxAngleValue){
			   
			  maxAngle=angle;
			  maxAngleValue=frequencyAngleCount[angle];
		   }
	   }*/
	   
	   
	   //$("#current_angle_df").val(maxAngle);
    
   
    /*if(frequencySelected==true && maximizeFrequency==false){
    	updateFrequencyCone();
    }*/
}

/**********************Power Filter*****************************************/
var showPowerFilter = function(){
	$("#max_power").val(max_power);
	$("#min_power").val(min_power);
	$("#graph_filter").modal('show');
}

var setPowerFilter = function(){
	
	var max = parseFloat($("#max_power").val());
	var min = parseFloat($("#min_power").val());
	
	if($("#max_power").val()=='' || $("#min_power").val()==''){
		swal("Error","Please Fill All the details","error");
		return;
	}
	
	if(min>max){
		swal("Error","Min. Power can't be greater than Max. Power","error");
		return;
	}
	
	if(max>30){
		swal("Error","Max. Power can't be greater than 30 dBm","error");
		return;
	}
	
	if(min<min_threshold){
		swal("Error","Min. Power can't be less than "+min_threshold+" dBm","error");
		return;
	}
	
	max_power = max;
	min_power = min;
	setSession(frequencyFilterVal, bandWidthFilter, sectorId);
	graphXAxis();
	
	$("#graph_filter").modal('hide');
}

var powerReset = function(){
	max_power = 0;
	min_power = min_threshold;
	
	$("#max_power").val(max_power);
	$("#min_power").val(min_power);
	setSession(frequencyFilterVal, bandWidthFilter, sectorId);
	graphXAxis();
	
	$("#graph_filter").modal('hide');
}
/********************************************************************************/

var showFrequencyFilter = function(){
	$("#frequencyFilter").val(frequencyFilterVal);
	$("#bandwidthFilter").val(bandWidthFilter);
	$("#sectorFilter").val(sectorId);
	$("#filter_modal").modal('show');
}

var setFilter = function(){
	frequencyFilterVal = $("#frequencyFilter").val();
	bandWidthFilter = $("#bandwidthFilter").val();
	sectorId = $("#sectorFilter").val();
	
	setSession(frequencyFilterVal, bandWidthFilter, sectorId);
	
	startFrequencyFilter = parseFloat(frequencyFilterVal) - parseFloat(bandWidthFilter)/2;
	stopFrequnecyFilter = parseFloat(frequencyFilterVal) + parseFloat(bandWidthFilter)/2;
	$("#filter_modal").modal('hide');
}

var resetFilter = function(){
	frequencyFilterVal = 3000;
	bandWidthFilter = 6000;
	sectorId = "5";
	
	setSession(frequencyFilterVal, bandWidthFilter, sectorId);
	
	startFrequencyFilter = parseFloat(frequencyFilterVal) - parseFloat(bandWidthFilter)/2;
	stopFrequnecyFilter = parseFloat(frequencyFilterVal) + parseFloat(bandWidthFilter)/2;
	$("#frequencyFilter").val(frequencyFilterVal);
	$("#bandwidthFilter").val(bandWidthFilter);
	$("#sectorFilter").val(sectorId);
}

var updateOfflinePeekTable = function(data)
{
	frequencyAngleCount={};
	
    for(var i in data)
    {

            /*if($(".status_table tbody tr").length >=200)
            {
                $(".status_table tbody tr:last").remove();
            }*/
            if(i == 0)
            {
                $(".status_table tbody").html("");
            }
            
            colorCode=colorToHex('0','255','0');
//            createLineForTheTower(currentNodeLat,currentNodeLon,100,90-data[i].ptzangle,data[i].ip_add,colorCode);
            date=data[i].time.split(" ");
            //powerValue=(Math.pow(10,parseInt(data[i].power)+27.55)/20);
    		//distance=powerValue/900;
    		
    		exp = (27.55 - (20 * Math.log10(frequencyFilter)) + (Math.abs(data[i].power)+33)) / 20.0;
            		distance=Math.pow(10.0, exp);
    		
            $(".status_table tbody").prepend("<tr><td>"+data[i].deviceName+"</td><td>"+data[i].power+"</td><td>"+data[i].frequency+"</td><td>"+date[1]+"</td><td>"+data[i].ptzangle+"</td><td>"+data[i].antennaId+"</td><td><button type='button' onclick=maximizeAngle("+data[i].frequency+")  class='btn btn-primary btn-sm'>Maximize</button></td></tr>");
           
            $("#peek_table_date_time").text(date[0]);
            if(data[i].ptzangle in frequencyAngleCount){
            	frequencyAngleCount[data[i].ptzangle]=frequencyAngleCount[data[i].ptzangle]+1;
            }
            else{
            	frequencyAngleCount[data[i].ptzangle]=1;
            }

    }
    console.log(frequencyAngleCount);
    	
}

var convertToGraphData = function(data)
{
    var graphData =[];
    var i=0;
    for(var j in data)
    {
        var obj = {};

        obj.x = j;
        obj.y = data[j];

        graphData[i]=obj

        i++;
    }
    return graphData;
}

/*var convertToData = function(data, appendFlag)
{
    var temp = [];
    for(var t =0;t<13;t++){
		if(t<5)
			myLineChart.data.datasets[t].data = [];
    	myScatterChart.data.datasets[t].data = [];
	}
	
	if(appendFlag){
		temp = xData.shift();
	}
	
    for(var j in data)
    {
        var obj1 = {};

        obj1.x = j;
        obj1.y = data[j];
        //xData.unshift(data[j]);
		//temp.unshift(j);
		temp.unshift(obj1);
        //graphData[i]=obj

        //i++;
    }
    xData.unshift(temp);
    //return graphData;
	
	//graphData = [];
	//xData.unshift(data);
	var len = xData.length;
	if(len>25){
		xData.pop();
	}
	var inde = 0;
	var count =0;
	for(var x in xData){
		for(var y in xData[x]){
			var obj = {};
			var object = xData[x][y];
			obj.x = object.x;
			obj.y = inde;
			//graphData.push(obj);
			if(count<2){
				myLineChart.data.datasets[count].data.push(object);
			}
			else{}
			if(object.y>-10){
				myScatterChart.data.datasets[0].data.push(obj);
			}else if (object.y<=-10 && object.y>-20){
				myScatterChart.data.datasets[1].data.push(obj);
			}else if (object.y<=-20 && object.y>-30){
				myScatterChart.data.datasets[2].data.push(obj);
			}else if (object.y<=-30 && object.y>-40){
				myScatterChart.data.datasets[3].data.push(obj);
			}else if (object.y<=-40 && object.y>-50){
				myScatterChart.data.datasets[4].data.push(obj);
			}else if (object.y<=-50 && object.y>-60){
				myScatterChart.data.datasets[5].data.push(obj);
			}else if (object.y<=-60 && object.y>-70){
				myScatterChart.data.datasets[6].data.push(obj);
			}else if (object.y<=-70 && object.y>-80){
				myScatterChart.data.datasets[7].data.push(obj);
			}else if (object.y<=-80 && object.y>-90){
				myScatterChart.data.datasets[8].data.push(obj);
			}else if (object.y<=-90 && object.y>-100){
				myScatterChart.data.datasets[9].data.push(obj);
			}else if (object.y<=-100 && object.y>-110){
				myScatterChart.data.datasets[10].data.push(obj);
			}else if (object.y<=-110 && object.y>-120){
				myScatterChart.data.datasets[11].data.push(obj);
			}else if (object.y<=-120){
				myScatterChart.data.datasets[12].data.push(obj);
			}
		}
		count++;
		inde--;
	}
	//console.log(graphData);
	/*for(var i =-len+1;i<=0;i++){
		var obj = {};
		obj.x = xData[-i];
		obj.y = i;
		graphData.push(obj);
	}
	//return graphData;
}*/

//on 08-02-2019 for last 5 timestamp data
/*var convertToData = function(data, appendFlag)
{
    var temp = [];
    for(var t =0;t<13;t++){
		if(t<5)
			myLineChart.data.datasets[t].data = [];
    	//myScatterChart.data.datasets[t].data = [];
	}
	
	if(appendFlag){
		temp = xData.shift();
	}
	
    for(var j in data)
    {
        var obj1 = {};

        obj1.x = j;
        obj1.y = data[j];
		temp.unshift(obj1);
    }
    xData.unshift(temp);
    
	var len = xData.length;
	if(len>2){
		xData.pop();
	}
	
	var count =0;
	for(var x in xData){
		for(var y in xData[x]){
			var object = xData[x][y];
			if(count<2){
				myLineChart.data.datasets[count].data.push(object);
			}
			else{}
		}
		count++;
	}
	for(var x in temp){
		var object = temp[x];
		var obj = {};
		obj.x = object.x;
		obj.y = index;
		if(object.y>-10){
			myScatterChart.data.datasets[0].data.push(obj);
		}else if (object.y<=-10 && object.y>-20){
			myScatterChart.data.datasets[1].data.push(obj);
		}else if (object.y<=-20 && object.y>-30){
			myScatterChart.data.datasets[2].data.push(obj);
		}else if (object.y<=-30 && object.y>-40){
			myScatterChart.data.datasets[3].data.push(obj);
		}else if (object.y<=-40 && object.y>-50){
			myScatterChart.data.datasets[4].data.push(obj);
		}else if (object.y<=-50 && object.y>-60){
			myScatterChart.data.datasets[5].data.push(obj);
		}else if (object.y<=-60 && object.y>-70){
			myScatterChart.data.datasets[6].data.push(obj);
		}else if (object.y<=-70 && object.y>-80){
			myScatterChart.data.datasets[7].data.push(obj);
		}else if (object.y<=-80 && object.y>-90){
			myScatterChart.data.datasets[8].data.push(obj);
		}else if (object.y<=-90 && object.y>-100){
			myScatterChart.data.datasets[9].data.push(obj);
		}else if (object.y<=-100 && object.y>-110){
			myScatterChart.data.datasets[10].data.push(obj);
		}else if (object.y<=-110 && object.y>-120){
			myScatterChart.data.datasets[11].data.push(obj);
		}else if (object.y<=-120){
			myScatterChart.data.datasets[12].data.push(obj);
		}
	}
	if(index>25){
		if(index>50){
			for(var i=0;i<13;i++){
				//myScatterChart.data.datasets[i].data.splice(0,100);
			}
		}
		var output = [];
		var min = index-25;
		output.push({
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Time'
				},
				ticks: {
					display: false,
					min: min,
					max: index,
					stepSize: 5
				},
				gridLines:{
					drawOnChartArea: false,
					color: "#101010",
					lineWidth:0
				}
				});
		myScatterChart.options.scales.yAxes = output;
	}
	index++;
}*/

var clearGraph = function(){
	/*for(var t =0;t<13;t++){
		if(t<3 && systemType!='standalone')					//previous changes systemType=='integrated'
			myLineChart.data.datasets[t].data = [];
		else if(t<1)				//new code for single and integrated release
			myLineChart.data.datasets[t].data = [];
	}*/
	myScatterChart.update();
    myLineChart.update();
}

var convertToData = function(data, appendFlag)
{
    var temp = [];
    for(var t =0;t<13;t++){
		if(t<3 && systemType!='standalone')					//previous changes systemType=='integrated'
			myLineChart.data.datasets[t].data = [];
		else if(t<1)				//new code for single and integrated release
			myLineChart.data.datasets[t].data = [];
    	//myScatterChart.data.datasets[t].data = [];
	}
	
	if(appendFlag){
		temp = xData.shift();
	}
	
    for(var j in data)
    {
        var obj1 = {};

        obj1.x = j;
        obj1.y = data[j].power;
		temp.unshift(obj1);
    }
    xData.unshift(temp);
    
	var len = xData.length;
	if(len>2){
		xData.pop();
	}
	
	/*var count =0;
	for(var x in xData){
		for(var y in xData[x]){
			var object = xData[x][y];
			if(count<2){
				myLineChart.data.datasets[count].data.push(object);
			}
			else{}
		}
		count++;
	}*/
	
	for(var j in data)
    {
        var obj1 = {};

        obj1.x = j;
		
		var sector;
		
		//new code for single and consolidated release
		if(data[j].sector=='STRU'){
			var sector = 1;
		}
		else{
			sector = parseFloat(data[j].sector);
		}
		
		//old code
		//var sector = parseFloat(data[j].sector);
		
        obj1.y = data[j].power;
		//temp.unshift(obj1);
		myLineChart.data.datasets[sector-1].data.push(obj1);
    }
	
	for(var x in temp){
		var object = temp[x];
		var obj = {};
		obj.x = object.x;
		obj.y = index;
		var i = Math.floor((-object.y)/10);
		if (i<0)
		i=0;
		/*if(object.y>-10){
			myScatterChart.data.datasets[0].data.push(obj);
		}else if (object.y<=-10 && object.y>-20){
			myScatterChart.data.datasets[1].data.push(obj);
		}else if (object.y<=-20 && object.y>-30){
			myScatterChart.data.datasets[2].data.push(obj);
		}else if (object.y<=-30 && object.y>-40){
			myScatterChart.data.datasets[3].data.push(obj);
		}else if (object.y<=-40 && object.y>-50){
			myScatterChart.data.datasets[4].data.push(obj);
		}else if (object.y<=-50 && object.y>-60){
			myScatterChart.data.datasets[5].data.push(obj);
		}else if (object.y<=-60 && object.y>-70){
			myScatterChart.data.datasets[6].data.push(obj);
		}else if (object.y<=-70 && object.y>-80){
			myScatterChart.data.datasets[7].data.push(obj);
		}else if (object.y<=-80 && object.y>-90){
			myScatterChart.data.datasets[8].data.push(obj);
		}else if (object.y<=-90 && object.y>-100){
			myScatterChart.data.datasets[9].data.push(obj);
		}else if (object.y<=-100 && object.y>-110){
			myScatterChart.data.datasets[10].data.push(obj);
		}else if (object.y<=-110 && object.y>-120){
			myScatterChart.data.datasets[11].data.push(obj);
		}else if (object.y<=-120){
			myScatterChart.data.datasets[12].data.push(obj);
		}*/
		myScatterChart.data.datasets[i].data.push(obj);
	}
	if(index>25){
		/*if(index>50){
			for(var i=0;i<13;i++){
				//myScatterChart.data.datasets[i].data.splice(0,100);
			}
		}*/
		var output = [];
		var min = index-25;
		output.push({
				display: true,
				scaleLabel: {
					display: true,
					labelString: 'Capture'
				},
				ticks: {
					display: false,
					min: min,
					max: index,
					stepSize: 5
				},
				gridLines:{
					drawOnChartArea: false,
					color: "#101010",
					lineWidth:0
				}
				});
		myScatterChart.options.scales.yAxes = output;
	}
	index++;
}


var createLineForTheTower = function(lat,lon,lineLength,angle,ip,color)
{
	/*if(angle >=0 && angle<=90){
		angle=90-angle
	}
	else if(angle > 90 && angle<=180){
		angle=(180-angle)+270
	}
	else if(angle >180 && angle<=270){
		angle=(270-angle)+180;
	}
	else{
		angle=(360-angle)+90;
	}*/
	
	//console.log("hello value is    "+angle);
	/*if(angle<0){
		
		angle=360+parseFloat(angle);
	}
	lineEndPoint=calulateLatLongAtGivenAngleAndDistance(parseFloat(lat.trim()),parseFloat(lon.trim()),angle,lineLength,color);
    //alert(lineEndPoint);
     var line = addPolyLine([lat,lon],lineEndPoint,color);
     polylines.push(line);
	 
	 var dis = Math.round(parseFloat(lineLength/1000)*100)/100;
     
     circle=new L.Circle(lineEndPoint, 1).addTo(map);
     circle.on('mouseover', function(e) {
    	 var popup = L.popup()
    	   .setLatLng(e.latlng) 
    	   .setContent("Distance From Tower is "+dis+" Km.")
    	   .openOn(map);
     });
     circle.on('mouseout', function (e) {
         this.closePopup();
     });
     
     //circle.bindPopup();
     polylines.push(circle);*/ 
    //line.id = ip
//    polylines[ip] = line;
    
//    console.log(polylines[ip]);
	
}

var createLineForTheSPS = function(lat,lon,lineLength,angle,ip,color)
{
	/*if(angle >=0 && angle<=90){
		angle=90-angle
	}
	else if(angle > 90 && angle<=180){
		angle=(180-angle)+270
	}
	else if(angle >180 && angle<=270){
		angle=(270-angle)+180;
	}
	else{
		angle=(360-angle)+90;
	}*/
	
	//console.log("hello value is    "+angle);
	if(angle<0){
		
		angle=360+parseFloat(angle);
	}
	lineEndPoint=calulateLatLongAtGivenAngleAndDistance(parseFloat(lat.trim()),parseFloat(lon.trim()),angle,lineLength,color);
    //alert(lineEndPoint);
     var line = addPolyLine([lat,lon],lineEndPoint,color);
     polylines.push(line);
	 
	 var dis = Math.round(parseFloat(lineLength/1000)*100)/100;
     
     circle=new L.Circle(lineEndPoint, 1).addTo(map);
     circle.on('mouseover', function(e) {
    	 var popup = L.popup()
    	   .setLatLng(e.latlng) 
    	   .setContent("Distance From Tower is "+dis+" Km.")
    	   .openOn(map);
     });
     circle.on('mouseout', function (e) {
         this.closePopup();
     });
     
     //circle.bindPopup();
     polylines.push(circle); 
    //line.id = ip
//    polylines[ip] = line;
    
//    console.log(polylines[ip]);
	
}

var createLineForTheAlarm = function(lat,lon,lineLength,angle,ip,color)
{
	/*if(angle >=0 && angle<=90){
		angle=90-angle
	}
	else if(angle > 90 && angle<=180){
		angle=(180-angle)+270
	}
	else if(angle >180 && angle<=270){
		angle=(270-angle)+180;
	}
	else{
		angle=(360-angle)+90;
	}*/
	
	//console.log("hello value is    "+angle);
	if(angle<0){
		
		angle=360+parseFloat(angle);
	}
	lineEndPoint=calulateLatLongAtGivenAngleAndDistance(parseFloat(lat.trim()),parseFloat(lon.trim()),angle,lineLength,color);
    //alert(lineEndPoint);
     var line = addPolyLine([lat,lon],lineEndPoint,color);
     alarmPolylines.push(line);
	 
	 var dis = Math.round(parseFloat(lineLength/1000)*100)/100;
     
     circle=new L.Circle(lineEndPoint, 1).addTo(map);
     circle.on('mouseover', function(e) {
    	 var popup = L.popup()
    	   .setLatLng(e.latlng) 
    	   .setContent("Distance From Tower is "+dis+" Km.")
    	   .openOn(map);
     });
     circle.on('mouseout', function (e) {
         this.closePopup();
     });
     
     //circle.bindPopup();
     alarmPolylines.push(circle); 
    //line.id = ip
//    polylines[ip] = line;
    
//    console.log(polylines[ip]);
	
}


function createSemiCircle(){
	
	 L.semiCircle([currentNodeLat,currentNodeLon], {
         radius: 2000,
         fill: true,
         fillColor:'#a1ba03',
         fillOpacity: 0.5,
         color: '#a1ba03',
         opacity: 0.5,
         startAngle: 20,
         stopAngle: 200
     }).addTo(map);
}
var clearData = function()
{
	console.log("Inside clear Data");
    graphDataForCurrentSelectedNode = [];
    $(".status_table tbody").html("");
   
    /*for(var i in polylines)
    {
    	polylines[i].remove();
    }
    
    
    
    map.removeLayer(polyLines);
    polylines = [];
	*/
}

var removePolylines=function(){
	
	console.log("Inside removePolyLines");
	//console.log(polylines);
	 for(var i in alarmPolylines)
	    {
			//console.log(polyLines[i]);
	    	alarmPolylines[i].remove();
	    }
	    
	 
	    //map.removeLayer(polyLines);
	    //polylines = [];
}

var removeTowerPolylines=function(){
	
	console.log("Inside removeSelectedPolyLines");
	 for(var i in polylines)
	    {
			//console.log(i);
	    	//polylines[i].remove();
			polylines[i].remove();
			
	    }
	    
	   
	    //map.removeLayer(polyLines);
	    //polylines = [];
}



/*************************** MAP Section **********************************/
function loadLeafMap()
{
    map = new L.Map('map_leaf', {fullscreenControl: true,center: new L.LatLng(28.7041, 77.1025),  zoom: 14, minZoom: 12.5,maxZoom: 18});

    L.control.scale({position: 'bottomright'}).addTo(map);
    
    map.zoomControl.setPosition('bottomright');

    var googleMaplayer = L.gridLayer.googleMutant({
            type: 'roadmap' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
        }).addTo(map);
	mapServerIp=$("#map_server_ip").val();

    //var openstreatMapLayer = L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      var openstreatMapLayer = L.tileLayer('http://'+mapServerIp+'/hot/{z}/{x}/{y}.png', {
        attribution: 'Imagery Â© <a href="http://mapbox.com">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(map);


    towerMarkerLayer = L.layerGroup().addTo(map);
	alarmMarkerLayer = L.layerGroup().addTo(map);
	pathLayer = L.layerGroup().addTo(map);

    var toggleOfflineOnlineMap =  L.Control.extend({

          options: {
            position: 'bottomright'
          },

          onAdd: function (map)
          {
            var container = L.DomUtil.create('img');
            container.id="map_type";
            container.title="Offline";
            container.src="images/offline.png"
            container.style.backgroundColor = 'white';
            $(container).addClass("offline");
            container.style.width = '33px';
            container.style.height = '33px';

            container.onclick = function(){
              //console.log($(container));
              if($(container).hasClass('offline')){
                 map.removeLayer(openstreatMapLayer);
                 container.title="Online";
                 $(container).removeClass("offline");
                 $(container).addClass("online");
                 container.src="images/online.png"
            }
            else {
                //map.removeLayer(googleMapLayer);
                map.addLayer(openstreatMapLayer);
                container.title="Offline";
                $(container).addClass("offline");
                 $(container).removeClass("online");
                 container.src="images/offline.png"
            }

            }
            return container;
          }
        });


    var compasImg =  L.Control.extend({

          options: {
            position: 'bottomright'
          },

          onAdd: function (map)
          {
            var container = L.DomUtil.create('img');
            container.id="map_compass";
            container.title="Up(North)-Down(South)";
            container.src="images/compass.png";
            //container.style.backgroundColor = 'white';
            //$(container).addClass("offline");
            container.style.width = '33px';
            container.style.height = '33px';
            container.style.transform = "rotate(-42deg)";

            return container;
          }
        });

    var currentAngle =  L.Control.extend({

          options: {
            position: 'bottomleft'
          },

          onAdd: function (map)
          {
            var container = L.DomUtil.create('div');
            container.id="angle_map";
            container.title="Device current angle";
            container.style.backgroundColor = 'white';
            //$(container).addClass("offline");
            container.style.width = '45px';
            container.style.height = '35px';
            container.style["font-size"] = '13px';
            container.style["font-weight"] = '900';
            container.style.padding = '7PX';

            return container;
          }
        });

	var logo = L.control({position: 'bottomleft'});
    logo.onAdd = function(map){
        var div = L.DomUtil.create('div', 'myclass');
        div.innerHTML= "<img style = 'height: 275px;width: auto;' src='images/received_strength_analysis.png'/>";

        return div;
    }
    logo.addTo(map); 
	
	var currentData =  L.Control.extend({

	  options: {
		position: 'topright'
	  },

	  onAdd: function (map) 
	  {
		var container = L.DomUtil.create('select');
		container.id="data_type";
		container.title="Device current data";
		container.style.backgroundColor = 'white';
		container.style.clear = 'none';
		//$(container).addClass("offline");			
		//container.style.width = '45px';
		//container.style.height = '35px';
		container.style["font-size"] = '13px';
		container.style["font-weight"] = '900';
		container.style.padding = '7PX';
		
		return container;
	  }
	});
	
	var strengthData =  L.Control.extend({

	  options: {
		position: 'topright'
	  },

	  onAdd: function (map) 
	  {
		var container = L.DomUtil.create('select');
		container.id="strength_type";
		container.title="Strength Value";
		container.style.backgroundColor = 'white';
		container.style.clear = 'none';
		//$(container).addClass("offline");			
		//container.style.width = '45px';
		//container.style.height = '35px';
		container.style["font-size"] = '13px';
		container.style["font-weight"] = '900';
		container.style.padding = '7PX';
		
		return container;
	  }
	});
	 
    map.addControl(new toggleOfflineOnlineMap());
    map.addControl(new compasImg());
    //map.addControl(new currentAngle());
	map.addControl(new currentData());
	map.addControl(new strengthData());
	 
	$("#data_type").html("<option value=0>Today</option><option value=1>Till Yesterday</option><option value=2>Till Last 2 Day</option>");
	$("#strength_type").html("<option value=-1 selected>select</option><option value=0>1mW</option><option value=5>3mW</option><option value=10>10mW</option><option value=27>0.5W</option><option value=30>1W</option><option value=33>2W</option>");
	//var offset = $("#data_type").val();

	$("#data_type").change(function(){
		//resetLayers();
		getEventsData( $("#data_type").val(), $("#strength_type").val());
		//$.cookie('offset', $("#data_type").val());
		//location.reload();
	});

	$("#strength_type").change(function(){
		//resetLayers();
		getEventsData( $("#data_type").val(), $("#strength_type").val());
		//$.cookie('offset', $("#data_type").val());
		//location.reload();
	});
	 
	 /*if($.cookie('offset') != undefined)
	 {
		 $("#data_type").val($.cookie('offset'));
	 }*/
	 //getEventsData( $("#data_type").val());

}

var getEventsData = function(offset, rangeRatio){
    var emterPower="";
	var today = new Date();
	today.setDate(today.getDate()-offset);
	var date = today.getDate()<10?'0'+today.getDate():today.getDate();
	var month = today.getMonth()<9?'0'+(today.getMonth()+1):(today.getMonth()+1);
	
	var today1  =new Date();
	var date1 = today1.getDate()<10?'0'+today1.getDate():today1.getDate();
	var month1 = today1.getMonth()<9?'0'+(today1.getMonth()+1):(today1.getMonth()+1);
	
	var startDate = today.getFullYear()+'-'+month+'-'+date+' '+'00:00:00';
	var endDate = today1.getFullYear()+'-'+month1+'-'+date1+' '+'23:59:59';
	
	//var startDate = '2019-01-24'+' '+'00:00:00';
	//var endDate = '2019-01-24'+' '+'23:59:59';
	
	$.ajax({
        url:serviceUrls.alarmData+"/"+startDate+"/"+endDate,
        dataType:"json",
        success:function(data)
        {
			alarmMarkerLayer.clearLayers();
			mark = [];
			$("#bl_table tbody").html("");
			//var ed = endDate.getDate();
            for(var i in data){
				var d = date_diff_indays(new Date(data[i].time.split(" ")[0]), today1);//new Date(data[i].time.split(" ")[0]).getDate()-ed;
				var ratio = 0;
				var opac = d==0?1:(d==1?0.7:0.3); 
				//draw centroids on map
				if(rangeRatio!=-1){
					ratio = (rangeRatio - data[i].emitterpower)/20;
					emterPower=rangeRatio;
				}
				else{
					ratio = 0;
					emterPower=data[i].emitterpower;
				}
				var range = data[i].range*Math.pow(10,ratio);
				range = Math.round(parseFloat(range)*100)/100;
				
				var marker = setAlarmPositionOverMap(data[i].angle, range, opac, false);
				if(opac==1){
					marker.bindPopup("<table class='table e_tab'><thead><tr><th>Type</th><th>Value</th></tr></thead><tbody><tr><td>Frequency</td><td>"+data[i].frequency+"</td></tr><tr><td>Direction</td><td>"+data[i].angle+"</td></tr><tr><td>Range</td><td>"+range+"</td></tr><tr><td>Rx Level</td><td>"+data[i].power+"</td></tr></tbody></table>");//('<strong>Hummer</strong><br>'+data[i].angle+'');
					marker.id = mark.length;
					mark.push(marker);
					$("#bl_table tbody").append("<tr onclick=openPopup("+marker.id+")><td>"+data[i].time.split(" ")[0]+"</td><td>"+data[i].time.split(" ")[1]+"</td><td>"+data[i].trigger+"</td><td>"+data[i].frequency+"</td><td>"+data[i].angle+"</td><td>"+range+"</td><td>"+data[i].power+"</td><td>"+emterPower+"</td><td>"+data[i].duration+"</td><td>"+data[i].sample+"</td><td>"+data[i].bandwidth+"</td><td>"+data[i].cueid+"</td>");
				}
			}
			resizeTable("bl_table");
        }
    });
}

var openPopup = function(id){
	
	for(var i in mark){
		if(parseInt(mark[i].id)==id)
			mark[i].openPopup();
	}
	//map.alarmMarkerLayer(id).openPopup(); 
}

var date_diff_indays = function(dt1, dt2) {
	//dt1 = new Date(date1);
	//dt2 = new Date(date2);
	return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
}

var addTower = function(position,url)
{
    try
    {

        var myIcon = L.icon
        ({
                iconUrl: url,
                //iconSize: [15, 15],
                iconSize: [30, 30],
                iconAnchor: [15, 28]
                //iconAnchor: [7, 7]
        })
        //towerMarkerLayer.clearLayers();

        towerMarker = L.marker(position,{icon:myIcon}).addTo(towerMarkerLayer);
        map.setZoom(14);
        try
        {
            map.panTo(position);
        }
        catch(err)
        {

        }

    }
    catch(err)
    {

    }

    return towerMarker;

}

var addAlarmTower = function(position,url,opac,pan)
{
    try
    {

        var myIcon = L.icon
        ({
                iconUrl: url,
                //iconSize: [15, 15],
                iconSize: [20, 20],
                iconAnchor: [15, 28]
                //iconAnchor: [7, 7]
        })
        //towerMarkerLayer.clearLayers();

        alarmMarker = L.marker(position,{icon:myIcon,opacity:opac}).addTo(alarmMarkerLayer);
        map.setZoom(14);
        try
        {
			if(pan)
				map.panTo(position);
        }
        catch(err)
        {

        }

    }
    catch(err)
    {

    }

    return alarmMarker;

}

var addPolyLine = function(start,end,lineColor)
{

    // create a red polyline from an array of LatLng points
    var latlngs = [
        start,
        end
    ];
    var polyline = L.polyline(latlngs, {color: lineColor,opacity:0.2}).addTo(map);
    // zoom the map to the polyline
    //map.fitBounds(polyline.getBounds());
   
    return polyline;
}


var calulateLatLongAtGivenAngleAndDistance = function(lat1,lon1,angle,dist,color)
{
	//console.log("Distance is "+distance);
	//alert("Angle in final calculation"+angle);
    return latLng= L.GeometryUtil.destination(L.latLng(lat1,lon1),angle,dist);
    //radius=6378.14;
    //brng = angle * (3.14/180);
    //latitude2 = Math.asin(Math.sin(lat)*Math.cos(distance/radius) + Math.cos(lat)*Math.sin(lat/radius)*Math.cos(brng));
    //longitude2 = lng + Math.atan2(Math.sin(brng)*Math.sin(distance/radius)*Math.cos(lat),Math.cos(distance/radius)-Math.sin(lat)*Math.sin(latitude2));
	
	//var start = L.marker([28.5492602, 77.1790986]).addTo(map);
	
	var start = L.marker([lat1, lon1]).addTo(map);
	var length = 5
	//var angle = 120
	
	
	if(angle >=0 && angle<=90){
		angle=90-angle
	}
	else if(angle > 90 && angle<=180){
		angle=(180-angle)+270
	}
	else if(angle >180 && angle<=270){
		angle=(270-angle)+180;
	}
	else{
		angle=(360-angle)+90;
	}
	
	
	
	
	var end_x = start.getLatLng().lng + length * Math.cos(angle * Math.PI / 180)
	var end_y = start.getLatLng().lat + length * Math.sin(angle * Math.PI / 180)
	var end = L.marker([end_y, end_x]).addTo(map)
		
	var line = L.polyline([start.getLatLng(), end.getLatLng()],{color: color,opacity:0.6}).addTo(map)
	polylines.push(line);
	
	
	//var lat = lat1 + dist * Math.cos(angle * Math.PI / 180)
	//var lng = lon1 + dist * Math.sin(angle * Math.PI / 180)

	/*var a = 6378137,
     b = 6356752.3142,
     f = 1 / 298.257223563, // WGS-84 ellipsiod
     s = dist,
     alpha1 = brng;
     sinAlpha1 = Math.sin(alpha1),
     cosAlpha1 = Math.cos(alpha1),
     tanU1 = (1 - f) * Math.tan(toRad(lat1)),
     cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1)), sinU1 = tanU1 * cosU1,
     sigma1 = Math.atan2(tanU1, cosAlpha1),
     sinAlpha = cosU1 * sinAlpha1,
     cosSqAlpha = 1 - sinAlpha * sinAlpha,
     uSq = cosSqAlpha * (a * a - b * b) / (b * b),
     A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq))),
     B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq))),
     sigma = s / (b * A),
     sigmaP = 2 * Math.PI;
 while (Math.abs(sigma - sigmaP) > 1e-12) {
  var cos2SigmaM = Math.cos(2 * sigma1 + sigma),
      sinSigma = Math.sin(sigma),
      cosSigma = Math.cos(sigma),
      deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
  sigmaP = sigma;
  sigma = s / (b * A) + deltaSigma;
 };
 var tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1,
     lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1, (1 - f) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp)),
     lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1),
     C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha)),
     L = lambda - (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM))),
     revAz = Math.atan2(sinAlpha, -tmp); // final bearing
 	console.log(toDeg(lat2))
 	console.log(lon1 + toDeg(L));
 	
 	lat = Number(Number(toDeg(lat2)));	
	lng = Number(Number(lon1 + toDeg(L)));*/
	latLng={"lat":lat1,"lng":lon1};
 	return latLng;
	
	
	//return destVincenty(lat,lng,angle,distance);
	//console.log(destVincenty(lat,lng,angle,distance));
	//console.log(latitude2+" "+longitude2);
	//return latLng;
}

function destVincenty(lat1, lon1, brng, dist) {
 var a = 6378137,
     b = 6356752.3142,
     f = 1 / 298.257223563, // WGS-84 ellipsiod
     s = dist,
     alpha1 = brng;
     sinAlpha1 = Math.sin(alpha1),
     cosAlpha1 = Math.cos(alpha1),
     tanU1 = (1 - f) * Math.tan(toRad(lat1)),
     cosU1 = 1 / Math.sqrt((1 + tanU1 * tanU1)), sinU1 = tanU1 * cosU1,
     sigma1 = Math.atan2(tanU1, cosAlpha1),
     sinAlpha = cosU1 * sinAlpha1,
     cosSqAlpha = 1 - sinAlpha * sinAlpha,
     uSq = cosSqAlpha * (a * a - b * b) / (b * b),
     A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq))),
     B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq))),
     sigma = s / (b * A),
     sigmaP = 2 * Math.PI;
 while (Math.abs(sigma - sigmaP) > 1e-12) {
  var cos2SigmaM = Math.cos(2 * sigma1 + sigma),
      sinSigma = Math.sin(sigma),
      cosSigma = Math.cos(sigma),
      deltaSigma = B * sinSigma * (cos2SigmaM + B / 4 * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - B / 6 * cos2SigmaM * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
  sigmaP = sigma;
  sigma = s / (b * A) + deltaSigma;
 };
 var tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1,
     lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1, (1 - f) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp)),
     lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1),
     C = f / 16 * cosSqAlpha * (4 + f * (4 - 3 * cosSqAlpha)),
     L = lambda - (1 - C) * f * sinAlpha * (sigma + C * sinSigma * (cos2SigmaM + C * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM))),
     revAz = Math.atan2(sinAlpha, -tmp); // final bearing
 	console.log(toDeg(lat2))
 	console.log(lon1 + toDeg(L));
 	return L.latLng(toDeg(lat2),lon1 + toDeg(L));
 //return new LatLon(toDeg(lat2), lon1 + toDeg(L));
};

function toDeg(n) {
 return n * 180 / Math.PI;
};

function toRad(n) {
 return n * Math.PI / 180;
};
var createArcFromGivenAngle = function(center,angle)
{
    var truf_center = turf.point(center);//([-75, 40]);

    var radius = 100;

    //var bearing1 = angle/2;

    var bearing1 = (angle - 30)<0?360+(angle-30):(angle-30);

    var bearing2 = (angle + 30)>360?(angle + 30)-360:(angle + 30);

    var arc = turf.lineArc(truf_center, radius, bearing1, bearing2);


    var latlngs = [];
    latlngs.push(center);
    var len = arc.geometry.coordinates.length
    for(i=0;i<arc.geometry.coordinates.length;i++)
    {
        //latlngs.push(arc.geometry.coordinates[i]);
    }
latlngs.push(arc.geometry.coordinates[0],arc.geometry.coordinates[len-1]);


    var polygon = L.polygon(latlngs, {color: 'red'}).addTo(map);
    //map.fitBounds(polygon.getBounds());
    return polygon;
}





/*************************** MAP Section **********************************/
/*****************************validation***********************************/

var validateAddNodeForm = function()
{
    var emptyCheck = false;
    $("#add_node_form input").each(function(){
        if(!checkIfTextBoxIsEmpty($(this)))
        {

            emptyCheck = true;
            //break;
        }
    });
    if(emptyCheck)
    {
        swal("Error","Please Fill All the details","error");
        return false;
    }

    if(!ValidateIPaddress($("#node_ip").val()))
    {
                return false;
    }

    return true;
}


var validateDefForm = function()
{
    var emptyCheck = false;
    $("#def_form input").each(function(){
        if(!checkIfTextBoxIsEmpty($(this)))
        {

            emptyCheck = true;
            //break;
        }
    });
    if(emptyCheck)
    {
        swal("Error","Please Fill All the details","error");
        return false;
    }
    return true;
}

var checkIfTextBoxIsEmpty = function(element)
{
    if(element.val().trim().length <= 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

function ValidateIPaddress(ipaddress)
{
 if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress))
  {
    return (true)
  }
swal("Error","You have entered an invalid IP address!","error")
return (false)
}
/*****************************validation***********************************/

var initData = function()
{
    loadLeafMap();
	
	ptzRollOffset = parseFloat($("#ptz_roll_offset").val());
	ptzAngleOffset = parseFloat($("#ptz_north_offset").val());
	startTime = $("#start_time").val()
	if(ptzAngleOffset<0)
		$("#ptz_status").html(ptzAngleOffset+360);
	else
		$("#ptz_status").html(ptzAngleOffset);
	$("#start_status").html(startTime);
	
	initializeConfig1();
	
	var option =
{
		"width": "95%",
		"height": "95%",
		"top": "2.5%",
		"left": "2.5%",
		"right": "2.5%",
		"bottom": "2.5%",
		"background": "white"
	}
	
	$(".fullscreen").fullscreen(option,function(element){
        cosole.log(element);
    });
	
	$(".fullscreen").click(function()
	{
		if($(this).find('i').hasClass("fa-window-maximize"))
		{
			$(this).find('i').removeClass("fa-window-maximize");
			$(this).find('i').addClass('fa-window-minimize');
		}
		else
		{
			$(this).find('i').removeClass("fa-window-minimize");
			$(this).find('i').addClass('fa-window-maximize');
		}
	});
	
	//$(".fullscreen").fullscreen(option);

    var ctx = document.getElementById('canvas').getContext('2d');
    
    myLineChart = new Chart(ctx, config1);
	
	var ctx1 = document.getElementById('canvas1').getContext('2d');
  
	myScatterChart = Chart.Scatter(ctx1, config2);
	ctx1.height = 200;
    Chart.defaults.global.animation = false;
    
	
    $('#node_color').colorpicker();
    $('#color').colorpicker();
	getNodesData();
	//drawInitial();
    connectSocket();
    //
    loadFaultTable();
    faultStatusEvent();
}

var drawLine = function(path)
{
   
    //var polyline = L.polyline(path, {color: 'red'}).addTo(map);
    try
    {
        var polyline = L.polyline(path, {color: 'red'});
        //var polyline = L.polyline([[0,0]], {color: 'red'}).addTo(map);
        //lines.push(polyline);
        pathLayer.addLayer(polyline);   
    }
    catch(err)
    {
       
    }
   
}

var loadGpsData = function(){
	var today = new Date();
	var date = today.getDate()<10?'0'+today.getDate():today.getDate();
	var month = today.getMonth()<9?'0'+(today.getMonth()+1):(today.getMonth()+1);
	
	var startDate = today.getFullYear()+'-'+month+'-'+date+' '+'00:00:00';
	var endDate = today.getFullYear()+'-'+month+'-'+date+' '+'23:59:59';
	
	//var startDate = '2018-10-10'+' '+'00:00:00';
	//var endDate = '2018-10-10'+' '+'23:59:59';
	var period = "10";
	
	$.ajax({
        url:serviceUrls.getGpsData+"/"+period,      //startDate+"/"+endDate,
        dataType:"json",
        success:function(data)
        {
			var path = [];
			for(var i in data)
			{
				path.push([data[i].lat,data[i].lon]);
				
				/*if(path.length >= 1 && distanceBetweenLatLon(parseFloat(path[path.length-1][0]),parseFloat(path[path.length-1][1]),parseFloat(data[i].lat),parseFloat(data[i].lon))> accuracyGPS )
				{
					path.push([data[i].lat,data[i].lon]);   
				}
				else if(path.length==0)
				{
					path.push([data[i].lat,data[i].lon]);   
				}*/
			}
			/*for(var k in lines)
			{
				lines[k].remove();
			}
			for(var j in lastEventPt)
			{
			   
				if(path.length > 1 && distanceBetweenLatLon(parseFloat(path[path.length-1][0]),parseFloat(path[path.length-1][1]),parseFloat(lastEventPt[j][0]),parseFloat(lastEventPt[j][1])) > accuracyGPS )
				{
					path.push(lastEventPt[j]);   
				}
				else if(path.length==0)
				{
					path.push(lastEventPt[j]);   
				}   
			}*/
		   
			if(path.length>1)
			{
			   
				drawLine(path);
			}
		   
			/*if(path.length>=1)
			{
			   
				addTower(path[(path.length)-1],'../../resources/images/tower.png');
			}
		   
			try
			{
				//map.panTo(new L.LatLng(path[(path.length)-1][0],path[(path.length)-1][1]));   
			}
			catch(err)
			{
			   
			}*/
		   
		}
    });
}

var loadEvents = function(){
	var today = new Date();
	var date = today.getDate()<10?'0'+today.getDate():today.getDate();
	var month = today.getMonth()<9?'0'+(today.getMonth()+1):(today.getMonth()+1);
	
	var startDate = today.getFullYear()+'-'+month+'-'+date+' '+'00:00:00';
	var endDate = today.getFullYear()+'-'+month+'-'+date+' '+'23:59:59';
	
	//var startDate = '2018-10-10'+' '+'00:00:00';
	//var endDate = '2018-10-10'+' '+'23:59:59';
	
	$.ajax({
        url:serviceUrls.peakData+"/"+startDate+"/"+endDate,
        dataType:"json",
        success:function(data)
        {
            for(var i in data){
				if(data[i].antennaid!=antennaId){
					$("#el_table tbody").append("<tr><td>"+data[i].time.split(" ")[0]+"</td><td>"+data[i].time.split(" ")[1]+"</td><td>"+data[i].frequency+"</td><td>"+data[i].angle+"</td><td>"+data[i].power+"</td><td>"+data[i].sector+"</td><td>"+data[i].type+"</td><td>"+data[i].technology+"</td></tr>");//<td><button type='button' onclick=maximizeAngle("+data[i].frequency+")  class='btn btn-primary btn-sm'>Maximize</button></td></tr>");
					//resizeTable("el_table");
					eventCount++;
				}
				if(eventCount>200)
					break;
			}
            
            $('#oldPeakDiv').css('display','none');
            $('#frequencyValue').css('display','block');
        	$('#frequencyTab').css('display','block');
        	$('#bandwidthValue').css('display','block');
        	$('#bandwidthTab').css('display','block');
        	$('#showFrequencyFilter').css('display','block');
        	$('#currPeakDiv').css('display','block');
        	$('#lastOtRecords').css('display','none');
        }
    });
}

var loadAlarms = function(){
	var today = new Date();
	var date = today.getDate()<10?'0'+today.getDate():today.getDate();
	var month = today.getMonth()<9?'0'+(today.getMonth()+1):(today.getMonth()+1);
	
	var startDate = today.getFullYear()+'-'+month+'-'+date+' '+'00:00:00';
	var endDate = today.getFullYear()+'-'+month+'-'+date+' '+'23:59:59';
	
	//var startDate = '2018-12-03'+' '+'00:00:00';
	//var endDate = '2018-12-03'+' '+'23:59:59';
	
	$.ajax({
        url:serviceUrls.alarmData+"/"+startDate+"/"+endDate,
        dataType:"json",
        success:function(data)
        {
            for(var i in data){
				$("#bl_table tbody").append("<tr><td>"+data[i].time.split(" ")[0]+"</td><td>"+data[i].time.split(" ")[1]+"</td><td>"+data[i].trigger+"</td><td>"+data[i].frequency+"</td><td>"+data[i].angle+"</td><td>"+data[i].range+"</td><td>"+data[i].power+"</td><td>"+data[i].duration+"</td><td>"+data[i].sample+"</td><td>"+data[i].bandwidth+"</td><td>"+data[i].cueid+"</td>");
				//draw centroids on map
				setAlarmPositionOverMap(data[i].angle, data[i].range, 1, false);
			}
        }
    });
}

var loadEventCue = function(){
	var today = new Date();
	var date = today.getDate()<10?'0'+today.getDate():today.getDate();
	var month = today.getMonth()<9?'0'+(today.getMonth()+1):(today.getMonth()+1);
	
	var startDate = today.getFullYear()+'-'+month+'-'+date+' '+'00:00:00';
	var endDate = today.getFullYear()+'-'+month+'-'+date+' '+'23:59:59';
	
	//var startDate = '2019-01-24'+' '+'00:00:00';
	//var endDate = '2019-01-24'+' '+'23:59:59';
	
	$.ajax({
        url:serviceUrls.eventCue+"/"+startDate+"/"+endDate,
        dataType:"json",
        success:function(data)
        {
            for(var i in data){
				$("#cue_table tbody").append("<tr><td>"+data[i].eventDate.split(" ")[1]+"</td><td>"+data[i].nodeType+"</td><td>"+data[i].eventType+"</td><td>"+data[i].eventData+"</td></tr>");
				//resizeTable("cue_table");
			}
        }
    });
}

var getSession = function(){
	
	$.ajax({
        url:serviceUrls.getSession,
        success:function(data)
        {
            var freq = parseFloat(data.split(",")[0]);
            var band = parseFloat(data.split(",")[1]);
            var sector = parseFloat(data.split(",")[2]);
			var max = parseFloat(data.split(",")[3]);
			var min = parseFloat(data.split(",")[4]);
			var threshold = parseFloat(data.split(",")[5]);
			
            if(isNaN(freq)){
            	setSession(3000,6000,sectorId);
            }
            else{
            	frequencyFilterVal = freq;
            	bandWidthFilter = band;
            	sectorId = sector;
				max_power = max;
				if(threshold != min_power){//min<min_power){
					setSession(frequencyFilterVal, bandWidthFilter, sectorId);
				}
				else{
					min_power = min;
				}
				startFrequencyFilter = parseFloat(frequencyFilterVal) - parseFloat(bandWidthFilter)/2;
				stopFrequnecyFilter = parseFloat(frequencyFilterVal) + parseFloat(bandWidthFilter)/2;
				$("#frequencyValue").text(freq+" ");
				$("#bandwidthValue").text(band+" ");
            }
            //new code
            graphXAxis();
        }
    });
}

var setSession = function(freq, band, sectorId){
	
	var sessionData =
    {
        "freq":freq,
        "band":band,
        "sectorId":sectorId,
		"max_power":max_power,
		"min_power":min_power,
		"threshold":min_threshold
    }
	
	$("#frequencyValue").text(freq+" ");
	$("#bandwidthValue").text(band+" ");
				
	$.ajax({
        url:serviceUrls.setSession,
        data:sessionData,
        success:function(data)
        {
           
        }
    });
}

var resizeTable = function(id)
{
    $i = 0;
    $("#"+id+" tbody tr:first-of-type td").each(function()
    {
        $i++;
        width = $(this).outerWidth();
        $("#"+id+" thead th:nth-of-type(" +$i+")").css({"min-width": width+"px"});
    });
} 

function getDeviceInfo(deviceId){
	 $.ajax({
	        url:serviceUrls.deviceinfo+"/"+deviceId,
	//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
	        type:"post",

	        success:function(data)
	        {
	        	
	        	$("#node_name").val(data.name);
	        	$("#node_ip").val(data.ip_add);
	        	$("#node_color").val(data.color);
	        	$("#node_lat").val(data.lat);
	        	$("#node_lon").val(data.lon);
	        
	        	$("#node_id").val(data.id);
	        	if(data.id!=0){
	        		$('#node_ip').attr('readonly', true);
	        	} 
	        	$("#node_color").css("background",$("#node_color").val());
	             $("#node_color").css("color",$("#node_color").val());
	        	
	        },
	 error:function(){
		 swal("Error","Something Went Wrong","error");
	 }

	    });
}

var testInit  = function()
{
    var polylines = {};
    addTower([28,73],"images/tower.png");

    lineEndPoint = calulateLatLongAtGivenAngleAndDistance(28,73,0,100);
    var line = ([28,73],lineEndPoint,'red');
    line.id = 1
    polylines[1] =line ;

    var updateLine  = function(id,angle)
    {
        line = polylines[id];
        var path =  line.getLatLngs();
        lineEndPoint = calulateLatLongAtGivenAngleAndDistance(path[0].lat,path[0].lng,angle,100);
        path[1] = lineEndPoint;
        line.setLatLngs(path);
    }
    var count = 0;

}

function getPtzInfo(){
	
	 /*$.ajax({
	        url:serviceUrls.ptzinfo+"/"+current_ip_addr,
	//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
	        type:"post",

	        success:function(data)
	        {
	        	
	        	//$("#ptz_ip").val(data.ptz_ip);
	        	//$("#ptz_angle_offset").val(data.ptz_offset);
				ptzAngleOffset = parseFloat(data.ptz_offset);
				$("#ptz_status").html(ptzAngleOffset);
	        	
	        }

	    });*/
}

function addPtzNode(){
	
	
	 
	var ptzData =
    {
            "device_ip":$("#current_node_ip").val(),
            "ptz_ip":$("#ptz_ip").val(),
            "ptz_offset":$("#ptz_angle_offset").val()
            
    }
	$.ajax({
	        url:serviceUrls.addPtz+"/"+$("#current_node").val(),
	        data:ptzData,
	        type:"post",
	        
	        success:function(data)
	        {
	            swal("Success","Ptz Data Has Been Saved","success");
	            $("#addPtzModal").modal('hide');
	           
	           
	            
	        }

	    });
}

$(document).ready(function()
{
	//document.getElementById("manual").disabled = true;
	//document.getElementById("auto").disabled = true;
	$("#header").load("header");
	getSensorConfiguration();
	
	systemType = $("#system_type").val();
	
	//oprmode=$("#opr_mode").val();
	if(systemType=="standalone"){				//previous changes systemType!="integrated"
		
		usedDataset = standaloneDataset;
		/*$("#alarm_div").hide();
		$("#cue_div").hide();
		$("#status_div").hide();
		$("#event_div").addClass("h-100");
		$("#event_div").removeClass("h-50");
		$("#graph_div").addClass("col-7");
		$("#graph_div").removeClass("col-5");*/
	}
	else{
		usedDataset = integratedDataset;
	}
	
	getColorCodeList();
	
	events();
	getSession();
	
    initData();
	loadEvents();
	//events();
	//loadAlarms();
	getEventsData(0,-1);
	checkNodeStatus();
	loadEventCue();
	updateMaximizeEvent();
	workerExample();
	workerExample1();
	getCurrentEvent();
	
	disableMaximize();
	loadGpsData();
	deployPeakDataGrid([]);	
});

/*function createTablePartition(){
	var date = new Date();
	currentDate = date.getDate()+1+"_"+date.getMonth()+"_"+date.getFullYear();
	$.ajax({
 		url:serviceUrls.createTablePartition,
 		data:{"currentDate":currentDate},
 		success:function(data)
 		{
 			console.log("URL Is Hitting For Partitioning");
 			}
 		}
}
*/
function getOfflineData(reportType){
	

    startDate=$("#search-from-date").val().split("/").join("-")
    endDate=$("#search-to-date").val().split("/").join("-");
    
        $.ajax({
        url:serviceUrls.offlineData+"/"+startDate+"/"+endDate+"/"+$("#current_node").val(),
//url:"http://10.100.207.117:9000/security_server/"+serviceUrls.addNode,
        type:"post",

        success:function(data)
        {
            //generateCsvData(data);
        	if(reportType=='csv'){
        		 createCsvFile(data);
        	}
        	else if(reportType=='tsv'){
        		createTsvFile(data);
        	}
        	else{
        		 plotOfflineData(data);
        	}
           
           
            
        }

    });
}



 async function plotOfflineData(data){
	xData = [];
    for( var i in data) {

        let delayres = await delay(1000);
        updateOfflinePeekTable(data[i]);
        createOrUpdateOfflineGraph(data[i]);
    }
}
 async function delay(delayInms) {
    return new Promise(resolve  => {
      setTimeout(() => {
        resolve(2);
      }, delayInms);
    });
  }

 $(document).ready(function () {
        $("input[type='radio']").on('change', function () {
             var selectedValue = $("input[name='dataAccessRadio']:checked").val();
             if (selectedValue=="Offline") {
                 //getRealTimeData=false;
                 $("#dateFilter").show();
               }
             else{
                 getRealTimeData=true;
                 $("#dateFilter").hide();
             }
         });

        $("#search-from-date").datetimepicker();
        $("#search-to-date").datetimepicker();
//        $('#search-from-date, #search-to-date').datetimepicker();
    });
 
 function getColorCode(signal){
 
 	
	 if(signal<=0 && signal>-10){
		 return colorToHex(255,0,0);
	 }
	 else if(signal <=10 && signal>-20){
		 return colorToHex(255,128,0);
	 }
	 else if(signal<=-20 && signal>-30){
		 return colorToHex(255,165,0);
	 }
	 
	 else if(signal<=-30 && signal>-40){
		 return colorToHex(255,206,0);
	 }
	 
	 else if(signal<=-40 && signal>-50){
		 return colorToHex(255,255,0);
	 }
	 
	 else if(signal<=-50 && signal>-60){
		 return colorToHex(184,255,0);
	 }
	 
	 else if(signal<=-60 && signal>-70){
		 return colorToHex(0,255,0);
	 }
	 else if(signal<=-70 && signal>-80){
		 return colorToHex(0,208,0);
	 }
	 else if(signal<=-80 && signal>-90){
		 return colorToHex(0,196,196);
	 }
	 else if(signal<=-90 && signal>-100){
		 return colorToHex(0,148,255);
	 }
	 else if(signal<=-100 && signal>-110){
		 return colorToHex(80,80,255);
	 }
	 else if(signal<=-110 && signal>-120){
		 return colorToHex(0,38,255);
	 }
	 else if(signal<=-120 && signal>-130){
		 return colorToHex(142,63,255);
	 } 
 }
 function colorToHex(red, green, blue) {

		green = parseInt(green);
		blue = parseInt(blue);
		red = parseInt(red);
		return "#" + hex(red) + hex(green) + hex(blue);

	}
 
 function hex(x) {
		var hexDigits = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
				"a", "b", "c", "d", "e", "f");
		return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
	}

 /**********************************************************@sunil**************************************************/

 var updateStatusBMS = function(data)
 {
 		//data = data.split(",");
 		
 		var ip = data[0];
 		var status = data[20];
 		$("#sl_table tr").each(function()
 		{

 		console.log($(this).find("td:eq(1)").text());
 		if($(this).find("td:eq(1)").text() == ip)
 		{
 			($(this).find("td:eq(2)").text(status+" %"));
 		}

 		});	
 }	 

var faultStatusEvent = function()
{
	$("#fault_status").parent().click(function(){
		 $("#fault_modal").modal("show");	
	 });
}


var createGraphConfiguration = function(numberOfSensors){
	if(numberOfSensors == "3" && systemType!= "standalone"){
		    integratedDataset = [{
            label: 'Sector 1',
            backgroundColor: colorToHex(167, 147, 141),
            borderColor: colorToHex(167, 147, 141),
            //data: [{x: 2400,y: -90}, {x: 1300,y: -20},{x:1800,y:-39}],
            data: [],
            fill: false
            //,showLine:true
        },
        {
			label: 'Sector 2',
			borderColor: colorToHex(21,151,155),
			backgroundColor: colorToHex(21,151,155),
			data: [],
			fill:false
		},
		{
			label: 'Sector 3',
			borderColor: colorToHex(255,195,0),
			backgroundColor: colorToHex(255,195,0),
			data: [],
			fill:false
		}];		

		
	}
	
	else if(numberOfSensors == "2" && systemType!= "standalone"){
			integratedDataset = [{
            label: 'Sector 1',
            backgroundColor: colorToHex(167, 147, 141),
            borderColor: colorToHex(167, 147, 141),
            data: [],
            fill: false
        },
        {
			label: 'Sector 2',
			borderColor: colorToHex(21,151,155),
			backgroundColor: colorToHex(21,151,155),
			data: [],
			fill:false
		}];
	}
	
	else if(numberOfSensors == "1" &&  systemType!= "standalone"){
			integratedDataset = [{
            label: 'Sector 1',
            backgroundColor: colorToHex(167, 147, 141),
            borderColor: colorToHex(167, 147, 141),
            data: [],
            fill: false
        }];
	}
	
	else if(systemType == "standalone"){
			standaloneDataset = [{
            label: 'STRU',
            backgroundColor: colorToHex(179, 255, 217),
            borderColor: colorToHex(179, 255, 217),
            data: [],
            fill: false
        }];
	}
}

var getSensorConfiguration = function(){
	
	$.ajax({
	 		url:serviceUrls.getSensorConfiguration,
	 		dataType:"json",
	 		async:false,
	 		success:function(data)
	 		{
	 			numberOfSensors = data;
	 			createGraphConfiguration(numberOfSensors);
	 		}
	 	});

	
}
 var createFaultTable = function()
 {
 	
	 
	 //$("#fault-table tbody").html("");
	 $.ajax({
 		url:serviceUrls.fault,
 		dataType:"json",
 		success:function(data)
 		{
 			/*var count = 0;
 			
 			
 			
 			for(var i in data)
 			{
 				var ack = "";
 				if(!data[i].acknowledged)
 				{
 					count =1;
 					ack="<button class='btn btn-info' onclick='ackFault("+data[i].id+")'>Acknowledge</button>"
 				}
 				
 				
 				//2009-02-13T23:31:30.000+0000
 				
 				var time = data[i].generationTime.substring(0,data[i].generationTime.indexOf(".")-1).replace("T"," ");
 				$("#fault-table tbody").append("<tr><td>"+data[i].ip+"</td><td>"+data[i].eventDesctiption+"</td><td>"+time+"</td><td>"+ack+"</td></tr>");
 			}
 			
 			if(count == 1)
 			{
 				$("#fault_status").parent().addClass("blinking");
 			}
 			else
 			{
 				$("#fault_status").parent().removeClass("blinking");
 			}*/
 		}
 	});
 }
 
 var ackFault = function()
 {
	 
	 var grid = $("#fault-table");
     var rowKey = grid.getGridParam("selrow");
     var id = grid.jqGrid ('getCell', rowKey, 'id');
	 $.ajax({
	 		url:serviceUrls.ackFault,
	 		data:{id:id},
	 		success:function(data)
	 		{
	 			grid.trigger( 'reloadGrid' );
	 		}
	 	});
 }
 
 var clearFault = function()
 {
	 
	 var grid = $("#fault-table");
     var rowKey = grid.getGridParam("selrow");
     var id = grid.jqGrid ('getCell', rowKey, 'id');
	 $.ajax({
	 		url:serviceUrls.clearFault,
	 		data:{id:id},
	 		success:function(data)
	 		{
	 			grid.trigger( 'reloadGrid' );
	 		}
	 	});
 }
 
 var updateFaultTable =  function(data)
 {
	 
	 //loadFaultTable();
	 //createFaultTable();
 }
 
 
 var loadFaultTable = function()
 {
	 var color=['red','green','yellow','orange','white'];
	 
	 jQuery("#fault-table").jqGrid(
	 {
		   	url:serviceUrls.fault,
			datatype: "json",
			loadonce: true,
		   	//colNames:['ID','Date', 'Client', 'Amount','Tax','Total','Notes'],
		   	colModel:[
				{name:'Date',index:'logtime'},
				{name:'Component',index:'componentType'},
		   		{name:'Event',index:'eventType'},
		   		{name:'Severity',index:'severityType'},
		   		{name:'Desctiption',index:'eventDesctiption'},
		   	],
		   	afterInsertRow: function(rowId, data)
	        {
	            //$("#fault-table").jqgrid('setCell',rowId,'severity', '', {'background-color': + color[data.severity]});
	        },
		   	gridComplete:function()
		   	{
				$(".jqgrow", "#fault-table").contextMenu('contextMenu', {
		            bindings: {
		                'ack': function (t) {
		                	ackFault();
		                },
		                'clear': function (t) {
		                	clearFault();
		                }
		            },
		            onContextMenu: function (event, menu) {
		                var rowId = $(event.target).parent("tr").attr("id")
		                var grid = $("#fault-table");
		                grid.setSelection(rowId);
		                return true;
		            }
		        });

		   	},
		   	autowidth:true,
		   	shrinkToFit:false,
		   	rowNum:10,
		   	rowList:[10,20,30],
		   	pager: '#fault-pager',
		   	sortname: 'id',
		    viewrecords: true,
		    sortorder: "desc",
		    caption:"Alarms"
		});
		jQuery("#fault-table").jqGrid('navGrid','#fault-pager',{edit:false,add:false,del:false});

		
		//createFaultTable();
 }

 
 /*Highlight Button On Click*/
 var header = document.getElementById("event_div");
 var btns = header.getElementsByClassName("evnt_button");
 for (var i = 0; i < btns.length; i++) {
 btns[i].addEventListener("click", function() {
 var current = document.getElementsByClassName("active");
 current[0].className = current[0].className.replace(" active", "");
 this.className += " active";
 	  });
 	}
 
 /******************************************************************************************************************/
