var rowData;

var getAlarmData = function(){
	$.ajax({
		type:"get",
	   	url:serviceUrls.getAlarm,
		datatype: "json",
	    success:function(data)
		{
	    	var entity=JSON.parse(data.entity);
	    	showAlarmCount(entity);
	    	loadAlarms(entity);
		}
	});
}
var getFaultAlarmData = function(){
	$.ajax({
		type:"get",
	   	url:serviceUrls.getAlarm,
		datatype: "json",
	    success:function(data)
		{
		   $("#grid").jqGrid("GridUnload")

	    	var entity=JSON.parse(data.entity);
	    	showAlarmCount(entity);
	    	loadAlarms(entity);
		}
	});
}

var showAlarmCount = function(data){
	var info = 0;
	var warning = 0;
	var minor = 0;
	var major = 0;
	var critical = 0;
	
	for(var i=0;i<data.length;i++){
		switch(data[i].severity){
		case 1:
			info++;
			break;
		case 2:
			warning++;
			break;
		case 3:
			minor++;
			break;
		case 4:
			major++;
			break;
		case 5:
			critical++;
			break;
		}
	}
	
    $('#info_alarms').text(info);
	$('#warning_alarms').text(warning);
	$('#minor_alarms').text(minor);
	$('#major_alarms').text(major);
	$('#critical_alarms').text(critical);
	$('#total_alarms').text(info+warning+minor+major+critical);
}



var loadAlarms = function(data)
{
	jQuery("#grid").jqGrid({
	   data:data,
		datatype: "local",
		async:false,
		toppager: false,
	   	colNames:['Id', 'Date Time', 'Node IP', 'Report', 'Alarm', 'Component Id', 'Component Type', 'Managed Object Id', 'Managed Object Type', 'Event Id', 'Event Type', 'Severity', 'Event Description', 'Time', 'Ack', 'Clear'],
	   	colModel:[
	   		{name:'id',index:'id', width:55,  hidden:true},
	   		{name: 'logTime', 
	   			align: 'center', search: false, width: 70,
	   			formatter: 'date',
	   			formatoptions: {srcformat: "ISO8601Long", newformat: "d/m/Y h:i:s" }
	   		},
	   		{name:'ip',index:'ip', width:60},
	   		{name:'report',index:'report', width:70, hidden:true},
	   		{name:'alarm',index:'alarm', width:70, hidden:true},
	   		{name:'componentId',index:'componentId', width:90},
	   		{name:'componentType',index:'componentType', width:60, formatter:mapComponentType},
	   		{name:'managedObjectId',index:'managedObjectId', width:50, hidden: true},
	   		{name:'managedObjectType',index:'managedObjectType', width:70,formatter:mapManagedObjectType},
	   		{name:'eventId',index:'eventId', width:70, hidden: true},
	   		{name:'eventType',index:'eventType', width:70, hidden: true},
	   		{name:'severity',index:'severity', width:70,formatter:setSeverityNameByValue,
	   			
	   			cellattr : function(rowId, cellValue, rawObject, cm, rdata) {
	   				
	   				switch(cellValue){
	   				
	   				case "Info":
	   					return 'style="background-color:rgb(0, 251, 251)"';
	   					
	   				case "Warning":
	   					return 'style="background-color:rgb(250, 70, 220)"';
	   					
	   				case "Minor":
	   					return 'style="background-color:rgb(255, 255, 0)"';
	   				
	   				case "Major":
	   					return 'style="background-color:rgb(255, 128, 0)"';
	   				
	   				case "Critical":
	   					return 'style="background-color:rgb(255,0,0)"';
	   					
	   				default :
	   					return 'style="background-color:rgb(46, 194, 226)"';
	   				}
	   			}
	   		},
	   		
	   		{name:'eventDescription',index:'eventDescription', width:150},
	   		
	   		{name: 'generationTime', 
		   		align: 'center', search: false, width: 80,hidden:true,
		   		formatter: 'date',
		   		formatoptions: {srcformat: "ISO8601Long", newformat: "d/m/Y h:i:s" }
	   		},
	   	
	   		
	   		
	   		{name: 'acknowledgement', index: 'acknowledgement', width: 60, align: 'center',hidden: true,
            formatter: 'checkbox', editoptions: { value: '1:0' },
            formatoptions: { disabled: false },
            },
            
            { name: 'clear', index: 'clear', width: 60,align: 'center', sortable: false, jsonmap: "id",
                formatter:function (cellvalue, options, rowObject) {
                	//return "<i style='cursor:pointer;color:red;' title='Delete' class=\"ti-trash\" onclick='javascript:clearCurrentAlarmById("+cellvalue+");'/>";
                	return "<i class=\"ti-trash\" style='font-size:12px;color:red' onclick='javascript:clearCurrentAlarmById("+rowObject.id+","+rowObject.severity+");'/>";
                	
                }
            }        
	   	],
	   	
	   	rowNum:1000,
	   	rowList:[15,20,100,1000],
	    width: 1400,
        height: 300,
	   	pager: '#pager2',
	   	sortname: 'logTime',
	    viewrecords: true,
	    sortorder: "desc",
	    caption:"Current Alarm",
	    gridview: true,
	   
	    beforeSelectRow: function (rowid, e) {
	        var $self = $(this),
	            iCol = $.jgrid.getCellIndex($(e.target).closest("td")[0]),
	            cm = $self.jqGrid("getGridParam", "colModel"),
	            localData = $self.jqGrid("getLocalRow", rowid);
	        if (cm[iCol].name === "acknowledgement" && e.target.tagName.toUpperCase() === "INPUT" && e.target.disabled) {
	            // set local grid data
	        	setAckTrue(rowid);	
	        }
	        
	        return true; // allow selection
	    },
	       
		 loadComplete: function() {
			   var i=getColumnIndexByName('acknowledgement');
			   // nth-child need 1-based index so we use (i+1) below
			   $("tbody > tr.jqgrow > td:nth-child("+(i+1)+") > input",
			      this).click(function(e) {
			    	  /*swal({
			    		  title: "Are you sure?",
			    		  text: "Once deleted, you will not be able to recover this imaginary file!",
			    		  icon: "warning",
			    		  buttons: true,
			    		  dangerMode: true,
			    		})
			    		.then((willDelete) => {
			    		  if (willDelete) {
			    			$('#tr_clr_flag').attr('disabled',false);
			    		    swal("Poof! Your imaginary file has been deleted!", {
			    		      icon: "success",
			    		    });
			    		  } else {
			    		    swal("Your imaginary file is safe!");
			    		  }
			    		});  */
			    	if(confirm("Are you sure!")){
			    		disableIfChecked(this);
			    	}
			    	else{
			    		$(this).attr('checked', false);
			    	}
			    }).each(function(e) {
			        disableIfChecked(this);
			    });
		 }
	});	
}

var setAckTrue = function(id)
{
	$.ajax({
		type:"post",
		url:serviceUrls.setAckTrue,
		data:{"id":id, "acknowledgement":true},
	    success:function(data)
		{
		}
	});
}

$(document).ready(function(){
	
	 getAlarmData();
	 connectSocket();
});

var getColumnIndexByName = function(columnName) {
    var cm = $("#grid").jqGrid('getGridParam','colModel'),i=0,l=cm.length;
    for (; i<l; i++) {
        if (cm[i].name===columnName) {
            return i; // return the index
        }
    }
    return -1;
}

var disableIfChecked = function(elem){
    if ($(elem).is(':checked')) {
        $(elem).attr('disabled',true);
    }
}

var clearCurrentAlarmById = function(id,severity)
{
	
	swal({
	      title: "Are you sure?",
	      text: "Are You Sure You Want To Delete Alarm!!",
	      icon: "warning",
	      buttons: [
	        'No, cancel it!',
	        'Yes, I am sure!'
	      ],
	      dangerMode: true,
	    }).then(function(isConfirm) {
	      if (isConfirm) {
	    	  $.ajax({
	  			type:"POST",
	  		    url:serviceUrls.clearCurrentAlarmById,
	  		    data:{"id":id,"severity":severity},
	  		    success:function(data)
	  			{
	  		    	if(Object.values(data)[0]=="success"){
	  		    		//updateCountOfSeverity(severity);
	  		    	}
	  			}
	  			});
	      } else {
	        swal("Cancelled", "Alarm Has Not Been Deleted", "error");
	      }
	    })
	    
}

var updateCountOfSeverity = function(severity){
	
	switch(severity){
		case 1:
			var curr_total = $('#total_alarms').text();
			var curr_info = $('#info_alarms').text();
			$('#total_alarms').text(--curr_total);
			$('#info_alarms').text(--curr_info);
			break;	
		case 2:
			var curr_total = $('#total_alarms').text();
			var curr_warn = $('#warning_alarms').text();
			$('#total_alarms').text(--curr_total);
			$('#warning_alarms').text(--curr_warn);
			break;	
		case 3:
			var curr_total = $('#total_alarms').text();
			var curr_minor = $('#minor_alarms').text();
			$('#total_alarms').text(--curr_total);
			$('#minor_alarms').text(--curr_minor);
			break;	
		case 4:
			var curr_total = $('#total_alarms').text();
			var curr_major = $('#major_alarms').text();
			$('#total_alarms').text(--curr_total);
			$('#major_alarms').text(--curr_major);
			break;
		case 5:
			var curr_total = $('#total_alarms').text();
			var curr_critical = $('#critical_alarms').text();
			$('#total_alarms').text(--curr_total);
			$('#critical_alarms').text(--curr_critical);
			break;	
	}
}

var connectSocket = function()
{

    var stompClient = null;
    
    var socket = new SockJS(endPoint.socket);

    stompClient = Stomp.over(socket);
	stompClient.debug = null
    stompClient.connect({}, function (frame)
    {
			stompClient.subscribe(topics.fault, function (data) 
			{
				var msg = data.body;
				if(msg!=null){
					
					var msg=JSON.parse(msg);
					if(msg.status==null || msg.status==undefined){
						//location.reload();
						 getFaultAlarmData();
					}else{
						if(msg.status==true){
							$('#grid').jqGrid('delRowData',msg.id);
							updateCountOfSeverity(msg.severity);
						}
					}
				}
			});
     });
}

function setSeverityNameByValue (cellvalue, options, rowObject)
{
   switch(cellvalue){
   case 1:
	   return "Info";
   case 2:
	   return "Warning";
   case 3:
	   return "Minor";
   case 4:
	   return "Major";
   case 5:
	   return "Critical";
   default :
	   
   }
}
function mapManagedObjectType (cellvalue, options, rowObject)
{
   switch(cellvalue){
   case 1:
	   return "BMS";
   case 2:
	   return "PT";
   case 3:
	   return "JM";
   case 4:
	   return "HUMMER";
	   
   default : 
	   return cellvalue;
	   
   }
}

function mapComponentType (cellvalue, options, rowObject)
{
   switch(cellvalue){
   
   case 31:
	   return "SPS";
   case 1:
	   return "BMS";
	   
   default : 
	   return cellvalue;
	   
   }
}