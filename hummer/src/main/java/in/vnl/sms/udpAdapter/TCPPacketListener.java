package in.vnl.sms.udpAdapter;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import in.vnl.sms.model.ConfigData;
import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.TriggerData;
import in.vnl.sms.service.DBDataService;

//THIS CLASS IS NOT USED AS OF NOW. ITS PARTIALLY IMPLEMENTED..

@Component
@PropertySource("classpath:application.properties")
public class TCPPacketListener {
	
	private ArrayList<String> serverAddresses = new ArrayList<String>();
	//private ArrayList<String> triggerThNamesList = new ArrayList<String>();
	//private ArrayList<String> configThNamesList = new ArrayList<String>();
	public static HashMap<String, Boolean> existingState = new HashMap<String, Boolean>();
	
	//ExecutorService triggerThExecutor = Executors.newFixedThreadPool(10);
	//ExecutorService configThExecutor = Executors.newFixedThreadPool(10);
	@Autowired
	public DBDataService dbserv;
	
	@Value("${udp_port}")
	int udp_port;
	@Value("${tcp_server_port}")
	int tcp_server_port;
	@Value("${heartbeat_interval}")
	int heartbeat_interval;
	@Value("${retry_interval}")
	int retry_interval;
	@Value("${retries}")
	int retries;
	
	ServerSocket serverSocket;
	Socket triggerSocket, configSocket;
	byte[] receiveData;
	byte[] sendData;
	
/*	public PacketListener(int port) throws IOException
	{
		//dbserv.checkDependency();
		//PENDING-->INPUT SOCKET FROM PROPERTY FILE
		serverSocket = new DatagramSocket(udp_port);
	    receiveData = new byte[1024];
	    sendData = new byte[1024];
	    //service();
	}*/
	
	public TCPPacketListener() throws IOException  {
		//this(3004);		
	
	}
	
	public ServerSocket getSocketInstance()
	{
		return serverSocket;
	}
	/*public void createSocketConnection() {
		try {
			serverSocket = new DatagramSocket(3001);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    receiveData = new byte[1024];
	    sendData = new byte[1024];
	}*/
    
    public void service() throws IOException
    {
    	dbserv.updateIs_ActiveFalse();

    	System.out.println("PacketListener: Going to listen on port..."+tcp_server_port);
    	String sentence;
    	serverSocket = new ServerSocket(tcp_server_port);
	    
    	while(true)
        {
    		
    		  Socket receivePacket = serverSocket.accept();
    		  BufferedReader inFromClient = new BufferedReader(new InputStreamReader(receivePacket.getInputStream()));
    		  DataOutputStream outToClient = new DataOutputStream(receivePacket.getOutputStream());
    		  sentence = inFromClient.readLine();
    			System.out.println("Received: " + sentence);
    				                       
              if(sentence.contains("SMSHeartBeat"))
              {
            	  if(serverAddresses.contains(receivePacket.getInetAddress().getHostAddress()))
            	  {
            		  //get the running thread and update status
            		  Thread serverHB;
            		  if(sentence.contains("SMSHeartBeat,Calibrat") || sentence.contains("SMSHeartBeat,Monitoring"))
            		  {
            			  for (Thread t : Thread.getAllStackTraces().keySet()) {
                			  if (t.getName().equals(receivePacket.getInetAddress().getHostAddress())) 
                			  {
                				  //boolean threadState = existingState.get(t.getName());

                				  t.interrupt();
                				  
                				  if(sentence.contains("SMSHeartBeat,Calibrat"))
                				  {
                					  String[] hbMessage = sentence.split(",");
                    				  dbserv.updateDeviceState(receivePacket.getInetAddress().getHostAddress(), "Calibrating");

                				  }else if(sentence.contains("SMSHeartBeat,Monitoring")){
                					  
                					  String[] hbMessage = sentence.split(",");
                    				  dbserv.updateDeviceState(receivePacket.getInetAddress().getHostAddress(), "Monitoring");
                			  	  }
                				  

                				 /* if(!threadState)
                				  {
                					  //PENDING---> Start SMS Trigger packet exchange
                					  Thread triggerThread = (new Thread(new TriggerThread(receivePacket.getAddress().toString())));
                					  //PENDING-->To make thread unique, give thread name acc to unit no present in udp packet...if multiple SMSTriger packet can be sent.
                					  triggerThread.setName("unit No.-?");
                					  triggerThNamesList.add("unit No.-?");
                					  triggerThread.start();
                				  }else {
                					  //Already in active state. Regular HB message
                				  }*/

                			  }else {
                				  //unable to find thread with that name. Remove it's name from list and create a new one.
                			  }
                		  }
            		  }else {
            			  //This is the case of error condition where HB is received again although the state is active.
            			  for (Thread t : Thread.getAllStackTraces().keySet()) {
                			  if (t.getName().equals(receivePacket.getInetAddress().getHostAddress())) 
                			  {
                				  boolean threadState = existingState.get(t.getName());

                				  t.interrupt();

                				  if(!threadState)
                				  {
                					  sendTriggerData(receivePacket.getInetAddress().getHostAddress(),null);
                				  }
                			  }else {
                				  //unable to find thread. Need to check this scenario
                			  }
                		  }
            		  }
            		  

            	  }else {		//CHANGE TEST::::::::: if(!(sentence.contains("SMSHeartBeat,Calibration") || sentence.contains("SMSHeartBeat,Monitoring"))){
            		  
            		  List<DeviceInfo> devices = dbserv.getDevicesList();
            		  List<String> allIPs = new ArrayList<String>();
            		  for(DeviceInfo device : devices)
            		  {
            			  allIPs.add(device.getIp_add());
            		  }
            		  
            		  if(allIPs.contains(receivePacket.getInetAddress().getHostAddress()))
            		  {
            			  serverAddresses.add(receivePacket.getInetAddress().getHostAddress());
                		  //start a new thread for this server IP for heartbeat
                		  
                		  if(sentence.contains("SMSHeartBeat,Calibrat") || sentence.contains("SMSHeartBeat,Monitoring")) {
                			  
                			  Thread serverHB = (new Thread(new HBThread(receivePacket.getInetAddress().getHostAddress(), dbserv, heartbeat_interval)));
                    		  serverHB.setName(receivePacket.getInetAddress().getHostAddress());
                    		  serverHB.start();
                			  
                		  }else if(sentence.contains("SMSHeartBeat,")) {
                			  String[] hbTimeTemp = sentence.split(",");
                    		  String hbStartTime = hbTimeTemp[1] +" " + (hbTimeTemp[2].split("#"))[0];
                    		  
                    		  HBThread hbObj = new HBThread();
                			  hbObj.setStartTime(hbStartTime);
                			  
                    		  Thread serverHB = (new Thread(new HBThread(receivePacket.getInetAddress().getHostAddress(), dbserv,hbStartTime, heartbeat_interval)));
                    		  serverHB.setName(receivePacket.getInetAddress().getHostAddress());
                    		  serverHB.start();
                			  
                		  }else if(sentence.contains("SMSHeartBeat")) {
                			  Thread serverHB = (new Thread(new HBThread(receivePacket.getInetAddress().getHostAddress(), dbserv, heartbeat_interval)));
                    		  serverHB.setName(receivePacket.getInetAddress().getHostAddress());
                    		  serverHB.start();
                		  }
                		  /*String[] hbTimeTemp = sentence.split(",");
                		  String hbStartTime = hbTimeTemp[1] +" " + (hbTimeTemp[2].split("#"))[0];*/
                		  
                		  //PENDING------> Make Thread pool
                		  /*Thread serverHB = (new Thread(new HBThread(receivePacket.getAddress().getHostAddress(), serverSocket, dbserv,hbStartTime, heartbeat_interval)));
                		  serverHB.setName(receivePacket.getAddress().getHostAddress());
                		  serverHB.start();*/
                		    
                		  sendTriggerData(receivePacket.getInetAddress().getHostAddress(),null);
            		  }
            		  
                	  
            	  }
              }/*else if(sentence.contains("SMSTriggerACK")){
            	  //Handling for Ack received for SMSTrigger
            	  for (Thread t : Thread.getAllStackTraces().keySet()) {
        			  //PENDING---> Find unit no in ACK and find that thread from its number.
            		  if (t.getName().equals(receivePacket.getInetAddress().getHostAddress() + "Trigger") && (!(t.getState().name().equals("TERMINATED")))) 
        			  {
            			  System.out.println("PacketListener: Received Trigger ACK");
            			  String[] hbTimeTemp = sentence.split(",");
                		  String hbStartTime = hbTimeTemp[1] +" " + (hbTimeTemp[2].split("#"))[0];
                		  
            			  HBThread hbObj = new HBThread();
            			  hbObj.setStartTime(hbStartTime);
        				  t.interrupt();
        				  
        				//When SMSTrigger response is received, send Config message.
                    	  Thread configThread = (new Thread(new ConfigThread(receivePacket.getInetAddress().getHostAddress(), serverSocket, dbserv,retry_interval, retries)));
                    	  configThread.setName(receivePacket.getInetAddress().getHostAddress() + "Config");
                    	  //configThNamesList.add(receivePacket.getAddress() + "Config");
                    	  configThread.start();
        			  }
            		  if(t.getName().equals(receivePacket.getInetAddress().getHostAddress() + "SaveUITrigger") && (!(t.getState().name().equals("TERMINATED"))))
            		  {
            			  System.out.println("PacketListener: SaveUITrigger ACK");
            			  t.interrupt();
            		  }
            		  
            	  }
            	  
              }*/else if(sentence.contains("SMSSettingsACK")){
            	  
    			  System.out.println("PacketListener: Received Config ACK");
            	  for (Thread t : Thread.getAllStackTraces().keySet()) {
            		  //PENDING---> Find unit no in ACK and find that thread from its number.
            		  if (t.getName().equals(receivePacket.getInetAddress().getHostAddress() + "Config") && (!(t.getState().name().equals("TERMINATED")))) 
            		  {
            			  dbserv.updateDeviceState(receivePacket.getInetAddress().getHostAddress(), "Running");
            			  t.interrupt();
            		  }
            		  if(t.getName().equals(receivePacket.getInetAddress().getHostAddress() + "SaveUIConfig") && (!(t.getState().name().equals("TERMINATED"))))
            		  {
            			  System.out.println("PacketListener: SaveUIConfig ACK");
            			  t.interrupt();
            		  }
            	  }
              }else if(sentence.contains("Power")){
                	//PENDING----> Handling for other udp packets to be done, if required
            	  String[] peakInfoValues = (sentence.split(";"))[1].split(",");
            	  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
					LocalDate localDate = LocalDate.now();
            	  //PENDING--> Need ot handle index out of bounds exception in case packet data is not received in proper structure. We shall ignore that packet.
            	  dbserv.recordPeakInfo(receivePacket.getInetAddress().getHostAddress(), peakInfoValues[0], peakInfoValues[1], localDate+" "+peakInfoValues[2], peakInfoValues[3], peakInfoValues[4]);

                }else if(sentence.contains("LEDON")){
                	//String[] ledInfoValue = sentence.split("\\\\");
              	  //dbserv.recordLEDInfo(receivePacket.getAddress().getHostAddress(), ledInfoValue[1].replaceAll("\\n", ""));
                	dbserv.recordLEDInfo(receivePacket.getInetAddress().getHostAddress());
                	//dbserv.sendLEDDataToSocket(receivePacket.getAddress().getHostAddress());

                }else {
                	//JUNK PACKET. DROP IT
                }
             
              
             /* InetAddress IPAddress = receivePacket.getAddress();
              int port = receivePacket.getPort();
              String capitalizedSentence = sentence.toUpperCase();
              sendData = capitalizedSentence.getBytes();
              DatagramPacket sendPacket =
              new DatagramPacket(sendData, sendData.length, IPAddress, port);
              serverSocket.send(sendPacket);*/
        }
    }
    
    public void sendTriggerData(String device_ip, ConfigData cData)
    {

    	try {
    		if(triggerSocket==null)
    		{
    			//We can make a map of all created trigger sockets when needs to be handled for multiple devices.
    			triggerSocket = new Socket(device_ip, 252);
    		}
    		TriggerData tData = dbserv.getTriggerData(device_ip);
    		byte[] message = ("SMSTrigger,unit_no:"+tData.getUnit_no() + "\n" +
    				" product_name:"+tData.getProduct_name()+"\n" + 
    				" model:"+tData.getModel()+"\n" + 
    				" mfg_date:"+tData.getMfg_date()+"\n" + 
    				" client:"+tData.getClient()+"\n" + 
    				" location:"+tData.getLocation()+"\n" + 
    				" country:091" + "\n" +
    				"").getBytes();

    		System.out.println("Sending Trigger Data to server:");
    		triggerSocket.getOutputStream().write(message);
    		System.out.println("Trigger Data Sent.");
    		
    		byte[] resp = new byte[1000];
			
			int bytesRead = -1;
			do {
				bytesRead = triggerSocket.getInputStream().read(resp);
			} while(bytesRead == -1);
			
			System.out.println("Received " + bytesRead + " bytes from server");

    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}

    	//Check for Trigger ACK and send configData
    	sendConfigData(device_ip, cData);
    }
    
    public void sendConfigData(String device_ip, ConfigData cdata)
    {   	
    	try {
    		if(configSocket==null)
    		{		
    			configSocket = new Socket(device_ip, 130);

    			System.out.println("ConfigThread: Sending Config Data");

    			if(cdata==null)
    			{
    				System.out.println("ConfigThread: CData is null........................");
    				//add changes from ConfigThread class
    				//cdata = dbserv.getConfigData(device_ip);
    			}

    			byte[] message = ("\n" + "floor:"+cdata.getFloor()+"\n" + 
    					"room:"+cdata.getRoom()+"\n" + 
    					"calibration:"+cdata.getCalibration()+"\n" + 
    					"start_freq:"+cdata.getStart_freq()+"\n" + 
    					"stop_freq:"+cdata.getStop_freq()+"\n" + 
    					"threshold:"+cdata.getThreshold()+"\n" + 
    					"mask_offset:"+cdata.getMask_offset()+"\n" + 
    					"use_mask:"+cdata.getUse_mask()+"\n" + 
    					"start_time:"+cdata.getStart_time()+"\n" + 
    					"stop_time:"+cdata.getStop_time()+"\n" + 
    					"cable_length:"+cdata.getCable_length()+"\n" + 
    					"preamp_type:"+cdata.getPreamp_type()+"\n" + 
    					"gsm_dl:"+cdata.getGsm_dl()+"\n" + 
    					"wcdma_dl:"+cdata.getWcdma_dl()+"\n" + 
    					"wifi_band:"+cdata.getWifi_band()+"\n" + 
    					"lte_dl:"+cdata.getLte_dl()+"\n" + 
    					"band_start1:"+cdata.getBand_start1()+"\n" + 
    					"band_stop1:"+cdata.getBand_stop1()+"\n" + 
    					"band_en1:"+cdata.getBand_en1()+"\n" + 
    					"band_start2:"+cdata.getBand_start2()+"\n" + 
    					"band_stop2:"+cdata.getBand_stop2()+"\n" + 
    					"band_en2:"+cdata.getBand_en2()+"\n" + 
    					"band_start3:"+cdata.getBand_start3()+"\n" + 
    					"band_stop3:"+cdata.getBand_stop3()+"\n" + 
    					"band_en3:"+cdata.getBand_en3()+"\n" + 
    					"band_start4:"+cdata.getBand_start4()+"\n" + 
    					"band_stop4:"+cdata.getBand_stop4()+"\n" + 
    					"band_en4:"+cdata.getBand_en4()+"\n" + 
    					"band_start5:"+cdata.getBand_start5()+"\n" + 
    					"band_stop5:"+cdata.getBand_stop5()+"\n" +
    					//"band_en5:"+cdata.getBand_en5()+"\r\n" + 
    					"band_en5:"+cdata.getBand_en5()+ 
    					"").getBytes();

    			System.out.println("Sending Trigger Data to server:");
    			configSocket.getOutputStream().write(message);
    			System.out.println("Trigger Data Sent.");

    			byte[] resp = new byte[1000];

    			int bytesRead = -1;
    			do {
    				bytesRead = configSocket.getInputStream().read(resp);
    			} while(bytesRead == -1);

    			System.out.println("Received " + bytesRead + " bytes from server");
    		}
    	} catch (IOException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }
   /* public static void main(String[] args) {
		PacketListener pl;
		try {
			pl = new PacketListener(2000);
			pl.service();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}
