package in.vnl.sms.udpAdapter;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.vnl.sms.model.PeakDetectionInfo;
import in.vnl.sms.service.DBDataService;

public class PeakDataThread implements Runnable{

	DBDataService dbserv;
	static BlockingQueue<String> queue;
	Logger logger = LoggerFactory.getLogger(PeakDataThread.class);
	//String time = new String("");
	//int co=0;
	
	public PeakDataThread()
	{
		
	}
	
	public PeakDataThread(DBDataService dbserv)
	{
		queue = new LinkedBlockingQueue<String>();
		this.dbserv = dbserv;
	}
	
	@Override
	public void run() {
		
		
		try {
			String path;
			String sentence;
			String destinationIP;
			while(true)
			{
				List<PeakDetectionInfo> peakDataList = new ArrayList<PeakDetectionInfo>();
				boolean flag = false;
				
				if((path = queue.take()) != null )
				{
					ArrayList<String> multipleEntries = new ArrayList<String>();
					multipleEntries.add(path);
					
					if(queue.size() >10)
					{
						queue.drainTo(multipleEntries);
					}
					//System.out.println("QUEUE DRAINED size "+multipleEntries.size());
					for(int i=0; i < multipleEntries.size(); i++)
					{
						Instant start = Instant.now();
						
						String temp[] =  multipleEntries.get(i).split("=");
						sentence = temp[0].trim();
						destinationIP = temp[1].trim();
						
						if(sentence.contains("PTZAngle")) {
							PTZPacketSender.setCurrentAngle(Integer.parseInt(destinationIP));
						}
						else {
							//co++;
							try {
								PeakDetectionInfo peakData = null;
								boolean isPTZAntennaData = false;
								//List<PeakDetectionInfo> peakDataList = new ArrayList<PeakDetectionInfo>();
								String[] sentenceNEW = sentence.split("#");
								String[] peakPackets = sentenceNEW[0].split(";");
								for(int index=1; index <= peakPackets.length-1; index++)
								{
									String[] peakInfoValues = peakPackets[index].split(",");
									/*if(co>5) {//!time.equals(peakInfoValues[2])) {
										//logger.warn("prev "+time);
										time = peakInfoValues[2];
										//logger.warn("new "+time);
										flag = true;
									}*/
									LocalDate localDate = LocalDate.now();
									//PENDING--> Need ot handle index out of bounds exception in case packet data is not received in proper structure. We shall ignore that packet.
									if(peakInfoValues.length == 5 && peakInfoValues[4].equals("0"))
									{
										if(peakInfoValues.length < 4)
										{
											peakData = dbserv.recordPeakInfo(destinationIP, peakInfoValues[0], peakInfoValues[1], localDate+" "+peakInfoValues[2], 100+"",null);
											isPTZAntennaData=false;
											//logger.debug("PeakDataThread - "+"\tTime : "+peakInfoValues[2]+"\tFreq : "+peakInfoValues[1]+"\tPower : "+peakInfoValues[0]);
										}else if(DBDataService.rotatePTZ)
										{
											peakData = dbserv.recordPeakInfo(destinationIP, peakInfoValues[0], peakInfoValues[1], localDate+" "+peakInfoValues[2], peakInfoValues[3],null);
											isPTZAntennaData=true;
											//logger.debug("PeakDataThread - "+"\tTime : "+peakInfoValues[2]+"\tFreq : "+peakInfoValues[1]+"\tPower : "+peakInfoValues[0]);
										}
										
									}else {
										peakData = dbserv.recordPeakInfo(destinationIP, peakInfoValues[0], peakInfoValues[1], localDate+" "+peakInfoValues[2], peakInfoValues[3], peakInfoValues[4]);
										if(peakData!=null)
											peakDataList.add(peakData);
									}
									
									//sajal on 13-12-18
									/*if(peakData!=null)
										peakDataList.add(peakData);*/
									
									peakData=null;
								}
								if(isPTZAntennaData)
								{
									/*if(DBDataService.rotatePTZ)
									{*/if(peakDataList.size()>0 && flag) {
											dbserv.sendPeakDataToSocket(peakDataList);
											peakDataList.clear();
											flag = false;
											//co=0;
										}
									//}
								}else {
									if(peakDataList.size()>0) {				// && flag changed on 14-06-19
										dbserv.sendPeakDataToSocket(peakDataList);
										peakDataList.clear();
										flag = false;
										//co=0;
									}
								}
							}catch (Exception e) {
								logger.error(e.toString());
								e.printStackTrace();
								// TODO: handle exception
							}
						}
						
						Instant end = Instant.now();
						logger.warn("time : "+Duration.between(start, end).toMillis());
					}
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
			e.printStackTrace();
		}
	}
}
