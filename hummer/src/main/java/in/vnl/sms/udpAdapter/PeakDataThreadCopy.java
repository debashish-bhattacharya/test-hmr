package in.vnl.sms.udpAdapter;

import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import in.vnl.sms.model.PeakDetectionInfo;
import in.vnl.sms.service.DBDataService;
import in.vnl.sms.service.DatabaseService;

public class PeakDataThreadCopy implements Runnable{

	DBDataService dbserv;
	DatabaseService databaseSevice;
	static BlockingQueue<String> queue;
	Logger logger = LoggerFactory.getLogger(PeakDataThreadCopy.class);
	
	public PeakDataThreadCopy()
	{
		
	}
	
	public PeakDataThreadCopy(DBDataService dbserv, DatabaseService databaseService)
	{
		queue = new LinkedBlockingQueue<String>();
		this.dbserv = dbserv;
		this.databaseSevice = databaseService;
	}
	
	@Override
	public void run() {
		
		
		try {
			String path;
			String sentence;
			String destinationIP;
			while(true)
			{
				List<PeakDetectionInfo> peakDataList = new ArrayList<PeakDetectionInfo>();
				
				if((path = queue.take()) != null )
				{
					ArrayList<String> multipleEntries = new ArrayList<String>();
					multipleEntries.add(path);
					
					if(queue.size() >10)
					{
						queue.drainTo(multipleEntries);
					}
					logger.debug("QUEUE DRAINED size "+multipleEntries.size());
					for(int i=0; i < multipleEntries.size(); i++)
					{
						Instant start = Instant.now();
						int length = 0;
						
						String temp[] =  multipleEntries.get(i).split("=");
						sentence = temp[0].trim();
						destinationIP = temp[1].trim();
						
						try {
							
							String[] sentenceNEW = sentence.split("#");
							
							List<String> peakPackets = Arrays.asList(sentenceNEW[0].split(";"));
							
							length = peakPackets.size();
							
							int scale = length/3;
							
							ForkJoinPool pool = new ForkJoinPool();
							PeakForkThread peakForkThread1, peakForkThread2, peakForkThread3, peakForkThread4, peakForkThread5, peakForkThread6;
							
							if(length<30) {
								peakForkThread1 = new PeakForkThread(peakPackets.subList(1, length), destinationIP, dbserv);
								
								pool.execute(peakForkThread1);
							    
							    do
							    {
							         
							    } while (!peakForkThread1.isDone());
								
							    //Shut down ForkJoinPool using the shutdown() method.
							    pool.shutdown();
							    
							    peakDataList.addAll(peakForkThread1.join());
							}
							else {		//if(DBDataService.rotatePTZ)
								scale = length/6;
								peakForkThread1 = new PeakForkThread(peakPackets.subList(1, scale), destinationIP, dbserv);
								peakForkThread2 = new PeakForkThread(peakPackets.subList(scale, 2*scale), destinationIP, dbserv);
								peakForkThread3 = new PeakForkThread(peakPackets.subList(2*scale, 3*scale), destinationIP, dbserv);
								peakForkThread4 = new PeakForkThread(peakPackets.subList(3*scale, 4*scale), destinationIP, dbserv);
								peakForkThread5 = new PeakForkThread(peakPackets.subList(4*scale, 5*scale), destinationIP, dbserv);
								peakForkThread6 = new PeakForkThread(peakPackets.subList(5*scale, length), destinationIP, dbserv);
							      
							    pool.execute(peakForkThread1);
							    pool.execute(peakForkThread2);
							    pool.execute(peakForkThread3);
							    pool.execute(peakForkThread4);
							    pool.execute(peakForkThread5);
							    pool.execute(peakForkThread6);
							    
							    do
							    {
							         
							    } while ((!peakForkThread1.isDone()) || (!peakForkThread2.isDone()) || (!peakForkThread3.isDone()) ||
							    		(!peakForkThread4.isDone()) || (!peakForkThread5.isDone()) || (!peakForkThread6.isDone()));
								
							    //Shut down ForkJoinPool using the shutdown() method.
							    pool.shutdown();
							    
							    peakDataList.addAll(peakForkThread1.join());
							    peakDataList.addAll(peakForkThread2.join());
							    peakDataList.addAll(peakForkThread3.join());
							    peakDataList.addAll(peakForkThread4.join());
							    peakDataList.addAll(peakForkThread5.join());
							    peakDataList.addAll(peakForkThread6.join());
							}
//							else {
//								peakForkThread1 = new PeakForkThread(peakPackets.subList(1, scale), destinationIP, dbserv);
//								peakForkThread2 = new PeakForkThread(peakPackets.subList(scale, 2*scale), destinationIP, dbserv);
//								peakForkThread3 = new PeakForkThread(peakPackets.subList(2*scale, length), destinationIP, dbserv);
//							      
//							    pool.execute(peakForkThread1);
//							    pool.execute(peakForkThread2);
//							    pool.execute(peakForkThread3);
//							    
//							    do
//							    {
//							         
//							    } while ((!peakForkThread1.isDone()) || (!peakForkThread2.isDone()) || (!peakForkThread3.isDone()));
//								
//							    //Shut down ForkJoinPool using the shutdown() method.
//							    pool.shutdown();
//							    
//							    peakDataList.addAll(peakForkThread1.join());
//							    peakDataList.addAll(peakForkThread2.join());
//							    peakDataList.addAll(peakForkThread3.join());
//							}
						    
							peakForkThread1 = null;
							peakForkThread2 = null;
							peakForkThread3 = null;
							peakForkThread4 = null;
							peakForkThread5 = null;
							peakForkThread6 = null;
							//if (!(dbserv.Opr_mode.equals("4")&&!dbserv.env_record_save)){
								saveToDb(peakDataList);
							//}
							if(peakDataList.size()>0) {				// && flag changed on 14-06-19
								dbserv.sendPeakDataToSocket(peakDataList);
								peakDataList.clear();
							}
							
						}catch (Exception e) {
							logger.error(e.toString());
							e.printStackTrace();
							// TODO: handle exception
						}
						
						Instant end = Instant.now();
						logger.debug("Peak Data Thread Copy  size : "+ length+" time : "+Duration.between(start, end).toMillis());
					}
				}
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error(e.toString());
			e.printStackTrace();
		}
	}
	
	public void saveToDb(List<PeakDetectionInfo> data) {
		List<String> recordPeak = new ArrayList<String>();
		for(PeakDetectionInfo pinfo : data) {
			String nul = "NULL";
			if(!pinfo.getFrequency().equalsIgnoreCase("NaN")) {
			String query="insert into peak_info (ip_add,power,frequency,time,device_id,angle,currentconfigid,antennaid,alarm,jmdevice_id,type,sector,technology,lat,lon) values ("+
					"'"+pinfo.getIp_add()+"','"+pinfo.getPower()+"','"+pinfo.getFrequency()+"','"+pinfo.getTime()+"',"+pinfo.getDevice_id().getId()+",'"+pinfo.getAngle()+"',"+pinfo.getCurrentconfigid()
					+",'"+pinfo.getAntennaid()+"',"+pinfo.getAlarm()+",";
			
			if(pinfo.getJmdevice_id()==null) {
				query=query+nul;
			}
			else {
				query = query+pinfo.getJmdevice_id().getId();
			}
			
			query = query + ",'"+pinfo.getType()+"','"+pinfo.getSector()+"','"+pinfo.getTechnology()+
					"','"+pinfo.getLat()+"','"+pinfo.getLon()+"')";
			
			//recordPeak.add(pinfo);
			
			recordPeak.add(query);
  	  	}
			
		}
		try {
			databaseSevice.executeBatchInsertion(recordPeak);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}
}
