package in.vnl.sms.udpAdapter;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import in.vnl.sms.service.DBDataService;

@Component
@PropertySource("classpath:application.properties")
public class PTZPacketSender{

	@Value("${ptz_tcp_port}")
	int ptz_tcp_port;
	@Value("${DGPS_notConnectedCheck}")
	int DGPS_notConnectedCheck;
	private static PTZPacketSender single_instance = null;
	public static int current_angle = 361; // Compass Angle will vary between 0 to 360
	public static int current_tilt = 361;
	public static int prev_angle = -1;
	public static int  scanVal = 1;
	public static String tilt_position = null;
	Socket client;
	/*int port;
	String serverName;
	int direction;
	int speed;
	int target;*/
	
	static Logger logger = LoggerFactory.getLogger(PTZPacketSender.class); 
	
	private PTZPacketSender() {
		// TODO Auto-generated constructor stub
	}

	public int getLastAngle() throws IOException
    {
        if (current_angle == 361)
        {
        	return (getCurrentAngle("",ptz_tcp_port));
        }

        return current_angle;
    }
	
	public static PTZPacketSender getInstance()
    {
        if (single_instance == null)
            single_instance = new PTZPacketSender();

        return single_instance;
    }
	
	public static void setCurrentAngle(int currentAngle)
	{
		if(PTZPacketSender.prev_angle==361)
		{
			PTZPacketSender.prev_angle=currentAngle;
		}else {
			
			if(currentAngle >= PTZPacketSender.prev_angle && PTZPacketSender.scanVal%2 == 1)
			{
				PTZPacketSender.prev_angle = currentAngle;
			}else if(currentAngle < PTZPacketSender.prev_angle && PTZPacketSender.scanVal%2 == 1)
			{
				PTZPacketSender.prev_angle = currentAngle;
				PTZPacketSender.scanVal++;
			}else if(currentAngle <= PTZPacketSender.prev_angle && PTZPacketSender.scanVal %2 == 0)
			{
				PTZPacketSender.prev_angle = currentAngle;
			}else if(currentAngle > PTZPacketSender.prev_angle && PTZPacketSender.scanVal%2 == 0)
			{
				PTZPacketSender.prev_angle = currentAngle;
				PTZPacketSender.scanVal++;
			}
		}
		//logger.warn("PTZ Angle : "+currentAngle);
		PTZPacketSender.current_angle=currentAngle;
	}
	
	
	
	/*public PTZPacketSender(String serverName, int port, int direction, int speed, int target) throws IOException{
	    this.port = port;
	    this.port=port;
	    this.direction=direction;
	    this.speed=speed;
	    this.target=target;
	}*/
	
	/*
	 * For Up/Down/Left/Right operation only
	 */
	public void sendPacket(String serverName, int port, int direction, int speed, int target) throws UnknownHostException, IOException {
		
		int tryConnect=1;
		while(tryConnect <3)
		{
			DataOutputStream out = null;
			try {
				if(client==null)
				{
					System.out.println("PTZPacketSender: Connecting to " + serverName + " on port " + port);
					client = new Socket(serverName, port);
				}
				OutputStream outToServer = client.getOutputStream();
				out = new DataOutputStream(outToServer);
				
				ByteBuffer bb;
				if(direction==28)
					bb = ByteBuffer.allocate(11);
				else 
					bb = ByteBuffer.allocate(7);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				/*if(direction==10)
				{
					bb.putShort((short)16);
				}else {
					bb.putShort((short)direction);
				}
				bb.putShort((short)101);
				bb.putFloat((float)10);
				bb.putFloat((float)target);
				bb.put((byte)0x0A);*/
				bb.putShort((short)direction);
				bb.put((byte)18);
				bb.putFloat((float)target);
				
				if(direction==28)
					bb.putFloat((float)target);
				
				out.write(bb.array());
				out.close();
				client.close();
				client = null;
				
				tryConnect++;
				tryConnect++;
				/*InputStream inFromServer = client.getInputStream();
				DataInputStream in = new DataInputStream(inFromServer);

				System.out.println("Server says " + in.readUTF());*/
				
				
			} catch (IOException e) {
				if(tryConnect<2)
				{
					System.out.println("PTZPacketSender : Trying to set one more time after 2 min");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					client = new Socket(serverName, port);
					tryConnect++;
				}else {
					tryConnect++;
					tryConnect++;
					e.printStackTrace();
					out.close();
					client = null;
					throw e;
				}
				
			}
		}
		
	}
	
	//Pan or Tilt
	public void jumpToPositionPacket(String serverName, int port, int goToPosition, int reportingTime, boolean isPanCommand) throws UnknownHostException, IOException{
		logger.debug("enter in jumpToPositionPacket ");
		int tryConnect=1;
		while(tryConnect <3)
		{
			DataOutputStream out = null;
			try {
				if(client==null)
				{
					System.out.println("PTZPacketSender: Connecting to " + serverName + " on port " + port);
					client = new Socket(serverName, port);
				}
				logger.debug("tiltAngel is "+goToPosition);
				OutputStream outToServer = client.getOutputStream();
				out = new DataOutputStream(outToServer);
				logger.debug("PTZPacketSender: Sending Jump Command");

				System.out.println("PTZPacketSender: Sending Jump Command");
				
				ByteBuffer bb = ByteBuffer.allocate(12);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				
				bb.putShort((short)24);
				if(isPanCommand)
				{
					bb.put((byte)0);
				}else {
					bb.put((byte)1);
				}
				bb.put((byte)18);
				bb.putFloat((float)goToPosition);
				bb.putInt((int)reportingTime);
				
				out.write(bb.array());
				out.close();
				client.close();
				client = null;
				
				tryConnect++;
				tryConnect++;
				/*InputStream inFromServer = client.getInputStream();
				DataInputStream in = new DataInputStream(inFromServer);

				System.out.println("Server says " + in.readUTF());*/
				
				
			} catch (IOException e) {
				if(tryConnect<2)
				{
					System.out.println("PTZPacketSender : Trying to set one more time after 2 min");
					logger.debug("PTZPacketSender : Trying to set one more time after 2 min");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					client = new Socket(serverName, port);
					tryConnect++;
				}else {
					tryConnect++;
					tryConnect++;
					e.printStackTrace();
					out.close();
					client = null;
					throw e;
				}
				
			}
		}
		
	}
	
public void scanPacket(String serverName, int port, int startAngle, int stopAngle, int deltaAngle, int holdTime, boolean isReverse, int reportingTime, boolean isPanCommand) throws UnknownHostException, IOException{
		
		int tryConnect=1;
		while(tryConnect <3)
		{
			DataOutputStream out = null;
			try {
				if(client==null)
				{
					System.out.println("PTZPacketSender: Connecting to " + serverName + " on port " + port);
					client = new Socket(serverName, port);
				}
				OutputStream outToServer = client.getOutputStream();
				out = new DataOutputStream(outToServer);
								
				ByteBuffer bb = ByteBuffer.allocate(25);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				
				bb.putShort((short)23);
				//For testing tilt scan, uncomment below line
				//isPanCommand=false;
				if(isPanCommand)
				{
					bb.put((byte)0);
				}else {
					bb.put((byte)1);
				}
				bb.put((byte)18);
				bb.putFloat((float)startAngle);
				bb.putFloat((float)stopAngle);
				bb.putFloat((float)deltaAngle);
				bb.putInt(holdTime);
				if(isReverse)
				{
					bb.put((byte)1);
				}else {
					bb.put((byte)0);
				}
				bb.putInt((int)reportingTime);
				
				out.write(bb.array());
				
				tryConnect++;
				tryConnect++;
				/*InputStream inFromServer = client.getInputStream();
				DataInputStream in = new DataInputStream(inFromServer);

				System.out.println("Server says " + in.readUTF());*/
				
				
			} catch (IOException e) {
				if(tryConnect<2)
				{
					System.out.println("PTZPacketSender : Trying to set one more time after 2 min");
					try {
						Thread.sleep(120000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					client = new Socket(serverName, port);
					tryConnect++;
				}else {
					tryConnect++;
					tryConnect++;
					e.printStackTrace();
					out.close();
					throw e;
				}
				
			}
		}
		
	}



public void scanPacketWithRotation(String serverName, int port, int startAngle, int stopAngle, int deltaAngle, int holdTime, int noOfRotation, int reportingTime, boolean isPanCommand) throws UnknownHostException, IOException{
	logger.debug("enter in scanPacketWithRotation");
	int tryConnect=1;
	
	while(tryConnect <3)
	{
		DataOutputStream out = null;
		try {
			if(client==null)
			{
				System.out.println("PTZPacketSender: Connecting to " + serverName + " on port " + port);
				client = new Socket(serverName, port);
			}
			logger.debug("start angle to send ptz  "+startAngle);
			logger.debug("stop angle to send ptz  "+stopAngle);
			OutputStream outToServer = client.getOutputStream();
			out = new DataOutputStream(outToServer);
							
			ByteBuffer bb = ByteBuffer.allocate(28);
			bb.order(ByteOrder.LITTLE_ENDIAN);
			
			bb.putShort((short)23);
			//For testing tilt scan, uncomment below line
			//isPanCommand=false;
			if(isPanCommand)
			{
				bb.put((byte)0);
			}else {
				bb.put((byte)1);
			}
			bb.put((byte)18);
			bb.putFloat((float)startAngle);
			bb.putFloat((float)stopAngle);
			bb.putFloat((float)deltaAngle);
			bb.putInt(holdTime);
			//noOfRotation = 4; // To be decided
			bb.putInt(noOfRotation);
			bb.putInt((int)reportingTime);
			
			out.write(bb.array());
			out.close();
			client.close();
			client = null;
			
			tryConnect++;
			tryConnect++;
			/*InputStream inFromServer = client.getInputStream();
			DataInputStream in = new DataInputStream(inFromServer);

			System.out.println("Server says " + in.readUTF());*/
			
			
		} catch (IOException e) {
			if(tryConnect<2)
			{
				logger.debug("PTZPacketSender : Trying to set one more time after 2 min");
				System.out.println("PTZPacketSender : Trying to set one more time after 2 min");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				client = new Socket(serverName, port);
				tryConnect++;
			}else {
				tryConnect++;
				tryConnect++;
				e.printStackTrace();
				out.close();
				client = null;
				throw e;
			}
			
		}
	}
	
}


	
	public static short reverse(short x) {
	    ByteBuffer bbuf = ByteBuffer.allocate(2);
	    bbuf.order(ByteOrder.BIG_ENDIAN);
	    bbuf.putShort(x);
	    bbuf.order(ByteOrder.LITTLE_ENDIAN);
	    return bbuf.getShort(0);
	}
	
	public static float reverse(float x) {
	    ByteBuffer bbuf = ByteBuffer.allocate(8);
	    bbuf.order(ByteOrder.BIG_ENDIAN);
	    bbuf.putFloat(x);
	    bbuf.order(ByteOrder.LITTLE_ENDIAN);
	    return bbuf.getFloat(0);
	}
	
	public int getCurrentAngle(String serverName, int port) throws IOException
	{
		int tryConnect=1;
		while(tryConnect <3)
		{
			OutputStream outToServer;
			DataOutputStream out = null;
			try {
				if(client==null)
				{
					System.out.println("PTZPacketSender: Connecting to " + serverName + " on port " + port);
					client = new Socket(serverName, port);
				}

				outToServer = client.getOutputStream();
				out = new DataOutputStream(outToServer);


				ByteBuffer bb = ByteBuffer.allocate(2);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				bb.putShort((short)10);
				//bb.put((byte)0x0A);

				out.write(bb.array());

				
				byte[] resp = new byte[10];
				
				int bytesRead = -1;
				do {
					bytesRead = client.getInputStream().read(resp);
				} while(bytesRead == -1);
				
				System.out.println("PTZPacketSender: getCurrentAngle: Received " + bytesRead + " bytes from server");
				System.out.println("PTZPacketSender: " + resp[2]+"   "+ resp[3]);
				
				String angleInHex = String.format("%02X ", resp[5]).trim()+String.format("%02X ", resp[4]).trim()+String.format("%02X ", resp[3]).trim()+String.format("%02X ", resp[2]).trim();
				Long i = Long.parseLong(angleInHex, 16);
		        Float angle = Float.intBitsToFloat(i.intValue());
		        System.out.println("PTZPacketSender: Angle rec : "+ angle);
		        
		        tryConnect++;
		        tryConnect++;
		        
		        out.close();
				client.close();
				client = null;
				
				current_angle = Math.round(angle);
				if(current_angle <0)
				{
					current_angle = 0;
				}
		        return current_angle;
		        
			}catch (IOException e) {
				if(tryConnect<2)
				{
					System.out.println("PTZPacketSender : Trying to get one more time after 2 min");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					client = new Socket(serverName, port);
					tryConnect++;
				}else {
					tryConnect++;
					tryConnect++;
					e.printStackTrace();
					out.close();
					client = null;
					throw e;
				}
				
			}
		}
		return current_angle;
		
	}
	
	
	public int getCurrentTilt(String serverName, int port) throws IOException
	{
		int tryConnect=1;
		while(tryConnect <3)
		{
			OutputStream outToServer;
			DataOutputStream out = null;
			try {
				if(client==null)
				{
					System.out.println("PTZPacketSender: Connecting to " + serverName + " on port " + port);
					client = new Socket(serverName, port);
				}

				outToServer = client.getOutputStream();
				out = new DataOutputStream(outToServer);


				ByteBuffer bb = ByteBuffer.allocate(2);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				bb.putShort((short)10);
				//bb.put((byte)0x0A);

				out.write(bb.array());

				
				byte[] resp = new byte[10];
				
				int bytesRead = -1;
				do {
					bytesRead = client.getInputStream().read(resp);
				} while(bytesRead == -1);
				
				//System.out.println("Received " + bytesRead + " bytes from server");
				//System.out.println(resp[2]+"   "+ resp[3]);
				
				String tiltInHex = String.format("%02X ", resp[9]).trim()+String.format("%02X ", resp[8]).trim()+String.format("%02X ", resp[7]).trim()+String.format("%02X ", resp[6]).trim();
				Long i = Long.parseLong(tiltInHex, 16);
		        Float tilt = Float.intBitsToFloat(i.intValue());
		        System.out.println("PTZPacketSender: Tilt rec : "+ tilt);
		        
		        out.close();
				client.close();
				client = null;
				
		        tryConnect++;
		        tryConnect++;
				return Math.round(tilt);
		        
			}catch (IOException e) {
				if(tryConnect<2)
				{
					System.out.println("PTZPacketSender : Trying to get one more time after 2 min");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					client = new Socket(serverName, port);
					tryConnect++;
				}else {
					tryConnect++;
					tryConnect++;
					e.printStackTrace();
					out.close();
					client = null;
					throw e;
				}
				
			}
		}
		return current_tilt;
		
	}
	
public void setCurrentTime(String serverName, int port) throws UnknownHostException, IOException{
		
		int tryConnect=1;
		while(tryConnect <3)
		{
			DataOutputStream out = null;
			try {
				if(client==null)
				{
					System.out.println("PTZPacketSender: Connecting to " + serverName + " on port " + port);
					client = new Socket(serverName, port);
				}
				OutputStream outToServer = client.getOutputStream();
				out = new DataOutputStream(outToServer);
				
				System.out.println("PTZPacketSender: Sending Jump Command");
				
				ByteBuffer bb = ByteBuffer.allocate(6);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				
				bb.putShort((short)31);
				bb.putFloat((int)(System.currentTimeMillis()/1000));
				
				out.write(bb.array());
				out.close();
				client.close();
				client = null;
				
				tryConnect++;
				tryConnect++;				
				
			} catch (IOException e) {
				if(tryConnect<2)
				{
					System.out.println("PTZPacketSender : Trying to set one more time after 2 min");
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					client = new Socket(serverName, port);
					tryConnect++;
				}else {
					tryConnect++;
					tryConnect++;
					e.printStackTrace();
					out.close();
					client = null;
					throw e;
				}
				
			}
		}
		
	}

public float[] sendGPSCommand(String serverName, int port, boolean sync, boolean syncValue) throws UnknownHostException, IOException{
	
	int tryConnect=1;
	
	float values[] = null;
	
	while(tryConnect <3)
	{
		DataOutputStream out = null;
		try {
			if(client==null)
			{
				System.out.println("PTZPacketSender: Connecting to " + serverName + " on port " + port);
				client = new Socket(serverName, port);
			}
			OutputStream outToServer = client.getOutputStream();
			out = new DataOutputStream(outToServer);
			
			//System.out.println("PTZPacketSender: Sending stop scan Command");
			
			ByteBuffer bb;
			
			if(sync) {
				bb = ByteBuffer.allocate(3);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				
				bb.putShort((short)42);
				bb.put((byte)(syncValue?1:0));
				
				out.write(bb.array());
			}
			else {
				bb = ByteBuffer.allocate(2);
				bb.order(ByteOrder.LITTLE_ENDIAN);
				
				bb.putShort((short)41);	
				
				out.write(bb.array());
				
				byte[] resp = new byte[36];
				
				int bytesRead = -1;
				do {
					bytesRead = client.getInputStream().read(resp);
				} while(bytesRead == -1);
				
				System.out.println("Received " + bytesRead + " bytes from server");
				//System.out.println(resp[2]+"   "+ resp[3]);
				
				String command = String.format("%02X ", resp[6]).trim();
				Long enable = Long.parseLong(command, 16);
				//disscuss with ptz team to bypass device not connected check
				if(DGPS_notConnectedCheck!=1) {
				enable= (long) 1;
				}
				command = String.format("%02X ", resp[7]).trim();
				Long valid = Long.parseLong(command, 16);
				
				logger.info("valid or not "+valid);
				
				String value = "";
				Long  i;
				
				values = new float[6];
				
				values[4] = 0;
				values[5] = 0;
				
				if(enable == 1) {
					
					values[5] = 1;
					
					if(valid == 1) {
						
						values[4] = 1;
						value = String.format("%02X ", resp[11]).trim()+String.format("%02X ", resp[10]).trim()+String.format("%02X ", resp[9]).trim()+String.format("%02X ", resp[8]).trim();
						i = Long.parseLong(value, 16);
				        values[0] = Float.intBitsToFloat(i.intValue());
				        System.out.println("PTZPacketSender: Pan : "+ values[0]);
				        
				        value = String.format("%02X ", resp[27]).trim()+String.format("%02X ", resp[26]).trim()+String.format("%02X ", resp[25]).trim()+String.format("%02X ", resp[24]).trim();
						i = Long.parseLong(value, 16);
						values[1] = Float.intBitsToFloat(i.intValue());
				        System.out.println("PTZPacketSender: Yaw : "+ values[1]);
					}
				        
			        value = String.format("%02X ", resp[31]).trim()+String.format("%02X ", resp[30]).trim()+String.format("%02X ", resp[29]).trim()+String.format("%02X ", resp[28]).trim();
					i = Long.parseLong(value, 16);
					values[2] = Float.intBitsToFloat(i.intValue());
			        System.out.println("PTZPacketSender: Lat : "+ values[2]);
			        
			        value = String.format("%02X ", resp[35]).trim()+String.format("%02X ", resp[34]).trim()+String.format("%02X ", resp[33]).trim()+String.format("%02X ", resp[32]).trim();
					i = Long.parseLong(value, 16);
					values[3] = Float.intBitsToFloat(i.intValue());
			        System.out.println("PTZPacketSender: Lon : "+ values[3]);
				}
		        
			}
	        
			out.close();
			client.close();
			client = null;
			
			tryConnect++;
			tryConnect++;				
			
		} catch (IOException e) {
			if(tryConnect<2)
			{
				System.out.println("PTZPacketSender : Trying to set one more time after 2 min");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				client = new Socket(serverName, port);
				tryConnect++;
			}else {
				tryConnect++;
				tryConnect++;
				e.printStackTrace();
				out.close();
				client = null;
				//throw e;
			}
			
		}
	}
	
	return values;
}

public void stopPTZScan(String serverName, int port) throws UnknownHostException, IOException{
	
	int tryConnect=1;
	while(tryConnect <3)
	{
		DataOutputStream out = null;
		try {
			if(client==null)
			{
				System.out.println("PTZPacketSender: Connecting to " + serverName + " on port " + port);
				client = new Socket(serverName, port);
			}
			OutputStream outToServer = client.getOutputStream();
			out = new DataOutputStream(outToServer);
			
			System.out.println("PTZPacketSender: Sending stop scan Command");
			
			ByteBuffer bb = ByteBuffer.allocate(2);
			bb.order(ByteOrder.LITTLE_ENDIAN);
			
			bb.putShort((short)9);
			
			out.write(bb.array());
			out.close();
			client.close();
			client = null;
			
			tryConnect++;
			tryConnect++;				
			
		} catch (IOException e) {
			if(tryConnect<2)
			{
				System.out.println("PTZPacketSender : Trying to set one more time after 2 min");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				client = new Socket(serverName, port);
				tryConnect++;
			}else {
				tryConnect++;
				tryConnect++;
				e.printStackTrace();
				out.close();
				client = null;
				throw e;
			}
			
		}
	}
}

/*	@Override
	public void run() {
		System.out.println("Connecting to " + serverName + " on port " + port);
		try {
			client = new Socket(serverName, port);
			System.out.println("Just connected to " + client.getRemoteSocketAddress());

			OutputStream outToServer = client.getOutputStream();
			DataOutputStream out = new DataOutputStream(outToServer);
			
			out.writeShort(direction);
			out.writeInt(speed);
			out.writeInt(target);
			out.writeBytes("\n");
			
			out.writeShort(direction);
			out.writeInt(speed);
			out.writeInt(target);
			out.writeBytes("\n");
			
			//out.writeUTF("Hello from " + client.getLocalSocketAddress());
			InputStream inFromServer = client.getInputStream();
			DataInputStream in = new DataInputStream(inFromServer);

			System.out.println("Server says " + in.readUTF());
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}*/
	public static void main(String[] args) {
		try {
			//PTZPacketSender.getInstance().sendPacket("10.100.207.251",2000,2,10,4);
			PTZPacketSender.getInstance().getCurrentAngle("10.100.207.251",2000);
		}/* catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/catch(Exception e) {
			e.printStackTrace();
		}
	}
	 
}
