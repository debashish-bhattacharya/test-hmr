package in.vnl.sms.udpAdapter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import in.vnl.sms.model.PeakDetectionInfo;
import in.vnl.sms.service.DBDataService;

public class PeakForkThread extends RecursiveTask<List<PeakDetectionInfo>>{

	DBDataService dbserv;
	//This attribute will store the peaks this task is going to process.
	private List<String> peakList;
	private final String destinationIP;
	private LocalDate localDate = LocalDate.now();
	 
    //Implement the constructor of the class to initialize its attributes
    public PeakForkThread(List<String> peakList, String destinationIP, DBDataService dbserv)
    {
       this.destinationIP = destinationIP;
       this.peakList = peakList;
       this.dbserv = dbserv;
    }
	 
    //Implement the compute() method. As you parameterized the RecursiveTask class with the List<PeakDetectionInfo> type,
    //this method has to return an object of that type.
    @Override
    protected List<PeakDetectionInfo> compute()
    {
       //List to store the peaks objects returned.
       List<PeakDetectionInfo> list = new ArrayList<PeakDetectionInfo>();
       //PeakForkThread tasks to store the subtasks that are going to process the peaks
       List<PeakForkThread> tasks = new ArrayList<PeakForkThread>();
       
       while(true)
       {
          if (peakList.size()>10)
          {
             PeakForkThread task = new PeakForkThread(new ArrayList<String>(peakList.subList(0, 10)), destinationIP, dbserv);
             task.fork();
             tasks.add(task);
             peakList = peakList.subList(10, peakList.size());
          }
          else
          {
        	  for(String peakPackets : peakList) {
        		  String peakInfoValues[] = peakPackets.split(",");
	        	  PeakDetectionInfo peak = dbserv.recordPeakInfo(destinationIP, peakInfoValues[0], peakInfoValues[1], localDate+" "+peakInfoValues[2], peakInfoValues[3], peakInfoValues[4]);
	              list.add(peak);
        	  }
        	  break;
          }
       }

       //If the list of the PeakForkThread subtasks has more than 50 elements,
       //write a message to the console to indicate this circumstance.
       if (tasks.size() > 50)
       {
          System.out.printf("%d tasks ran.\n", tasks.size());
       }
       //add to the list of peaks the results returned by the subtasks launched by this task.
       addResultsFromTasks(list, tasks);
       //Return the list of peaks
       return list;
       
    }
	 
    //For each task stored in the list of tasks, call the join() method that will wait for its finalization and then will return the result of the task.
    //Add that result to the list of strings using the addAll() method.
    private void addResultsFromTasks(List<PeakDetectionInfo> list, List<PeakForkThread> tasks)
    {
       for (PeakForkThread item : tasks)
       {
          list.addAll(item.join());
       }
    }
	   
}
