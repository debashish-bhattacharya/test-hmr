package in.vnl.sms.authorization;

import in.vnl.sms.exceptions.auth.AuthenticationException;
import in.vnl.sms.exceptions.auth.AuthorizationException;

public interface CheckAuthorization {

	public void checkAuthentication() throws AuthenticationException;
	public boolean checkAuthorization(String[] roles) throws AuthorizationException;
}
