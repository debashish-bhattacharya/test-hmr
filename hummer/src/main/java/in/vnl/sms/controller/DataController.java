package in.vnl.sms.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.vnl.sms.model.Alarm;
import in.vnl.sms.model.AlarmInfo;
import in.vnl.sms.model.AlarmsData;
import in.vnl.sms.model.AuditLog;
import in.vnl.sms.model.ColorStripsData;
import in.vnl.sms.model.Commands;
import in.vnl.sms.model.ConfigData;
import in.vnl.sms.model.ConfigProfileData;
import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.Devices;
import in.vnl.sms.model.Event;
import in.vnl.sms.model.FreqPowMinThresData;
import in.vnl.sms.model.GpsData;
import in.vnl.sms.model.JMDeviceData;
import in.vnl.sms.model.JMNodeMapping;
import in.vnl.sms.model.LEDData;
import in.vnl.sms.model.MaskingScreenData;
import in.vnl.sms.model.NodeData;
import in.vnl.sms.model.NodesInfo;
import in.vnl.sms.model.PTZData;
import in.vnl.sms.model.PeakDetectionInfo;
import in.vnl.sms.model.PeakInfoPojo;
import in.vnl.sms.model.PeakJMData;
import in.vnl.sms.model.Priority;
import in.vnl.sms.model.SensorConfig;
import in.vnl.sms.model.SystemConfigurationData;
import in.vnl.sms.model.TaskPriority;
import in.vnl.sms.model.TriggerData;
import in.vnl.sms.model.UserEntity;
import in.vnl.sms.model.UserSetting;
import in.vnl.sms.report.ReportServer;
import in.vnl.sms.repository.DevicesRepository;
import in.vnl.sms.repository.MaskingScreenRepository;
import in.vnl.sms.repository.SystemConfigurationRepository;
import in.vnl.sms.service.AlarmsService;
import in.vnl.sms.service.AutoBackupService;
import in.vnl.sms.service.BmsConfigService;
import in.vnl.sms.service.CommandsService;
import in.vnl.sms.service.Common;
import in.vnl.sms.service.DBDataService;
import in.vnl.sms.service.DevicesService;
import in.vnl.sms.service.NodesInfoService;
import in.vnl.sms.service.UdpServer;

@RestController
@RequestMapping("/api")
public class DataController {

	Logger logger = LoggerFactory.getLogger(DataController.class);
	 
	
	public DataController() {
		super();
		System.out.println("Test in daata controller");
	}

	@Autowired
	private DBDataService dataServiceObj;
	@Autowired
	private ReportServer reportServer;
	@Autowired
	private AutoBackupService autoBackupService;
	
	@Autowired
	private CommandsService cs;
	
	@Autowired
	private DevicesService ds;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private SystemConfigurationRepository scr;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	private BmsConfigService bmss;
	
	@Autowired
	private AlarmsService as;
	
	@Autowired
	private NodesInfoService ns;
	
	
	@Value("${autoBackupDirectory}")
	String autoDirectory;
	
	@Value("${maxTilt}")
	int maxTilt;
	@Value("${minTilt}")
	int minTilt;
	@Value("${maxPan}")
	int maxPan;
	@Value("${minPan}")
	int minPan;
	
	public static boolean scheduleDb = false;
	List<ConfigData> configData1 = new ArrayList<ConfigData>();
	static String operatingSystem = System.getProperty("os.name").toLowerCase();
	
	/*@PostMapping(value="/storeData", consumes = MediaType.ALL_VALUE)
	public ResponseEntity<Map<String, Object>> insertData(TriggerData data)
	{
		dataServiceObj.storeData(data);
		Map<String, Object> response = new HashMap<>();
		response.put("success", true);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}*/
	
	/*@RequestMapping("/getTriggerData/{ipAdd}")
	public Map<String, String> getTriggerData(String ipAdd)
	{
		Map<String, String> tdMap = dataServiceObj.getTriggerData(ipAdd);
		return tdMap;
	}*/
	
	@RequestMapping("/getTriggerData/{ip_add}")
	public TriggerData getTriggerData(@PathVariable("ip_add") String ip_add)
	{
		
		return dataServiceObj.getTriggerData(ip_add);
		
	}
	
	@RequestMapping("/getConfigData/{ip_add}")
	public List<ConfigData> getConfigData(@PathVariable("ip_add") String ip_add)
	{
		logger.info("URL HIT! getConfigData");
		return dataServiceObj.getConfigData(ip_add);
		
	}
	
	@CrossOrigin
	@RequestMapping("/getConfigDataByID/{device_id}")
	public ConfigData getConfigDataByID(@PathVariable("device_id") Long device_id)
	{
		System.out.println("URL HIT! getConfigDataByID");
		logger.info("URL HIT! getConfigDataByID");
		return dataServiceObj.getConfigDataByID(device_id, "1");
	}
	
	/*----Sahil----*/
	@CrossOrigin
	@RequestMapping("/sensorConfigDeploy")
	public List<SensorConfig> getSensorConfig(){
		System.out.println("URL HIT! getSensorConfig");
		logger.info("URL HIT! getSensorConfig");
		return dataServiceObj.getSensorConfig();
	}
	
	@CrossOrigin
	@RequestMapping("/getSensorConfiguration")
	public String getNumberOfSensors(){
		System.out.println("URL HIT! getNumberOfSensors");
		logger.info("URL HIT! getNumberOfSensors");
		return dataServiceObj.getNumberOfSensors();
	}
	@CrossOrigin
	@RequestMapping("/userSensorConfigDeploy")
	public List<SensorConfig> getUserSensorConfig(){
		System.out.println("URL HIT! getUserSensorConfig");
		logger.info("URL HIT! getUserSensorConfig");
		return dataServiceObj.getUserSensorConfig();
	}
	
	
	/*For Addition On Admin Page*/
	@CrossOrigin
	@PostMapping("/editSensorConfig")
	/*public void updateSensorConfig(SensorConfig sensorData) {*/
	public ResponseEntity<Map<String, Object>> editSensorConfig(SensorConfig sensorData){
		System.out.println("URL HIT! editSensorConfig");
		logger.info("URL HIT! editSensorConfig");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			dataServiceObj.editSensorConfig(sensorData);
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
	}
		}
	
	
	
	/*For Updation On Operations Page*/
	@CrossOrigin
	@PostMapping("/updateSensorConfig")
	/*public void updateSensorConfig(SensorConfig sensorData) {*/
	public ResponseEntity<Map<String, Object>> updateSensorConfig(SensorConfig sensorData){
		System.out.println("URL HIT! updateSensorConfigp");
		logger.info("URL HIT! updateSensorConfig");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			dataServiceObj.updateSensorConfig(sensorData);
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
	}
	}
	
	
	
	@CrossOrigin
	@RequestMapping("/getConfigDataByRoom")
	public ConfigData getConfigDataByID()
	{
		System.out.println("URL HIT! getConfigDataByRoom");
		logger.info("URL HIT! getConfigDataByRoom");
		return dataServiceObj.getConfigDataByRoom("1");
	}
	
	@CrossOrigin
	@PostMapping("/recordConfigData/{device_id}/{antennanum}/{preamptypepath}")
	public ResponseEntity<Map<String, Object>> recordConfigData(ConfigData cData, @PathVariable("device_id") Long device_id, @PathVariable("antennanum") String antennaNum, @PathVariable("preamptypepath") String preAmpTypePath )
	{
		System.out.println("URL HIT! recordConfigData");
		logger.info("URL HIT! recordConfigData");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		int Clb=Integer.parseInt(cData.getCalibration());
		// Calibration Sstop handling
		if(Clb==2) {
		cData.setCalibration("0");
		}
		List<ConfigData> configData = new ArrayList<ConfigData>();
		
		if(!DBDataService.changesAllowed) {
			response.put("result", "fail");
			response.put("message", "Configuartion in progess please wait");
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
		if(DBDataService.rotatePTZ){
			response.put("result", "fail");
			response.put("message", "Maximization in progress please stop Maximization and try again");
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
		configData.add(cData);
		configData1.add(cData);
		
		//cData.setRoom("2");
		//configData.add(cData);
		
		int startfreq = Integer.parseInt(cData.getStart_freq());
		int stopfreq = Integer.parseInt(cData.getStop_freq());
		int threshold = Integer.parseInt(cData.getThreshold());
		int maxThreshold = DBDataService.calculateMaxThreshold(startfreq, stopfreq);
		String oprMode=cData.getRef_level();
		int sys_default_threshold=Integer.parseInt( env.getProperty("sys_default_threshold"));
		if(oprMode.equals("l2")) {
			sys_default_threshold=Integer.parseInt( env.getProperty("sys_normal_threshold"));
		}
		
		else if(oprMode.equals("l3")) {
			sys_default_threshold=Integer.parseInt( env.getProperty("sys_hsm_threshold"));
		}
		 maxThreshold =Integer.parseInt( env.getProperty("user_min_threshold"));
		// sys_default_threshold=Integer.parseInt( env.getProperty("sys_default_threshold"));
	    	if(threshold<maxThreshold && threshold!=sys_default_threshold){
			response.put("result", "fail");
			response.put("message", "Threshold cannot be less than "+maxThreshold+" for given frequency range");
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
		
		if(DBDataService.isCalibrating&&(Clb!=2)) {
			response.put("result", "fail");
			response.put("message", "Cannot apply new Settings already Scanning");
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
		
		if(cData.getCalibration().equals("1")) {
			if(!DBDataService.isManualOverride && DBDataService.isAutoEnabled) {
				response.put("result", "fail");
				response.put("message", "Cannot set Calibration. Please enable Manual Overide and disable Auto");
				responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
				return responseEntity;
			}
		}
		
		try {
			DBDataService.getbandconfig();
			if(!DBDataService.systemType.equals("standalone"))				//changed here DBDataService.systemType.equals("integrated")
				dataServiceObj.sendStopCommandToHummer(false);
			else
				dataServiceObj.sendStopCommandToHummer(true);
			
			dataServiceObj.recordConfigData(configData, device_id, antennaNum, preAmpTypePath);

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			DBDataService.changesAllowed=false;
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	@CrossOrigin
	@PostMapping("/addDevice")
	public ResponseEntity<Map<String, Object>> recordDeviceData(HttpServletRequest request)
	{
		System.out.println("URL HIT! recordDeviceData");
		logger.info("URL HIT! recordDeviceData");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		try {
			String ip_add = request.getParameter("ip");
			String name = request.getParameter("name");
			String color = "#9e7e7e";
			String north_offset = request.getParameter("north_offset");
			String sector_offset = request.getParameter("sector_offset");
			String lat = request.getParameter("lat");
			String lon = request.getParameter("lon");
			String is_active = "false";
			String state = "Not Reachable";
			String unitno = request.getParameter("unit");
			long id;
			try {
				id=Long.parseLong(request.getParameter("id"));	
			}
			catch(Exception exception) {
				id=0l;
			}
			System.out.println(dataServiceObj.getDeviceIp());
			if(name.equals(DBDataService.nodeIdMap.get(3))) {
				if(dataServiceObj.getDevicesList().size()>0) {
					response.put("result", "fail");
					response.put("message", "Node Already Exist");
					responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
					return responseEntity;
				}
				else {
					dataServiceObj.recordDeviceData(ip_add,name,color,lat,lon,is_active,state,id);
					dataServiceObj.recordDeviceTriggerData(ip_add,name,color,lat,lon,is_active,state,unitno);
					dataServiceObj.recordDeviceConfigData(ip_add,name,color,lat,lon,is_active,state);
				}
			}
			else if(name.equals(DBDataService.nodeIdMap.get(2))) {
				if(dataServiceObj.getPTZList()!=null) {
					response.put("result", "fail");
					response.put("message", "Node Already Exist");
					responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
					return responseEntity;
				}
				else if(dataServiceObj.getDeviceIp()==null) {
					response.put("result", "fail");
					response.put("message", "Please add HUMMER first");
					responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
					return responseEntity;
				}
				else {
					dataServiceObj.recordPTZData(ip_add, name, north_offset, sector_offset);
				}
			}
			else if(name.equals(DBDataService.nodeIdMap.get(6))) {
				dataServiceObj.recordNodeData(ip_add, name, state);
				//dataServiceObj.updateNodeData();
				
				bmss.save(ip_add, "load", "", "2");
				bmss.save(ip_add, "periodicity", "", "5");
				bmss.save(ip_add, "systemtime", "", ""+System.currentTimeMillis());
				
			}
			else if(name.equals(DBDataService.nodeIdMap.get(8))){
				dataServiceObj.recordNodeData(ip_add, name, state);
			}
			else {
				if(dataServiceObj.getNodeByType(name)!=null) {
					response.put("result", "fail");
					response.put("message", "Node Already Exist");
					responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
					return responseEntity;
				}
				else {
					dataServiceObj.recordNodeData(ip_add, name, state);
					//dataServiceObj.updateNodeData();
				}
			}
			
			dataServiceObj.checkNodeStatus();
			
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;			
		}
		catch(Exception exception) {
			logger.error(exception.toString());
			response.put("result", "fail");
			response.put("message", exception.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}	
	}
	
	@CrossOrigin
	@PostMapping("/addPTZ/{device_id}")
	public ResponseEntity<Map<String, Object>> addPTZ(PTZData ptzData, @PathVariable("device_id") Long device_id)
	{
		System.out.println("URL HIT! addPTZ");
		logger.info("URL HIT! addPTZ");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			//dataServiceObj.recordPTZData(ptzData, device_id);

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	@CrossOrigin
	@RequestMapping("/checkNodeStatus")
	public List<NodeData> checkNodeStatus()
	{
		System.out.println("URL HIT! checkNodeStatus");
		return dataServiceObj.checkNodeStatus();
	}
	
	@CrossOrigin
	@RequestMapping("/getJmData")
	public List<JMDeviceData> getJmData()
	{
		System.out.println("URL HIT! getJmData");
		return dataServiceObj.jmNodeData();
	}
	
	@CrossOrigin
	@RequestMapping("/getJMList")
	public List<JMNodeMapping> getJmList()
	{
		System.out.println("URL HIT! getJmList");
		return dataServiceObj.getJMNodeList();
	}
	
	@CrossOrigin
	@RequestMapping("/getColorStripData/{profile}")
	public List<ColorStripsData> getColorStripData(@PathVariable("profile") String profile)
	{	
		System.out.println("URL HIT! Going to get all ColorCodeList");
		logger.info("URL HIT! Going to get all ColorCodeList");
		return dataServiceObj.getColorCodeList(profile);
	}
	
	@CrossOrigin
	@PostMapping("/addMaskingData")
	public ResponseEntity<Map<String, Object>> addMaskingData(MaskingScreenData maskingData)
	{
		System.out.println("URL HIT! maskingData");
		logger.info("URL HIT! maskingData");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			maskingData.setProfile(DBDataService.defaultProfile);
			dataServiceObj.recordMaskingData(maskingData);

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	@CrossOrigin
	@PostMapping("/addBandFilterFrq")
	public ResponseEntity<Map<String, Object>> addBandFilterFrq(HttpServletRequest request)
	{
		System.out.println("URL HIT! addBandFilterFrq");
		logger.info("URL HIT! addBandFilterFrq");
		int strtFrq=0;
		int stpfrq=0;
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		int stratFrq=Integer.parseInt(request.getParameter("startfrequency"));
		int stopFrq=Integer.parseInt(request.getParameter("stopfrequency"));
		try {
			JSONArray bandData = new JSONArray();
		    bandData=dataServiceObj.getBandDataList();
		    if(bandData.length()!=0) {
		    for(int i=0; i<bandData.length();i++) {
		    	JSONObject JO= bandData.getJSONObject(i);
		    	 strtFrq=Integer.parseInt(JO.getString("start_frq"));
		    	 stpfrq=Integer.parseInt(JO.getString("stop_frq"));
		    	if(stratFrq>=strtFrq&&stratFrq<=(stpfrq)) {
		    	response.put("result", "fail");
				response.put("message", "Frequency Band Already Exists");
				responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
				return responseEntity;
		    	}
		    }
		    }
			dataServiceObj.SaveBandFilter(String.valueOf(stratFrq),String.valueOf(stopFrq));

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	@CrossOrigin
	@PostMapping("/UpdateBandFilterFrq")
	public ResponseEntity<Map<String, Object>> UpdateBandFilterFrq(HttpServletRequest request)
	{
		System.out.println("URL HIT! UpdateBandFilterFrq");
		logger.info("URL HIT! UpdateBandFilterFrq");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		String stratFrq=request.getParameter("startfrequency");
		String stopFrq=request.getParameter("stopfrequency");
		String Status=request.getParameter("status");
		try {
			
			dataServiceObj.UpdateBandFilter(stratFrq,stopFrq,Status);

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	@CrossOrigin
	@PostMapping("/removeMaskingData")
	public ResponseEntity<Map<String, Object>> removeMaskingData(int id)
	{
		System.out.println("URL HIT! removeMaskingData");
		logger.info("URL HIT! removeMaskingData");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			System.out.println(id);
			dataServiceObj.deleteMaskingData(id);
			//msr.deleteById(id);
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	@CrossOrigin
	@PostMapping("/deleteFrqBandData")
	public ResponseEntity<Map<String, Object>> deleteFrqBandData(int id)
	{
		System.out.println("URL HIT! deleteFrqBandData");
		logger.info("URL HIT! deleteFrqBandData");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			System.out.println(id);
			dataServiceObj.deleteBandFilter(id);
			
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	@CrossOrigin
	@PostMapping("/updatePriority")
	public ResponseEntity<Map<String, Object>> updatePriority(HttpServletRequest request)
	{
		String priority=request.getParameter("prioNames");
		System.out.println("URL HIT! updatePriority");
		logger.info("URL HIT! updatePriority");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			String[] strArray=priority.split(",");
			dataServiceObj.updatePriority(strArray);

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	
	@CrossOrigin
	@PostMapping("/defineColorStrips")
	public ResponseEntity<Map<String, Object>> defineColorStrips(ColorStripsData colorStrips)
	{
		System.out.println("URL HIT! colorStrips");
		logger.info("URL HIT! colorStrips");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			colorStrips.setProfile(DBDataService.defaultProfile);
			dataServiceObj.recordColorStripsData(colorStrips);

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	@CrossOrigin
	@PostMapping("/removeColorStrips")
	public ResponseEntity<Map<String, Object>> removeColorStrips(int id)
	{
		System.out.println("URL HIT! removeColorStrips");
		logger.info("URL HIT! removeColorStrips");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			dataServiceObj.deleteColorStripsData(id);

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}

	
	
	@CrossOrigin
	@PostMapping("/addSystemConfiguration")
	public ResponseEntity<Map<String, Object>> addSystemConfiguration(SystemConfigurationData systemConfigurationData)
	{
		System.out.println("URL HIT! systemConfigurationData");
		logger.info("URL HIT! systemConfigurationData");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			
			if(DBDataService.isCalibrating)
				throw new Exception("Cannot set System Configuration as System is Calibrating. Please wait.");
			
			systemConfigurationData.setProfile(DBDataService.defaultProfile);
			dataServiceObj.recordSystemConfigurationData(systemConfigurationData);

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	@CrossOrigin
	@PostMapping("/recordDevicePriority/{device_id}")
	public ResponseEntity<Map<String, Object>> recordDevicePriority(TaskPriority tpData, @PathVariable("device_id") String device_id)
	{
		System.out.println("URL HIT! recordDevicePriority");
		logger.info("URL HIT! recordDevicePriority");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			dataServiceObj.recordDevicePriority(tpData);

			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	@CrossOrigin	
	@RequestMapping("/getPTZInfo/{ip_add}")
	@ResponseBody
	public PTZData getPTZInfo(@PathVariable("ip_add") String ip_add){
		try{
			PTZData ptzInfo= dataServiceObj.getPTZInfo(ip_add);
			
			return ptzInfo;
		}
		catch(Exception exception) {
			logger.error(exception.toString());
		}
		return null;
	}
	
	@CrossOrigin
	@RequestMapping("/getPtzData/{ip_add}")
	@ResponseBody
	public ArrayList<Integer> getPTZTilt_and_Angle(@PathVariable("ip_add") String ip_add){
		ArrayList<Integer> angleTiltArrayList=new ArrayList<Integer>();
		try{
			
			int the_angle= dataServiceObj.ptzGetAngle(ip_add);
			int the_tilt= dataServiceObj.ptzGetTilt(ip_add);
			
			angleTiltArrayList.add(the_angle);
			angleTiltArrayList.add(the_tilt);
			
			return angleTiltArrayList;
		}
		catch(Exception exception) {
			logger.error(exception.toString());
		}
		return angleTiltArrayList;
	}
	
	@CrossOrigin
	@PostMapping("deleteDevice/{ip_add}/{type}")
	public ResponseEntity<Map<String,Object>> deleteDevice( @PathVariable("ip_add") String ip_add, @PathVariable("type") String type){
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		logger.info("In delete device "+type);
		try {
			//dataServiceObj.deleteDeviceData(device_id);
			if(type.equals(DBDataService.nodeIdMap.get(3))) {
				dataServiceObj.deleteDeviceData(ip_add);
			}
			else if(type.equals(DBDataService.nodeIdMap.get(2))) {
				dataServiceObj.deletePTZData(ip_add);
			}
			else {
				dataServiceObj.deleteNodeData(ip_add);
				//dataServiceObj.updateNodeData();
			}
			dataServiceObj.checkNodeStatus();
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
		}
		catch(Exception exception) {
			logger.error(exception.toString());
			response.put("result", "fail");
			response.put("message", exception.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
	
	@CrossOrigin
	@PostMapping("spsUpdate/{current_ip}/{lat}/{lon}")
	public ResponseEntity<Map<String,Object>> spsUpdate(@PathVariable("current_ip") String current_ip, @PathVariable("lat") String lat, @PathVariable("lon") String lon){
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		logger.info("In SPS Update");
		
		try {
			dataServiceObj.spsUpdate(current_ip, lat, lon);
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
		}
		catch(Exception exception) {
			logger.error(exception.toString());
			response.put("result", "fail");
			response.put("message", exception.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
	
	@CrossOrigin
	@PostMapping("ptzUpdate/{current_ip}/{ptz_offset}/{sector_offset}")
	public ResponseEntity<Map<String,Object>> ptzUpdate(@PathVariable("current_ip") String current_ip, @PathVariable("ptz_offset") String ptz_offset, @PathVariable("sector_offset") String sector_offset){
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		logger.info("In PTZ Update");
		
		try {
			dataServiceObj.ptzUpdate(current_ip, ptz_offset, sector_offset,"0","0");
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
		}
		catch(Exception exception) {
			logger.error(exception.toString());
			response.put("result", "fail");
			response.put("message", exception.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
	
	@CrossOrigin
	@PostMapping("ptzOperation/{current_ip_addr}/{ptzOpr}/{target}")
	public ResponseEntity<Map<String,Object>> ptzOperation(@PathVariable("current_ip_addr") String ip_add, @PathVariable("ptzOpr") String ptzOpr, @PathVariable("target") String target){
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		logger.info("In PTZ Operation "+ptzOpr);
		
		int the_angle= dataServiceObj.ptzGetAngle(ip_add);
		int the_tilt= dataServiceObj.ptzGetTilt(ip_add);
		int tiltUp;
		int angle;
		int tar = Integer.parseInt(target);
		
		try {
			//dataServiceObj.deleteDeviceData(device_id);
			if(ptzOpr.equals("UP")) {
				 tiltUp=the_tilt-Integer.parseInt(target);
				 if(tiltUp<minTilt) {
					response.put("result", "fail");
					//response.put("message", exception.getMessage());
					response.put("message", "Angle is out of Range min Tilt" + minTilt);
					responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
					return responseEntity;
					
				}
				dataServiceObj.performPtzOpr(ip_add, 2, tar);
			}
			else if(ptzOpr.equals("DOWN")) {
				 tiltUp=the_tilt+Integer.parseInt(target);
				 if(tiltUp>maxTilt) {
					response.put("result", "fail");
					//response.put("message", exception.getMessage());
					response.put("message", "Angle is out of Range max Tilt" + maxTilt);
					responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
					return responseEntity;
					
				}
				dataServiceObj.performPtzOpr(ip_add, 7, tar);
			}
			else if(ptzOpr.equals("LEFT")) {
				 angle=the_angle-Integer.parseInt(target);
				 if(angle<minPan) {
					response.put("result", "fail");
					//response.put("message", exception.getMessage());
					response.put("message", "Angle is out of Range Min Angle" + minPan);
					responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
					return responseEntity;
					
				}
				dataServiceObj.performPtzOpr(ip_add, 4, tar);
			}
			else if(ptzOpr.equals("RIGHT")) {
				 angle=the_angle+Integer.parseInt(target);
				 if(angle>maxPan) {
					response.put("result", "fail");
					//response.put("message", exception.getMessage());
					response.put("message", "Angle is out of Range Max Angle" + maxPan);
					responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
					return responseEntity;
					
				}
				dataServiceObj.performPtzOpr(ip_add, 5, tar);
			}
			else {
				dataServiceObj.performPtzOpr(ip_add, 1, tar);
			}
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
		}
		catch(Exception exception) {
			logger.error(exception.toString());
			response.put("result", "fail");
			//response.put("message", exception.getMessage());
			response.put("message", "Device not reachable, couldn't perform operation");
			
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}
	
	@CrossOrigin
	@PostMapping("/addJmDevice")
	public ResponseEntity<Map<String, Object>> recordJMDeviceData(HttpServletRequest request)
	{
		logger.info("URL HIT! recordJMDeviceData");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		try {
			String deviceId = request.getParameter("jmdeviceid");
			String antennaid = request.getParameter("antennaid");
			String sector = request.getParameter("sector");
			dataServiceObj.recordJM(deviceId, antennaid, sector);
			
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;			
		}
		catch(Exception exception) {
			logger.error(exception.toString());
			response.put("result", "fail");
			response.put("message", exception.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}	
	}
	
	//get User setting
	@CrossOrigin
	@RequestMapping("/getUserSetting")
	public UserSetting getUserSetting()
	{
		System.out.println("URL HIT! getJmData");
		return dataServiceObj.getUserSetting();
	}
	
	
	@CrossOrigin
	@RequestMapping("/getPtzOffset")
	public int getPtzOffset(HttpServletRequest request)
	{
		System.out.println("URL HIT! getPtzOffset");
		logger.info("URL HIT! getPtzOffset");
		
		int offset = 0;
		String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }
        
        String falconIp = Common.nodeIpMap.get(DBDataService.nodeIdMap.get(4));
        
		if(remoteAddr.equals(falconIp)) {
			dataServiceObj.sendToFalconOffset(falconIp);
			//return offset+DBDataService.ptz_roll_offset;
		}
		else {
			offset = dataServiceObj.getPtzOffset();
		}
		
		return offset;
	}
	
	
	//change userSetting from this method
	@CrossOrigin
	@PostMapping("/changeUserSetting")
	public ResponseEntity<Map<String, Object>> changeUserSetting(HttpServletRequest request)
	{
		logger.info("URL HIT! changeUserSetting");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		try {
			
			String type = request.getParameter("type");
			String mode = request.getParameter("mode");
			String gps = request.getParameter("gps");
			String accuracy = request.getParameter("accuracy");
		    //String opr_mode=request.getParameter("oprmode");
			/*
			 * if(!DBDataService.changesAllowed) { response.put("result", "fail");
			 * response.put("message", "Configuartion in progess please wait");
			 * responseEntity = new ResponseEntity<Map<String, Object>>(response,
			 * HttpStatus.BAD_REQUEST); return responseEntity; }
			 */
			if(DBDataService.isCalibrating)
				throw new Exception("Cannot set User Settings as System is Calibrating. Please wait.");
			
			dataServiceObj.updateUserSetting(type, mode, gps, Double.parseDouble(accuracy));
			if(type.equals("hummer")) {
				SystemConfigurationData sysconfig =scr.getSystemConfigurationData(dataServiceObj.defaultProfile);
				sysconfig.setTmdas(0);
				scr.save(sysconfig);
				
			}
			
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;			
		}
		catch(Exception exception) {
			logger.error(exception.toString());
			response.put("result", "fail");
			response.put("message", exception.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}	
	}
	
	//-------------===BMS DATA===-------------
	@CrossOrigin
	@PostMapping("/bmsData")
	public ResponseEntity<Map<String, Object>> BMSData(HttpServletRequest request)
	{
		logger.info("URL HIT! BMSData");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		try 
		{
			String id = request.getParameter("ID");
			String btn_PR = request.getParameter("pr_key");
			String ip = request.getParameter("ip");
			
			System.out.println("------------------BMS-----------");
			System.out.println(id);
			System.out.println(btn_PR);
			System.out.println(ip);
			
			if(id.equals("11")) 
			{
				btn_PR = Instant.now().getEpochSecond()+"";
			}
			
			String result = executeUdpBMS(ip,id,btn_PR);
			
			String[] resultData = result.split(",");
			
			if(resultData[2].equals("0") || resultData[2].equals("0*")) 
			{
				switch(id) 
				{
					case "1" :
						bmss.save(ip, "load", "", "1");
					break;
					case "2" :
						bmss.save(ip, "load", "", "2");
					break;
					case "4" :
						synchBMSStatus(ip);
					break;
					case "5" :
						bmss.save(ip, "periodicity", "", btn_PR);
					break;
					case "10" :
						synchBMSStatus(ip);
					break;
					case "11" :
						synchBMSStatus(ip);
					break;
				}
				response.put("result", "success");
			}
			else 
			{
				response.put("result", "fail");
			}
			
			
			
			response.put("message", "Added");
			if(resultData.length >=4) 
			{
				response.put("data", ip+","+getResultData(result));
			}
			
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;			
		}
		catch(Exception exception) {
			logger.error(exception.toString());
			response.put("result", "fail");
			response.put("message", exception.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}	
	}
	
	
	public String getResultData(String result) 
	{
		String[] resultData = result.split(",");
		StringBuilder returnData = new StringBuilder("");
		
		if(resultData.length >=4) 
		{
			//String aa returnData = resultData[resultData.length-1].replace("*", "");
			//returnData = resultData
			//returnData.
			
			for(int i=3;i<resultData.length;i++) 
			{
				if(i==resultData.length-1) 
				{
					returnData.append(resultData[i]);
				}
				else 
				{
					returnData.append(resultData[i]+",");
				}
			}
			
			
		}
		return returnData.toString().replace("*", "");
	}
	
	public String executeUdpBMS(String ip,String id,String msg) 
	{
		Commands cmd = cs.getCommand(Long.parseLong(id));
		String result =  "0,0,0";
		if(cmd.getCmd() != null && !cmd.getCmd().equalsIgnoreCase("")) 
		{
			String port = env.getProperty("bms_udp_port");
			//@sandeep
			
			if(msg != null && !msg.equalsIgnoreCase("")) 
			{
				msg = ","+msg;
			}
			result = dataServiceObj.sendBMS(ip,Integer.parseInt(port),cmd.getCmd()+""+msg);
		}
		return result;
	}
	
	public void synchBMSStatus(String ip) 
	{
		String result = executeUdpBMS(ip,"7","");
		bmss.save(ip, "load", "", getResultData(result));
		
		result = executeUdpBMS(ip,"8","");
		bmss.save(ip, "periodicity", "", getResultData(result));
		result = executeUdpBMS(ip,"9","");
		bmss.save(ip, "systemtime", "", getResultData(result));
		
	}
	
	@GetMapping("/devices")
	public List<Devices> getDevices()
	{
			return ds.getDevices();
	}
	
	
	
	
	/*@CrossOrigin
	@PostMapping("deleteNode/{ip_add}")
	public ResponseEntity<Map<String,Object>> deleteNode( @PathVariable("ip_add") String ip_add){
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		try {
			dataServiceObj.deleteNodeData(ip_add);
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
		}
		catch(Exception exception) {
			response.put("result", "fail");
			response.put("message", exception.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		return responseEntity;
	}*/
	
	@CrossOrigin
	@PostMapping("/rotateDevice")
	public ResponseEntity<Map<String, Object>> rotateDevice(HttpServletRequest request)
	{
		System.out.println("URL HIT! rotateDevice");
		logger.info("URL HIT! rotateDevice");
		String ip_add = request.getParameter("ip_add");
		String startAngle = request.getParameter("startAngle");
		String stopAngle = request.getParameter("stopAngle");
		String bandwidth = request.getParameter("bandwidth");
		String tiltAngleProperty = request.getParameter("tilt_angle");
		String frequency = request.getParameter("frequency");
		String switchValue = request.getParameter("switch");
		int strtAngle=Integer.parseInt(startAngle);
		int stpAngle=Integer.parseInt(stopAngle);
		
		//add switch value
		DBDataService.switchVal = switchValue;
		
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		if(strtAngle<minPan) {
			response.put("result", "fail");
			response.put("message", "Angle is out Of Range min Angle" + minPan);
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
		else if(stpAngle>maxPan) {
			response.put("result", "fail");
			response.put("message", "Angle is out Of Range max Angle" + maxPan );
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
		try {
			/*if(DBDataService.isAutoTriggeredEvent || DBDataService.isBlacklistTriggeredEvent || DBDataService.isUGSTriggeredEvent)
			{
				dataServiceObj.shutdownTimer();
				dataServiceObj.stopRotateDevice(ip_add);
			}
			DBDataService.isUserTriggeredEvent = true;
			dataServiceObj.rotateDevice(ip_add, startAngle, stopAngle, Integer.parseInt(tiltAngleProperty));*/
			if(DBDataService.ptzReachable && !DBDataService.isFreeze && !DBDataService.isCalibrating)
				dataServiceObj.addToQueue(DBDataService.manualEvent, ip_add, frequency, Double.parseDouble(bandwidth), startAngle, stopAngle, Integer.parseInt(tiltAngleProperty), 0, 0, LocalDateTime.now(), 100.0);
			else if(DBDataService.isFreeze)
				throw new Exception("Cannot perform operation as System has been freezed. Please free space to restore functionality.");
			else if(DBDataService.isCalibrating)
				throw new Exception("Cannot perform operation as System is Calibrating. Please wait.");
			else
				throw new Exception("Cannot perform operation as STRU is not reachable");
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	@CrossOrigin
	@PostMapping("/stopRotateDevice")
	public ResponseEntity<Map<String, Object>> stopRotateDevice(HttpServletRequest request)
	{
		System.out.println("URL HIT! stopRotateDevice");
		logger.info("URL HIT! stopRotateDevice");
		String ip_add = request.getParameter("ip_add");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			dataServiceObj.stopRotateDevice(ip_add);

			response.put("result", "success");
			response.put("message", "ok");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	@CrossOrigin
	@RequestMapping("/getSession")
	public String getSessionData(HttpServletRequest request)
	{
		System.out.println("URL HIT! getSession");
		logger.info("URL HIT! getSession");
		String freq = (String) request.getSession().getAttribute("freq");
		String band = (String) request.getSession().getAttribute("band");
		String sectorId = (String) request.getSession().getAttribute("sectorId");
		String max = (String) request.getSession().getAttribute("max_power");
		String min = (String) request.getSession().getAttribute("min_power");
		String threshold = (String) request.getSession().getAttribute("threshold");
		
		return freq+","+band+","+sectorId+","+max+","+min+","+threshold;
	}
	
	@CrossOrigin
	@RequestMapping("/setSession")
	public void setSessionData(HttpServletRequest request)
	{
		System.out.println("URL HIT! setSession");
		logger.info("URL HIT! setSession");
		String freq = request.getParameter("freq");
		String band = request.getParameter("band");
		String sectorId = request.getParameter("sectorId");
		String max = request.getParameter("max_power");
		String min = request.getParameter("min_power");
		String threshold = request.getParameter("threshold");
		
		request.getSession().setAttribute("freq", freq);
		request.getSession().setAttribute("band", band);
		request.getSession().setAttribute("sectorId", sectorId);
		request.getSession().setAttribute("max_power", max);
		request.getSession().setAttribute("min_power", min);
		request.getSession().setAttribute("threshold", threshold);
	}
	
	@RequestMapping("/getdummy")
	public String  getdummy()
	{
		
		return "Dummy Test";
		
	}
	
	@CrossOrigin
	@RequestMapping("/getPeakInfoData/{start_time}/{end_time}")
	public List<PeakDetectionInfo> getPeakInfoRecord(@PathVariable("start_time") String startTime, @PathVariable("end_time") String endTime)
	{
		System.out.println("URL HIT! getPeakInfoRecord");
		logger.info("URL HIT! getPeakInfoRecord");
		List<PeakDetectionInfo> pkinfo = dataServiceObj.getPeakInfoRecord(startTime,endTime);
		return pkinfo;
	}
	
	@CrossOrigin
	@RequestMapping("/getOldPeakInfoData/{start_time}/{end_time}/{systemType}")
	public List<PeakDetectionInfo> getOldPeakInfoRecord(@PathVariable("start_time") String startTime, @PathVariable("end_time") String endTime, @PathVariable("systemType") String systemType)
	{
		System.out.println("URL HIT! getOldPeakInfoRecord");
		logger.info("URL HIT! getOldPeakInfoRecord");
		List<PeakDetectionInfo> pkinfo = dataServiceObj.getOldPeakInfoRecord(startTime,endTime,systemType);
		return pkinfo;
	}
	
	@CrossOrigin
	@RequestMapping("/getAlarmInfoData/{start_time}/{end_time}")
	public List<AlarmInfo> getAlarmInfoRecord(@PathVariable("start_time") String startTime, @PathVariable("end_time") String endTime)
	{
		System.out.println("URL HIT! getAlarmInfoRecord");
		logger.info("URL HIT! getAlarmInfoRecord");
		List<AlarmInfo> arinfo = dataServiceObj.getAlarmInfoRecord(startTime,endTime);
		System.out.println(arinfo);
		return arinfo;
	}
	
	@CrossOrigin
	@RequestMapping("/getMaximizeData")
	public List<AlarmInfo> getMaximizeRecord()
	{
		System.out.println("URL HIT! getMaximizeRecord");
		logger.info("URL HIT! getMaximizeRecord");
		List<AlarmInfo> arinfo = dataServiceObj.getMaximizeRecord();
		return arinfo;
	}
	
	@CrossOrigin
	@RequestMapping("/getEventCue/{start_time}/{end_time}")
	public List<Event> getEventCueRecord(@PathVariable("start_time") String startTime, @PathVariable("end_time") String endTime)
	{
		System.out.println("URL HIT! getEventCueRecord");
		logger.info("URL HIT! getEventCueRecord");
		List<Event> event = dataServiceObj.getEventCueRecord(startTime,endTime);
		return event;
	}
	
	@CrossOrigin
	@RequestMapping("/getGpsData/{period}")
	public List<GpsData> getGpsData(@PathVariable("period") String period)
	{
		System.out.println("URL HIT! getGpsData");
		logger.info("URL HIT! getGpsData");
		
		Date start = new Date(System.currentTimeMillis() - 60 * Integer.parseInt(period) * 1000);
		Date end = new Date();
		
		List<GpsData> gpsData = dataServiceObj.getGpsData(start, end);
		return gpsData;
	}
	
	/*@RequestMapping("/getPeakInfoData/{ip_add}")
	public PeakDetectionInfo getPeakInfoRecordByIP(@PathVariable("ip_add") String ip_add)
	{
		System.out.println("URL HIT!");
		PeakDetectionInfo pkinfo =dataServiceObj.getPeakInfoRecordByIP(ip_add);
		System.out.println("peak info data ---> " + pkinfo.getFrequency());
		return pkinfo;
	}*/
	
	@CrossOrigin
	@RequestMapping("/getLedOnStatus/{device_id}")
	public LEDData getLEDOnStatus(@PathVariable("device_id") Long device_id)
	{
		System.out.println("LED - "+ dataServiceObj.getLEDStatus(device_id));
		return dataServiceObj.getLEDStatus(device_id);
	}
	
	@CrossOrigin
	@RequestMapping("/getDevicesList")
	public List<DeviceInfo> getDevicesList()
	{	
		System.out.println("URL HIT! Going to get all devices");
		logger.info("URL HIT! Going to get all devices");
		return dataServiceObj.getDevicesList();
	}
	
	@CrossOrigin
	@RequestMapping("/getPriorityList")
	public List<Priority> getPriorityList()
	{	
		System.out.println("URL HIT! Going to get all devices");
		logger.info("URL HIT! Going to get all devices");
		return dataServiceObj.getPriorityList();
	}
	
	@CrossOrigin
	@RequestMapping("/getMaskingDataList/{profile}")
	public List<MaskingScreenData> getMaskingDataList(@PathVariable("profile") String profile)
	{	
		System.out.println("URL HIT! Going to get all getMaskingDataList");
		logger.info("URL HIT! Going to get all getMaskingDataList");
		return dataServiceObj.getMaskingDataList(profile);
	}
	@CrossOrigin
	@RequestMapping("/getBandDataList")
	public String getBandDataList() throws ClassNotFoundException, SQLException, JSONException
	{	
		System.out.println("URL HIT! Going to get all getBandDataList");
		logger.info("URL HIT! Going to get all getBandDataList");
		String Data =  dataServiceObj.getBandDataList().toString();
		return Data;
	}
	@CrossOrigin
	@RequestMapping("/getColorCodeList/{profile}")
	public List<ColorStripsData> getColorCodeList(@PathVariable("profile") String profile)
	{	
		System.out.println("URL HIT! Going to get all ColorCodeList");
		logger.info("URL HIT! Going to get all ColorCodeList");
		return dataServiceObj.getColorCodeList(profile);
	}
	
	@CrossOrigin
	@RequestMapping("/getSystemConfiguration/{profile}")
	public SystemConfigurationData getSystemConfiguration(@PathVariable("profile") String profile)
	{	
		System.out.println("URL HIT! Going to get all SystemConfigurationList");
		logger.info("URL HIT! Going to get all SystemConfigurationList");
		return dataServiceObj.getSystemConfiguration(profile);
	}
	
	@CrossOrigin
	@RequestMapping("/getThresholdMap")
	public List<FreqPowMinThresData> getThresholdMap()
	{	
		return DBDataService.freqPowMinThres;
	}
	
	
	@CrossOrigin
	@RequestMapping("/getRefLevel")
	public String getRefLevel() throws ClassNotFoundException, SQLException, JSONException{
		System.out.println("URL HIT! refl");
		logger.info("URL HIT! get refl");
		String CalibData=dataServiceObj.getRfl().toString();
		return CalibData;
		
	}
	@CrossOrigin
	@RequestMapping("/getConfigProfile/{profile}")
	public ConfigProfileData getConfigProfile(@PathVariable("profile") String profile)
	{	
		System.out.println("URL HIT! Going to get all Config by Profile");
		logger.info("URL HIT! Going to get all Config by Profile");
		return dataServiceObj.getConfigProfile(profile);
	}
	
	@CrossOrigin
	@RequestMapping("/getDeviceInfo/{device_id}")
	public DeviceInfo getDeviceInfo(@PathVariable("device_id") Long device_id)
	{	
		System.out.println("URL HIT! Going to getDeviceInfo");
		logger.info("URL HIT! Going to getDeviceInfo");
		return dataServiceObj.getDeviceInfo(device_id);
	}
	
	
	@CrossOrigin
	@PostMapping("/peakinfo/range/{start_time}/{end_time}/{device_id}")
    @ResponseBody
    public Map<String, List<PeakInfoPojo>> getPeakInfoDataRange(@PathVariable("start_time")String startTime, @PathVariable("end_time")String endTime,@PathVariable("device_id")Long deviceId){
        try{
            Map<String, List<PeakInfoPojo>> detectedPeaks= dataServiceObj.getPeakInfoRecordBetweenTime(startTime, endTime,deviceId);

            return detectedPeaks;
        }
        catch(Exception exception) {
            logger.error(exception.toString());
        }
        return null;
    }
	
	@CrossOrigin
	@RequestMapping("/getCurrentEvent")
    public String getCurrentEvent(){
		
		System.out.println("URL HIT! Going to get all UsersList");
		logger.info("URL HIT! Going to get all UsersList");
		
		if(DBDataService.currentEventName!=null)
			return DBDataService.currentEventName;
		
		//remove if causing problem. added for double safe check
		dataServiceObj.notifyWait();
		
		return "Idle";
    }
	
	@CrossOrigin
	@RequestMapping("/dataPurge")
    public ResponseEntity<Map<String, Object>> dataPurge(HttpServletRequest request){
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			String startTime = request.getParameter("startTime");
			String endTime = request.getParameter("endTime");
			String type = request.getParameter("type");
			
			if(!type.equals("")) {
				dataServiceObj.resetAll(type);
			}
			else {
				dataServiceObj.purgeData(startTime, endTime);
			}
			response.put("result", "success");
			response.put("message", "Purged");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
    }
	
	@CrossOrigin
	@RequestMapping("/downloadBackupReport")
	public ResponseEntity downloadReport(HttpServletRequest request)
	{
		logger.info("URL HIT! getBackup");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();

        try {
            String fileName = request.getParameter("fileName");
            String filePath = autoBackupService.getFilePath(fileName);
            File file = new File(filePath);
            
            System.out.println(file.getName()+"   "+filePath);

            return createFileAttachmentResponse(filePath);
			
        } catch (Exception e) {
        	logger.error(e.toString()); 
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
        }	
	}
	
	@CrossOrigin
	@RequestMapping("/getBackup")
	public List<String> getBackup()
	{
		logger.info("URL HIT! getBackup");
		return autoBackupService.getReportNames();		
	}
	
	@CrossOrigin
	@PostMapping("/deleteBackup")
	public ResponseEntity<Map<String, Object>> deletBackup(HttpServletRequest request)
	{
		logger.info("URL HIT! deleteBackup");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		try {
			String reportName = request.getParameter("fileName");
			autoBackupService.deleteReport(reportName);
			
			response.put("result", "success");
			response.put("message", "Added");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;			
		}
		catch(Exception exception) {
			logger.error(exception.toString());
			response.put("result", "fail");
			response.put("message", exception.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}	
	}
	
	@CrossOrigin
	@RequestMapping("/profileOpr")
	public ResponseEntity<Map<String, Object>> profileOperation(HttpServletRequest request)
	{
		//System.out.println("URL HIT! factoryResetDevice");
		logger.info("URL HIT! profileOpr");
		
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			String operation = request.getParameter("operation");
			String profileName = request.getParameter("profile");
			
			if(operation.equals("save")) {
				dataServiceObj.saveProfileData(profileName);
			}
			else if(operation.equals("apply")) {
				dataServiceObj.applyProfileData(profileName);
			}
			else if(operation.equals("delete")) {
				dataServiceObj.deleteProfileData(profileName);
			}

			response.put("result", "success");
			response.put("message", "ok");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		
		}catch (Exception e) {
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	@CrossOrigin
	@RequestMapping("/getProfiles")
    public List<String> getProfiles(){
		
		System.out.println("URL HIT! Going to get all Profiles List");
		logger.info("URL HIT! Going to get all Profiles List");
		return dataServiceObj.getProfiles();
    } 

	@CrossOrigin
	@RequestMapping("/getUsers")
    public String getUsers(){
		
		System.out.println("URL HIT! Going to get all UsersList");
		logger.info("URL HIT! Going to get all UsersList");
		return dataServiceObj.getUsers();
    } 
	
	@CrossOrigin
	@PostMapping("/updatePassword")
    @ResponseBody
    public ResponseEntity<Map<String, Object>> updatePassword(HttpServletRequest request){
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			String type = request.getParameter("type");
			String password = request.getParameter("changedPassword");
			
			dataServiceObj.changePassword(type, password);
			
			response.put("result", "success");
			response.put("message", "Purged");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
    } 
	
	@CrossOrigin
	@RequestMapping("/fetchReport")
	public ResponseEntity fetchReportToSys(HttpServletRequest request)
	{
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		logger.debug("FETCH REPORT");
		String startTime = null, endTime = null, seprator = null;
        try {
            startTime = request.getParameter("START_TIME");
            endTime = request.getParameter("STOP_TIME");
            seprator = Integer.parseInt(request.getParameter("REPORT_TYPE")) == 1 ? "," : "\t";
            
            String filePath = reportServer.createReport(startTime, endTime, seprator);
            File file = new File(filePath);
            String fileName = file.getName();
            System.out.println(fileName+"   "+filePath);

            return createFileAttachmentResponse(filePath);
			
        } catch (Exception e) {
        	logger.error(e.toString()); 
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
        }	
       
	}
	
	@CrossOrigin
	@RequestMapping("/dbBackup")
	public ResponseEntity dbBackup(HttpServletRequest request)
	{
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		logger.debug("DB BACKUP");
		
        try {
        	if(!scheduleDb) {
        	Runnable runnable = new Runnable(){
                public void run(){
                	try {
                		reportServer.createDbBackup(autoDirectory);
                	}catch(Exception e) {
                		logger.error(e.getMessage());
                	}
                }
            };
            Thread reportThread = new Thread(runnable);
            reportThread.start();
            
        	response.put("result", "success");
			response.put("message", "ok");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
			}
        	else {
        		response.put("result", "fail");
    			response.put("message", "Scheduled Backup Already running. Please try after some time.");
    			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
    			return responseEntity;
        	}
        	/*if(DBDataService.currentEventName==null) {
        		List<DeviceInfo> device = dataServiceObj.getDevicesList();
        		String ip_add = null;
        		
				if(device.size()>0) {
					ip_add = device.get(0).getIp_add();
	        		dataServiceObj.sendStopCommandToHummer(false);
				}*/
	            
				/*String filePath = reportServer.createDbBackup();
	            File file = new File(filePath);
	            String fileName = file.getName();
	            System.out.println(fileName+"   "+filePath);
		            
		        /*if(device.size()>0 && ip_add!=null) {
		        	dataServiceObj.startSps(ip_add);
		        }*/
		        
		        //return createFileAttachmentResponse(filePath);
			/*}
        	else {
				throw new Exception("Please first stop any running operation");
			}*/
			
        } catch (Exception e) {
        	logger.error(e.toString()); 
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
        }	
	}
	
	@CrossOrigin
	@RequestMapping("/sysLog")
	public ResponseEntity getSystemLog(HttpServletRequest request)
	{
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		logger.debug("System Logs");
		
        try {   
				String filePath = reportServer.createSystemLogs();
	            File file = new File(filePath);
	            String fileName = file.getName();
	            System.out.println(fileName+"   "+filePath);
		        
		        return createFileAttachmentResponse(filePath);
			
        } catch (Exception e) {
        	logger.error(e.toString()); 
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
        }	
	}
	
	/*public void recordPeakInfo()
	{
		//Need to check if there's any use of it from web server prospective
	}*/
	
	
	@CrossOrigin
	@RequestMapping("/factoryReset")
	public ResponseEntity<Map<String, Object>> factoryResetDevice(HttpServletRequest request)
	{
		//System.out.println("URL HIT! factoryResetDevice");
		logger.info("URL HIT! factoryResetDevice");
		
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			if(DBDataService.currentEventName==null) {
				List<DeviceInfo> device = dataServiceObj.getDevicesList();
				if(device.size()>0) {
					String ip_add = device.get(0).getIp_add();
					dataServiceObj.factoryResetDevice(ip_add);
		
					response.put("result", "success");
					response.put("message", "ok");
					responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
					return responseEntity;
				}
				else {
					throw new Exception("Please first add SPS Deivce");
				}
			}
			else {
				throw new Exception("Please first stop any running operation");
			}
		}catch (Exception e) {
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	//From ugs to trgl event trigger. No response required
	@CrossOrigin
	@PostMapping("/Trigger")
	public ResponseEntity<Map<String, Object>> UGSTrigger(HttpServletRequest request, @RequestBody String data)
	{

		System.out.println("URL HIT! UGSTrigger");
		logger.info("URL HIT! UGSTrigger");
		
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			logger.warn(data);
			JSONObject revicedJsonData = new JSONObject(data);
			String transId = String.valueOf(revicedJsonData.getInt("TRANS_ID"));
			String timestamp = revicedJsonData.getString("TIME_STAMP") ;
			String latitude = String.valueOf(revicedJsonData.getDouble("LATITUDE"));
			String longitude = String.valueOf(revicedJsonData.getDouble("LONGITUDE"));
			int signalStrength = revicedJsonData.getInt("SIGNAL_STRENGTH");
			int eventType = revicedJsonData.getInt("EVENT_TYPE");
			String classification = revicedJsonData.getString("CLASSIFICATION") ;
			int timeout = revicedJsonData.getInt("TIME_OUT");
			
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			LocalDateTime dateTime = LocalDateTime.parse(timestamp, formatter);
			/*AuditLog auditLog = new AuditLog();
			auditLog.setLogType("Cue");
			auditLog.setDescription("Recieve\tOxfam\t"+transId+"\t"+latitude+"\t"+longitude+"\t"+timeout+"\t"+validity+"\t");*/
			if(timeout>0 && DBDataService.systemType.equals("integrated") && !DBDataService.isFreeze && !DBDataService.isCalibrating) {
				dataServiceObj.createAuditlog("Recieve Cue", "Recieve\tEvent:"+DBDataService.ugsEvent+"\tTrans ID:"+transId+"\tLatitude:"+latitude+"\tLongitude:"+longitude+"\tTimeout:"+timeout);
			
				dataServiceObj.addToQueue(transId, DBDataService.ugsEvent, latitude, longitude, timeout, timeout, dateTime);//DBDataService.validity, dateTime);
			}
			response.put("result", "success");
			response.put("message", "ok");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	
	}
	
	//Get device inventory details from trgl to cms. Response required 
	@CrossOrigin
	@GetMapping("/GET_SUB_INVENTORY")
	public String getDeviceInventroyDetail(HttpServletRequest request)
	{
		logger.debug("Get_device_inventory_details:");
        JSONArray deviceInfo = new JSONArray();
        
        //JSONObject sendDeviceInfo = new JSONObject();
        LinkedHashMap<String, String> inventoryObj = null;
        
        try {
        	Iterator<NodeData> itr = dataServiceObj.checkNodeStatus().iterator();
        	if(!itr.hasNext()) {
        		inventoryObj = new LinkedHashMap<String, String>(); 
        		inventoryObj.put("Status","Inventory Not Available");
        		deviceInfo.put(inventoryObj);
        	}
        	while(itr.hasNext()) {
        		NodeData nd = itr.next();
        		inventoryObj = new LinkedHashMap<String, String>();
        		
        		inventoryObj.put("Name",nd.getType());
        		inventoryObj.put("IP",nd.getIp_add());                
                inventoryObj.put("Status",nd.getStatus());
                
                deviceInfo.put(inventoryObj);
        	}
        } catch (Exception e) {
            logger.error(e.toString());
            inventoryObj = new LinkedHashMap<String, String>(); 
            inventoryObj.put("Status","Inventory Not Available");
            deviceInfo.put(inventoryObj);
        }
        
        /*try {
			sendDeviceInfo.put("DeviceInfo", deviceInfo);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
        
        return deviceInfo.toString();
	}
	
	//Get inventory details from trgl to cms. Response required 
	@CrossOrigin
	@GetMapping("/GET_INVENTORY_DETAILS")
	public String getInventroyDetail(HttpServletRequest request)
	{
		logger.debug("Get_inventory_details:");
        InetAddress localhost=null;
        JSONObject inventoryObj=new JSONObject();
        JSONArray deviceInfo = new JSONArray();
        
        JSONObject sendDeviceInfo = new JSONObject();
        
        try {
            localhost = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        String deviceIp=localhost.getHostAddress().trim();
        String lat="";
        String lon="";
        String angleOffset="";
        try {
        	Iterator<DeviceInfo> itr = dataServiceObj.getDevicesList().iterator();
        	while(itr.hasNext()) {
        		DeviceInfo d = itr.next();
        		lat = d.getLat();
        		lon = d.getLon();
        		PTZData ptzInfo= dataServiceObj.getPTZInfo(d.getIp_add());
        		angleOffset = String.valueOf(Integer.parseInt(ptzInfo.getPtz_offset()) + DBDataService.ptz_roll_offset);
        	}
            inventoryObj.put("DEVICE_IP",deviceIp);
            inventoryObj.put("LATITUDE",lat);
            inventoryObj.put("LONGITUDE",lon);
            inventoryObj.put("COVERAGE","2.5");
            inventoryObj.put("OFFSET",angleOffset);
            inventoryObj.put("STATE","1");
            
            deviceInfo.put(inventoryObj);
        } catch (JSONException e) {
            logger.error(e.toString());
        }
        
        try {
			sendDeviceInfo.put("DeviceInfo", deviceInfo);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return sendDeviceInfo.toString();
	}
	
	//Get status details from trgl to cms. Response required 
	@CrossOrigin
	@GetMapping("/GET_STATUS")
	public String getStatus(HttpServletRequest request)
	{
		logger.debug("get_status");
        JSONObject status = new JSONObject();
        
        String mode = DBDataService.currentEventName!=null?DBDataService.currentEventName:"Idle";
        System.out.println(mode);
        
        /*File file = new File(System.getProperty("user.dir"));
    	long totalSpace = file.getTotalSpace();
    	long usableSpace = file.getUsableSpace();
    	int used = 100 - (int)((usableSpace*100)/totalSpace);

        String space = String.valueOf(used);
        
        int val = Integer.parseInt(env.getProperty("freeze"));
        String freeze = used>val?"1":"0";*/
        
        String freeze = DBDataService.isFreeze?"1":"0";
        
        try {
        	status.put("MODE", mode);
            status.put("SPACE", DBDataService.usedSpace+"%");
            status.put("FREEZE", freeze);
        } catch (JSONException e) {
            logger.error(e.toString());
        }

        return status.toString();
	}
	
	@CrossOrigin
	@GetMapping("/RESTART")
	public String getRestart(HttpServletRequest request)
	{
		logger.debug("Restart Device:");
        JSONObject obj = new JSONObject();

        String shutdownCmd;
        
        //add restart command
        if(operatingSystem.contains("win")) {
        	shutdownCmd = "shutdown -r";
        }
        else{
        	shutdownCmd = "reboot";
        }
        try {
			Process child = Runtime.getRuntime().exec(shutdownCmd);
			obj.put("Restart","1");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			try {
				obj.put("Restart","0");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
        
        return obj.toString();
	}
	
	
	
	@CrossOrigin
	@PostMapping("/REPLAY")
	public String getReplayRequestFromFinley(HttpServletRequest request, @RequestBody String data)
	{
		System.out.println("Replay Request Recieved" + data);
		String startTime="";
		String endTime="";
		try {
            JSONObject revicedJsonData = new JSONObject(data);
            startTime = revicedJsonData.getString("START_TIME");
            endTime = revicedJsonData.getString("STOP_TIME");
        } catch (Exception e) {
        	logger.error(e.toString());        
        }
		
		JSONArray replayRecords = dataServiceObj.sendReplayToFinley(startTime, endTime);
		System.out.println(replayRecords.toString());
		return replayRecords.toString();
	}
	
	
	
	
	//Fetch report from trgl and send to cms. Response required
	@CrossOrigin
	@RequestMapping(value = "/FETCH_REPORT", method = RequestMethod.POST, produces = "application/zip")
	public ResponseEntity fetchReport(HttpServletRequest request, @RequestBody String data)
	{
		logger.debug("FETCH REPORT");
		String startTime = null,endTime = null,seprator = null;
        try {
            JSONObject revicedJsonData = new JSONObject(data);
            startTime = revicedJsonData.getString("START_TIME");
            endTime = revicedJsonData.getString("STOP_TIME");
            seprator = revicedJsonData.getInt("REPORT_TYPE") == 1 ? "," : "\t";
        } catch (Exception e) {
        	logger.error(e.toString());        
        }	


        String filePath = reportServer.createReport(startTime, endTime, seprator);
        File file = new File(filePath);
        String fileName = file.getName();
        System.out.println(fileName+"   "+filePath);

        /*ResponseBuilder responseBuilder = Response.ok((Object) file);
        responseBuilder.header("Content-Disposition", "attachment; filename=\""+fileName+"\"");
        return responseBuilder.build();*/
        
        //response.setHeader("Content-Disposition", "attachment; filename=\""+fileName+"\"");
        return createFileAttachmentResponse(filePath);
	
	}
	
	//Fetch report from trgl and send to cms. Response required
	@CrossOrigin
	//@PostMapping("/EVENT_REPORT")
	@RequestMapping(value = "/EVENT_REPORT", method = RequestMethod.POST, produces = "application/json")
	public String eventReport(HttpServletRequest request, @RequestBody String data)
	{
		logger.info("URL HIT! EVENT_Report Trigger");
		String startTime = null,endTime = null;
		boolean all = false;
        try {
            JSONObject recievedJsonData = new JSONObject(data);
            startTime = recievedJsonData.getString("START_TIME");
            endTime = recievedJsonData.getString("STOP_TIME");
            all = recievedJsonData.getInt("TYPE") == 1 ? true : false;
        } catch (Exception e) {
            logger.error(e.toString());
        }
		
        JSONArray events = reportServer.createEventReport(startTime, endTime, all);
        
        /*ResponseBuilder responseBuilder = Response.ok((Object) events);
        responseBuilder.header("Content-Disposition", "attachment; filename=\""+events+"\"");
        return responseBuilder.build();*/
        //return Response.status(201).entity(events.toString()).build();
        return events.toString();
	
	}
	
	//CMS will request C2 for acknowledge a specific alarm. Response required
	@CrossOrigin
	@PostMapping("/ACK_ALARM")
	public String acknowledgeEvent(HttpServletRequest request, @RequestBody String data)
	{

		logger.info("URL HIT! ACK_ALARM Trigger");
		JSONObject result = new JSONObject();
		
		try {
			//add functionality
			JSONObject revicedJsonData = new JSONObject(data);
			String deviceIp = revicedJsonData.getString("DEVICE_ID");
			int transId = revicedJsonData.getInt("TRANS_ID");
			int action = revicedJsonData.getInt("ACTION");
			
			int res=1;
			
			result.put("RESULT", res==1?"Success":"Fail");
			
		}catch (Exception e) {
			logger.error(e.toString());
		}
		
		return result.toString();
	}
	@CrossOrigin
	@PostMapping("/tmdas/ChangeMode")
	public ResponseEntity<Map<String, Object>> FalconChangeModeTrigger(HttpServletRequest request, @RequestBody String data)
	{
		logger.info("URL HIT! TMDAS ChangeMode");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
		JSONObject revicedJsonData = new JSONObject(data);
		String rd = revicedJsonData.getString("radiating");
		dataServiceObj.cahngeConfigData(configData1,rd);
		response.put("result", "success");
		response.put("message", "ok");
		responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
		return responseEntity;
		}catch (Exception e) {
		logger.error(e.toString());
		response.put("result", "fail");
		response.put("message", e.getMessage());
		responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		return responseEntity;
		}
		
	
		}
	
	
	//From falcon to trgl event trigger. No response required
	@CrossOrigin
	@PostMapping("/tmdas/TRIGGER")
	public ResponseEntity<Map<String, Object>> FalconTrigger(HttpServletRequest request, @RequestBody String data)
	{
		logger.info("URL HIT! TMDAS Trigger");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			if(DBDataService.systemType.equals("integrated") && !DBDataService.isFreeze && !DBDataService.isCalibrating) {
				JSONObject revicedJsonData = new JSONObject(data);
				
				String timestamp = revicedJsonData.getString("TIME_STAMP");
	            String transId = revicedJsonData.getString("TRANS_ID");
	            String cueId = revicedJsonData.getString("CUE_ID");
	            String ip = revicedJsonData.getString("DEVICE_IP");
	            String frequency = revicedJsonData.getString("FREQ");
	            double bandwidth = revicedJsonData.getDouble("BANDWIDTH");
	            String angle = revicedJsonData.getString("ANGLE");
	            String sector = revicedJsonData.getString("SECTOR");
	            String eventType = revicedJsonData.getString("EVENT_TYPE");
	            int timeout = revicedJsonData.getInt("TIME_OUT");
	            int validity = revicedJsonData.getInt("VALIDITY");
	            
	            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime dateTime = LocalDateTime.parse(timestamp, formatter);
				//dataServiceObj.ugsTriggerInfo(latitude, longitude);
				/*AuditLog auditLog = new AuditLog();
				auditLog.setLogType("Cue");
				auditLog.setDescription("Recieve\tFalcon\t"+transId+"\t"+sector+"\t"+frequency+"\t"+timeout+"\t"+validity+"\t");*/
				dataServiceObj.createAuditlog("Recieve Cue", "Recieve\tEvent:"+DBDataService.falconEvent+"\tTrans ID:"+transId+"\tSector:"+sector+"\tFrequency:"+frequency+"\tBandwidth:"+bandwidth+"\tTimeout:"+timeout+"\tValidity:"+validity);
				
				dataServiceObj.addToQueue(cueId, DBDataService.falconEvent, frequency, bandwidth, angle, sector, timeout, validity, dateTime);
				logger.info(data);
			}
			response.put("result", "success");
			response.put("message", "ok");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	//From falcon to trgl event trigger. No response required
	@CrossOrigin
	@PostMapping("/tmdas/Switch")
	public ResponseEntity<Map<String, Object>> FalconSwitch(HttpServletRequest request, @RequestBody String data)
	{
		logger.info("URL HIT! TMDAS Switch Info");
		ResponseEntity<Map<String, Object>> responseEntity;
		Map<String, Object> response = new HashMap<>();
		
		try {
			JSONObject revicedJsonData = new JSONObject(data);
			
            int s1 = revicedJsonData.getInt("S1");
            int s2 = revicedJsonData.getInt("S2");
            int s3 = revicedJsonData.getInt("S3");
            int ov = revicedJsonData.getInt("OV1");
            int oh = revicedJsonData.getInt("OH1");
            
            //DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
			//LocalDateTime dateTime = LocalDateTime.parse(timestamp, formatter);
			//dataServiceObj.ugsTriggerInfo(latitude, longitude);
			/*AuditLog auditLog = new AuditLog();
			auditLog.setLogType("Cue");
			auditLog.setDescription("Recieve\tFalcon\t"+transId+"\t"+sector+"\t"+frequency+"\t"+timeout+"\t"+validity+"\t");*/
			//dataServiceObj.createAuditlog("Cue", "Recieve\tFalcon\t"+transId+"\t"+sector+"\t"+frequency+"\t"+timeout+"\t"+validity);
			
			dataServiceObj.switchTo(s1, s2, s3, ov, oh);
			logger.info(data);
			
			response.put("result", "success");
			response.put("message", "ok");
			responseEntity = new ResponseEntity<Map<String, Object>>(response,HttpStatus.OK);
			return responseEntity;
		}catch (Exception e) {
			logger.error(e.toString());
			response.put("result", "fail");
			response.put("message", e.getMessage());
			responseEntity = new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
			return responseEntity;
		}
	}
	
	
	@PostConstruct
    public void createCache()
    {
        logger.info("inside method : createCache" );
        logger.info("Clearing events..........");



        logger.info("done");

        logger.info("starting udpserver at port "+env.getProperty("app.udpport")+"..........");

        UdpServer us = new 
        UdpServer(Integer.parseInt(env.getProperty("app.udpport")));
        applicationContext.getAutowireCapableBeanFactory().autowireBean(us);
        us.start();

        logger.info("done");

        logger.info("Exiting method : createCache" );
    }
	
	
	@GetMapping("/bms/prop")
	public HashMap<String,String> eventReport(@QueryParam("ip") String ip)
	{
		return bmss.findBMSProperties(ip);
	}
	
	
	@RequestMapping("/fault")
	public List<AlarmsData> getFaultData()
    {
		return as.getFaultData();
    }
	
	
	@RequestMapping(value="/ackFault")
	public  int acknowledgeFault(@QueryParam("id") Long id)
    {
		return as.acknowledgeAlarm(id);
    }
	
	
	@RequestMapping(value="/clearFault")
	public  int clearFault(@QueryParam("id") Long id)
    {
		return as.clearAlarm(id);
    }
	
	@CrossOrigin
	@GetMapping(value="/powerStatus")
	public  ArrayList<HashMap<String,String>> getBMSNodesStatus(HttpServletRequest request)
    {
		ArrayList<NodesInfo> nodes = ns.findNode("BMS");
		ArrayList<HashMap<String,String>> bmsData= new ArrayList<HashMap<String,String>>();
		//LinkedHashMap<String,String> hs = new LinkedHashMap<String,String>();
		
		if(nodes.size()>0) {
			for(int i=0;i<nodes.size();i++) 
			{
				String ip = nodes.get(i).getIpAdd();
				
				LinkedHashMap<String,String> hs = new LinkedHashMap<String,String>();
				
				hs.put("IP",ip);
				hs.put("Name","BMS");
				
				try 
				{
					String result = executeUdpBMS(ip,"6","GET_STATUS");
					
					String[] resultData = result.split(",");
					
					if(resultData[2].equals("0") || resultData[2].equals("0*")) 
					{
					
						String[] data = getResultData(result).split(",");
						
						hs.put("Voltage(V)", String.format("%.1f", Double.parseDouble(data[15])/1000));
						hs.put("SOC(%)", data[17]);
						hs.put("Current(A)", String.format("%.2f", Double.parseDouble(data[16])/1000));
						hs.put("Wattage(W)", String.format("%.1f", Double.parseDouble(data[15])*Double.parseDouble(data[16])/1000000));
						
					}
					else 
					{
						throw (new Exception());
					}
				}
				catch(Exception e) 
				{
					hs.put("Voltage", "N/A");
					hs.put("Current", "N/A");
					hs.put("Wattage", "N/A");
				}
				
				bmsData.add(hs);
			}
		}
		else {
			LinkedHashMap<String,String> hs = new LinkedHashMap<String,String>();
			hs.put("Result", "BMS not added");
			bmsData.add(hs);
		}
		return bmsData;
    }
	
	 public ResponseEntity createFileAttachmentResponse(String filepath)
	    {

	        File ff = new File(filepath);
	        InputStreamResource resource1 = null;
	        try {
	            resource1 = new InputStreamResource(new FileInputStream(ff));
	        } catch (Exception e) {
	            // TODO: handle exception
	        }

	        HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", ff.getName()));
	        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
	        headers.add("Pragma", "no-cache");
	        headers.add("Expires", "0");

	        return ResponseEntity.ok().header("filename", ff.getName())
	                .headers(headers)
	                .contentLength(ff.length())
	                .contentType(MediaType.parseMediaType("application/octet-stream"))
	                .body(resource1);
	    }

	
}