package in.vnl.sms.conveters;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import in.vnl.sms.model.RolePojo;
import in.vnl.sms.model.UserEntity;
import in.vnl.sms.model.UserUpdatePojo;
import in.vnl.sms.repository.UserRepository;
import in.vnl.sms.service.RoleService;



@Component
public class UserUpdatePojoToUserEntity implements Converter<UserUpdatePojo, UserEntity> {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleService roleService;

	@Autowired
	private RolePojoToRoleEntity rolePojoToRoleEntityConveter;

	@Override
	public UserEntity convert(UserUpdatePojo userPojo) {
		try {
			UserEntity userEntity = userRepository.findByUsername(userPojo.getUsername()).get();
			userEntity.setActive(userPojo.getActive());

			userEntity.setUsername(userPojo.getUsername());
			userEntity.setEmail(userPojo.getEmail());
			userEntity.setFirstName(userPojo.getFirstName());
			userEntity.setLastName(userPojo.getLastName());
			userEntity.setMobile(userPojo.getMobile());

			for (Long roleId : userPojo.getRoles()) {

				RolePojo rolePojo = roleService.getRole(roleId);
				userEntity.addRole(rolePojoToRoleEntityConveter.convert(rolePojo));
			}
			return userEntity;

		}

		catch (Exception exception) {
			throw exception;
		}

	}

}
