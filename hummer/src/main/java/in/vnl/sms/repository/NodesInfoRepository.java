package in.vnl.sms.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


import in.vnl.sms.model.BMSData;
import in.vnl.sms.model.NodesInfo;



public interface NodesInfoRepository extends CrudRepository<NodesInfo, Integer> 
{
	@Query(value="select n from NodesInfo n where n.name=:name")
	ArrayList<NodesInfo> findNodesByUseType(String name);
	
	@Query(value="select n from NodesInfo n")
	ArrayList<NodesInfo> getNodes();
	
}