package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import in.vnl.sms.model.AlarmsData;



@Repository
public interface AlarmsDataRepository extends CrudRepository<AlarmsData,Long>
{
	
	 @Override
	 List<AlarmsData> findAll();
	
}
