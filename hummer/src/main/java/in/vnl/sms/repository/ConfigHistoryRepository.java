package in.vnl.sms.repository;

import org.springframework.data.repository.CrudRepository;

import in.vnl.sms.model.ConfigDataHistory;

public interface ConfigHistoryRepository extends CrudRepository<ConfigDataHistory, Long> {

}
