package in.vnl.sms.repository;


import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.SystemConfigurationData;

public interface SystemConfigurationRepository extends CrudRepository<SystemConfigurationData, Integer> 
{
	@Query("select d from SystemConfigurationData d where d.profile=:profile")
	SystemConfigurationData getSystemConfigurationData(@Param("profile") String profile);
	
	@Modifying
	@Query("delete from SystemConfigurationData d where d.profile=:profile")
	void deleteSystemConfigurationDataListByProfile(@Param("profile") String profile);
	
	@Query("select d.profile from SystemConfigurationData d where d.profile<>:profile")
	List<String> getProfileData(@Param("profile") String profile);
}
