package in.vnl.sms.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.UserSetting;

public interface UserSettingRepository extends CrudRepository<UserSetting, Long>
{
	@Query("select us from UserSetting us where us.type=:type")
	UserSetting getDataByType(@Param("type") String type);
	
	@Query("select us from UserSetting us")
	UserSetting getData();
	
	/*@Modifying
	@Query(value="update UserSetting us SET us.value=:value where us.property=:property")
	void updateDataByProperty(@Param("property") String property, @Param("value") String value);*/
}
