package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.DeviceInfo;



public interface DeviceInfoRepository extends CrudRepository<DeviceInfo, Long> {

	@Query("select d from DeviceInfo d")
	List<DeviceInfo> getDeviceInfo();
	
	@Query("select d from DeviceInfo d where d.ip_add=:ip_add")
	DeviceInfo getDataByIP(@Param("ip_add") String ip_add);
	
	@Query("select d from DeviceInfo d where d.name=:name")
	DeviceInfo getDataIP(@Param("name") String name);
	
	@Modifying
	@Query(value = "update DeviceInfo d SET d.is_active=:is_active, d.starttime=:starttime, d.state=:state where d.ip_add=:ip_add")
	void updateActiveStatus(@Param("is_active") String is_active, @Param("ip_add") String ip_add, @Param("starttime") String starttime, @Param("state") String state);
	
	@Modifying
	@Query(value = "update DeviceInfo d SET d.is_active=:is_active, d.starttime=:starttime where d.ip_add=:ip_add")
	void updateActiveStatusOnly(@Param("is_active") String is_active, @Param("ip_add") String ip_add, @Param("starttime") String starttime);
	
	@Modifying
	@Query(value = "update DeviceInfo d SET d.is_active=:is_active where d.ip_add=:ip_add")
	void updateActiveOnly(@Param("is_active") String is_active, @Param("ip_add") String ip_add);
	
	@Modifying
	@Query(value = "update DeviceInfo d SET d.is_active=:is_active, d.state=:state where d.ip_add=:ip_add")
	void updateActiveWithoutTime(@Param("is_active") String is_active, @Param("ip_add") String ip_add, @Param("state") String state);
	
	@Modifying
	@Query(value="update DeviceInfo d SET d.state=:state where d.ip_add=:ip_add")
	void updateDeviceState(@Param("ip_add") String ip_add, @Param("state") String state);
	
	@Modifying
	@Query(value="update DeviceInfo d SET d.lat=:lat, d.lon=:lon where d.ip_add=:ip_add")
	void updateGPSData(@Param("ip_add") String ip_add, @Param("lat") String lat, @Param("lon") String lon);
	
	@Query("select d from DeviceInfo d where d.id=:id")
	DeviceInfo getDataByID(@Param("id") Long id);
	
	@Modifying
	@Query(value = "update DeviceInfo d SET d.is_active=:is_active")
	void updateIsActive(@Param("is_active") String is_active);
}
