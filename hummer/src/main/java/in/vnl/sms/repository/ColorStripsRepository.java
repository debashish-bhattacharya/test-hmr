package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import in.vnl.sms.model.ColorStripsData;

public interface ColorStripsRepository extends CrudRepository<ColorStripsData, Integer> 
{
	@Query("select d from ColorStripsData d where d.profile=:profile ORDER BY d.id ASC")
	List<ColorStripsData> getColorCodeList(@Param("profile") String profile);
	
	@Query("select d from ColorStripsData d where d.visible=:visible and d.profile=:profile ORDER BY d.id ASC")
	List<ColorStripsData> getColorCodeListByVisiblity(@Param("visible") boolean visible, @Param("profile") String profile);
	
	@Modifying
	@Query("delete from ColorStripsData d where (d.visible=:visible or d.id>:id) and d.profile=:profile")
	void deleteColorCodeListByVisiblity(@Param("visible") boolean visible, @Param("id") int id, @Param("profile") String profile);
	
	@Query("select d from ColorStripsData d where (d.visible=:visible or d.id>:id) and d.profile=:profile ORDER BY d.id ASC")
	List<ColorStripsData> getColorCodeListByVisiblityAndId(@Param("visible") boolean visible, @Param("id") int id, @Param("profile") String profile);
	
}
