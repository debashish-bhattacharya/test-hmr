package in.vnl.sms.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import in.vnl.sms.model.PTZData;



public interface PTZRepository extends CrudRepository<PTZData, Long> {

	@Query("select pt from PTZData pt where pt.device_ip=:device_ip")
	PTZData getPTZInfo(@Param("device_ip") String device_ip);
	
	@Query("select pt from PTZData pt where pt.ptz_ip=:ptz_ip")
	PTZData getPTZInfoByIP(@Param("ptz_ip") String ptz_ip);
	
	@Query("select pt from PTZData pt")
	PTZData getPTZList();
	
	@Modifying
	@Transactional
	@Query(value= "update PTZData pt set pt.ptz_offset=:ptz_offset, pt.sector_offset=:sector_offset where pt.device_ip=:current_ip")
	void updateOffset(@Param("current_ip") String current_ip, @Param("ptz_offset") String ptz_offset, @Param("sector_offset") String sector_offset);
}
