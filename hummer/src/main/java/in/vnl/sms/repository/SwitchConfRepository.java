package in.vnl.sms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import in.vnl.sms.model.SwitchConf;

public interface SwitchConfRepository extends CrudRepository<SwitchConf, Integer> 
{
	@Query("select s.val from SwitchConf s where (s.sid=:sid1 and (s.pathid=:path11 or s.pathid=:path12)) or (s.sid=:sid2 and (s.pathid=:path21 or s.pathid=:path22)) or (s.sid=:sid3 and (s.pathid=:path31 or s.pathid=:path32)) or (s.sid=:sid4 and (s.pathid=:path41 or s.pathid=:path42)) ORDER BY s.seq desc")
	List<String> getSwitchConf(int sid1,int path11, int path12, int sid2,int path21, int path22,
								int sid3,int path31, int path32, int sid4,int path41, int path42);
}
