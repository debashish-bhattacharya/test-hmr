package in.vnl.sms.service;

import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import in.vnl.sms.model.NodesInfo;



@Service
public class Common {

	
	public static HashMap<String,String> nodes = new HashMap<String,String>();
	public static HashMap<String,String> nodeIpMap = new HashMap<String, String>();
	
	Logger logger = LoggerFactory.getLogger(Common.class);
	
	@Autowired
	private NodesInfoService nis;
	
	public Common() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	@PostConstruct
    public void initializeNodeCache()
    {
        List<NodesInfo> nds = nis.getNodesInfo();
        nodes.clear();
        nodeIpMap.clear();
        
        logger.error("Inside initializeNodeCache");
        
        for(int i=0;i<nds.size();i++)
        {
        	NodesInfo node = nds.get(i);
            nodes.put(node.getIpAdd(), node.getName());
            nodeIpMap.put(node.getName(), node.getIpAdd());
        }
    }
	
	
}
