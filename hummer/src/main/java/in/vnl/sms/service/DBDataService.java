package in.vnl.sms.service;

import static java.lang.Math.PI;
import static java.lang.Math.asin;
import static java.lang.Math.atan2;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toDegrees;
import static java.lang.Math.toRadians;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;

import javax.transaction.Transactional;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientProperties;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import in.vnl.sms.CommUtils.CircularQueue;
import in.vnl.sms.model.AlarmInfo;
import in.vnl.sms.model.AuditLog;
import in.vnl.sms.model.ColorStripsData;
import in.vnl.sms.model.ConfigData;
import in.vnl.sms.model.ConfigProfileData;
import in.vnl.sms.model.DeviceDataInfo;
import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.Devices;
import in.vnl.sms.model.Event;
import in.vnl.sms.model.EventData;
import in.vnl.sms.model.EventDataComparator;
import in.vnl.sms.model.FreqAutoInfo;
import in.vnl.sms.model.FreqPowMinThresData;
import in.vnl.sms.model.GpsData;
import in.vnl.sms.model.JMData;
import in.vnl.sms.model.JMDeviceData;
import in.vnl.sms.model.JMDeviceDataHistory;
import in.vnl.sms.model.JMNodeMapping;
import in.vnl.sms.model.LEDData;
import in.vnl.sms.model.MaskingScreenData;
import in.vnl.sms.model.MaskingScreenDataComparator;
import in.vnl.sms.model.NodeData;
import in.vnl.sms.model.PTZData;
import in.vnl.sms.model.PTZPositionData;
import in.vnl.sms.model.PeakDetectionInfo;
import in.vnl.sms.model.PeakInfoPojo;
import in.vnl.sms.model.Priority;
import in.vnl.sms.model.SensorConfig;
import in.vnl.sms.model.SystemConfigurationData;
import in.vnl.sms.model.TaskPriority;
import in.vnl.sms.model.TriggerData;
import in.vnl.sms.model.UserEntity;
import in.vnl.sms.model.UserSetting;
import in.vnl.sms.report.ReportServer;
import in.vnl.sms.repository.AlarmRepository;
import in.vnl.sms.repository.AuditLogRepository;
import in.vnl.sms.repository.BMSRepository;
import in.vnl.sms.repository.ColorStripsRepository;
import in.vnl.sms.repository.ConfigHistoryRepository;
import in.vnl.sms.repository.ConfigProfileRepository;
import in.vnl.sms.repository.ConfigRepository;
import in.vnl.sms.repository.DataRepository;
import in.vnl.sms.repository.DeviceInfoRepository;
import in.vnl.sms.repository.FreqPowMinThresRepository;
import in.vnl.sms.repository.GpsDataRepository;
import in.vnl.sms.repository.JMRepository;
import in.vnl.sms.repository.JmDeviceDataHistoryRepository;
import in.vnl.sms.repository.JmDeviceDataRepository;
import in.vnl.sms.repository.JmNodeMappingRepository;
import in.vnl.sms.repository.LEDRepository;
import in.vnl.sms.repository.MaskingScreenRepository;
import in.vnl.sms.repository.NodeRepository;
import in.vnl.sms.repository.PTZPositionRepository;
import in.vnl.sms.repository.PTZRepository;
import in.vnl.sms.repository.PeakInfoRepository;
import in.vnl.sms.repository.PeakJMRepository;
import in.vnl.sms.repository.PriorityRepository;
import in.vnl.sms.repository.SensorConfigRepository;
import in.vnl.sms.repository.SwitchConfRepository;
import in.vnl.sms.repository.SystemConfigurationRepository;
import in.vnl.sms.repository.TaskPriorityRepository;
import in.vnl.sms.repository.UserRepository;
import in.vnl.sms.repository.UserSettingRepository;
import in.vnl.sms.udpAdapter.ConfigThread;
import in.vnl.sms.udpAdapter.PTZPacketSender;
import in.vnl.sms.udpAdapter.PacketListener;
import in.vnl.sms.udpAdapter.TCPPacketListener;
import in.vnl.sms.udpAdapter.TriggerThread2;
import in.vnl.sms.udpAdapter.UdpPacketSender;

@Service
@PropertySource("classpath:application.properties")
public class DBDataService {

	/*
	 * public DBDataServiceImpl() { super();
	 * System.out.println("inside DBDataService ------------------------"); }
	 */
	@Value("${enviroment_record_save}")
	public boolean env_record_save;
	@Value("${udp_port}")
	int udp_port;
	@Value("${retry_interval}")
	int retry_interval;
	@Value("${retries}")
	int retries;
	@Value("${commProtocol}")
	String commProtocol;
	@Value("${ptz_angle_rotation}")
	int ptz_angle_rotation;
	@Value("${ptz_tcp_port}")
	int ptz_tcp_port;
	@Value("${ptz_udp_port}")
	int ptz_udp_port;
	@Value("${switch_udp_port}")
	int switch_udp_port;
	@Value("${finleyUrl}")
	String finleyUrl;
	@Value("${falconUrl}")
	String falconUrl;
	@Value("${finleyOffsetUrl}")
	String finleyOffsetUrl;
	@Value("${falconOffsetUrl}")
	String falconOffsetUrl;
	@Value("${ptz_room}")
	int ptz_room;
	@Value("${ptz_speed}")
	int ptzSpeed;
	@Value("${bandwidth}")
	double bandwidth;
	// @Value("${ptz_roll_offset}")
	// int ptz_roll_offset;
	@Value("${ptz_tilt_offset}")
	int ptz_tilt_offset;
	@Value("${tiltAngleProperty}")
	int tiltAngleProperty;
	@Value("${maximizeall}")
	boolean maximizeAll;
	@Value("${jmvalid}")
	boolean jmValid;
	@Value("${finleyPort}")
	String finleyPort;
	@Value("${falconPort}")
	String falconPort;
	// @Value("${selfValidity}")
	// int selfValidity;
	@Value("${maxSamples}")
	int maxSamples;
	@Value("${jmRetries}")
	int jmRetries;
	@Value("${jmTimeout}")
	int jmTimeout;
	@Value("${jmDiff}")
	int jmDiff;
	@Value("${rfCableLength}")
	int rfCableLength;
	@Value("${freeze}")
	int freeze;
	@Value("${defaultBlBandwidth}")
	double defaultBlBandwidth;
	@Value("${ptzThreshold}")
	int ptzThreshold;
	@Value("${twogBandwidth}")
	double twogBandwidth;
	@Value("${threegBandwidth}")
	double threegBandwidth;
	@Value("${fourgBandwidth}")
	double fourgBandwidth;
	@Value("${wifiBandwidth}")
	double wifiBandwidth;
	@Value("${sys_default_threshold}")
	int sys_default_threshold;
	@Value("${sys_hsm_threshold}")
	int sys_hsm_threshold;
	@Value("${sys_normal_threshold}")
	int sys_normal_threshold;

	DatagramSocket serverSocket;

	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	@Autowired
	private DataRepository dr;
	@Autowired
	private PeakInfoRepository pr;
	@Autowired
	private PeakJMRepository pjmr;
	@Autowired
	private ConfigRepository cr;
	@Autowired
	private LEDRepository lr;
	@Autowired
	private DeviceInfoRepository dinfo;
	@Autowired
	private JMRepository jmRepo;
	@Autowired
	private PTZRepository ptzr;
	@Autowired
	private ConfigHistoryRepository chr;
	@Autowired
	private TaskPriorityRepository tpRepo;
	@Autowired
	DBDataService dbserviceObj;
	@Autowired
	private PTZPositionRepository ptzpr;
	@Autowired
	PacketListener pListener;
	@Autowired
	TCPPacketListener tcpListener;
	@Autowired
	private MaskingScreenRepository mstScr;
	@Autowired
	private PriorityRepository prior;
	@Autowired
	private ColorStripsRepository cStr;
	@Autowired
	private SystemConfigurationRepository scr;
	@Autowired
	private AuditLogRepository alr;
	@Autowired
	private AlarmRepository ar;
	@Autowired
	private NodeRepository nr;
	@Autowired
	private JmNodeMappingRepository jmnmr;
	@Autowired
	private JmDeviceDataRepository jmdr;
	@Autowired
	private JmDeviceDataHistoryRepository jmdhr;
	@Autowired
	private SwitchConfRepository scfr;
	@Autowired
	private BMSRepository bms;
	@Autowired
	private FreqPowMinThresRepository fpmt;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ConfigProfileRepository cpr;
	@Autowired
	private UserSettingRepository userSettingRepository;
	@Autowired
	private GpsDataRepository gpsDataRepository;

	@Autowired
	private DevicesService ds;
	@Autowired
	private Common cm;
	@Autowired
	private ReportServer rps;
	@Autowired
	private SensorConfigRepository sc;
	@Autowired
	private DatabaseService databaseService;
	public static StringBuilder bandconfig;
	Logger logger = LoggerFactory.getLogger(DBDataService.class);

	CircularQueue<PeakDetectionInfo> peakCacheBl = new CircularQueue<>(200);
	CircularQueue<PeakDetectionInfo> peakCacheOther = new CircularQueue<>(200);
	public Map<String, PeakDetectionInfo> falconData = new HashMap<String, PeakDetectionInfo>();
	public List<String> falconDataCache = new ArrayList<String>();
	public static List<FreqPowMinThresData> freqPowMinThres = new ArrayList<FreqPowMinThresData>();

	// for sectored. Need to change to a single ds
	CircularQueue<Double> antenna30Power = new CircularQueue<>(200);
	CircularQueue<Double> antenna30Freq = new CircularQueue<>(200);

	CircularQueue<Double> antenna90Power = new CircularQueue<>(200);
	CircularQueue<Double> antenna90Freq = new CircularQueue<>(200);

	CircularQueue<Double> antenna150Power = new CircularQueue<>(200);
	CircularQueue<Double> antenna150Freq = new CircularQueue<>(200);

	PriorityBlockingQueue<EventData> priorityQueue = new PriorityBlockingQueue<EventData>(5, new EventDataComparator());
//	List<PeakDetectionInfo> recordPeak = new ArrayList<PeakDetectionInfo>();
//	List<PeakDetectionInfo> recordPtzPeak = new ArrayList<PeakDetectionInfo>();
	List<ColorStripsData> twogUplinkFreq = new ArrayList<ColorStripsData>();
	List<ColorStripsData> threegUplinkFreq = new ArrayList<ColorStripsData>();
	List<ColorStripsData> fourgUplinkFreq = new ArrayList<ColorStripsData>();
	List<ColorStripsData> wifiUplinkFreq = new ArrayList<ColorStripsData>();

	List<ColorStripsData> emitterPowerMap = new ArrayList<ColorStripsData>();
	Map<String, String> sectorMapping = new HashMap<String, String>();
	Map<String, Double> sensorConfigSectorAngle = new HashMap<String, Double>();

	List<String> recordPeak = new ArrayList<String>();
	List<String> recordPtzPeak = new ArrayList<String>();

	public static HashMap<Integer, String> nodeIdMap = new HashMap<Integer, String>();

	String prevTime;
	JMData jmDataTemp;
	private static int scanTemp = 1;
	public static boolean rotatePTZ = false;
	public static int ptzOffset = 0;
	public static int ptz_roll_offset = 0;

	public static int smmStartFreq = 0;
	public static int smmStopFreq = 0;
	public static double rbwBandwidth = 3.2;

	public static boolean isUserTriggeredEvent = false;
	public static double frequencyForUserEvent;

	public static boolean isUGSTriggeredEvent = false;
	public static double frequencyForUGSEvent;

	public static double frequencyForFalconEvent;

	public static boolean isBlacklistTriggeredEvent = false;
	public static double frequencyForBlacklistEvent;

	public static boolean isAutoTriggeredEvent = false;
	public static double frequencyForAutoEvent;
	public static boolean maximization = false;
	public static double frequencyForPTZEvent;
	public static double startFreqForPTZEvent;
	public static double stopFreqForPTZEvent;
	public static int startAngle;
	public static int stopAngle;
	public static EventData eventData;

	public static Map<String, Integer> switchSetting = new HashMap<String, Integer>();
	public static Map<String, Integer> priority = new HashMap<String, Integer>();
	public static Map<String, JMDeviceDataHistory> jmData = new HashMap<String, JMDeviceDataHistory>();
	public static Map<Integer, String> jmNodeMap = new HashMap<Integer, String>();
	public static Set<MaskingScreenData> blackListFreq = new TreeSet<MaskingScreenData>(
			new MaskingScreenDataComparator());
	public static List<MaskingScreenData> whiteListFreq = new ArrayList<MaskingScreenData>();

	public static boolean isManualOverride = false;
	public static boolean isTmdasEnabled = false;
	public static boolean isAutoEnabled = false;
	public static int timeout = 0;
	public static int validity = 0;
	public static double selfTimeout = 0;
	public static int selfValidity = 0;
	public static int usedSpace = 0;
	public static boolean isCalibrating = false;
	public boolean ledon = false;
	public static boolean ptzReachable = false;
	public static int rbwMaxSamples;

	public static boolean isFreeze = false;

	public static int rffeCableLength = 0;

	public static int antennaToVehicleDiffAngle = 361;

	public static double gpsAccuracy;
	public static String aplliedband;
	public static String switchVal;
	public static String checkBSF;
	EventData previousBlEventData = null;
	EventData previousOtherEventData = null;
	public static String previousMode = "";
	public static String manualEvent = "MANUAL";
	public static String ugsEvent = "OXFAM";
	public static String falconEvent = "FALCON";
	public static String blEvent = "BLACKLIST";
	public static String autoEvent = "AUTO";

	public static String defaultProfile = "default";

	public static String systemType, systemMode, systemGps;
	//public static String Opr_mode;

	public static int systemType_ID = 0; // System_Type:(enum 0- Standalone , 1-Hummer Only , 2 Integrated )

	public Thread oprThread;

	public boolean eventValid;
	
	public static boolean changesAllowed=true;

	public int oxfamEventCount = 0;

	public Object synch = new Object();

	public static String antennaIdPort;
	
	public static String systemConfiguration;

	// public static String finleyUrl = "/finley/event";
	// public static String falconUrl = "/trgl/TRIGGER";

	// public static String finleyIp = "";
	// public static String falconIp = "";
	private final Object wait = new Object();
	public static String currentEventName = null;

	Instant ptzStartTime = Instant.now();

	Double powerTemp = -200d;
	AlarmInfo maximizeAlarmInfo;
	List<List<Double>> distanceMapTemp = new ArrayList<List<Double>>();
	double emitterPower = 30;

	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	Future future;
	ExecutorService executorTimerService;
	private String eventType;

	public void checkDependency() {
		System.out.println("Autowiring happening ");
	}

	/*
	 * static { //get all values from db Intervals intrvl = new Intervals(); intrvl.
	 * }
	 */
	/*
	 * public DBDataService() throws SocketException { // TODO Auto-generated
	 * constructor stub serverSocket = new DatagramSocket(udp_port);
	 * 
	 * }
	 */

	/*
	 * @Transactional public void storeData(TriggerData configObj) {
	 * dr.save(configObj);
	 * 
	 * }
	 */

	/*
	 * @Transactional public Map<String, String> getTriggerData(String ipAdd) {
	 * TriggerData td = dr.getTriggerData(ipAdd); HashMap<String, String>
	 * triggerDataMap = new HashMap<>(); triggerDataMap.put("Client",
	 * td.getClient()); triggerDataMap.put("IPAdd", td.getIpAdd());
	 * triggerDataMap.put("Location", td.getLocation());
	 * triggerDataMap.put("Mfg_date", td.getMfg_date()); triggerDataMap.put("Model",
	 * td.getModel()); triggerDataMap.put("Product_name", td.getProduct_name());
	 * triggerDataMap.put("unit_no", td.getunit_no()); triggerDataMap.put("country",
	 * td.getCountry());
	 * 
	 * return triggerDataMap; }
	 */

	/*
	 * public DBDataServiceImpl(DataRepository dr) { super(); this.dr = dr; }
	 */

	@Transactional
	public TriggerData getTriggerData(String ip_add) {
		// return new TriggerData("100","121", "121", "121", "121", "121", "121",
		// "121");
		return dr.getTriggerData(ip_add);

	}

	@Transactional
	public List<ConfigData> getConfigData(String ip_add) {
		// return new TriggerData("100","121", "121", "121", "121", "121", "121",
		// "121");
		return cr.getConfigData(ip_add, "" + ptz_room);

	}
	@Transactional
	public List<ConfigData> getConfigDatalist(String ip_add) {
		// return new TriggerData("100","121", "121", "121", "121", "121", "121",
		// "121");
		return cr.getConfigDatalist(ip_add);

	}
	@Transactional
	public ConfigData getConfigDataByID(Long device_id) {
		DeviceInfo dinfo1 = dinfo.getDataByID(device_id);
		return cr.getConfigDataByID(dinfo1);
	}

	@Transactional
	public ConfigData getConfigDataByID(Long device_id, String room) {
		DeviceInfo dinfo1 = dinfo.getDataByID(device_id);
		return cr.getConfigDataByID(dinfo1, room);
	}

	/*----Sahil----*/
	@Transactional
	public List<SensorConfig> getSensorConfig() {
		return sc.getSensorConfig();

	}

	@Transactional
	public JSONArray getRfl() throws ClassNotFoundException, SQLException, JSONException {
		String qurey="select distinct value from reflevel_calb where status='0'";
		
		JSONArray calibrationData=databaseService.getDbData(qurey);
		return calibrationData;
	}
	@Transactional
	public String getNumberOfSensors() {
		return sc.getNumberOfSensors();
	}

	@Transactional
	public List<SensorConfig> getUserSensorConfig() {
		return sc.getUserSensorConfig();
	}

	@Transactional
	public void updateSensorConfig(SensorConfig sensorData) throws Exception {
		sc.save(sensorData);
		sendSensorConfig();
	}

	@Transactional
	public void editSensorConfig(SensorConfig sensorData) throws Exception {

		String prevSector = null;

		Double prevAngle = 0.0;
		Double prevTempAngle = 0.0;

		// Saving The Sensor Number And Its Port As String In currentSensorNumPort
		int currentSensorNum = sensorData.getSensornum();
		int currentSensorPort = sensorData.getSensorport();
		String currentSensorNumPort = Integer.toString(currentSensorNum) + Integer.toString(currentSensorPort);

		int prevSensorPort;
		int prevSensorNum;
		String prevSensorNumPort;
		boolean currentPrev = false;

		// Saving The Previous Data In HashMap sensorConfigSectorAngle
		List<Object[]> prevData = sc.getPreviousData();
		for (int i = 0; i < prevData.size(); i++) {

			prevSector = (String) prevData.get(i)[0];
			prevAngle = (Double) prevData.get(i)[1];
			sensorConfigSectorAngle.put(prevSector, prevAngle);

			prevSensorNum = (Integer) prevData.get(i)[2];
			prevSensorPort = (Integer) prevData.get(i)[3];
			prevSensorNumPort = Integer.toString(prevSensorNum) + Integer.toString(prevSensorPort);
			if (currentSensorNumPort.equals(prevSensorNumPort)) {
				currentPrev = true;
				prevTempAngle = (Double) prevData.get(i)[1];

			}
		}

		// If Trying To Update The Existing Data Then Directly Save Other Wise Check
		// Further Conditions
		if (currentPrev) {
			sensorData.setAngle(prevTempAngle);
			sc.save(sensorData);
			sendSensorConfig();
		}

		else {
			// If STRU already exists and current Sector Is STRU The Throw An Exception
			if (sensorConfigSectorAngle.containsKey("STRU") && sensorData.getSector().equals("STRU") && !currentPrev) {
				System.out.println("You Cannot Insert More Then 1 STRU");
				throw new Exception("You Cannot Insert More Then 1 STRU");
			}

			// Trying To Get The Maximum Sector From DB
			Iterator<Map.Entry<String, Double>> itr = sensorConfigSectorAngle.entrySet().iterator();
			String maxSector = "0";
			Double maxAngle = 5000.0;
			Integer shouldBeSector = 0;
			Double shouldBeAngle = 0.0;

			/* Calculating Which Should Be The Current Sector */
			while (itr.hasNext()) {
				Map.Entry<String, Double> entry = itr.next();
				if (!entry.getKey().equals("STRU")) {
					if (entry.getKey().compareTo(maxSector) > 0) {
						maxSector = entry.getKey();
						maxAngle = entry.getValue();
					}
				}
			}

			/* In Case Of STRU Angle Should Always Be 0 */
			if (sensorData.getSector().equals("STRU")) {
				shouldBeAngle = 0.0;
				sensorData.setAngle(shouldBeAngle);
			}

			/*
			 * If There Is No Record Then Default max Angle Will Be 5000 And Therefore We
			 * Will Make First Angle As BeamWidth/2 Other Wise BeamWidth/2 Will Be Added To
			 * Max Angle
			 */
			else {
				shouldBeSector = Integer.parseInt(maxSector) + 1;
				if (maxAngle != 5000.0) {
					shouldBeAngle = maxAngle + sensorData.getBeamwidth();
					sensorData.setAngle(shouldBeAngle);
				}

				else {
					shouldBeAngle = sensorData.getBeamwidth() / 2;
					sensorData.setAngle(shouldBeAngle);
				}
			}

			if (!shouldBeSector.toString().equals(sensorData.getSector()) && !sensorData.getSector().equals("STRU")
					&& shouldBeSector < 4) {
				throw new Exception("Please Select The Sector" + shouldBeSector);
			}

			else {

				sc.save(sensorData);
				sendSensorConfig();
			}
		}
	}

	/*----Sahil----*/

	@Transactional
	public ConfigData getConfigDataByRoom(String room) {
		// DeviceInfo dinfo1 = dinfo.getDataByID(device_id);
		return cr.getConfigDataByRoom(room);
	}
	


	@Transactional
	public void recordConfigData(List<ConfigData> cData, Long device_id, String antennaNum, String preAmpTypePath)
			throws Exception {

		serverSocket = pListener.getSocketInstance();

		DeviceInfo dinfo1 = dinfo.getDataByID(device_id);
		String ip_addr = dinfo1.getIp_add();

		Iterator<ConfigData> iterator = cData.iterator();
		ConfigData configData = new ConfigData();
		while (iterator.hasNext()) {
			configData = iterator.next();
			configData.setIp_add(ip_addr);
			configData.setDevice_id(dinfo1);
		}

		if (!systemType.equals("standalone")) // systemType.equals("integrated") changed here
			cData = setupData(configData, false, antennaNum, preAmpTypePath,rotatePTZ);

		else
			cData = setupStandaloneData(configData, true, antennaNum, preAmpTypePath);

		TriggerData tData = dr.getTriggerData(dinfo1.getIp_add());

		if (commProtocol.equalsIgnoreCase("tcp")) {
			// Add implementation in case used in future
			// tcpListener.sendTriggerData(dinfo1.getIp_add(), cData);
			// cr.save(cData);
		} else if (commProtocol.equalsIgnoreCase("udp")) {
			ExecutorService es = Executors.newFixedThreadPool(1);
			Future<Boolean> f = es.submit(new TriggerThread2(ip_addr, serverSocket, tData, retry_interval, retries));
			try {
				System.out.println(
						"DBDataService: Value Returned ---------------------------------------------------------"
								+ f.get());
				if (f.get()) {
					systemConfiguration="Configuartion change by User Trigger";
					Thread configThread = (new Thread(
							new ConfigThread(ip_addr, serverSocket, cData,this, retry_interval, retries, maxSamples)));
					configThread.setName(ip_addr + "SaveUIConfig");
					// configThNamesList.add(receivePacket.getAddress() + "Config");
					configThread.start();

					// set Calibration to zero in db after applying setting
					if (cData.get(0).getCalibration().equals("1")) {
						configData.setCalibration("0");
						if (!systemType.equals("standalone"))
							setupData(configData, false, antennaNum, preAmpTypePath,rotatePTZ);
						else
							setupStandaloneData(configData, true, antennaNum, preAmpTypePath);
					}

					// not required now as switch is not present
					// sendConfigurationToSwitch();

//					Iterator<ConfigData> iterator1 = cData.iterator();
//					while(iterator1.hasNext()) {
//						cr.save(iterator1.next());
//					}
					// sajal15-12-18//cr.saveAll(cData);
				} else {
					throw new Exception();
				}
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				throw e2;
			} catch (ExecutionException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				throw e2;
			}
		}

		/*
		 * Thread triggerThread = (new Thread(new TriggerThread(cData.getIp_add(),
		 * serverSocket, tData,retry_interval)));
		 * triggerThread.setName(cData.getIp_add() + "SaveUITrigger");
		 * //triggerThNamesList.add(receivePacket.getAddress() + "Trigger");
		 * triggerThread.start();
		 * 
		 * try { System.out.println("Wait for Trigger Ack.");
		 * triggerThread.wait(retires*retry_interval); }catch (InterruptedException e) {
		 * System.out.
		 * println("DBDataService: Trigger ACK received. Send Config Data to save");
		 * 
		 * Thread configThread = (new Thread(new ConfigThread(cData.getIp_add(),
		 * serverSocket, cData,retry_interval))); configThread.setName(cData.getIp_add()
		 * + "SaveUIConfig"); //configThNamesList.add(receivePacket.getAddress() +
		 * "Config"); configThread.start(); try { configThread.join(); } catch
		 * (InterruptedException e1) { System.out.
		 * println("DBDataService: Config save ACK received. Saving Data Received from UI"
		 * ); //cr.save(cData); //Send Success }
		 */
		// }

		// Send Fail
	}

	@Transactional
	public void cahngeConfigData(List<ConfigData> cData,String rd) throws ClassNotFoundException, SQLException, JSONException {
		logger.debug("In cahngeConfigData");
		DeviceInfo dinfo1 = dinfo.getDataIP("SPS");
		ArrayList<String> al=new ArrayList<String>();
		String ip_addr = dinfo1.getIp_add();
		ConfigData config=new ConfigData ();
		
		String ipaddr = dinfo1.getIp_add();
		cData =getConfigDatalist(ipaddr);
		if(rd.equalsIgnoreCase("yes")) {
		JSONArray oprMode=databaseService.getBandDataList("select ref_level from config_data where ip_add='"+ip_addr+"'");
		
		try {
			String revicedJsonData=oprMode.getJSONObject(0).getString("ref_level");
			previousMode=revicedJsonData;
			if(revicedJsonData.equalsIgnoreCase("l3")) {
				sendCurrentMode("Normal");
				Iterator<ConfigData> iterator = cData.iterator();
				ConfigData configData = new ConfigData();
				while (iterator.hasNext()) {
					configData = iterator.next();
					
					configData.setRef_level("l2");;
					
					cr.save(configData);
				}
				
					//cData = setupData(configData, false, antennaum, preAmpTypepath);
				
				//String qurey="Update config_data set ref_level ='l2' where ip_add='"+ip_addr+"'";
				
				/*al.add(qurey);
				databaseService.executeBatchInsertion(al);*/
				}
			} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
		
			}else
				{
			if(previousMode.equalsIgnoreCase("l3")) {
				sendCurrentMode("High Senstivity");
				Iterator<ConfigData> iterator = cData.iterator();
				ConfigData configData = new ConfigData();
				while (iterator.hasNext()) {
					configData = iterator.next();
					configData.setRef_level("l3");
					cr.save(configData);
				}
				
					}
			
				}
			systemConfiguration="Configuartion change by System Trigger-Falcon";
			Thread configThread = (new Thread(
				new ConfigThread(ip_addr, serverSocket, cData,this, retry_interval, retries, maxSamples)));
			configThread.setName(ip_addr + "SaveUIConfig");
			
			configThread.start();
			logger.debug("Exit cahngeConfigData");
	}
	
	
	public void cahngeBySpsConfigData(String rd) throws ClassNotFoundException, SQLException, JSONException {
		logger.debug("In cahngeBySpsConfigData");
		Iterator<ConfigData> iterator = cr.findAll().iterator();
		ConfigData configData = new ConfigData();
		if(rd.equals("Normal")) {
		sendCurrentMode("Normal");		
			while (iterator.hasNext()) {
			configData = iterator.next();
			configData.setRef_level("l2");
			cr.save(configData);
		}
		}else
		{
			sendCurrentMode("Default");		
				while (iterator.hasNext()) {
				configData = iterator.next();
				configData.setRef_level("l1");
				cr.save(configData);
			}
	}
		createAuditlog("Configuartion change by Default Trigger-SPS",configData.toString());
		logger.debug("Exit cahngeBySpsConfigData");
	}
	
	
	public void sendConfigurationToSwitch() {

		List<String> preAmpValues = cr.getPreAmpValues();
		Integer preAmpAnt1 = Integer.parseInt(preAmpValues.get(0)) % 10;
		Integer preAmpAnt2 = Integer.parseInt(preAmpValues.get(1)) % 10;
		Integer preAmpAnt3 = Integer.parseInt(preAmpValues.get(2)) % 10;
		Integer preAmpAnt4 = Integer.parseInt(preAmpValues.get(3)) % 10;
		// System.out.println("Hello Sahil" + preAmpAnt1 + "Sahil" + preAmpAnt2 +
		// "Sahil" + preAmpAnt3 + "Sahil" + preAmpAnt4);
		switchConfiguration(1, preAmpAnt1, 5, 2, preAmpAnt2, 5, 3, preAmpAnt3, 5, 4, preAmpAnt4, 5);

	}

	public static int calculateMaxThreshold(int startFreq, int stopFreq) {
		int startThreshold = 0;
		int stopThreshold = 0;
		double thres = 0;
		int rbw = 0;
		int frequency = stopFreq - startFreq;

		Iterator<FreqPowMinThresData> itr = freqPowMinThres.iterator();
		while (itr.hasNext()) {
			FreqPowMinThresData fp = itr.next();
			if (startFreq >= fp.getStartfreq() && startFreq <= fp.getStopfreq()) {
				startThreshold = fp.getPower();
			}
			if (stopFreq >= fp.getStartfreq() && stopFreq <= fp.getStopfreq()) {
				stopThreshold = fp.getPower();
			}
		}

		rbw = frequency > 4 ? 10 : 1;
		thres = startThreshold > stopThreshold ? startThreshold : stopThreshold;

		int threshold = (int) (thres + 10 * Math.log10(frequency * 1000000.0)
				+ 10 * Math.log10(rbw / (frequency * 1000.0)) + 10);

		// thres = (int)(thres+(10*Math.log10(frequency*1000/rbw))+10)-1;
		System.out.println(threshold);

		return threshold;
	}

	@Transactional
	public void sendConfigDataForPtz(double freq, String threshold, String preamp, int maxPower) throws Exception {

		serverSocket = pListener.getSocketInstance();
		int bndwdth = (int) bandwidth / 2;
		logger.debug("In sendConfigDataForPtz bndwdth"+bndwdth);
		ConfigData configData = cr.getConfigDataByRoom("" + ptz_room);
		String ip_addr = configData.getIp_add();

		int startFreq = (int) freq - (int) bndwdth;
		int stopFreq = (int) freq + (int) bndwdth;
		//double startFreq = startFreqForPTZEvent;
		//double stopFreq = stopFreqForPTZEvent;

		if (startFreq < 0) {
			startFreq = 0;
			stopFreq = (int) freq + (int) freq;
		}
		if (stopFreq > 6000) {
			stopFreq = 6000;
			startFreq = (int) freq - (int) (6000 - freq);

		}
		configData.setStart_freq(String.valueOf(startFreq));
		configData.setStop_freq(String.valueOf(stopFreq));
		JSONArray oprMode=databaseService.getBandDataList("select ref_level from config_data where ip_add='"+ip_addr+"'");
		String revicedJsonData=oprMode.getJSONObject(0).getString("ref_level");
		
		int thres = calculateMaxThreshold(startFreq, stopFreq);
		if(revicedJsonData.equalsIgnoreCase("l3")) {
			thres=sys_hsm_threshold;
		}
		else if(revicedJsonData.equalsIgnoreCase("l2")) {
			thres=sys_normal_threshold;
		}
		else if(revicedJsonData.equalsIgnoreCase("l1")) {
			thres=sys_default_threshold;
		}
		//thres = sys_default_threshold;
		if (maxPower != 100) {
			maxPower = maxPower - ptzThreshold;
			thres = maxPower > thres ? maxPower : thres;
		}

		configData.setThreshold("" + thres);
		// configData.setThreshold("-90");

		// new switch code
		configData.setPreamp_type(preamp);

		if (threshold.equals("0"))
			configData.setThreshold(threshold);

		List<ConfigData> cData = new ArrayList<ConfigData>();
		cData.add(configData);

		cr.save(configData);

		TriggerData tData = dr.getTriggerData(ip_addr);

		if (commProtocol.equalsIgnoreCase("tcp")) {
			// Add implementation in case used in future
			// tcpListener.sendTriggerData(dinfo1.getIp_add(), cData);
			// cr.save(cData);
		} else if (commProtocol.equalsIgnoreCase("udp")) {
			ExecutorService es = Executors.newFixedThreadPool(1);
			Future<Boolean> f = es.submit(new TriggerThread2(ip_addr, serverSocket, tData, retry_interval, retries));
			try {
				System.out.println(
						"DBDataService: Value Returned ---------------------------------------------------------"
								+ f.get());
				if (f.get()) {
					Thread configThread = (new Thread(
							new ConfigThread(ip_addr, serverSocket, cData,this, retry_interval, retries, maxSamples)));
					configThread.setName(ip_addr + "SaveUIConfig");
					configThread.start();
					PTZPacketSender.current_angle = -1;
				} else {
					throw new Exception();
				}
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				throw e2;
			} catch (ExecutionException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				throw e2;
			}
		}
	}

	@Transactional
	public void sendConfigDataForSMM(int room, int path) throws Exception {

		serverSocket = pListener.getSocketInstance();

		ConfigData configData = cr.getConfigDataByRoom("" + room);
		String ip_addr = configData.getIp_add();
		configData.setPreamp_type("" + room + path);

		List<ConfigData> cData = new ArrayList<ConfigData>();
		cData.add(configData);

		cr.save(configData);

		TriggerData tData = dr.getTriggerData(ip_addr);

		if (commProtocol.equalsIgnoreCase("tcp")) {
			// Add implementation in case used in future
			// tcpListener.sendTriggerData(dinfo1.getIp_add(), cData);
			// cr.save(cData);
		} else if (commProtocol.equalsIgnoreCase("udp")) {
			ExecutorService es = Executors.newFixedThreadPool(1);
			Future<Boolean> f = es.submit(new TriggerThread2(ip_addr, serverSocket, tData, retry_interval, retries));
			try {
				System.out.println(
						"DBDataService: Value Returned ---------------------------------------------------------"
								+ f.get());
				if (f.get()) {
					Thread configThread = (new Thread(
							new ConfigThread(ip_addr, serverSocket, cData,this, retry_interval, retries, maxSamples)));
					configThread.setName(ip_addr + "SaveUIConfig");
					configThread.start();
				} else {
					throw new Exception();
				}
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				throw e2;
			} catch (ExecutionException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				throw e2;
			}
		}
	}

	@Transactional
	public void sendStopCommandToHummer(boolean isPTZ) throws Exception {

		serverSocket = pListener.getSocketInstance();
		String msg = "SMSStop";
		if (isPTZ) {
			msg = msg + "," + ptz_room + "1";
		} else {
			for (int i = 1; i < 5; i++) {
				if (i != ptz_room)
					msg = msg + "," + i + "1";
			}
		}

		ConfigData configData = cr.getConfigDataByRoom("" + ptz_room);
		String ip_addr = configData.getIp_add();

		TriggerData tData = dr.getTriggerData(ip_addr);

		if (commProtocol.equalsIgnoreCase("tcp")) {
			// Add implementation in case used in future
			// tcpListener.sendTriggerData(dinfo1.getIp_add(), cData);
			// cr.save(cData);
		} else if (commProtocol.equalsIgnoreCase("udp")) {
			ExecutorService es = Executors.newFixedThreadPool(1);
			Future<Boolean> f = es.submit(new TriggerThread2(ip_addr, serverSocket, tData, retry_interval, retries));
			try {
				System.out.println(
						"DBDataService: Value Returned ---------------------------------------------------------"
								+ f.get());
				if (f.get()) {
					Thread configThread = (new Thread(
							new ConfigThread(ip_addr, serverSocket, false, msg, retry_interval, 1)));
					configThread.setName(ip_addr + "StopUIConfig");
					configThread.start();
				} else {
					throw new Exception();
				}
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				throw e2;
			} catch (ExecutionException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
				throw e2;
			}
		}
	}

	public void sendStartCommandToHummer() {
		try {
			sendStopCommandToHummer(true);

			ConfigData cData = cr.getConfigDataByRoom("1");

			List<ConfigData> configData = new ArrayList<ConfigData>();
			configData.add(cData);
			recordConfigData(configData, cData.getDevice_id().getId(), "all", "1");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Transactional
	public List<String> getProfiles() {
		return scr.getProfileData(defaultProfile);
	}

	@Transactional
	public String getUsers() {
		// List<UserEntity> userEntity = new ArrayList<UserEntity>();
		String user = "";
		Iterator<UserEntity> userItr = userRepository.findAll().iterator();
		int i = 0;
		while (userItr.hasNext()) {
			// userEntity.add(userItr.next());
			if (i != 0)
				user = user + ",";
			user = user + userItr.next().getUsername();
			i++;
		}

		return user;
	}

	@Transactional
	public void changePassword(String type, String password) throws Exception {
		UserEntity user = userRepository.findByUsername(type).get();
		if (user == null) {
			/*
			 * user = new User(); user.setFirstName("Test"); user.setLastName("Test");
			 * user.setPassword(passwordEncoder.encode("test"));
			 * user.setEmail("test@test.com"); user.setEnabled(true);
			 * userRepository.save(user);
			 */
		} else {
			user.setPassword(new BCryptPasswordEncoder().encode(password));
			userRepository.save(user);
		}
	}

	@Transactional
	public void recordDevicePriority(TaskPriority tpData) throws Exception {
		tpRepo.save(tpData);
	}

	// @Transactional
	public void recordNodeData(String ip_add, String name, String status) {
		NodeData nodeData = new NodeData();
		nodeData.setIp_add(ip_add);
		nodeData.setType(name);
		nodeData.setStatus(status);
		nr.save(nodeData);

		cm.initializeNodeCache();
	}

	// @Transactional
	public void deleteNodeData(String ip_add) throws Exception {
		nr.delete(nr.getNodeByIP(ip_add));
		cm.initializeNodeCache();
	}

	@Transactional
	public void recordDeviceData(String ip_add, String name, String color, String lat, String lon, String is_active,
			String state, long id) {
		DeviceInfo dinfoNew = new DeviceInfo();
		dinfoNew.setIp_add(ip_add);
		dinfoNew.setName(name);
		dinfoNew.setColor(color);
		dinfoNew.setLat(lat);
		dinfoNew.setLon(lon);
		dinfoNew.setIs_active(is_active);
		dinfoNew.setState(state);
		if (id != 0)
			dinfoNew.setId(id);
		dinfo.save(dinfoNew);
		cm.initializeNodeCache();

	}

	@Transactional
	public void recordDeviceTriggerData(String ip_add, String name, String color, String lat, String lon,
			String is_active, String state, String unitno) {
		System.out.println("DBDataService: Save Trigger data");
		DeviceInfo dInfo = dinfo.getDataByIP(ip_add);
		TriggerData tData = new TriggerData();
		tData.setDevice_id(dInfo);
		tData.setIp_add(ip_add);
		tData.setUnit_no(unitno);
		tData.setProduct_name("SMS-IP");
		tData.setModel("TRGL");
		tData.setMfg_date("09/09/2018");
		tData.setClient("abc");
		tData.setLocation("GURGAON");
		tData.setCountry("091");
		dr.save(tData);
	}

	@Transactional
	public void deleteDeviceData(String ip_add) throws Exception {
		dinfo.delete(dinfo.getDataByIP(ip_add));
		cm.initializeNodeCache();
	}

	@Transactional
	public void recordDeviceConfigData(String ip_add, String name, String color, String lat, String lon,
			String is_active, String state) {
		DeviceInfo dInfo = dinfo.getDataByIP(ip_add);
		System.out.println("DBDataService: Save Trigger data");

		for (int i = 1; i < 5; i++) {
			ConfigData cData = new ConfigData();
			cData.setDevice_id(dInfo);
			cData.setIp_add(ip_add);
			cData.setFloor("1");
			cData.setRoom("" + i);
			cData.setCalibration("0");
			cData.setStart_freq("0");
			cData.setStop_freq("6000");
			cData.setThreshold("-70");
			cData.setMask_offset("0");
			cData.setUse_mask("0");
			cData.setStart_time("0000");
			cData.setStop_time("0000");
			cData.setCable_length("6");
			cData.setPreamp_type(i + "1");
			cData.setDistance("1500");
			cData.setTx_power("30");
			cData.setFade_margin("0");
			cData.setCalib_timer("5");
			cData.setGsm_dl("0");
			cData.setWcdma_dl("0");
			cData.setWifi_band("0");
			cData.setLte_dl("0");
			cData.setBand_start1("0");
			cData.setBand_stop1("0");
			cData.setBand_en1("0");
			cData.setBand_start2("0");
			cData.setBand_stop2("0");
			cData.setBand_en2("0");
			cData.setBand_start3("0");
			cData.setBand_stop3("0");
			cData.setBand_en3("0");
			cData.setBand_start4("0");
			cData.setBand_stop4("0");
			cData.setBand_en4("0");
			cData.setBand_start5("0");
			cData.setBand_stop5("0");
			cData.setBand_en5("0");
			cData.setCountry_code("091");
			cData.setRef_level("0");
			cData.setBand_specific("0");
			cData.setBand_filter("");
			if (i == ptz_room) {
				cData.setGsm_dl("1");
				cData.setWcdma_dl("1");
				cData.setWifi_band("1");
				cData.setLte_dl("1");
			}
			cr.save(cData);
			
			  AuditLog auditLog = new AuditLog();
			  auditLog.setLogType("Configuration Changed");
			  auditLog.setDescription(cData.toString()); alr.save(auditLog);
			 
			//createAuditlog("Configuration Changed", cData.toString());
		}

		// cr.save(cData);
		// cr.saveAll(c);
	}

	public List<ConfigData> setupData(ConfigData c, boolean ptz, String antennaNum, String preAmpTypePath,boolean chkpt) {
		List<ConfigData> configData = new ArrayList<ConfigData>();
		if (antennaNum.equals("all")) {
			for (int i = 1; i < 5; i++) {
				ConfigData cData = new ConfigData();
				cData.setDevice_id(c.getDevice_id());
				cData.setIp_add(c.getIp_add());
				cData.setFloor("1");
				cData.setRoom("" + i);
				cData.setCalibration(c.getCalibration());
				cData.setStart_freq(c.getStart_freq());
				cData.setStop_freq(c.getStop_freq());
				cData.setThreshold(c.getThreshold());
				cData.setMask_offset(c.getMask_offset());
				cData.setUse_mask(c.getUse_mask());
				cData.setStart_time(c.getStart_time());
				cData.setStop_time(c.getStop_time());
				cData.setCable_length(c.getCable_length());
				cData.setPreamp_type(i + preAmpTypePath);
				cData.setDistance(c.getDistance());
				cData.setTx_power(c.getTx_power());
				cData.setFade_margin(c.getFade_margin());
				cData.setCalib_timer(c.getCalib_timer());
				cData.setGsm_dl(c.getGsm_dl());
				cData.setWcdma_dl(c.getWcdma_dl());
				cData.setWifi_band(c.getWifi_band());
				cData.setLte_dl(c.getLte_dl());
				cData.setBand_start1(c.getBand_start1());
				cData.setBand_stop1(c.getBand_stop1());
				cData.setBand_en1(c.getBand_en1());
				cData.setBand_start2(c.getBand_start2());
				cData.setBand_stop2(c.getBand_stop2());
				cData.setBand_en2(c.getBand_en2());
				cData.setBand_start3(c.getBand_start3());
				cData.setBand_stop3(c.getBand_stop3());
				cData.setBand_en3(c.getBand_en3());
				cData.setBand_start4(c.getBand_start4());
				cData.setBand_stop4(c.getBand_stop4());
				cData.setBand_en4(c.getBand_en4());
				cData.setBand_start5(c.getBand_start5());
				cData.setBand_stop5(c.getBand_stop5());
				cData.setBand_en5(c.getBand_en5());
				cData.setCountry_code(c.getCountry_code());
				cData.setRef_level(c.getRef_level());
				cData.setBand_specific(c.getBand_specific());
				cData.setBand_filter(bandconfig.toString());
				
				if (i != ptz_room && !ptz)
					configData.add(cData);
				else if (i == ptz_room) {
					cData.setGsm_dl("1");
					cData.setWcdma_dl("1");
					cData.setWifi_band("1");
					cData.setLte_dl("1");
				  if(chkpt) {
					  configData.add(cData);
				  }
					if (ptz) {
						configData.add(cData);
					}
					else if(c.getCalibration().equals("1")) {
						configData.add(cData);
						
					}
				}
				cr.save(cData);
			}
		} else {
			ConfigData cData = new ConfigData();
			cData.setDevice_id(c.getDevice_id());
			cData.setIp_add(c.getIp_add());
			cData.setFloor("1");
			cData.setRoom("" + antennaNum);
			cData.setCalibration(c.getCalibration());
			cData.setStart_freq(c.getStart_freq());
			cData.setStop_freq(c.getStop_freq());
			cData.setThreshold(c.getThreshold());
			cData.setMask_offset(c.getMask_offset());
			cData.setUse_mask(c.getUse_mask());
			cData.setStart_time(c.getStart_time());
			cData.setStop_time(c.getStop_time());
			cData.setCable_length(c.getCable_length());
			cData.setPreamp_type(antennaNum + preAmpTypePath);
			cData.setDistance(c.getDistance());
			cData.setTx_power(c.getTx_power());
			cData.setFade_margin(c.getFade_margin());
			cData.setCalib_timer(c.getCalib_timer());
			cData.setGsm_dl(c.getGsm_dl());
			cData.setWcdma_dl(c.getWcdma_dl());
			cData.setWifi_band(c.getWifi_band());
			cData.setLte_dl(c.getLte_dl());
			cData.setBand_start1(c.getBand_start1());
			cData.setBand_stop1(c.getBand_stop1());
			cData.setBand_en1(c.getBand_en1());
			cData.setBand_start2(c.getBand_start2());
			cData.setBand_stop2(c.getBand_stop2());
			cData.setBand_en2(c.getBand_en2());
			cData.setBand_start3(c.getBand_start3());
			cData.setBand_stop3(c.getBand_stop3());
			cData.setBand_en3(c.getBand_en3());
			cData.setBand_start4(c.getBand_start4());
			cData.setBand_stop4(c.getBand_stop4());
			cData.setBand_en4(c.getBand_en4());
			cData.setBand_start5(c.getBand_start5());
			cData.setBand_stop5(c.getBand_stop5());
			cData.setBand_en5(c.getBand_en5());
			cData.setCountry_code(c.getCountry_code());
			cData.setRef_level(c.getRef_level());
			cData.setBand_specific(c.getBand_specific());
			cData.setBand_filter(bandconfig.toString());
			if (Integer.parseInt(antennaNum) != ptz_room && !ptz)
				configData.add(cData);
			else if (Integer.parseInt(antennaNum) == ptz_room) {
				cData.setGsm_dl("1");
				cData.setWcdma_dl("1");
				cData.setWifi_band("1");
				cData.setLte_dl("1");
				if (ptz) {
					configData.add(cData);
				}
			}
			cr.save(cData);
		}

		return configData;
		// cr.save(cData);
		// cr.saveAll(c);
	}

	public List<ConfigData> setupStandaloneData(ConfigData c, boolean ptz, String antennaNum, String preAmpTypePath) {
		List<ConfigData> configData = new ArrayList<ConfigData>();

		for (int i = 1; i < 5; i++) {
			ConfigData cData = new ConfigData();
			cData.setDevice_id(c.getDevice_id());
			cData.setIp_add(c.getIp_add());
			cData.setFloor("1");
			cData.setRoom("" + i);
			cData.setCalibration(c.getCalibration());
			cData.setStart_freq(c.getStart_freq());
			cData.setStop_freq(c.getStop_freq());
			cData.setThreshold(c.getThreshold());
			cData.setMask_offset(c.getMask_offset());
			cData.setUse_mask(c.getUse_mask());
			cData.setStart_time(c.getStart_time());
			cData.setStop_time(c.getStop_time());
			cData.setCable_length(c.getCable_length());
			cData.setPreamp_type(i + preAmpTypePath);
			cData.setDistance(c.getDistance());
			cData.setTx_power(c.getTx_power());
			cData.setFade_margin(c.getFade_margin());
			cData.setCalib_timer(c.getCalib_timer());
			cData.setGsm_dl(c.getGsm_dl());
			cData.setWcdma_dl(c.getWcdma_dl());
			cData.setWifi_band(c.getWifi_band());
			cData.setLte_dl(c.getLte_dl());
			cData.setBand_start1(c.getBand_start1());
			cData.setBand_stop1(c.getBand_stop1());
			cData.setBand_en1(c.getBand_en1());
			cData.setBand_start2(c.getBand_start2());
			cData.setBand_stop2(c.getBand_stop2());
			cData.setBand_en2(c.getBand_en2());
			cData.setBand_start3(c.getBand_start3());
			cData.setBand_stop3(c.getBand_stop3());
			cData.setBand_en3(c.getBand_en3());
			cData.setBand_start4(c.getBand_start4());
			cData.setBand_stop4(c.getBand_stop4());
			cData.setBand_en4(c.getBand_en4());
			cData.setBand_start5(c.getBand_start5());
			cData.setBand_stop5(c.getBand_stop5());
			cData.setBand_en5(c.getBand_en5());
			cData.setCountry_code(c.getCountry_code());
			cData.setRef_level(c.getRef_level());
			cData.setBand_specific(c.getBand_specific());
			cData.setBand_filter(bandconfig.toString());
			if (i != ptz_room && !ptz)
				configData.add(cData);
			else if (i == ptz_room) {
				if (ptz) {
					configData.add(cData);
				}
			}
			cr.save(cData);
		}

		return configData;
	}

	/*
	 * public void saveAuditLog(AuditLog auditLog) { alr.save(auditLog); }
	 */

	@Transactional
	public void recordJMData(String ip_add, String time, String angle) {
		System.out.println("DBDataService: Save JM Angle data");
		JMData jmData = new JMData();
		jmData.setIp_add(ip_add);
		jmData.setTime(time);
		jmData.setAngle(angle);
		// jmData.setLocaltime(LocalDateTime.now().toString());

		jmRepo.save(jmData);

		// NEW UPDATE: USE PTZ instead of JM for angle.
		// sendJMDataToSocket(jmData, time);
	}

	@Transactional
	public void recordJMGPSData(String ip_add, String time, double lat, double lon) {
		System.out.println("DBDataService: Save JM GPS data");
		JMData jmData = new JMData();
		jmData.setIp_add(ip_add);
		jmData.setTime(time);
		jmData.setLat(lat);
		jmData.setLon(lon);

		LocalDateTime now = LocalDateTime.now();
		/*
		 * DateTimeFormatter format =
		 * DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); String formatDateTime =
		 * now.format(format);
		 */
		jmData.setCreated_at(now);

		jmRepo.save(jmData);
	}

	@Transactional
	public void recordJMDateData(String ip_add, String time, String jmPacketDateTime) {
		System.out.println("DBDataService: Save JM Date Time data");
		JMData jmData = new JMData();
		jmData.setIp_add(ip_add);
		jmData.setTime(time);
		jmData.setJmpacketdatetime(jmPacketDateTime);
		// jmData.setLocaltime(LocalDateTime.now().toString());

		jmRepo.save(jmData);
	}

	public void sendJMDataToSocket(JMData jmData, String time) {
		/*
		 * if(prevTime==null) { jmDataTemp=jmData; prevTime=time; } else
		 * if(time.equals(prevTime)) { jmDataTemp = jmData; }else{
		 * messagingTemplate.convertAndSend("/topic/jmData", jmDataTemp);
		 * jmDataTemp=jmData; prevTime = time; }
		 */

		if (!time.equals(prevTime)) {
			if (prevTime != null)
				messagingTemplate.convertAndSend("/topic/jmData", jmDataTemp);

			jmDataTemp = jmData;
			prevTime = time;
		} else {
			jmDataTemp = jmData;
		}
	}

	@CrossOrigin
	@Transactional
	public void updateGPSDataAndLatLong(String current_ip, String lat, String lon) {
		dinfo.updateGPSData(current_ip, lat, lon);
		sendLatitudeLongitudeToSocket(lat, lon, ptzOffset);
	}

	@CrossOrigin
	@Transactional
	public void spsUpdate(String current_ip, String lat, String lon) {
		updateGPSDataAndLatLong(current_ip, lat, lon);
	}

	@CrossOrigin
	public void ptzUpdate(String current_ip, String ptz_offset, String sector_offset, String latitude,
			String longitude) {

		ptzr.updateOffset(current_ip, ptz_offset, sector_offset);
		ptzOffset = Integer.parseInt(ptz_offset);
		ptz_roll_offset = Integer.parseInt(sector_offset);
		createAuditlog("Configuration Changed", "North Offset : " + ptz_offset + "\tSector Offset : " + sector_offset);

		String smmIp = Common.nodeIpMap.get(nodeIdMap.get(3));
		DeviceInfo deviceInfo = dinfo.getDataByIP(smmIp);

		if (deviceInfo != null)
			sendLatitudeLongitudeToSocket(deviceInfo.getLat(), deviceInfo.getLon(), ptzOffset);

		// to check for correct latitude and longitude
		if (latitude.charAt(0) == '0' || longitude.charAt(0) == '0') {

		} else {
			updateGPSDataAndLatLong(smmIp, latitude, longitude);
		}

		String ip = Common.nodeIpMap.get(nodeIdMap.get(1));
		String falconIp = Common.nodeIpMap.get(nodeIdMap.get(4));

		logger.info("In ptz Update");
		if (ip != null)
			sendGetRequestToUrl("http://" + ip + ":" + finleyPort + finleyOffsetUrl);

		sendToFalconOffset(falconIp);
	}

	public void sendToFalconOffset(String ip) {

		String smmIp = Common.nodeIpMap.get(nodeIdMap.get(3));
		DeviceInfo deviceInfo = dinfo.getDataByIP(smmIp);

		String latitude = "0";
		String longitude = "0";

		if (deviceInfo != null) {
			latitude = deviceInfo.getLat();
			longitude = deviceInfo.getLon();
		}
		if (ip != null)
			sendOffset(ip, falconPort, falconOffsetUrl, ptzOffset + ptz_roll_offset, latitude, longitude);
	}

	@CrossOrigin
	public void performPtzOpr(String device_ip, int id, int value) throws IOException {
		if (id == 1) {
			PTZData ptzData = ptzr.getPTZInfo(device_ip);

			PTZPacketSender.getInstance().jumpToPositionPacket(ptzData.getPtz_ip(), ptz_tcp_port, 0, 0, true);

			PTZPacketSender.getInstance().jumpToPositionPacket(ptzData.getPtz_ip(), ptz_tcp_port, ptz_tilt_offset, 0,
					false);
		} else {
			PTZData ptzData = ptzr.getPTZInfo(device_ip);
			// Move left. For left: 0x04
			PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(), ptz_tcp_port, id, 0, value);
		}
	}

	@CrossOrigin
	public boolean rotateDevice(String device_ip, String startAngle, String stopAngle, int tiltAngleProperty,
			Double timeout, double maxPower) throws IOException {
		boolean running = false;
		logger.debug(" inside in rotateDevice" );
		if (oprThread != null && oprThread.isAlive() && oprThread.isInterrupted()) {
			stopRotateDeviceInterupted(device_ip);
			return false;
		}

		PTZData ptzData = ptzr.getPTZInfo(device_ip);
     
		emitterPower = calculateEmitterPower(frequencyForPTZEvent);
		

		if (currentEventName.equals(manualEvent))
			messagingTemplate.convertAndSend("/topic/ptzStarted", true);
		sendCurrentEvent(currentEventName);

		Double startingAngleDouble = Double.parseDouble(startAngle);
		int startingAngle = startingAngleDouble.intValue() - ptzOffset;
		
		logger.debug("startingAngle is" +startingAngle );
		try {
			/* String preamp = "41"; */
			String preamp = getStruAntennaIdPort();
			if (currentEventName.equals(manualEvent) && Common.nodeIpMap.containsKey(nodeIdMap.get(7))) {
				int val = Integer.parseInt(switchVal);
				switchConfiguration(1, 1, 5, 2, 1, 5, 3, 1, 5, 4, 1, Integer.parseInt(switchVal));
				val = val > 8 ? val - 5 : val - 1;
				preamp = String.valueOf(val) + "1";
			}
			maximization=true;
			sendConfigDataForPtz(frequencyForPTZEvent, "-1", preamp, (int) maxPower);
			//maximization=false;
		} catch (Exception e2) {
			// TODO Auto-generated catch block
			stopRotateDeviceInterupted(device_ip);
			logger.error(e2.getMessage());
			e2.printStackTrace();
			return false;
		}

		// commenting, since new interface command is now implemented
		/*
		 * int currentAngle =
		 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), 2000);
		 * 
		 * while(currentAngle!=startingAngle) { if(currentAngle > startingAngle) {
		 * //Move left. For left: 0x04
		 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,4,0,(
		 * currentAngle-startingAngle)); //PENDING--> NEED TO WAIT FOR PTZ TO MOVE
		 * 
		 * }else { //Move Right. For right: 0x02
		 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,2,0,(
		 * startingAngle-currentAngle)); //PENDING--> NEED TO WAIT FOR PTZ TO MOVE
		 * 
		 * } try { //Wait for PTZ to move to the required angle Thread.sleep(5000); }
		 * catch (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } currentAngle =
		 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), 2000);
		 * }//exit when ptz has reached the starting angle. Start scan now.
		 * 
		 */

		if (oprThread != null && oprThread.isAlive() && oprThread.isInterrupted()) {
			stopRotateDeviceInterupted(device_ip);
			return false;
		}

		try {
			/*
			 * //Testing int currentAngle =
			 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), 2000);
			 * System.out.println("Current Angle:------------------------------" +
			 * currentAngle); Thread.sleep(100); //move right
			 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,5,0,10);
			 * Thread.sleep(6000); currentAngle =
			 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), 2000);
			 * System.out.
			 * println("Current Angle:-----------------------move right 10-------" +
			 * currentAngle); Thread.sleep(6000); System.out.
			 * println("Current Angle:-----------------------move right 10-------" +
			 * currentAngle); //move left
			 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,4,0,5);
			 * currentAngle =
			 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), 2000);
			 * System.out.println("Current Angle:-----------------------move left 5-------"
			 * + currentAngle); Thread.sleep(100); //moveup
			 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,2,0,5); int
			 * currentTilt =
			 * PTZPacketSender.getInstance().getCurrentTilt(ptzData.getPtz_ip(), 2000);
			 * System.out.println("Current tilr:-----------------------move up 5-------" +
			 * currentTilt); Thread.sleep(100); //move down
			 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,7,0,5);
			 * currentTilt =
			 * PTZPacketSender.getInstance().getCurrentTilt(ptzData.getPtz_ip(), 2000);
			 * System.out.println("Current tilt:-----------------------move down 5-------" +
			 * currentTilt); Thread.sleep(100000); //Testing
			 */
			if (!currentEventName.equals(manualEvent)) {
				shutdownTimer();
				executorTimerService = Executors.newSingleThreadExecutor();

				future = executorTimerService.submit(new Runnable() {
					public void run() {
						try {
							logger.info("DBDataService: TIMER Started");
							logger.info("Timeout value : " + timeout);
							// Thread.sleep(timeout*60*1000);

							Thread.sleep((long) (timeout * 1000));
							logger.info("DBDataService: TIMER Expired");
							/*
							 * DBDataService ds = new DBDataService(); ds.timerExpired();
							 */

							List<DeviceInfo> devices = getDevicesList();
							List<String> allIPs = new ArrayList<String>();
							try {
								for (DeviceInfo device : devices) {
									allIPs.add(device.getIp_add());
								}

								stopRotateDevice(allIPs.get(0));
								logger.debug("method call is stopRotateDevice");
							} catch (Exception e) {// IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								logger.error(e.getMessage());
							}
						} catch (InterruptedException e) {
							logger.error("Interuppted in rotate device");
							// shutdownTimer();
							// stopRotateDeviceInterupted(device_ip);
							// Terminated!!
						}
					}
				});
			}

			/*
			 * System.out.
			 * println("DBDataService;:::  REACHED-----------------------Seperate thread started"
			 * ); System.out.println("DBDataService;::: "
			 * +executorTimerService.isTerminated()); executorTimerService.shutdownNow();
			 * Thread.sleep(2000); System.out.println("DBDataService;::: "
			 * +executorTimerService.isTerminated());
			 * 
			 * Thread.sleep(100000000);
			 */

			// PTZPacketSender.getInstance().setCurrentTime(ptzData.getPtz_ip(), 2000);
			/*
			 * PTZPacketSender.getInstance().jumpToPositionPacket(ptzData.getPtz_ip(),
			 * ptz_tcp_port, startingAngle, 0,true); Thread.sleep(200);
			 * 
			 * 
			 * if(PTZPacketSender.current_angle!=startingAngle) { int getCurrentAngle =
			 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(),
			 * ptz_tcp_port); while(getCurrentAngle!=startingAngle) { Thread.sleep(500);
			 * getCurrentAngle =
			 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(),
			 * ptz_tcp_port); } }
			 */
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.error("Interuppted in rotate device");
			e1.printStackTrace();
		}

		// set speed of PTZ to defined value
		logger.debug("send packet to ptz Speed"+ptzSpeed);
		PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(), ptz_tcp_port, 28, 0, ptzSpeed);
		
		
		tiltAngleProperty = tiltAngleProperty + ptz_tilt_offset;

		int tiltAngle = PTZPacketSender.getInstance().getCurrentTilt(ptzData.getPtz_ip(), ptz_tcp_port);
		if (tiltAngle != tiltAngleProperty) {
			/*
			 * if(tiltAngle > tiltAngleProperty) {
			 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,10,0,(
			 * tiltAngle-tiltAngleProperty)); }else {
			 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,8,0,(
			 * tiltAngleProperty-tiltAngle)); }
			 */
			try { 
				logger.debug("send packet to "+tiltAngleProperty);
				PTZPacketSender.getInstance().jumpToPositionPacket(ptzData.getPtz_ip(), ptz_tcp_port, tiltAngleProperty,
						0, false);
				Thread.sleep(100);
				
				tiltAngle = PTZPacketSender.getInstance().getCurrentTilt(ptzData.getPtz_ip(), ptz_tcp_port);
				while (tiltAngle <= tiltAngleProperty + 1 && tiltAngle >= tiltAngleProperty - 1)// tiltAngle!=tiltAngleProperty)
				{
					Thread.sleep(1000);
					tiltAngle = PTZPacketSender.getInstance().getCurrentTilt(ptzData.getPtz_ip(), ptz_tcp_port);
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				logger.error("Interuppted in rotate device sleep");
				shutdownTimer();
				stopRotateDeviceInterupted(device_ip);
				return false;
			}
		}

		if (oprThread != null && oprThread.isAlive() && oprThread.isInterrupted()) {
			shutdownTimer();
			stopRotateDeviceInterupted(device_ip);
			return false;
		}

		Double stoppingAngleDouble = Double.parseDouble(stopAngle);
		int stoppingAngle = stoppingAngleDouble.intValue() - ptzOffset;
		if(startingAngle>360) {
			startingAngle=startingAngle-360;
		}
		if(stoppingAngle>360) {
			stoppingAngle=stoppingAngle-360;
		}
		DBDataService.startAngle = startingAngle; //+ ptzOffset;
		DBDataService.stopAngle = stoppingAngle ;//+ ptzOffset;
		logger.debug("startAngle"+ DBDataService.startAngle);
		logger.debug("stopAngle"+ DBDataService.stopAngle);
        
		rotatePTZ = true;

		// Discussed with PT team, need to wait for 2 sec for motor to stop and accept
		// scan command.
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error("Interuppted in rotate device");
			shutdownTimer();
			stopRotateDeviceInterupted(device_ip);
			return false;
		}

		if (oprThread != null && oprThread.isAlive() && oprThread.isInterrupted()) {
			logger.error("Interuppted in rotate device");
			shutdownTimer();
			stopRotateDeviceInterupted(device_ip);
			return false;
		}

		System.out.println("DBDataService: Sending scan request");

		// Requirement changed. Commenting as now new command has been introduced.
		// PTZPacketSender.getInstance().scanPacket(ptzData.getPtz_ip(), ptz_tcp_port,
		// startingAngle, stoppingAngle, ptz_angle_rotation, 100, false, 0, true);
		// PTZPacketSender.getInstance().scanPacketWithRotation(ptzData.getPtz_ip(),
		// ptz_tcp_port, startingAngle, stoppingAngle, ptz_angle_rotation, 100, 4, 0,
		// true);
		// sajal//PTZPacketSender.getInstance().scanPacketWithRotation(ptzData.getPtz_ip(),
		// ptz_tcp_port, startingAngle, stoppingAngle, ptz_angle_rotation, 20, 0, 0,
		// true);
		logger.debug("send packet to stop and start angle");
		PTZPacketSender.getInstance().scanPacketWithRotation(ptzData.getPtz_ip(), ptz_tcp_port, startingAngle,
				stoppingAngle, 0, 0, 0, 50, true);
		// start recording time
		logger.debug("send packet to stop and start angle");
		ptzStartTime = Instant.now();
		PTZPacketSender.current_angle = -1;
		// commenting as new packet structure is to be implemented now.
		/*
		 * boolean reversePTZ=false; rotatePTZ = true; //Need to check. Probably we
		 * always have to move right because stop angle will be greater than start angle
		 * while(rotatePTZ) // Will get false when stop button is pressed from UI {
		 * 
		 * 
		 * 
		 * while(stoppingAngle-currentAngle > ptz_angle_rotation && !reversePTZ &&
		 * rotatePTZ) {
		 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,2,0,
		 * ptz_angle_rotation); try { Thread.sleep(4000); } catch (InterruptedException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); } currentAngle =
		 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), 2000); }
		 * 
		 * while((stoppingAngle-currentAngle > 0) && (stoppingAngle-currentAngle <=
		 * ptz_angle_rotation) && !reversePTZ && rotatePTZ) {
		 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,2,0,
		 * stoppingAngle-currentAngle); try { Thread.sleep(4000); } catch
		 * (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } currentAngle =
		 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), 2000);
		 * if(stoppingAngle==currentAngle) { reversePTZ = true;
		 * System.out.println("Reverse true"); } }
		 * 
		 * while((currentAngle-startingAngle > ptz_angle_rotation) && reversePTZ &&
		 * rotatePTZ) {
		 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,4,0,
		 * ptz_angle_rotation); try { Thread.sleep(4000); } catch (InterruptedException
		 * e) { // TODO Auto-generated catch block e.printStackTrace(); } currentAngle =
		 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), 2000); }
		 * 
		 * while((currentAngle-startingAngle > 0) && (currentAngle-startingAngle <=
		 * ptz_angle_rotation) && reversePTZ && rotatePTZ) { if(startingAngle==0) {
		 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,4,0,0);
		 * }else {
		 * PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(),2000,4,0,
		 * currentAngle-startingAngle); } try { Thread.sleep(4000); } catch
		 * (InterruptedException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } currentAngle =
		 * PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), 2000);
		 * if(startingAngle==currentAngle) { reversePTZ = false;
		 * System.out.println("Reverse false"); } } }
		 */
		return true;
	}

	protected void timerExpired() {
		List<DeviceInfo> devices = getDevicesList();
		List<String> allIPs = new ArrayList<String>();
		try {
			for (DeviceInfo device : devices) {
				allIPs.add(device.getIp_add());
			}

			stopRotateDevice(allIPs.get(0));

		} catch (Exception e) {// IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void stopRotateDevice(String ip_add) {
		if (rotatePTZ) {
			currentEventName = null;
			logger.info("DBDataService: stopRotateDevice");
			PTZData ptzData = ptzr.getPTZInfo(ip_add);

			try {
				PTZPacketSender.getInstance().stopPTZScan(ptzData.getPtz_ip(), ptz_tcp_port);
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}

			Instant ptzEndTime = Instant.now();
			Duration timeElapsed = Duration.between(ptzStartTime, ptzEndTime);

			sendCurrentEvent("Idle");

			DBDataService.rotatePTZ = false;

			try {
				// sendConfigDataForPtz(100,"0");
				sendStopCommandToHummer(true);

				if (systemType.equals("standalone")) { // changed here !systemType.equals("integrated")
					sendStartCommandToHummer();
				}

			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				logger.error(e1.getMessage());
			}

			calculateCentroid(timeElapsed.getSeconds());

			DBDataService.isUGSTriggeredEvent = false;
			DBDataService.isBlacklistTriggeredEvent = false;
			DBDataService.isAutoTriggeredEvent = false;
			DBDataService.isUserTriggeredEvent = false;
			PTZPacketSender.prev_angle = 361;
			PTZPacketSender.current_angle = -1;

			PTZPacketSender.scanVal = 1;
			startAngle = -1;
			stopAngle = -1;
			sendCurrentEvent("Idle");
			DBDataService.scanTemp = 1;
			powerTemp = -200d;

			maximizeAlarmInfo = null;

			oprThread = null;
			shutdownTimer();
			emitterPower = 30;

			/*
			 * try { System.out.println("DBDataService: Waiting for motor to stop");
			 * Thread.sleep(1); } catch (InterruptedException e) { // TODO Auto-generated
			 * catch block e.printStackTrace(); }
			 */
			synchronized (wait) {
				logger.warn("Notified execute Queue");
				wait.notify();
			}
		}
	}

	public void stopRotateDeviceInterupted(String ip_add) {
		createAuditlog("Cue",
				"Terminated\t" + eventData.getEventName() + "\tCueId:" + eventData.getCueId() + "\tPriority Event");
		if (rotatePTZ) {
			System.out.println("DBDataService: stopRotateDevice");
			currentEventName = null;

			logger.error("DBDataService: stopRotateDeviceInterupted");

			PTZData ptzData = ptzr.getPTZInfo(ip_add);

			try {
				PTZPacketSender.getInstance().stopPTZScan(ptzData.getPtz_ip(), ptz_tcp_port);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			// Instant ptzEndTime = Instant.now();
			// Duration timeElapsed = Duration.between(ptzStartTime, ptzEndTime);
			sendCurrentEvent("Idle");

			DBDataService.rotatePTZ = false;
			try {
				// sendConfigDataForPtz(100,"0");
				sendStopCommandToHummer(true);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		// calculateCentroid(timeElapsed.getSeconds());
		distanceMapTemp.clear();
		PTZPacketSender.scanVal = 1;
		DBDataService.scanTemp = 1;

		DBDataService.isUGSTriggeredEvent = false;
		DBDataService.isBlacklistTriggeredEvent = false;
		DBDataService.isAutoTriggeredEvent = false;
		DBDataService.isUserTriggeredEvent = false;
		PTZPacketSender.prev_angle = 361;
		PTZPacketSender.current_angle = -1;

		PTZPacketSender.scanVal = 1;
		startAngle = -1;
		stopAngle = -1;

		DBDataService.scanTemp = 1;
		powerTemp = -200d;
		sendCurrentEvent("Idle");
		maximizeAlarmInfo = null;

		// if(oprThread!=null && oprThread.isAlive()) {
		// oprThread.interrupt();
		// oprThread = null;
		// }
		oprThread = null;

//		oprThread = null;
		shutdownTimer();
		emitterPower = 30;

		/*
		 * try { System.out.println("DBDataService: Waiting for motor to stop");
		 * Thread.sleep(1); } catch (InterruptedException e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 */
		synchronized (wait) {
			logger.warn("Notified execute Queue");
			wait.notify();
		}
	}

	// NEW UPDATE: USE PTZ instead of JM for angle. Un-usable method renamed
	public void rotateDeviceOLD(String device_ip, String startAngle, String stopAngle) throws IOException {

		PTZData ptzData = ptzr.getPTZInfo(device_ip);

		String lastTimeDataRec = jmDataTemp.getTime();
		// Check if any packet is received today from JM. Can be removed if check not
		// required. Need to check in testing for diff scenarios
		if (LocalDate.now().toString().equals(lastTimeDataRec.split(" ")[0])) {
			/*
			 * Double currentAngleDouble = Double.parseDouble(jmDataTemp.getAngle()); int
			 * currentAngle = currentAngleDouble.intValue();
			 */
			Double startingAngleDouble = Double.parseDouble(startAngle);
			int startingAngle = startingAngleDouble.intValue();

			// Compare with the jmDataTemp variable getting updated every time packet from
			// JM is received.
			while ((int) Double.parseDouble(jmDataTemp.getAngle()) != startingAngle) {
				if ((int) Double.parseDouble(jmDataTemp.getAngle()) > startingAngle) {
					// Move left. For left: 0x04
					// Thread ptzThread1 = (new Thread(new
					// PTZPacketSender(ptzData.getPtz_ip(),2000,"0x04",0,((int)Double.parseDouble(jmDataTemp.getAngle())-startingAngle)+"")));
					PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(), ptz_tcp_port, 4, 0,
							((int) Double.parseDouble(jmDataTemp.getAngle()) - startingAngle));// PENDING--> NEED TO
																								// WAIT FOR PTZ TO MOVE

					/*
					 * Thread ptzThread1 = (new Thread(new
					 * PTZPacketSender(ptzData.getPtz_ip(),2000,4,0,0))); ptzThread1.start();
					 */
					// ptzPacket.sendPacket("0x04", "1",
					// ((int)Double.parseDouble(jmDataTemp.getAngle())-startingAngle)+"");

				} else {
					// Move Right. For right: 0x02
					// Thread ptzThread2 = (new Thread(new
					// PTZPacketSender(ptzData.getPtz_ip(),2000,"0x02",0,(startingAngle-(int)Double.parseDouble(jmDataTemp.getAngle()))+"")));
					PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(), ptz_tcp_port, 2, 0,
							(startingAngle - (int) Double.parseDouble(jmDataTemp.getAngle())));// PENDING--> NEED TO
																								// WAIT FOR PTZ TO MOVE
					/*
					 * Thread ptzThread2 = (new Thread(new
					 * PTZPacketSender(ptzData.getPtz_ip(),2000,2,0,0))); ptzThread2.start();
					 */
					// ptzPacket.sendPacket("0x02", "1",
					// (startingAngle-(int)Double.parseDouble(jmDataTemp.getAngle()))+"");
				}
				try {
					// Wait for PTZ to move to the required angle
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} // exit when ptz has reached the starting angle. Start scan now.

			// Can probably inform UI to start plot color based on DF (direction finder)
			// signal received in peak info
			// PENDING - Can inform UI websocket to start display of data. But then need to
			// check how to stop recording of data in db

			Double stoppingAngleDouble = Double.parseDouble(stopAngle);
			int stoppingAngle = stoppingAngleDouble.intValue();

			// Need to check. Probably we always have to move right because stop angle will
			// be greater than start angle
			// Thread ptzThread3 = (new Thread(new
			// PTZPacketSender(ptzData.getPtz_ip(),2000,"0x02",0,(stoppingAngle-startingAngle)+"")));

			PTZPacketSender.getInstance().sendPacket(ptzData.getPtz_ip(), ptz_tcp_port, 2, 0,
					(stoppingAngle - startingAngle));// PENDING--> NEED TO WAIT FOR PTZ TO MOVE
			/*
			 * Thread ptzThread3 = (new Thread(new
			 * PTZPacketSender(ptzData.getPtz_ip(),2000,2,0,0))); ptzThread3.start();
			 */
			// ptzPacket.sendPacket("0x02", "1", (stoppingAngle-startingAngle)+"");

		} else {
			// No Data received from JM today. Try again after sometime
		}

	}

	// @Transactional
	public void recordPTZData(String ip_add, String name, String north_offset, String sector_offset) {
		PTZData ptzData = new PTZData();
		String device_ip = getDeviceIp();

		ptzData.setDevice_ip(device_ip);
		ptzData.setPtz_ip(ip_add);
		ptzData.setPtz_offset(north_offset);
		ptzData.setSector_offset(sector_offset);
		ptzData.setName(name);

		ptzr.save(ptzData);

		ptzOffset = Integer.parseInt(north_offset);
		ptz_roll_offset = Integer.parseInt(sector_offset);

		cm.initializeNodeCache();
	}

	// @Transactional
	public void deletePTZData(String ip_add) throws Exception {
		ptzr.delete(ptzr.getPTZInfoByIP(ip_add));
		ptzOffset = 0;
		ptz_roll_offset = 0;
		cm.initializeNodeCache();

	}

	// @Transactional
	public void recordJM(String deviceId, String antennaid, String sector) {
		JMNodeMapping jmData = new JMNodeMapping();

		jmData.setDeviceid(Integer.parseInt(deviceId));
		jmData.setAntennaid(Integer.parseInt(antennaid));
		jmData.setSector(Integer.parseInt(sector));

		jmnmr.save(jmData);

		updateJMNodeMap();
	}
	
	public boolean checkfrqBand(double frq) {
		
		String[] applyband =aplliedband.split(",");
		boolean flag=false;
		String startfrq=null;
		String stopfrq=null;
		for(int i=0;i<applyband.length;i++)
		{
		String[] band=applyband[i].split("-");
		startfrq=band[0];
		stopfrq=band[1];
		
		if(Double.parseDouble(startfrq)<=frq && Double.parseDouble(stopfrq)>=frq) {
			flag=true;
			break;
			}
		}
		return flag;
		
	}
public double checkfrqBandforRBW(double frq) {
		
		String[] applyband =aplliedband.split(",");
		String startfrq=null;
		String stopfrq=null;
		double bandRbw = 0.0;
		for(int i=0;i<applyband.length;i++)
		{
		String[] band=applyband[i].split("-");
		startfrq=band[0];
		stopfrq=band[1];
		
		if(Double.parseDouble(startfrq)<=frq && Double.parseDouble(stopfrq)>=frq) {
			
			bandRbw=Double.parseDouble(stopfrq)-Double.parseDouble(startfrq);
			break;
			}
		}
		return calculateRbwBandwidth(bandRbw);
		
		
	}
	

	public double calculateEmitterPower(double frequency) {
		ColorStripsData colorStripsData;
		Iterator<ColorStripsData> emit = emitterPowerMap.iterator();
		double startFreq, stopFreq;
		double emitterpower = 30;

		while (emit.hasNext()) {
			colorStripsData = emit.next();
			startFreq = colorStripsData.getStartfreq();
			stopFreq = colorStripsData.getStopfreq();
			String tech=colorStripsData.getNetworktype();
			if (frequency >= startFreq && frequency <= stopFreq) {
				emitterpower = Integer.parseInt(colorStripsData.getDepl());
				
				if(tech.equalsIgnoreCase("LTE")) {
					//if(useBandFilter.equals("1")) {
					try 
					{
						
							if(checkBSF.equals("1")) {
								//boolean flag=checkfrqBand(frequency);
								if(checkfrqBand(frequency)) {
									double bdw=Math.round(stopFreq-startFreq);
									if (bdw<=5)
										bdw=5;
									else if (bdw>5 && bdw<=10)
										bdw=10;
									else if (bdw>10 && bdw<=15)
										bdw=15;
									else
										bdw=20;
										
									emitterpower=emitterpower-10 *(Math.log10((bdw)*5));
									emitterpower=Math.round(emitterpower*100.0)/100.0;
								}
							}
						
						
					}
					catch (Exception e)
					{
						logger.error("Invalid Bandwidth added in Emitter Info table ");
					}
//				//}
//				else
//					if(bandWidth2>=40) {
//						emitterpower=emitterpower-10 * (int)(Math.log(180/10));
//					}
//					else {
//						emitterpower=emitterpower-10 * (int)(Math.log(180));
//					}
					
				}
				
				break;
			}
		}

		return emitterpower;
	}

	public String findEmitterTech(double frequency) {
		ColorStripsData colorStripsData;
		Iterator<ColorStripsData> emit = emitterPowerMap.iterator();
		double startFreq, stopFreq;
		String tech = "";

		while (emit.hasNext()) {
			colorStripsData = emit.next();
			startFreq = colorStripsData.getStartfreq();
			stopFreq = colorStripsData.getStopfreq();

			if (frequency >= startFreq && frequency <= stopFreq) {
				tech = colorStripsData.getNetworktype();
				break;
			}
		}

		return tech;
	}

	public static double calculateRbwBandwidth(double band) {
		int scale = 100;
		if (band <= 40 && band > 4) {
			scale = 10;
		} else if (band <= 4) {
			scale = 1;
		}

		double bandwidth = (Math.ceil(((band) * 1000 / (rbwMaxSamples - 1)) / scale)) * scale;// changed here *2;
		bandwidth = bandwidth / 1000.0;
		return bandwidth;
	}

	// =================------------BMS DATA----------========
	@Transactional
	public String sendBMS(String ip, int port, String cmd) {
		return UdpPacketSender.getInstance().sendToBMS(ip, port, cmd);
	}
	
	public void SaveBandFilter(String startfrq, String stopfrq) {
		List<String> recordPeak = new ArrayList<String>();
		String query="insert into band_filter(start_frq,stop_frq)values ('"+startfrq+"','"+stopfrq+"')";
			recordPeak.add(query);
			try {
			databaseService.executeBatchInsertion(recordPeak);
				} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error(e.getMessage());
				}
	}
	public void deleteBandFilter(int id) {
		List<String> recordPeak = new ArrayList<String>();
		String query="delete from band_filter where id="+id+"";
			recordPeak.add(query);
			try {
			databaseService.executeBatchInsertion(recordPeak);
				} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error(e.getMessage());
				}
	}
	public void UpdateBandFilter(String startfrq, String stopfrq,String Status) {
		List<String> recordPeak = new ArrayList<String>();
		
		String query="update band_filter set status='"+Status+"' where start_frq='"+startfrq+"' and "+"stop_frq='"+stopfrq+"'" ;
			recordPeak.add(query);
  	  		try {
				databaseService.executeBatchInsertion(recordPeak);
  	  			} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
					e.printStackTrace();
					logger.error(e.getMessage());
  	  			}
	}
	// @Transactional
	public void recordMaskingData(MaskingScreenData maskingData) throws Exception {
		String type = maskingData.getFreqtype();

		double newStartFreq = maskingData.getFrequency() - maskingData.getBandwidth() / 2;
		double newStopFreq = maskingData.getFrequency() + maskingData.getBandwidth() / 2;

		Iterator<MaskingScreenData> itr = blackListFreq.iterator();
		while (itr.hasNext()) {
			MaskingScreenData msd = itr.next();

			double startFreq = msd.getFrequency() - msd.getBandwidth() / 2;
			double stopFreq = msd.getFrequency() + msd.getBandwidth() / 2;

			if (newStartFreq >= startFreq && newStartFreq <= stopFreq) {
				throw new Exception("Frequency Band Already Exists");
			}
			if (newStopFreq >= startFreq && newStopFreq <= stopFreq) {
				throw new Exception("Frequency Band Already Exists");
			}
			if (startFreq >= newStartFreq && startFreq <= newStopFreq) {
				throw new Exception("Frequency Band Already Exists");
			}
			if (stopFreq >= newStartFreq && stopFreq <= newStopFreq) {
				throw new Exception("Frequency Band Already Exists");
			}
		}

		Iterator<MaskingScreenData> itr1 = whiteListFreq.iterator();
		while (itr1.hasNext()) {
			MaskingScreenData msd = itr1.next();

			double startFreq = msd.getFrequency() - msd.getBandwidth() / 2;
			double stopFreq = msd.getFrequency() + msd.getBandwidth() / 2;

			if (newStartFreq >= startFreq && newStartFreq <= stopFreq) {
				throw new Exception("Frequency Band Already Exists");
			}
			if (newStopFreq >= startFreq && newStopFreq <= stopFreq) {
				throw new Exception("Frequency Band Already Exists");
			}
			if (startFreq >= newStartFreq && startFreq <= newStopFreq) {
				throw new Exception("Frequency Band Already Exists");
			}
			if (stopFreq >= newStartFreq && stopFreq <= newStopFreq) {
				throw new Exception("Frequency Band Already Exists");
			}
		}

		if (type.equalsIgnoreCase("blacklist")) {
			blackListFreq.add(maskingData);
		} else if (type.equalsIgnoreCase("whitelist")) {
			whiteListFreq.add(maskingData);
		}
		/*
		 * AuditLog auditLog = new AuditLog(); auditLog.setLogType("Target List");
		 * auditLog.setDescription(maskingData.getFreqtype()+"\t"+maskingData.
		 * getFrequency()+"\t"+ maskingData.getBandwidth()); alr.save(auditLog);
		 */
		createAuditlog("Target List", "Added" + "\tType:" + maskingData.getFreqtype() + "\tFrequency:"
				+ maskingData.getFrequency() + "\tBandwidth:" + maskingData.getBandwidth());
		mstScr.save(maskingData);
	}

	// implementation required in DataController
	// @Transactional
	public void deleteMaskingData(int id) {
		MaskingScreenData maskingData = mstScr.findById(id).get();

		/*
		 * if(maskingData.getFreqtype().equalsIgnoreCase("blacklist")) {
		 * Iterator<MaskingScreenData> itr = blackListFreq.iterator(); while
		 * (itr.hasNext()) { MaskingScreenData msd = itr.next(); if (msd.getId() == id)
		 * { createAuditlog("Target List",
		 * "Delete"+"\tType:"+maskingData.getFreqtype()+"\tFrequency:"+maskingData.
		 * getFrequency()+"\tBandwidth:"+ maskingData.getBandwidth()); itr.remove(); } }
		 * System.out.println(blackListFreq); } else
		 * if(maskingData.getFreqtype().equalsIgnoreCase("whitelist")) {
		 * Iterator<MaskingScreenData> itr = whiteListFreq.iterator(); while
		 * (itr.hasNext()) { MaskingScreenData msd = itr.next(); if (msd.getId() == id)
		 * { createAuditlog("Target List",
		 * "Delete"+"\tType:"+maskingData.getFreqtype()+"\tFrequency:"+maskingData.
		 * getFrequency()+"\tBandwidth:"+ maskingData.getBandwidth()); itr.remove(); } }
		 * System.out.println(whiteListFreq); }
		 */

		createAuditlog("Target List", "Delete" + "\tType:" + maskingData.getFreqtype() + "\tFrequency:"
				+ maskingData.getFrequency() + "\tBandwidth:" + maskingData.getBandwidth());

		mstScr.deleteById(id);
		// createAuditlog("Target List",
		// "Delete"+"\t"+maskingData.getFreqtype()+"\t"+maskingData.getFrequency()+"\t"+
		// maskingData.getBandwidth());

		blackListFreq.clear();
		whiteListFreq.clear();

		Iterator<MaskingScreenData> msd = mstScr.getMaskingDataList(defaultProfile).iterator();
		while (msd.hasNext()) {
			MaskingScreenData maskData = msd.next();
			if (maskData.getFreqtype().equalsIgnoreCase("blacklist")) {
				blackListFreq.add(maskData);
			} else if (maskData.getFreqtype().equalsIgnoreCase("whitelist")) {
				whiteListFreq.add(maskData);
			}
		}

//		System.out.println(blackListFreq);
//		System.out.println(whiteListFreq);
	}

	@Transactional
	public void recordColorStripsData(ColorStripsData colorStrips) {
		cStr.save(colorStrips);
		emitterPowerMap.add(colorStrips);
	}

	@Transactional
	public void deleteColorStripsData(int id) throws Exception {
		if (cStr.findById(id).get().getVisible() || id > 0) {
			cStr.deleteById(id);
			updateEmitterPowerMap();
		} else {
			throw new Exception("Cannot delete this row");
		}
	}

	@Transactional
	public void recordSystemConfigurationData(SystemConfigurationData systemConfigurationData) {
		// systemConfigurationData.setId(1);
		SystemConfigurationData scd = scr.getSystemConfigurationData(defaultProfile);
		scd.setUgs(systemConfigurationData.getUgs());
		scd.setTmdas(systemConfigurationData.getTmdas());
		scd.setAuto(systemConfigurationData.getAuto());
		scd.setTimeout(systemConfigurationData.getTimeout());
		scd.setValidity(systemConfigurationData.getValidity());
		scd.setSelftimeout(systemConfigurationData.getSelftimeout());
		scd.setSelfvalidity(systemConfigurationData.getSelfvalidity());
		scd.setRange(systemConfigurationData.getRange());
		scr.save(scd);

		isManualOverride = systemConfigurationData.getUgs() == 1 ? true : false;
		if (isManualOverride) {
			try {
				if (currentEventName != null && !currentEventName.equals(manualEvent))
					stopRotateDeviceInterupted(getDeviceIp());
				synchronized (synch) {
					oxfamEventCount = 0;
				}
			} catch (Exception e) {// UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		isTmdasEnabled = systemConfigurationData.getTmdas() == 1 ? true : false;
		isAutoEnabled = systemConfigurationData.getAuto() == 1 ? true : false;
		if (!isTmdasEnabled || !isAutoEnabled)
			falconDataCache.clear();
		timeout = systemConfigurationData.getTimeout();
		selfTimeout = systemConfigurationData.getSelftimeout();
		validity = systemConfigurationData.getValidity();
		selfValidity = systemConfigurationData.getSelfvalidity();
		String range =systemConfigurationData.getRange();
		// scr.save(systemConfigurationData);
		createAuditlog("System Configuration",
				"Manual Override:" + isManualOverride + "\tFalcon Enabled:" + isTmdasEnabled + "\tAuto Enabled:"
						+ isAutoEnabled + "\tTimeout:" + timeout + "\tValidity:" + validity + "\tSelf Timeout:"
						+ selfTimeout + "\tSelf Validity:" + selfValidity+ "\tRange:" + range);
	}

	@Transactional
	public PTZData getPTZInfo(String device_ip) {
		PTZData ptzInfo = ptzr.getPTZInfo(device_ip);
		if (ptzInfo == null) {
			return new PTZData();
		}
		return ptzInfo;
	}

	@Transactional
	public void purgeData(String startTime, String endTime) throws SQLException {
		System.out.println("Purge Data FROM DB");
		Connection connection = rps.getConnection();
		Statement statement = connection.createStatement();
		int result = 0;

		result = statement
				.executeUpdate("delete from peak_info where time between '" + startTime + "' and '" + endTime + "'");
		result = statement
				.executeUpdate("delete from alarm_info where time between '" + startTime + "' and '" + endTime + "'");
		result = statement
				.executeUpdate("delete from audit_log where logtime between '" + startTime + "' and '" + endTime + "'");

		// connection.commit();
		statement.close();
		connection.close();
//		pr.deleteBetweenTime(startTime, endTime);
//		ar.deleteBetweenTime(startTime, endTime);
//		alr.deleteBetweenTime(startTime, endTime);
	}

	@Transactional
	public void resetAll(String type) throws SQLException {
		System.out.println("Reset Data FROM DB");
		Connection connection = rps.getConnection();
		Statement statement = connection.createStatement();
		int result = 0;

		if (type.equals("all")) {
			// createAuditlog("Data Purge", "All Data Purged");
			result = statement.executeUpdate("TRUNCATE peak_info");
			result = statement.executeUpdate("TRUNCATE alarm_info");
			result = statement.executeUpdate("TRUNCATE audit_log");
			// connection.commit();
//			pr.deleteAll();
//			ar.deleteAll();
//			alr.deleteAll();
		} else if (type.equals("data_events")) {
			createAuditlog("Data Purge", "Events Data Purged");
			result = statement.executeUpdate("TRUNCATE peak_info");
			result = statement.executeUpdate("TRUNCATE alarm_info");
			// connection.commit();
//			pr.deleteAll();
//			ar.deleteAll();
		} else {
			// createAuditlog("Data Purge", "Audit Data Purged");
			result = statement.executeUpdate("TRUNCATE audit_log");
			// connection.commit();
			// alr.deleteAll();
		}

		statement.close();
		connection.close();
	}

	@Transactional
	public List<PeakDetectionInfo> getPeakInfoRecord(String startTime, String endTime) {
		System.out.println("GET PEAK DATA FROM DB");
		// DeviceInfo dinfo1 = dinfo.getDataByID(device_id);

		// return pr.findById(device_id);
		// return pr.getPeakInfoRecord(dinfo1);

		// changed here sajal
		// return pr.getPeakInfoRecordByTime(startTime, endTime, new PageRequest(0,
		// 300));

		// return pjmr.getPeakInfoRecordByIP(dinfo1.getIp_add());

		if (systemType.equals("standalone")) {
			return pr.getPeakInfoRecordStru(startTime, endTime, new PageRequest(0, 300));
		} else {
			return pr.getPeakInfoRecordNotStru(startTime, endTime, new PageRequest(0, 300));
		}
	}

	@Transactional
	public List<PeakDetectionInfo> getOldPeakInfoRecord(String startTime, String endTime, String systemType) {
		System.out.println("GET OLD PEAK DATA FROM DB");

		if (systemType.equals("standalone")) {
			return pr.getPeakInfoRecordStru(startTime, endTime, new PageRequest(0, 1000));
		} else {
			return pr.getPeakInfoRecordNotStru(startTime, endTime, new PageRequest(0, 1000));
		}

	}

	@Transactional
	public PeakDetectionInfo getPeakInfoRecordByIP(String ip_add) {
		return pr.getPeakInfoRecordByIP(ip_add);
	}

	@Transactional
	public List<AlarmInfo> getAlarmInfoRecord(String startTime, String endTime) {
		System.out.println("GET ALARM DATA FROM DB");
		// DeviceInfo dinfo1 = dinfo.getDataByID(device_id);

		// return pr.findById(device_id);
		// return pr.getPeakInfoRecord(dinfo1);
		return ar.getAlarmInfoRecordByTime(startTime, endTime, "centroid");
		// return pjmr.getPeakInfoRecordByIP(dinfo1.getIp_add());
	}

	@Transactional
	public List<AlarmInfo> getMaximizeRecord() {
		System.out.println("GET MAXIMIZE DATA");
		List<AlarmInfo> alarmInfo = new ArrayList<AlarmInfo>();
		AlarmInfo ar = new AlarmInfo();
		Iterator<List<Double>> i = distanceMapTemp.iterator();
		Iterator<Double> j;

		while (i.hasNext()) {
			j = i.next().iterator();
			ar.setRange(j.next());
			ar.setAngle(String.valueOf(j.next()));
			ar.setPower(String.valueOf(j.next()));
			alarmInfo.add(ar);
		}
		return alarmInfo;
	}

	@Transactional
	public List<Event> getEventCueRecord(String startTime, String endTime) {
		System.out.println("GET Event Cue FROM DB");

		List<Event> events = new ArrayList<Event>();
		List<AuditLog> auditLog = alr.getAuditBetweenTimeForType(startTime, endTime, "Cue");
		Iterator<AuditLog> itr = auditLog.iterator();
		while (itr.hasNext()) {
			AuditLog al = itr.next();
			Event e = new Event();
			e.setEventDate(al.getCreatedDateTime());
			e.setEventType(al.getLogType());
			String[] arr = al.getDescription().split("\t");

			String source = "";
			if (arr[0].equalsIgnoreCase("sent"))
				source = "HUMMER";
			else
				source = arr[1];
			e.setNodeType(source);
			// e.setNodeType(arr[1]);

			String text = "";
			for (int i = 0; i < arr.length; i++) {
				if (i != arr.length - 1)
					text = text + arr[i] + ',';
				else
					text = text + arr[i];
			}
			e.setEventData(text);
			events.add(e);
		}

		return events;
	}

	@Transactional
	public LEDData getLEDStatus(Long device_id) {
		DeviceInfo dinfo1 = dinfo.getDataByID(device_id);
		return lr.getLEDStatus(dinfo1);
	}

	@Transactional
	public List<DeviceInfo> getDevicesList() {
		System.out.println("DBDataService: Calling DB methods to get complete list");
		return dinfo.getDeviceInfo();
	}

	@Transactional
	public NodeData getNodeByType(String type) {
		System.out.println("DBDataService: Calling DB methods to get complete node list");
		return nr.getNodeByType(type);
	}

	@Transactional
	public PTZData getPTZList() {
		System.out.println("DBDataService: Calling DB methods to get complete ptz list");
		return ptzr.getPTZList();
	}

	@Transactional
	public List<Priority> getPriorityList() {
		System.out.println("DBDataService: Calling DB methods to get complete Priority list");
		return prior.getPriorityList();
	}

	@Transactional
	public List<ColorStripsData> getColorCodeList(String profile) {
		System.out.println("DBDataService: Calling DB methods to get complete ColorStripsData list");
		return cStr.getColorCodeList(profile);
	}

	@Transactional
	public SystemConfigurationData getSystemConfiguration(String profile) {
		System.out.println("DBDataService: Calling DB methods to get complete SystemConfiguration list");
		return scr.getSystemConfigurationData(profile);
	}

	@Transactional
	public List<MaskingScreenData> getMaskingDataList(String profile) {
		System.out.println("DBDataService: Calling DB methods to get complete MaskingData list");
		return mstScr.getMaskingDataList(profile);
	}
	@Transactional
	public JSONArray getBandDataList() throws JSONException{
		System.out.println("DBDataService: Calling DB methods to get complete getBandDataList list");
		String qurey="SELECT * FROM band_filter";
		JSONArray Data = null;
		try {
			Data = databaseService.getBandDataList(qurey);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
		return Data;
	}
	
	@Transactional
	public static JSONArray getBandDataList1() throws JSONException{
		System.out.println("DBDataService: Calling DB methods to get complete getBandDataList1 list");
		String qurey="SELECT * FROM band_filter where status = 'true'";
		JSONArray Data = null;
		try {
			DatabaseService dbData = new DatabaseService();
			Data = dbData.getBandDataList(qurey);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   
		return Data;
	}
	public List<FreqPowMinThresData> getMinFreqPowThresMap() {
		System.out.println("DBDataService: Calling DB methods to get complete Frequency Power Threshold list");
		return fpmt.getMinThresholdData();
	}

	@Transactional
	public DeviceInfo getDeviceInfo(Long device_id) {
		return dinfo.getDataByID(device_id);
	}

	@CrossOrigin
	@Transactional
	// @SendTo("/topic/activeStatus")
	public void updateActiveStatusWithState(String is_active, String ip_add, String starttime, String state) {
		System.out.println("DBDataService: updateActiveStatusWithState");
		// messagingTemplate.convertAndSend("/topic/activeStatus", "Hello");

		DeviceInfo d = dinfo.getDataByIP(ip_add);
		if (d != null) {
			String previousState = d.getState() != null ? d.getState() : "";
			String active = d.getIs_active() != null ? d.getIs_active() : "";

			logger.info("In updateActiveStatusWithState Previous state = " + previousState + " New State : " + state);

			if (!previousState.equals(state) && !active.equals(is_active)) {
				createAuditlog("System Status", "IP:" + ip_add + "\tState:" + state);
				dinfo.updateActiveStatus(is_active, ip_add, starttime.trim(), state);
			}
		}
		// dinfo.updateActiveStatus(is_active, ip_add,starttime.trim(),state);

		System.out.println("DBDataService: Send Data to socket...");
		messagingTemplate.convertAndSend("/topic/activeStatus", dinfo.getDataByIP(ip_add));
		System.out.println("DBDataService: Sent Data!");
	}

	@CrossOrigin
	@Transactional
	public void updateActiveStatusOnly(String is_active, String ip_add, String starttime) {
		System.out.println("DBDataService: updateActiveStatusOnly");
		if (starttime == null) {
			dinfo.updateActiveOnly(is_active, ip_add); // Just in case null time is received
		} else {
			dinfo.updateActiveStatusOnly(is_active, ip_add, starttime.trim());
		}

		System.out.println("DBDataService: Send data to socket on Status update only");
		messagingTemplate.convertAndSend("/topic/activeStatus", dinfo.getDataByIP(ip_add));
	}

	@CrossOrigin
	@Transactional
	public void updateActiveWithoutTime(String is_active, String ip_add, String state) {
		System.out.println("DBDataService: updateActiveWithoutTime");

		DeviceInfo d = dinfo.getDataByIP(ip_add);
		if (d != null) {
			String previousState = d.getState() != null ? d.getState() : "";
			String active = d.getIs_active() != null ? d.getIs_active() : "";

			logger.info("In updateActiveWithoutTime Previous state = " + previousState + " New State : " + state);

			if (!previousState.equals(state) && !active.equals(is_active)) {
				createAuditlog("System Status", "IP:" + ip_add + "\tState:" + state);
				dinfo.updateActiveWithoutTime(is_active, ip_add, state);
			}
		}
		// dinfo.updateActiveWithoutTime(is_active, ip_add, state);

		messagingTemplate.convertAndSend("/topic/activeStatus", dinfo.getDataByIP(ip_add));
	}

	@Transactional
	public void updateDeviceState(String ip_add, String state) {
		DeviceInfo d = dinfo.getDataByIP(ip_add);
		if (d != null) {
			String previousState = d.getState() != null ? d.getState() : "";

			logger.info("In updateDeviceState Previous state = " + previousState + " New State : " + state);

			if (!previousState.equals(state)) {
				createAuditlog("System Status", "IP:" + ip_add + "\tState:" + state);
				dinfo.updateDeviceState(ip_add, state);
			}
		}
	}

	@Transactional
	public void updateGPSData(String ip_add, String latitude, String longitude, double accuracy) {
		/*
		 * AuditLog auditLog = new AuditLog(); auditLog.setLogType("System Status");
		 * auditLog.setDescription(ip_add+"\t"+state); alr.save(auditLog);
		 */
		// createAuditlog("System Status", ip_add+"\t"+state);
		boolean flag = false;

		// logger.info(latitude+" "+longitude);

		String ptzIp = Common.nodeIpMap.get(nodeIdMap.get(2));
		String smmIp = Common.nodeIpMap.get(nodeIdMap.get(3));

		DeviceInfo deviceInfo = dinfo.getDataByIP(smmIp);
		if (deviceInfo != null) {
			double oldLat = Double.parseDouble(deviceInfo.getLat());
			double oldLon = Double.parseDouble(deviceInfo.getLon());
			double lat = Double.parseDouble(latitude);
			double lon = Double.parseDouble(longitude);

			if (systemMode.equals("moving")) {
				// if(systemGps.equals("smm")) removed concept of updating offset from dgps

				double distance = distanceBetweenCoordinate(oldLat, oldLon, lat, lon);
				if (distance > accuracy) {
					updateOffset(oldLat, oldLon, lat, lon);
					flag = true;
				}
			} else {
				double distance = distanceBetweenCoordinate(oldLat, oldLon, lat, lon);
				if (distance > accuracy)
					flag = true;
			}

			if (flag) {
				// logger.info("Updated GPS Data");
				if (smmIp != null && smmIp.equals(ip_add) && systemGps.equals("smm"))
					updateGPSDataAndLatLong(smmIp, latitude, longitude);
				else if (ptzIp != null && ptzIp.equals(ip_add) && systemGps.equals("dgps"))
					updateGPSDataAndLatLong(smmIp, latitude, longitude);
			}
		}

	}

	/*
	 * @Transactional public void updateGPSDataFromPtz(String lat, String lon) {
	 * /*AuditLog auditLog = new AuditLog(); auditLog.setLogType("System Status");
	 * auditLog.setDescription(ip_add+"\t"+state); alr.save(auditLog);
	 * //createAuditlog("System Status", ip_add+"\t"+state);
	 * 
	 * String ip = Common.nodeIpMap.get(nodeIdMap.get(3));
	 * 
	 * if(systemMode.equals("moving")) { DeviceInfo deviceInfo =
	 * dinfo.getDataByIP(ip); if(deviceInfo!=null)
	 * updateOffset(Double.parseDouble(deviceInfo.getLat()),
	 * Double.parseDouble(deviceInfo.getLon()), Double.parseDouble(lat),
	 * Double.parseDouble(lon)); }
	 * 
	 * if(ip!=null) dinfo.updateGPSData(ip, lat, lon);
	 * 
	 * }
	 */

	private static int getAntennaToVehicleDiffAngle() {

		return antennaToVehicleDiffAngle;

	}

	private static void setAntennaToVehicleDiffAngle(int angle) {

		antennaToVehicleDiffAngle = angle;

	}

	public void updateOffset(double oldLat, double oldLon, double lat, double lon) {

		int bearing = calcBearingBetweenTwoGpsLoc(oldLat, oldLon, lat, lon);

		if (getAntennaToVehicleDiffAngle() > 360) {

			int angleOffset = ptzOffset;

			if (angleOffset < 0)
				angleOffset = (angleOffset + 360) % 360;

			int antennaToVehicleDiffAngle = bearing - angleOffset;

			setAntennaToVehicleDiffAngle(antennaToVehicleDiffAngle);
		}

		int offset = calcNewAngleOffset(bearing);
		/* double angle = angleFromCoordinate(oldLat, oldLon, lat, lon); */

		String smmIp = Common.nodeIpMap.get(nodeIdMap.get(3));

		// int offset = (ptzOffset + (int)angle)%360;

		ptzUpdate(smmIp, String.valueOf(offset), String.valueOf(ptz_roll_offset), "0", "0");
	}

	public void updateOffset(float pan, float northOffset) {

		String smmIp = Common.nodeIpMap.get(nodeIdMap.get(3));

		int offset = (int) (northOffset - pan);

		ptzUpdate(smmIp, String.valueOf(offset), String.valueOf(ptz_roll_offset), "0", "0");
	}

	@Transactional
	public void updatePriority(String strArray[]) {
		for (int p = 0; p < strArray.length; p++) {
			priority.put(strArray[p], p + 1);
			prior.updatePriority(p + 1, strArray[p]);
		}
	}

	@Transactional
	public int getPtzOffset() {
		String ip = Common.nodeIpMap.get(nodeIdMap.get(2));
		String smmIp = Common.nodeIpMap.get(nodeIdMap.get(3));

		boolean gpsFlag = true;
		boolean deviceFlag = false;

		if (ip != null && systemGps.equals("dgps")) {
			try {
				float values[] = PTZPacketSender.getInstance().sendGPSCommand(ip, ptz_tcp_port, false, false);

				if (values[5] == 1) {
					deviceFlag = true;
					if (values[4] == 1) {
						int offset = (int) (values[1] - values[0]);

						ptzUpdate(smmIp, String.valueOf(offset), String.valueOf(ptz_roll_offset),
								String.valueOf(values[2]), String.valueOf(values[3]));
					} else {
						gpsFlag = false;
						ptzUpdate(smmIp, String.valueOf(ptzOffset), String.valueOf(ptz_roll_offset),
								String.valueOf(values[2]), String.valueOf(values[3]));
					}
				}
				// ptzOffset = offset;

			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.info("Couldn't update new data from dgps");
				ptzUpdate(smmIp, String.valueOf(ptzOffset), String.valueOf(ptz_roll_offset), "0", "0");
				e.printStackTrace();
			}
		}

		return deviceFlag ? (gpsFlag ? ptzOffset : -1000) : -999;
	}

	@Transactional
	public List<GpsData> getGpsData(Date start, Date end) {
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

		/*
		 * try { //Date start = sdf.parse(startTime); //Date end = sdf.parse(endTime);
		 * //return gpsDataRepository.getGpsDataBetweenTime(start, end); } catch
		 * (ParseException e) { // TODO Auto-generated catch block e.printStackTrace();
		 * }
		 */

		return gpsDataRepository.getGpsDataBetweenTime(start, end);
	}

	@Transactional
	public UserSetting getUserSetting() {
		return userSettingRepository.getData();
	}

	@Transactional
	public void updateUserSetting(String type, String mode, String gps, double accuracy) {
		String Operation = null;
		UserSetting userSetting = userSettingRepository.getData();
		userSetting.setType(type);
		userSetting.setMode(mode);
		userSetting.setGps(gps);
		userSetting.setGpsAccuracy(accuracy);
		//userSetting.setOpr_mode(opr_mode);
		userSettingRepository.save(userSetting);
		/*
		 * // userSettingRepository.updateDataByProperty(property, value); switch
		 * (opr_mode) { case "1": Operation = "Safe Mode"; break; case "2": Operation =
		 * "Default"; break; case "3": Operation = "High Sensitivity"; break; case "4":
		 * Operation = "Environment"; break; }
		 */

		createAuditlog("User System Configuration",
				"Type : " + type + " Mode : " + mode + " GPS : " + gps);

		String oldvalue = systemType;
		String oldGpsValue = systemGps;
		String oldMode = systemMode;
		//String oldOprMode = Opr_mode;
		systemType = type;
		systemMode = mode;
		systemGps = gps;
		gpsAccuracy = accuracy;
		//Opr_mode = opr_mode;
		// set rffecablelength
		/*setRffeCableLength(systemType);

		if (!oldMode.equals(mode) && mode.equals("moving")) {
			setAntennaToVehicleDiffAngle(361);
		}

		if (type.equals("standalone") && !type.equals(oldvalue)) {
			setStandaloneProfile();
		} else if (!type.equals("standalone") && !type.equals(oldvalue)) { // changed here type.equals("integrated")
			setIntegratedProfile();
		} else if (!opr_mode.equals(oldOprMode)) {
			if (type.equals("standalone")) {
				setStandaloneProfile();
			} else {
				setIntegratedProfile();
			}
		}

		if (!systemGps.equals(oldGpsValue))
			setUserSetting(systemMode, systemGps);

	}*/
		setRffeCableLength(systemType);
		
		if(!oldMode.equals(mode) && mode.equals("moving")) {
			setAntennaToVehicleDiffAngle(361);
			//DBDataService.changesAllowed=false;
		}
		
		if(type.equals("standalone") && !type.equals(oldvalue)) {
			setStandaloneProfile();
			//DBDataService.changesAllowed=false;
		}
		else if(!type.equals("standalone") && !type.equals(oldvalue)){				//changed here type.equals("integrated") 
			setIntegratedProfile();
			//DBDataService.changesAllowed=false;
		}
		
		if(!systemGps.equals(oldGpsValue))
			setUserSetting(systemMode, systemGps);
		//DBDataService.changesAllowed=false;
		
	}

	public void setUserSetting(String mode, String gps) {

		String ip = Common.nodeIpMap.get(nodeIdMap.get(2));

		if (ip != null) {
			try {
				if (gps.equals("dgps") && mode.equals("moving")) { // !gps.equals(oldGpsValue) &&
					PTZPacketSender.getInstance().sendGPSCommand(ip, ptz_tcp_port, true, true);
				} else if (gps.equals("smm")) {
					PTZPacketSender.getInstance().sendGPSCommand(ip, ptz_tcp_port, true, false);
				}
			} catch (IOException e) {
				logger.error(e.getMessage());
			}
		}
	}

	public void setStandaloneProfile() {

		SystemConfigurationData systemConfigurationData = scr.getSystemConfigurationData(defaultProfile);
		systemConfigurationData.setUgs(1);
		systemConfigurationData.setTmdas(0);
		systemConfigurationData.setAuto(0);
		recordSystemConfigurationData(systemConfigurationData);

		if (currentEventName != null) {
			createAuditlog("Cue", "Terminated\t" + eventData.getEventName() + "\tCueId:" + eventData.getCueId()
					+ "\tStandAlone Profile Applied");
		}

		try {
			sendStopCommandToHummer(false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sendStartCommandToHummer();
	}

	public void setIntegratedProfile() {
		try {
			sendStopCommandToHummer(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sendStartCommandToHummer();
	}

	/*
	 * public void updateNodeData() { System.out.println("Update Node Data");
	 * Iterator<NodeData> itr = nr.getNodeInfo().iterator(); finleyIp = ""; falconIp
	 * = ""; while(itr.hasNext()) { NodeData nodeData = itr.next();
	 * if(nodeData.getType().equals("Finley")) { finleyIp = nodeData.getIp_add(); }
	 * else if(nodeData.getType().equals("Falcon")) { falconIp =
	 * nodeData.getIp_add(); } } }
	 */

	public void setRffeCableLength(String type) {
		rffeCableLength = 0;
		systemType_ID = 0;
		if (type.equals("integrated")) {
			rffeCableLength = rfCableLength;
			systemType_ID = 2;
		} else if (type.equals("hummer")) {
			systemType_ID = 1;
		}

	}

	@Transactional
	public void updateAllData() {
		// getSystemtype from db
		UserSetting ust = userSettingRepository.getData(); // getDataByProperty("type");
		systemType = ust != null ? ust.getType() : null;
		systemMode = ust != null ? ust.getMode() : null;
		systemGps = ust != null ? ust.getGps() : null;
		gpsAccuracy = ust != null ? ust.getGpsAccuracy() : 0;
	//	Opr_mode = ust != null ? ust.getOpr_mode() : null;
		// set rbwMaxSamples
		rbwMaxSamples = maxSamples;

		System.out.println("####################" + rbwMaxSamples + "#######################");

		// set rffecablelength
		setRffeCableLength(systemType);

		// if initiaaly started from moving state
		if (systemMode.equals("moving")) {
			setAntennaToVehicleDiffAngle(361);
		}

		// added to initially check for dgps setting
		setUserSetting(systemMode, systemGps);

		/*
		 * if(systemType.equals("standalone")) { setStandaloneProfile(); }
		 */

		// update priority states from db
		Iterator<Priority> i = getPriorityList().iterator();
		while (i.hasNext()) {
			Priority p = i.next();
			priority.put(p.getName(), p.getPriority());
		}
		System.out.println(priority);

		// update systemConfiguratioData from db
		SystemConfigurationData systemConfigurationData = getSystemConfiguration(defaultProfile);
		isManualOverride = systemConfigurationData.getUgs() == 1 ? true : false;
		isTmdasEnabled = systemConfigurationData.getTmdas() == 1 ? true : false;
		isAutoEnabled = systemConfigurationData.getAuto() == 1 ? true : false;
		timeout = systemConfigurationData.getTimeout();
		selfTimeout = systemConfigurationData.getSelftimeout();
		validity = systemConfigurationData.getValidity();
		selfValidity = systemConfigurationData.getSelfvalidity();

		// update maskingScreenData from db
		Iterator<MaskingScreenData> ms = getMaskingDataList(defaultProfile).iterator();
		while (ms.hasNext()) {
			MaskingScreenData maskingData = ms.next();
			if (maskingData.getFreqtype().equalsIgnoreCase("blacklist")) {
				blackListFreq.add(maskingData);
			} else if (maskingData.getFreqtype().equalsIgnoreCase("whitelist")) {
				whiteListFreq.add(maskingData);
			}
		}

		// fetch ip of other systems from db
		// updateNodeData();

		// fetch uplink frequencies from uplink.properties
		getUplinkFrequency();

		// Getting Sector Mapping From DB
		getSectorMappingFromDb();

		// fetch sector mapping from sector.properties
		// getSectorMapping();

		// fetch jmnode data from db
		updateJMNodeMap();

		// get history jm data from db
		// updateJmList();

		// fetch emitter power range map
		updateEmitterPowerMap();

		// setDefaultSwitchSettings
		setSwitchSetting(1, 1, 1, 1);

		// fetch id and node mapping from db
		Iterator<Devices> itr = ds.getDevices().iterator();
		while (itr.hasNext()) {
			Devices d = itr.next();
			nodeIdMap.put((int) d.getId(), d.getNode_name());
		}

		// update node ip map
		cm.initializeNodeCache();

		// get min threshold frequency mapping
		freqPowMinThres = getMinFreqPowThresMap();

		PTZData ptz = getPTZList();

		if (ptz != null) {
			ptzOffset = Integer.parseInt(ptz.getPtz_offset());
			ptz_roll_offset = Integer.parseInt(ptz.getSector_offset());
		}

	}

	public void updateEmitterPowerMap() {
		emitterPowerMap = cStr.getColorCodeList(defaultProfile);
	}

	/* Sahil Ahuja */
	@Transactional
	public void getSectorMappingFromDb() {
		Integer antennaId;
		Integer port;
		String antennaPort;
		String sector;

		List<Object[]> sMapping = sc.getSectorMapping();
		System.out.println(sMapping);

		for (int i = 0; i < sMapping.size(); i++) {
			antennaId = (Integer) sMapping.get(i)[0];
			port = (Integer) sMapping.get(i)[1];
			sector = (String) sMapping.get(i)[2];

			antennaPort = Integer.toString(antennaId) + Integer.toString(port);
			sectorMapping.put(antennaPort, sector);

		}
	}

	public String getStruAntennaIdPort() {

		Iterator<Map.Entry<String, String>> itr = sectorMapping.entrySet().iterator();
		System.out.println("HashMap Is:" + sectorMapping);
		while (itr.hasNext()) {
			Map.Entry<String, String> entry = itr.next();
			if (entry.getValue().equals("STRU")) {
				antennaIdPort = entry.getKey();
				return antennaIdPort;
			}
		}

		return "41";

	}

	public void getSectorMapping() {
		String resourceName = "sector.properties"; // could also be a constant
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties prop = new Properties();

		try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
			prop.load(resourceStream);

			Enumeration<?> e = prop.propertyNames();
			while (e.hasMoreElements()) {
				String antennaid = (String) e.nextElement();
				String sector = prop.getProperty(antennaid);
				sectorMapping.put(antennaid, sector);
			}

			resourceStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getUplinkFrequency() {
		String resourceName = "uplink.properties"; // could also be a constant
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		Properties prop = new Properties();

		ColorStripsData freq;
		try (InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
			prop.load(resourceStream);

			Enumeration<?> e = prop.propertyNames();
			while (e.hasMoreElements()) {
				String key = (String) e.nextElement();
				if (key.contains("2g")) {
					freq = new ColorStripsData();
					String frq[] = prop.getProperty(key).split(",");
					int startfreq = Integer.parseInt(frq[0]);
					freq.setStartfreq(startfreq);

					// key = (String) e.nextElement();
					int stopfreq = Integer.parseInt(frq[1]);
					freq.setStopfreq(stopfreq);

					twogUplinkFreq.add(freq);
				} else if (key.contains("3g")) {
					freq = new ColorStripsData();
					String frq[] = prop.getProperty(key).split(",");
					int startfreq = Integer.parseInt(frq[0]);
					freq.setStartfreq(startfreq);

					// key = (String) e.nextElement();
					int stopfreq = Integer.parseInt(frq[1]);
					freq.setStopfreq(stopfreq);

					threegUplinkFreq.add(freq);
				} else if (key.contains("4g")) {
					freq = new ColorStripsData();
					String frq[] = prop.getProperty(key).split(",");
					int startfreq = Integer.parseInt(frq[0]);
					freq.setStartfreq(startfreq);

					// key = (String) e.nextElement();
					int stopfreq = Integer.parseInt(frq[1]);
					freq.setStopfreq(stopfreq);

					fourgUplinkFreq.add(freq);
				} else if (key.contains("wifi")) {
					freq = new ColorStripsData();
					String frq[] = prop.getProperty(key).split(",");
					int startfreq = Integer.parseInt(frq[0]);
					freq.setStartfreq(startfreq);

					// key = (String) e.nextElement();
					int stopfreq = Integer.parseInt(frq[1]);
					freq.setStopfreq(stopfreq);

					wifiUplinkFreq.add(freq);
				}
				// float value = Float.parseFloat(prop.getProperty(key));
				// System.out.println("Key : " + key + ", Value : " + value);
			}

			resourceStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@CrossOrigin
	// @Transactional
	public PeakDetectionInfo recordPtzPeakInfo(String ip_add, String power, String frequency, String time,
			String antennaid, String angle, boolean maximized) {
		PeakDetectionInfo pinfo = new PeakDetectionInfo();
		try {
			DeviceInfo dinfo1 = dinfo.getDataByIP(ip_add);
			pinfo.setDevice_id(dinfo1);
			pinfo.setFrequency(frequency);
			pinfo.setIp_add(ip_add);
			pinfo.setPower(power);
			pinfo.setTime(time);
			pinfo.setAntennaid(antennaid);
			pinfo.setSector(sectorMapping.get(antennaid));
			pinfo.setInserttime(new Date());
			pinfo.setLat(Double.parseDouble(dinfo1.getLat()));
			pinfo.setLon(Double.parseDouble(dinfo1.getLon()));
            if(!frequency.equalsIgnoreCase("NaN") ) {
            	
            
			int ptz_angle;
			boolean isBlFrequency = false;
			boolean isUplinkFrequency = false;
			boolean isWlFrequency = false;
			//if (Opr_mode.equals("4")) {// &&(!env_record_save)){
				/*
				 * int trueAngleWithOffset = Integer.parseInt(angle)+ptzOffset+ptz_roll_offset;
				 * 
				 * if(trueAngleWithOffset<0) { trueAngleWithOffset = 360+trueAngleWithOffset;
				 * 
				 * }
				 * 
				 * 
				 * //double emitfreq = Double.parseDouble(frequency);
				 * //pinfo.setTechnology(findEmitterTech(emitfreq));
				 * 
				 * pinfo.setAngle(trueAngleWithOffset+"");
				 */
				//pinfo.setType("Env_Mode");
				// pinfo.setCurrentconfigid(-1);
				// pinfo.setAlarm(isBlFrequency);
				// return pinfo;
			//} else {

				JMDeviceDataHistory jmdevice_id = jmData.get(antennaid);
				pinfo.setJmdevice_id(jmdevice_id);
				// System.out.println("INTEMEDIATE CHECK 1 "+ System.currentTimeMillis());
				// NEW UPDATE: USE PTZ instead of JM for angle.

				// check for blacklist frequency in scanned frequency
				// MaskingScreenData blMaskingScreenData;
				MaskingScreenData maskingScreenData;
				// PeakDetectionInfo blPeakInfo = null;
				double tempRBW=rbwBandwidth;
				if(!rotatePTZ) {
				if(checkBSF.equals("1")) {
					tempRBW=checkfrqBandforRBW(Double.parseDouble(frequency));
					if (tempRBW==0.0)
					tempRBW=rbwBandwidth;
				}
				}
				double startFreq = Double.parseDouble(frequency) - tempRBW / 2;
				double stopFreq = Double.parseDouble(frequency) + tempRBW / 2;
				Iterator<MaskingScreenData> itr = blackListFreq.iterator();
				while (itr.hasNext()) {
					maskingScreenData = itr.next();
					double freq = maskingScreenData.getFrequency();
					
					double blStartFreq,blStopFreq;

					 blStartFreq = maskingScreenData.getFrequency() -
					 maskingScreenData.getBandwidth()/2 - tempRBW / 2;
					 blStopFreq = maskingScreenData.getFrequency() +
					 maskingScreenData.getBandwidth()/2 + tempRBW / 2;
					// blStartFreq = maskingScreenData.getFrequency() -
					// maskingScreenData.getBandwidth()/2;
					// blStopFreq = maskingScreenData.getFrequency() +
					// maskingScreenData.getBandwidth()/2;
					// startFreq = freq - rbwBandwidth/2;
					// stopFreq = freq + rbwBandwidth/2;

					if (Double.parseDouble(frequency) >= blStartFreq &&  Double.parseDouble(frequency) <= blStopFreq) {

						/*
						 * blPeakInfo = new PeakDetectionInfo();
						 * blPeakInfo.setFrequency(String.valueOf(freq)); blPeakInfo.setPower(power);
						 * blPeakInfo.setLat(maskingScreenData.getBandwidth());
						 */

						// blMaskingScreenData = new MaskingScreenData();
						// blMaskingScreenData.setFrequency(maskingScreenData.getFrequency());
						// blMaskingScreenData.setBandwidth(maskingScreenData.getBandwidth());
						isBlFrequency = true;
						break;
					}
				}

				if (isBlFrequency) {
					// set type for peakInfo
					pinfo.setType("BL");

					// check for uplink frequency in scanned frequency
					isUplinkFrequency = checkUplinkFrequency(Double.parseDouble(frequency));
				} else {
					// check for whitelist frequency in scanned frequency
					Iterator<MaskingScreenData> itr2 = whiteListFreq.iterator();
					while (itr2.hasNext()) {
						maskingScreenData = itr2.next();
						double freq = Double.parseDouble(frequency);
						startFreq = maskingScreenData.getFrequency() - maskingScreenData.getBandwidth() / 2;
						stopFreq = maskingScreenData.getFrequency() + maskingScreenData.getBandwidth() / 2;
						if (freq >= startFreq && freq <= stopFreq) {
							isWlFrequency = true;
							break;
						}
					}

					// set type for peakInfo
					pinfo.setType(isWlFrequency ? "WL" : "Others");
				}
			//}
			if (angle == null) {
				if (DBDataService.rotatePTZ && maximized) {
					// System.out.println("PTZ CURRENT ANGLE IS "+PTZPacketSender.current_angle + "
					// FILLING IT");
					logger.debug("current angle " + PTZPacketSender.current_angle);
					if (PTZPacketSender.current_angle == 362) {
						ptz_angle = PTZPacketSender.getInstance().getCurrentAngle((ptzr.getPTZInfo(ip_add)).getPtz_ip(),
								ptz_tcp_port);
					} else {
						// Variable maintained to avoid overhead of getting angle every from PTZ
						ptz_angle = PTZPacketSender.current_angle;
					}

					int ptzWithOffset;

					ptzWithOffset = ptz_angle + ptzOffset;
					/*
					 * if(DBDataService.ptzOffset==333) { PTZData ptzData = ptzr.getPTZInfo(ip_add);
					 * DBDataService.ptzOffset = Integer.parseInt(ptzData.getPtz_offset());
					 * ptzWithOffset = ptz_angle + ptzOffset; }else { ptzWithOffset = ptz_angle +
					 * ptzOffset; }
					 */

					if (ptzWithOffset < 0) {
						ptzWithOffset = (360 + ptzWithOffset);
					}

					double emitfreq = Double.parseDouble(frequency);
					pinfo.setTechnology(findEmitterTech(emitfreq));

					pinfo.setAngle(ptzWithOffset + "");
					pinfo.setCurrentconfigid(-1);
					pinfo.setAlarm(isBlFrequency);

					String nul = "NULL";
					String query = "insert into peak_info (ip_add,power,frequency,time,device_id,angle,currentconfigid,antennaid,alarm,jmdevice_id,type,sector,technology,lat,lon) values ("
							+ "'" + pinfo.getIp_add() + "','" + pinfo.getPower() + "','" + pinfo.getFrequency() + "','"
							+ pinfo.getTime() + "'," + pinfo.getDevice_id().getId() + ",'" + pinfo.getAngle() + "',"
							+ pinfo.getCurrentconfigid() + ",'" + pinfo.getAntennaid() + "'," + pinfo.getAlarm() + ",";

					if (pinfo.getJmdevice_id() == null) {
						query = query + nul;
					} else {
						query = query + pinfo.getJmdevice_id().getId();
					}

					query = query + ",'" + pinfo.getType() + "','" + pinfo.getSector() + "','" + pinfo.getTechnology()
							+ "','" + pinfo.getLat() + "','" + pinfo.getLon() + "')";

					recordPtzPeak.add(query);

					if (recordPtzPeak.size() > 20) {
						databaseService.executeBatchInsertion(recordPtzPeak);
						recordPtzPeak.clear();
					}

					double freq = Double.parseDouble(frequency);
 
					int ptzAngle=ptzWithOffset-ptzOffset;
					if(ptzAngle>360) {
						ptzAngle=ptzAngle-360;
					}
					logger.debug(freq + "\t" + startFreqForPTZEvent + "\t" + stopFreqForPTZEvent + "\t" + startAngle
							+ "\t" + ptzWithOffset);
					if (currentEventName != null && (freq >= startFreqForPTZEvent && freq <= stopFreqForPTZEvent)
							&& (ptzAngle >= startAngle && ptzAngle <= stopAngle)) {
						maintainProbableLatLon(power, ptzWithOffset, freq, isBlFrequency);

					}

				} else {

					double emitfreq = Double.parseDouble(frequency);
					pinfo.setTechnology(findEmitterTech(emitfreq));

					if (PTZPacketSender.current_angle == 362) {
						ptz_angle = PTZPacketSender.getInstance().getCurrentAngle((ptzr.getPTZInfo(ip_add)).getPtz_ip(),
								ptz_tcp_port);
					} else {
						// Variable maintained to avoid overhead of getting angle every from PTZ
						ptz_angle = PTZPacketSender.current_angle;
					}

					int ptzWithOffset;

					ptzWithOffset = ptz_angle + ptzOffset;

					if (ptzWithOffset < 0) {
						ptzWithOffset = (360 + ptzWithOffset);
					}

					pinfo.setAngle(ptzWithOffset + "");
					pinfo.setCurrentconfigid(-1);
					pinfo.setAlarm(isBlFrequency);

					String nul = "NULL";
					String query = "insert into peak_info (ip_add,power,frequency,time,device_id,angle,currentconfigid,antennaid,alarm,jmdevice_id,type,sector,technology,lat,lon) values ("
							+ "'" + pinfo.getIp_add() + "','" + pinfo.getPower() + "','" + pinfo.getFrequency() + "','"
							+ pinfo.getTime() + "'," + pinfo.getDevice_id().getId() + ",'" + pinfo.getAngle() + "',"
							+ pinfo.getCurrentconfigid() + ",'" + pinfo.getAntennaid() + "'," + pinfo.getAlarm() + ",";

					if (pinfo.getJmdevice_id() == null) {
						query = query + nul;
					} else {
						query = query + pinfo.getJmdevice_id().getId();
					}

					query = query + ",'" + pinfo.getType() + "','" + pinfo.getSector() + "','" + pinfo.getTechnology()
							+ "','" + pinfo.getLat() + "','" + pinfo.getLon() + "')";

					recordPtzPeak.add(query);

					if (recordPtzPeak.size() > 20) {
						//if (!(Opr_mode.equals("4") && !env_record_save)) {
							databaseService.executeBatchInsertion(recordPtzPeak);
					//	}
						recordPtzPeak.clear();

					}
				}
			}
		}
            
    	} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			logger.error("record ptz peak info :" + e);
		}

		return pinfo;
	}

	@CrossOrigin
	// @Transactional
	public PeakDetectionInfo recordPeakInfo(String ip_add, String power, String frequency, String time,
			String antennaid, String angle) {
		PeakDetectionInfo pinfo = new PeakDetectionInfo();
		try {
			
			DeviceInfo dinfo1 = dinfo.getDataByIP(ip_add);
			pinfo.setDevice_id(dinfo1);
			pinfo.setFrequency(frequency);
			pinfo.setIp_add(ip_add);
			pinfo.setPower(power);
			pinfo.setTime(time);
			pinfo.setAntennaid(antennaid);
			pinfo.setSector(sectorMapping.get(antennaid));
			pinfo.setInserttime(new Date());
			pinfo.setLat(Double.parseDouble(dinfo1.getLat()));
			pinfo.setLon(Double.parseDouble(dinfo1.getLon()));
		    if(!frequency.equalsIgnoreCase("NaN") ) {
			boolean isBlFrequency = false;
			boolean isUplinkFrequency = false;
			boolean isWlFrequency = false;
			int ptz_angle;
			PeakDetectionInfo blPeakInfo = null;
			//if (Opr_mode.equals("4")) {// &&(!env_record_save)){
				/*
				 * int trueAngleWithOffset = Integer.parseInt(angle) + ptzOffset +
				 * ptz_roll_offset;
				 * 
				 * if (trueAngleWithOffset < 0) { trueAngleWithOffset = 360 +
				 * trueAngleWithOffset;
				 * 
				 * }
				 */

				// double emitfreq = Double.parseDouble(frequency);
				// pinfo.setTechnology(findEmitterTech(emitfreq));

				// pinfo.setAngle(trueAngleWithOffset + "");
				//isWlFrequency = true;
				//pinfo.setType("Env_Mode");
				// pinfo.setCurrentconfigid(-1);
				// pinfo.setAlarm(isBlFrequency);
				// return pinfo;
			//} else {

				JMDeviceDataHistory jmdevice_id = jmData.get(antennaid);
				pinfo.setJmdevice_id(jmdevice_id);
				// System.out.println("INTEMEDIATE CHECK 1 "+ System.currentTimeMillis());
				// NEW UPDATE: USE PTZ instead of JM for angle.

				// check for blacklist frequency in scanned frequency
				// MaskingScreenData blMaskingScreenData;
			
				MaskingScreenData maskingScreenData;
				double tempRBW=0.0;
				if(checkBSF.equals("1")) {
					tempRBW=checkfrqBandforRBW(Double.parseDouble(frequency));
					if (tempRBW==0.0)
					tempRBW=rbwBandwidth;
				}
				else
					tempRBW=rbwBandwidth;
				double startFreq = Double.parseDouble(frequency) - tempRBW / 2;
				double stopFreq = Double.parseDouble(frequency) + tempRBW / 2;
				Iterator<MaskingScreenData> itr = blackListFreq.iterator();
				
				
				
				while (itr.hasNext()) {
					maskingScreenData = itr.next();
					double freq = maskingScreenData.getFrequency();
					double blStartFreq,blStopFreq;

					 blStartFreq = maskingScreenData.getFrequency() -
					 maskingScreenData.getBandwidth()/2 - tempRBW / 2;
					 blStopFreq = maskingScreenData.getFrequency() +
					 maskingScreenData.getBandwidth()/2 + tempRBW / 2;
					// startFreq = freq - rbwBandwidth/2;
					//stopFreq = freq + rbwBandwidth/2;

								 
					//if (freq >= startFreq && freq <= stopFreq) {
					 if ( Double.parseDouble(frequency) >= blStartFreq &&  Double.parseDouble(frequency) <= blStopFreq) {
						blPeakInfo = new PeakDetectionInfo();
						blPeakInfo.setFrequency(String.valueOf(freq));
						blPeakInfo.setPower(power);
						blPeakInfo.setLat(maskingScreenData.getBandwidth());
						blPeakInfo.setIp_add(ip_add);
					

						// blMaskingScreenData = new MaskingScreenData();
						// blMaskingScreenData.setFrequency(maskingScreenData.getFrequency());
						// blMaskingScreenData.setBandwidth(maskingScreenData.getBandwidth());
						isBlFrequency = true;
						break;
					}
				}

				if (isBlFrequency) {
					// set type for peakInfo
					pinfo.setType("BL");

					// check for uplink frequency in scanned frequency
					isUplinkFrequency = checkUplinkFrequency(Double.parseDouble(blPeakInfo.getFrequency()));
				} else {
					// check for whitelist frequency in scanned frequency
					Iterator<MaskingScreenData> itr2 = whiteListFreq.iterator();
					while (itr2.hasNext()) {
						maskingScreenData = itr2.next();
						double freq = Double.parseDouble(frequency);
						startFreq = maskingScreenData.getFrequency() - maskingScreenData.getBandwidth() / 2;
						stopFreq = maskingScreenData.getFrequency() + maskingScreenData.getBandwidth() / 2;
						if (freq >= startFreq && freq <= stopFreq) {
							isWlFrequency = true;
							break;
						}
					}

					// set type for peakInfo
					pinfo.setType(isWlFrequency ? "WL" : "Others");
				}
			//}
			if (angle == null) {
				if (DBDataService.rotatePTZ) {
					System.out.println("PTZ CURRENT ANGLE IS " + PTZPacketSender.current_angle + " FILLING IT");
					logger.warn("current angle " + PTZPacketSender.current_angle);
					if (PTZPacketSender.current_angle == 362) {
						ptz_angle = PTZPacketSender.getInstance().getCurrentAngle((ptzr.getPTZInfo(ip_add)).getPtz_ip(),
								ptz_tcp_port);
					} else {
						// Variable maintained to avoid overhead of getting angle every from PTZ
						ptz_angle = PTZPacketSender.current_angle;
					}

					int ptzWithOffset;

					ptzWithOffset = ptz_angle + ptzOffset;

					if (ptzWithOffset < 0) {
						ptzWithOffset = (360 + ptzWithOffset);
					}

					pinfo.setAngle(ptzWithOffset + "");
					pinfo.setCurrentconfigid(-1);
					pinfo.setAlarm(isBlFrequency);

					double freq = Double.parseDouble(frequency);
					int ptzAngle=ptzWithOffset-ptzOffset;
					if(ptzAngle>360) {
						ptzAngle=ptzAngle-360;
					}
					logger.warn(freq + "\t" + startFreqForPTZEvent + "\t" + stopFreqForPTZEvent + "\t" + startAngle
							+ "\t" + ptzWithOffset);
					if (currentEventName != null && (freq >= startFreqForPTZEvent && freq <= stopFreqForPTZEvent)
							&& (ptzAngle > startAngle && ptzAngle < stopAngle)) {
						maintainProbableLatLon(power, ptzWithOffset, freq, isBlFrequency);
					}
				}

			} else {

				int trueAngleWithOffset = Integer.parseInt(angle) + ptzOffset + ptz_roll_offset;
				if (trueAngleWithOffset < 0) {
					trueAngleWithOffset = 360 + trueAngleWithOffset;
				}

				double emitfreq = Double.parseDouble(frequency);
				pinfo.setTechnology(findEmitterTech(emitfreq));

				pinfo.setAngle(trueAngleWithOffset + "");
				pinfo.setCurrentconfigid(-1);
				pinfo.setAlarm(isBlFrequency);

				// send to finley in case of bl frequency data recieved
				if (isAutoEnabled) {
					if (isBlFrequency) {
						if (blPeakInfo != null) {
							blPeakInfo.setAngle(pinfo.getAngle());
							peakCacheBl.add(blPeakInfo);
							//frequency=	blPeakInfo.getFrequency();
						
							// add to falconData queue if unique bl frequency is recieved
							if (isUplinkFrequency && isTmdasEnabled) {
								if (falconData.containsKey(blPeakInfo.getFrequency())) {
								
									float pow = Float.parseFloat(falconData.get(blPeakInfo.getFrequency()).getPower());
									float temp = Float.parseFloat(power);
									if (temp > pow)
										falconData.put(blPeakInfo.getFrequency(), blPeakInfo);
								} else {
									falconData.put(blPeakInfo.getFrequency(), blPeakInfo);
								}
							}
						}
					} else if (!isWlFrequency) {
						peakCacheOther.add(pinfo);
					}
				}

				if (isBlFrequency) {
					// send To Finley only bl peakinfo
					String transId = generateTransId();
					// sendToFinley(transId, transId, ip_add, frequency, angle, power, -1.0, false,
					// -1.0, -1.0);
				}

				if (!isWlFrequency) {
					if (angle.equals("30")) {
						synchronized (antenna30Power) {
							antenna30Power.add(Double.parseDouble(power));
							antenna30Freq.add(Double.parseDouble(frequency));
						}
					} else if (angle.equals("90")) {
						synchronized (antenna90Power) {
							antenna90Power.add(Double.parseDouble(power));
							antenna90Freq.add(Double.parseDouble(frequency));
						}
					} else if (angle.equals("150")) {
						synchronized (antenna150Power) {
							antenna150Power.add(Double.parseDouble(power));
							antenna150Freq.add(Double.parseDouble(frequency));
						}
					}
				}
			}
		  }
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println(e);
			logger.error("record peak info :" + e);
		}
		return pinfo;
	}

	public void maintainProbableLatLon(String power, int ptzWithOffset, double maximizeFrequency, boolean bl) {

		if (PTZPacketSender.scanVal > DBDataService.scanTemp) {
			// send data to CS
			// distanceMapTemp.clear();
			if (powerTemp > -200d) {

				// sajal added on 11-01-2019
				double frequency = 0;
				String angle = "0";
				boolean isBl = false;
				if (maximizeAlarmInfo != null) {
					angle = maximizeAlarmInfo.getAngle();
					isBl = maximizeAlarmInfo.getAlarm();
					frequency = Double.parseDouble(maximizeAlarmInfo.getFrequency());
				}

				DBDataService.scanTemp = PTZPacketSender.scanVal;
				double exp = (27.55 - (20 * Math.log10(frequency)) + (emitterPower - powerTemp)) / 20.0;
				double distance = Math.round(Math.pow(10.0, exp) * 100.0) / 100.0;

				AlarmInfo arinfo = createAlarm("", "", "maximize", eventData.getEventName(), powerTemp.toString(),
						String.valueOf(frequency), angle, distance, 0, PTZPacketSender.scanVal, isBl, emitterPower,
						eventData.getBandwidth());
				ar.save(arinfo);

				// AlarmInfo arinfo = createAlarm("maximize", eventData.getEventName(),
				// powerTemp.toString(), String.valueOf(maximizeFrequency),
				// String.valueOf(ptzWithOffset), distance, 0, PTZPacketSender.scanVal);
				// ar.save(arinfo);
				sendAlarm(arinfo);
				logger.debug("alarm info"+arinfo);
				System.out.println("Final Power sent   " + powerTemp);

				// new code
				// maximizeAlarmInfo = null;
				List<Double> temp = new ArrayList<Double>();
				temp.add(distance);
				temp.add(Double.valueOf(angle));
				temp.add(powerTemp);
				distanceMapTemp.add(temp);
				powerTemp = -200d;
			}
		} // Need to handle for last scan rotation, whatever number is decided.

		if (powerTemp < Double.parseDouble(power)) {
			double exp = (27.55 - (20 * Math.log10(maximizeFrequency)) + (emitterPower - Float.parseFloat(power))) / 20.0;
			double distance = Math.round(Math.pow(10.0, exp) * 100.0) / 100.0;

			AlarmInfo arinfo = createAlarm("", "", "maximize", eventData.getEventName(), power,
					String.valueOf(maximizeFrequency), String.valueOf(ptzWithOffset), distance, 0,
					PTZPacketSender.scanVal, bl, emitterPower, eventData.getBandwidth());
			if (maximizeAll) {
				ar.save(arinfo);
				sendAlarm(arinfo);
			}
			maximizeAlarmInfo = arinfo;
			// above code added sajal 11-01-19

			powerTemp = Double.parseDouble(power);
//			List temp = new ArrayList();
//			temp.add(distance);
//			temp.add(ptzWithOffset);
//			temp.add(powerTemp);
//			distanceMapTemp.put(maximizeFrequency, temp);
		} // below code added sajal 11-01-19
		else {
			if (maximizeAll) {
				double exp = (27.55 - (20 * Math.log10(maximizeFrequency)) + (emitterPower - Float.parseFloat(power))) / 20.0;
				double distance = Math.round(Math.pow(10.0, exp) * 100.0) / 100.0;

				AlarmInfo arinfo = createAlarm("", "", "maximize", eventData.getEventName(), power,
						String.valueOf(maximizeFrequency), String.valueOf(ptzWithOffset), distance, 0,
						PTZPacketSender.scanVal, bl, emitterPower, eventData.getBandwidth());
				ar.save(arinfo);
				logger.debug(" maximizeAll info"+arinfo);
				sendAlarm(arinfo);
			}
		}
	}

	public void calculateCentroid(long duration) {

		String transId = eventData.getTransId();
		String cueId = eventData.getCueId();

		String currentName = eventData.getEventName();

		if (distanceMapTemp.size() == 0) {
			if (powerTemp > -200d) {

				// sajal added on 11-01-2019
				double frequency = 0;
				String angle = "0";
				boolean isBl = false;
				if (maximizeAlarmInfo != null) {
					angle = maximizeAlarmInfo.getAngle();
					isBl = maximizeAlarmInfo.getAlarm();
					frequency = Double.parseDouble(maximizeAlarmInfo.getFrequency());
				}

				DBDataService.scanTemp = PTZPacketSender.scanVal;
				double exp = (27.55 - (20 * Math.log10(frequency)) + (emitterPower - powerTemp)) / 20.0;
				double distance = Math.round(Math.pow(10.0, exp) * 100.0) / 100.0;

				AlarmInfo arinfo = createAlarm("", "", "maximize", eventData.getEventName(), powerTemp.toString(),
						String.valueOf(frequency), angle, distance, 0, PTZPacketSender.scanVal, isBl, emitterPower,
						eventData.getBandwidth());
				ar.save(arinfo);

				// AlarmInfo arinfo = createAlarm("maximize", eventData.getEventName(),
				// powerTemp.toString(), String.valueOf(maximizeFrequency),
				// String.valueOf(ptzWithOffset), distance, 0, PTZPacketSender.scanVal);
				// ar.save(arinfo);
				// sendAlarm(arinfo);
				System.out.println("Final Power sent   " + powerTemp);

				// new code
				// maximizeAlarmInfo = null;
				List<Double> temp = new ArrayList<Double>();
				temp.add(distance);
				temp.add(Double.valueOf(angle));
				temp.add(powerTemp);
				distanceMapTemp.add(temp);
				powerTemp = -200d;
			}
		}

		// use distanceMapTemp map to calculate value of centroid after trigger recieved
		if (distanceMapTemp.size() > 0) {
			boolean isBlFrequency = false;
			double maximizedFreq = frequencyForPTZEvent;

			// calculate centroid
			double[] values = calculateAngleAndDistanceAndPower(distanceMapTemp);

			// check for blacklist frequency in scanned frequency
			MaskingScreenData maskingScreenData;
			double startFreq;
			double stopFreq;
			if (blackListFreq != null && blackListFreq.size() > 0) {
				Iterator<MaskingScreenData> i = blackListFreq.iterator();

				while (i.hasNext()) {
					maskingScreenData = i.next();
					startFreq = maskingScreenData.getFrequency() - maskingScreenData.getBandwidth() / 2;
					stopFreq = maskingScreenData.getFrequency() + maskingScreenData.getBandwidth() / 2;
					if (maximizedFreq >= startFreq && maximizedFreq <= stopFreq) {
						isBlFrequency = true;
						break;
					}
				}
			}

			int sample = PTZPacketSender.scanVal;// distanceMapTemp.size();
			AlarmInfo arinfo = createAlarm(transId, cueId, "centroid", eventData.getEventName(),
					String.format("%.2f", values[0]), String.valueOf(maximizedFreq), String.valueOf(values[1]),
					Math.round(values[2] * 100.0) / 100.0, (int) duration, sample, isBlFrequency, emitterPower,
					eventData.getBandwidth());

			// Send triggers to different nodes based on condition and save data.
			if (currentName.equals(autoEvent) && isBlFrequency) {
				// send trigger to TMDAS
				// sendToFalcon(eventData.getDevice_ip(), maximizedFreq, , eventType, latitude,
				// longitude, timeout, validity);
			}
			if (currentName.equals(ugsEvent) && isTmdasEnabled && checkUplinkFrequency(frequencyForPTZEvent)) {
				// send trigger to TMDAS
				// check for GSM and WCDMA frequency before sending
				ColorStripsData colorStripsData;
				Iterator<ColorStripsData> coloritr = threegUplinkFreq.iterator();
				boolean isthreegUplinkFrequency = false;

				while (coloritr.hasNext()) {
					colorStripsData = coloritr.next();
					double freq = frequencyForPTZEvent;
					double startFreq1 = colorStripsData.getStartfreq();
					double stopFreq1 = colorStripsData.getStopfreq();
					if (freq >= startFreq1 && freq <= stopFreq1) {
						isthreegUplinkFrequency = true;
						break;
					}
				}

				coloritr = fourgUplinkFreq.iterator();
				boolean isfourgUplinkFrequency = false;

				while (coloritr.hasNext()) {
					colorStripsData = coloritr.next();
					double freq = frequencyForPTZEvent;
					double startFreq1 = colorStripsData.getStartfreq();
					double stopFreq1 = colorStripsData.getStopfreq();
					if (freq >= startFreq1 && freq <= stopFreq1) {
						isfourgUplinkFrequency = true;
						break;
					}
				}

				double bandw;
				if (isfourgUplinkFrequency)
					bandw = fourgBandwidth;
				else if (isthreegUplinkFrequency)
					bandw = threegBandwidth;
				else
					bandw = twogBandwidth;
				// bandw =
				// calculateRbwBandwidth(bandwidth);//(Math.round((bandwidth*1000/(maxSamples-1))/100)+1)*100/1000.0;//changed
				// here *2/1000.0;

				sendToFalcon(transId, cueId, Common.nodeIpMap.get(nodeIdMap.get(3)), frequencyForPTZEvent, bandw,
						arinfo.getAngle(), 5, 0.0, 0.0, timeout, validity);
			}

			// send trigger to CMS
			double[] latlon = getDeviceLatLon();
			if (latlon != null) {
				latlon = calculatePoint(latlon[0], latlon[1], arinfo.getRange(), values[1]);
				arinfo.setLatitude(latlon[0]);
				arinfo.setLongitude(latlon[1]);

				sendToFinley(transId, cueId, Common.nodeIpMap.get(nodeIdMap.get(3)), String.valueOf(maximizedFreq),
						arinfo.getAngle(), arinfo.getPower(), arinfo.getRange(), true, latlon[0], latlon[1]);
			}
			// save data in db
			ar.save(arinfo);
			sendAlarm(arinfo);

			distanceMapTemp.clear();
			PTZPacketSender.scanVal = 1;
			DBDataService.scanTemp = 1;
		} else {
			if (currentName.equals(ugsEvent) && isTmdasEnabled && checkUplinkFrequency(frequencyForPTZEvent)) {

//				double device_lat = java.lang.Math.toRadians(Double.parseDouble(device_info.getLat()));
//				double device_lon = java.lang.Math.toRadians(Double.parseDouble(device_info.getLon()));
//				double ugs_lat = java.lang.Math.toRadians(Double.parseDouble(eventData.getLatitude()));
//				double ugs_lon = java.lang.Math.toRadians(Double.parseDouble(eventData.getLongitude()));
//				
//				double angleInDegree = angleFromCoordinate(device_lat, device_lon, ugs_lat, ugs_lon);
				// send trigger to tmdas
				sendToFalcon(transId, cueId, Common.nodeIpMap.get(nodeIdMap.get(3)), -1, 0, "0", 5,
						Double.parseDouble(eventData.getLatitude()), Double.parseDouble(eventData.getLongitude()),
						timeout, validity);
				// send trigger to cms
				// sendToFinley(deviceIp, frequencyForPTZEvent, angle, power, probableDistance,
				// evevntType, eventData.getLatitude(), eventData.getLongitude());
			}
			// save data in db
			createAuditlog("Cue", "Finished\t" + currentName + "\tCueId:" + cueId + "\tNo Frequency Found");
		}
	}

	/* ---------------Replay Feature-------------- */
	public JSONArray sendReplayToFinley(String startTime, String endTime) {
		// Getting Records From DB For ReplayFeature
		AuditLog auditLog = new AuditLog();
		List<AuditLog> auditLogList = alr.getReplayRecords(startTime, endTime, "Cue");
		JSONArray jsonArray = new JSONArray();

		for (int i = 0; i < auditLogList.size(); i++) {
			JSONObject jb = new JSONObject();
			try {

				// Getting Source From Description
				String desc = auditLogList.get(i).getDescription();
				String[] arr = desc.split("\t");
				String source = "";
				if (arr[0].equalsIgnoreCase("sent"))
					source = "HUMMER";
				else
					source = arr[1];

				// Getting CueId From Description
				String cueId = arr[2].split(":")[1];
				String text = "";
				for (int j = 0; j < arr.length; j++) {
					if (j != arr.length - 1)
						text = text + arr[j] + ',';
					else
						text = text + arr[j];
				}

				jb.put("DATE", auditLogList.get(i).getCreatedDateTime());
				jb.put("DETAIL", text);
				jb.put("CUE_ID", cueId);
				jb.put("SOURCE", source);
				jsonArray.put(jb);
			} catch (JSONException e) {
				logger.error(e.toString());
			}

		}

		return jsonArray;
		/* return Response.status(201).entity(jsonList.toString()).build(); */
	}
	/* ---------------Replay Feature-------------- */

	public boolean checkUplinkFrequency(Double frequency) {

		Iterator<ColorStripsData> itr1 = twogUplinkFreq.iterator();
		ColorStripsData colorStripsData;
		double startFreq;
		double stopFreq;
		boolean isUplinkFrequency = false;

		while (itr1.hasNext()) {
			colorStripsData = itr1.next();
			startFreq = colorStripsData.getStartfreq();
			stopFreq = colorStripsData.getStopfreq();
			if (frequency > startFreq && frequency < stopFreq) {
				isUplinkFrequency = true;
				break;
			}
		}

		if (!isUplinkFrequency) {
			itr1 = threegUplinkFreq.iterator();
			while (itr1.hasNext()) {
				colorStripsData = itr1.next();
				startFreq = colorStripsData.getStartfreq();
				stopFreq = colorStripsData.getStopfreq();
				if (frequency > startFreq && frequency < stopFreq) {
					isUplinkFrequency = true;
					break;
				}
			}
		}

		if (!isUplinkFrequency) {
			itr1 = fourgUplinkFreq.iterator();
			while (itr1.hasNext()) {
				colorStripsData = itr1.next();
				startFreq = colorStripsData.getStartfreq();
				stopFreq = colorStripsData.getStopfreq();
				if (frequency > startFreq && frequency < stopFreq) {
					isUplinkFrequency = true;
					break;
				}
			}
		}

		return isUplinkFrequency;
	}

	private AlarmInfo createAlarm(String transid, String cueid, String type, String trigger, String power, String freq,
			String angle, double range, int duration, int sample, boolean alarm, double emitterpower, double bandwidth) {
		AlarmInfo arinfo = new AlarmInfo();

		Date now = new Date();
		String time = sdf.format(now);

		arinfo.setTransid(transid);
		arinfo.setCueid(cueid);
		arinfo.setIp_addr(Common.nodeIpMap.get(nodeIdMap.get(3)));
		arinfo.setType(type);
		arinfo.setTrigger(trigger);
		arinfo.setPower(power);
		arinfo.setFrequency(freq);
		arinfo.setTime(time);
		arinfo.setAngle(angle);
		arinfo.setRange(range);
		arinfo.setDuration(duration);
		arinfo.setSample(sample);
		arinfo.setLatitude(0.0);
		arinfo.setLongitude(0.0);
		arinfo.setAlarm(alarm);
		arinfo.setEmitterpower(emitterpower);
		arinfo.setBandwidth(bandwidth);

		return arinfo;
	}

	public void createAuditlog(String type, String desc) {
		try {
		
		Date now = new Date();
		String time = sdf.format(now);
		AuditLog auditLog = new AuditLog();

		auditLog.setCreatedDateTime(time);
		auditLog.setLogType(type);
		auditLog.setDescription(desc);
        
		alr.save(auditLog);
		if (type.equals("Cue"))
			sendCueForEvent(time, type, desc);
		}
		catch(Exception e) {
			int a =10;
			System.out.print("Exception in"+type+desc);
		}
	}

	public void sendPeakDataToSocket(List<PeakDetectionInfo> peakDataList) {
		messagingTemplate.convertAndSend("/topic/PeakInfo", peakDataList);
	}

	/*----Sahil----*/
	public void sendLatitudeLongitudeToSocket(String lat, String lon, int ptzoffset) {
		DeviceDataInfo deviceDataInfo = new DeviceDataInfo(lat, lon, ptzoffset);
		messagingTemplate.convertAndSend("/topic/LatLongValue", deviceDataInfo);
	}
	/*----Sahil----*/

	// Send data related to triggered and auto events to ui.
	public void sendDataForEvent(String eventName, double frequency, int startAngle, int stopAngle) {
		FreqAutoInfo freqAuto = new FreqAutoInfo(eventName, frequency, startAngle, stopAngle);

		messagingTemplate.convertAndSend("/topic/FreqAutoEvent", freqAuto);
	}

	public void sendStatusForNode(List<NodeData> nodeData) {
		messagingTemplate.convertAndSend("/topic/NodeStatus", nodeData);
	}

	public void sendStatusForJMNode(List<JMDeviceData> jmNodeData) {
		messagingTemplate.convertAndSend("/topic/JmDeviceData", jmNodeData);
	}

	public void sendAlarm(AlarmInfo arinfo) {
		messagingTemplate.convertAndSend("/topic/AlarmInfo", arinfo);
	}

	public void sendCurrentEvent(String event) {
		messagingTemplate.convertAndSend("/topic/curevent", event);
	}
	public void sendCurrentMode(String Mode) {
		messagingTemplate.convertAndSend("/topic/currentmode", Mode);
	}
	public void sendSensorConfig() {
		List<SensorConfig> sensorConfig = getSensorConfig();
		messagingTemplate.convertAndSend("/topic/configdeploy", sensorConfig);
	}

	public void sendCueForEvent(String timeStamp, String type, String desc) {
		Event event = new Event();
		event.setEventDate(timeStamp);
		String[] arr = desc.split("\t");

		String source = "";
		if (arr[0].equalsIgnoreCase("sent"))
			source = "HUMMER";
		else
			source = arr[1];
		event.setNodeType(source);

		String cueId = arr[2].split(":")[1];

		String text = "";
		for (int i = 0; i < arr.length; i++) {
			if (i != arr.length - 1)
				text = text + arr[i] + ',';
			else
				text = text + arr[i];
		}

		event.setEventType(type);
		event.setEventData(text);

		messagingTemplate.convertAndSend("/topic/EventCueInfo", event);
		sendCueToFinley(timeStamp, cueId, source, text);
	}

	@CrossOrigin
	@Transactional
	public void recordLEDInfo(String ip_add) {
		try {
			if (!ledon) {
				ledon = true;
				System.out.println("----led_on------");
				DeviceInfo dinfo1 = dinfo.getDataByIP(ip_add);
				System.out.println("Object--- " + dinfo1);
				LEDData lData = new LEDData();
				lData.setDevice_id(dinfo1);
				lData.setIp_add(ip_add);
				lData.setLed_on(1);
				lr.save(lData);

				messagingTemplate.convertAndSend("/topic/LEDInfo", lr.getLEDStatusByIP(ip_add));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		// lr.recordLEDInfo(ip_add,led_on);
	}

	@Transactional
	public void updateIs_ActiveFalse() {
		System.out.println("Making All Active States False.");
		try {
			dinfo.updateIsActive("false");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@Transactional
	public Map<String, List<PeakInfoPojo>> getPeakInfoRecordBetweenTime(String startTime, String endTime,
			Long deviceId) {
		try {

			DeviceInfo deviceInfo = this.getDeviceInfo(deviceId);
			List<PeakDetectionInfo> peaks = pr.getPeakInfoRecordBetweenTime(startTime, endTime, deviceInfo);
			List<PeakInfoPojo> peaksPojo = new ArrayList<>();
			Map<String, List<PeakInfoPojo>> peaksDict = new TreeMap();
			for (PeakDetectionInfo peak : peaks) {
				PeakInfoPojo peakInfo = new PeakInfoPojo();
				peakInfo.setDeviceName(peak.getDevice_id().getName());
				peakInfo.setFrequency(peak.getFrequency());
				peakInfo.setIp_add(peak.getIp_add());
				peakInfo.setPower(peak.getPower());
				peakInfo.setTime(peak.getTime());
				peakInfo.setId(peak.getId());
				peakInfo.setPtzangle(peak.getAngle());
				peakInfo.setCurrentconfigid(peak.getCurrentconfigid());

				if (peaksDict.get(peakInfo.getTime()) != null) {
					peaksDict.get(peakInfo.getTime()).add(peakInfo);
				} else {

					List<PeakInfoPojo> newPeakList = new ArrayList<>();
					newPeakList.add(peakInfo);
					peaksDict.put(peakInfo.getTime(), newPeakList);
				}

			}

			return peaksDict;
		} catch (Exception exception) {
			exception.printStackTrace();
			throw exception;
		}
	}

	@CrossOrigin
	@Transactional
	public void startSps(String ip_add) {
		try {
			recordConfigData(getConfigData(ip_add), (dinfo.getDataByIP(ip_add)).getId(), "all", "1");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@CrossOrigin
	@Transactional
	public void factoryResetDevice(String ip_add) {
		try {
			System.out.println("DBDataService: factoryResetDevice");

			createAuditlog("Reset", "Factory Reset Device");

			// delete all config history
			chr.deleteAll();

			// set default data config
			recordDeviceConfigData(ip_add, null, null, null, null, null, null);

			// apply config data on device
			recordConfigData(getConfigData(ip_add), (dinfo.getDataByIP(ip_add)).getId(), "all", "1");

			// apply default System Configuration
			SystemConfigurationData systemConfigurationData = new SystemConfigurationData();
			systemConfigurationData.setUgs(1);
			systemConfigurationData.setTmdas(0);
			systemConfigurationData.setAuto(0);
			systemConfigurationData.setTimeout(1);
			systemConfigurationData.setValidity(1);
			systemConfigurationData.setSelftimeout(1.0);
			systemConfigurationData.setSelfvalidity(1);
			recordSystemConfigurationData(systemConfigurationData);

			// clear all blacklist and whitelist frequencies
			mstScr.deleteMaskingDataListByProfile(defaultProfile);
			blackListFreq.clear();
			whiteListFreq.clear();

			// clear all colorStripData excluding default
			cStr.deleteColorCodeListByVisiblity(true, 0, defaultProfile);

			updateEmitterPowerMap();

			// delete all peak data
			// pr.deleteAll(); // can use truncate also.
			// delete audit log
			// alr.deleteAll();
			// ptzr.deleteAll();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	@CrossOrigin
	@Transactional
	public ConfigProfileData getConfigProfile(String profileName) {
		return cpr.getConfigProfileDataByProfile(profileName);
	}

	@CrossOrigin
	@Transactional
	public void applyProfileData(String profileName) {
		try {
			logger.info("DBDataService: applyProfileData");

			createAuditlog("Profile Action", "Apply Profile " + profileName);

			deleteProfileData(defaultProfile);

			// add all sps configuration as new
			try {
				List<ConfigData> c = new ArrayList<ConfigData>();
				c.add(ConfigProfileData.copy(cpr.getConfigProfileDataByProfile(profileName)));
				recordConfigData(c, (dinfo.getDataByIP(getDeviceIp())).getId(), "all", "1");
			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			// add all System Configuration data to particular profile
			SystemConfigurationData scd = scr.getSystemConfigurationData(profileName);
			scr.save(scd.createCopy(scd, defaultProfile));

			// add all blacklist and whitelist frequencies to particular profile
			blackListFreq.clear();
			whiteListFreq.clear();
			Iterator<MaskingScreenData> msd = mstScr.getMaskingDataList(profileName).iterator();
			while (msd.hasNext()) {
				MaskingScreenData maskingData = msd.next();
				if (maskingData.getFreqtype().equalsIgnoreCase("blacklist")) {
					blackListFreq.add(maskingData);
				} else if (maskingData.getFreqtype().equalsIgnoreCase("whitelist")) {
					whiteListFreq.add(maskingData);
				}
				mstScr.save(maskingData.createCopy(maskingData, defaultProfile));
			}

			// add all colorStripData excluding default to particular profile
			Iterator<ColorStripsData> csd = cStr.getColorCodeListByVisiblityAndId(true, 0, profileName).iterator();
			while (csd.hasNext()) {
				ColorStripsData cd = csd.next();
				cStr.save(cd.createCopy(cd, defaultProfile));
			}

			updateEmitterPowerMap();

			// update systemConfiguratioData from db
			// SystemConfigurationData systemConfigurationData =
			// getSystemConfiguration(defaultProfile);
			isManualOverride = scd.getUgs() == 1 ? true : false;
			isTmdasEnabled = scd.getTmdas() == 1 ? true : false;
			isAutoEnabled = scd.getAuto() == 1 ? true : false;
			timeout = scd.getTimeout();
			selfTimeout = scd.getSelftimeout();
			validity = scd.getValidity();
			selfValidity = scd.getSelfvalidity();

			// update maskingScreenData from db

			/*
			 * Iterator<MaskingScreenData> ms =
			 * getMaskingDataList(defaultProfile).iterator(); while(ms.hasNext()) {
			 * MaskingScreenData maskingData = ms.next();
			 * if(maskingData.getFreqtype().equalsIgnoreCase("blacklist")) {
			 * blackListFreq.add(maskingData); } else
			 * if(maskingData.getFreqtype().equalsIgnoreCase("whitelist")) {
			 * whiteListFreq.add(maskingData); } }
			 */

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	@CrossOrigin
	@Transactional
	public void saveProfileData(String profileName) {
		try {
			logger.info("DBDataService: saveProfileData");

			createAuditlog("Profile Action", "Save Profile " + profileName);

			// add sps configuration
			cpr.save(ConfigProfileData.copy(cr.getConfigDataByRoom("1"), profileName));

			// add all System Configuration data to particular profile
			SystemConfigurationData scd = scr.getSystemConfigurationData(defaultProfile);
			scr.save(scd.createCopy(scd, profileName));

			// add all blacklist and whitelist frequencies to particular profile
			Iterator<MaskingScreenData> msd = mstScr.getMaskingDataList(defaultProfile).iterator();
			while (msd.hasNext()) {
				MaskingScreenData md = msd.next();
				mstScr.save(md.createCopy(md, profileName));
			}

			// add all colorStripData excluding default to particular profile
			Iterator<ColorStripsData> csd = cStr.getColorCodeListByVisiblityAndId(true, 0, defaultProfile).iterator();
			while (csd.hasNext()) {
				ColorStripsData cd = csd.next();
				cStr.save(cd.createCopy(cd, profileName));
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	@CrossOrigin
	@Transactional
	public void deleteProfileData(String profileName) {
		try {
			logger.info("DBDataService: deleteProfileData");

			if (!profileName.equals(defaultProfile))
				createAuditlog("Profile Action", "Delete Profile " + profileName);

			// delete profile of sps
			cpr.deleteByProfile(profileName);

			// delete profile from System Configuration
			scr.deleteSystemConfigurationDataListByProfile(profileName);

			// clear all blacklist and whitelist frequencies
			mstScr.deleteMaskingDataListByProfile(profileName);

			// clear all colorStripData excluding default
			cStr.deleteColorCodeListByVisiblity(true, 0, profileName);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.error(e.getMessage());
		}
	}

	// set Switch Settings
	public void setSwitchSetting(int s1, int s2, int s3, int s4) {

		switchSetting.put("s1", s1);
		switchSetting.put("s2", s2);
		switchSetting.put("s3", s3);
		switchSetting.put("s4", s4);
	}

	// data received to change to sector
	public void switchTo(int s1, int s2, int s3, int ov, int oh) {

		if (Common.nodeIpMap.containsKey(nodeIdMap.get(7))) {
			switchConfiguration(1, s1, 5, 2, s2, 5, 3, s3, 5, 4, ov, 9);

			// send config to the SMM which is switched with preamp type
			try {
				if (s1 != switchSetting.get("s1")) {
					sendConfigDataForSMM(1, s1);
				} else if (s2 != switchSetting.get("s2")) {
					sendConfigDataForSMM(2, s2);
				} else if (s3 != switchSetting.get("s3")) {
					sendConfigDataForSMM(3, s3);
				}
				setSwitchSetting(s1, s2, s3, ov);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
	}

	// send switch command to rf switching node
	public void switchConfiguration(int sid1, int path11, int path12, int sid2, int path21, int path22, int sid3,
			int path31, int path32, int sid4, int path41, int path42) {
		List<String> seq = scfr.getSwitchConf(sid1, path11, path12, sid2, path21, path22, sid3, path31, path32, sid4,
				path41, path42);
		String command = "0001001100";

		Iterator<String> itr = seq.iterator();
		while (itr.hasNext())
			command = command + itr.next();
		if (Common.nodeIpMap.get(nodeIdMap.get(7)) != null)
			UdpPacketSender.getInstance().send(Common.nodeIpMap.get(nodeIdMap.get(7)), switch_udp_port, command,
					retries);

	}

	public void deleteCache() {
		peakCacheBl.clear();
		peakCacheOther.clear();
	}

	// Scheduler to calculate auto event and send trigger to falcon
	public static void getbandconfig()
	{
		JSONArray ja=new JSONArray();
		StringBuilder sbf=new StringBuilder();
		try
		{
		ja =getBandDataList1();
		System.out.println(ja);
		for (int i=0;i<ja.length(); i++) {
			JSONObject JO= ja.getJSONObject(i);
			String strtFrq=JO.getString("start_frq");
			String stpfrq=JO.getString("stop_frq");
			 
			  sbf.append(strtFrq);  
			  sbf.append("-"); 
			  sbf.append(stpfrq); 
			  sbf.append(","); 

			System.out.println(sbf);
		}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		bandconfig=sbf;
	}
	
	public String calculateDistance(double frequencyForPow, String maxPower) {
		
		double emtpower=calculateEmitterPower(frequencyForPow);
		double exp = (27.55 - (20 * Math.log10(frequencyForPow)) + (emtpower - Float.parseFloat(maxPower))) / 20.0;
		double distance = Math.round(Math.pow(10.0, exp) * 100.0) / 100.0;
		String distanceEmit=Double.toString(distance)+"@".concat(Double.toString(emtpower));
		return distanceEmit; 
		
	}
	public void findAutoEvent() {

		logger.warn("inside find auto event");
		List<PeakDetectionInfo> temp;
		logger.warn("inside auto 1");
		// calculate for blacklist frequency
		if (peakCacheBl != null && peakCacheBl.size() > 0) {
			logger.warn("inside auto bl event detection1");
			try {
			//temp = new ArrayList<PeakDetectionInfo>(peakCacheBl.subList(0, peakCacheBl.size()));
			temp = new ArrayList<PeakDetectionInfo>(peakCacheBl);
			logger.warn("inside auto bl event detection2");
			synchronized (temp) {
				List<Double> allPow = new ArrayList<Double>();
				logger.warn("inside auto bl event detection3");
				System.out.println("List size: " + temp.size());
				logger.warn("inside auto bl event detection4");
				if (!temp.isEmpty()) {
					
					logger.warn("inside auto bl event detection");
					/*
					 * for (int i = 0; i < temp.size(); i++) {
					 * allPow.add(Double.parseDouble(((PeakDetectionInfo) temp.get(i)).getPower()));
					 * }
					 */
					Iterator<PeakDetectionInfo> it = temp.iterator();
					while (it.hasNext()) {
						PeakDetectionInfo p = it.next();
						if (p != null) {
							allPow.add(Double.parseDouble(p.getPower()));
						}
					}
					logger.debug("0");
					double maxPower = Collections.max(allPow);
					logger.debug("1");
					int index = allPow.indexOf(maxPower);
					logger.debug("2");
					String frequencyForPow = (temp.get(index)).getFrequency();
					double bandwidth = (temp.get(index)).getLat();

					if (bandwidth == 0) {
						bandwidth = calculateBandwidth(frequencyForPow);
						bandwidth = bandwidth != 0 ? bandwidth : defaultBlBandwidth;
					}

					logger.debug("3");
					int antennaAngle = Integer.valueOf((temp.get(index)).getAngle());

					int tilt = tiltAngleProperty;

					double offset = ptzOffset;
					if (offset < 0) {
						offset = offset + 360;
					}
					offset = offset + ptz_roll_offset;
					if (offset > 360)
						offset = offset - 360;

					if (angle_is_between_angles(antennaAngle, offset, offset + 60)) {
						JMDeviceDataHistory jd = jmData.get("11");
						if (jd != null && jmValid) {
							tilt = (int) Float.parseFloat(jd.getTilt());
							logger.info("Tilt value : ----------------------- " + tilt);
						}
					} else if (angle_is_between_angles(antennaAngle, offset + 60, offset + 120)) {
						JMDeviceDataHistory jd = jmData.get("21");
						if (jd != null && jmValid) {
							tilt = (int) Float.parseFloat(jd.getTilt());
							logger.info("Tilt value : ----------------------- " + tilt);
						}
					} else if (angle_is_between_angles(antennaAngle, offset + 120, offset + 180)) {
						JMDeviceDataHistory jd = jmData.get("31");
						if (jd != null && jmValid) {
							tilt = (int) Float.parseFloat(jd.getTilt());
							logger.info("Tilt value : ----------------------- " + tilt);
						}
					}

					logger.debug("4");
					DBDataService.frequencyForAutoEvent = Double.parseDouble(frequencyForPow);
					logger.debug("5");
					DBDataService.isAutoTriggeredEvent = true;
					logger.debug("6");
					System.out.println("DBDataService:--------" + antennaAngle);
					logger.debug("7");
					/*
					 * double bandw =
					 * (Math.round(((smmStopFreq-smmStartFreq)*1000/(maxSamples-1))/100)+1)*100*2;
					 * bandw = bandw/1000;
					 */
					logger.debug("8");
					SystemConfigurationData scd = scr.getSystemConfigurationData(defaultProfile);
					/*
					 * int range=Integer.parseInt(scd.getRange()); String
					 * distanceEmit=calculateDistance(Double.parseDouble(frequencyForPow),String.
					 * valueOf(maxPower)); String[] parts = distanceEmit.split("-"); Double
					 * distance=Double.parseDouble(parts[0]); Double
					 * emtpow=Double.parseDouble(parts[1]);
					 */
					// jmData.get()
					/*if (!isFreeze && !isCalibrating)
						if(distance>range && distance>0) {
							createAuditlog("Cue", "Terminated" + "\t" + eventData.getEventName() + "\tCueId:"
									+ eventData.getCueId()+"\tfrequency:"+frequencyForPow+"\tEmtPower:"+emtpow+"\tDistance:"+distance+ "\tdistance greater than configured range");
						}
*/						//else {
					if (!isFreeze && !isCalibrating)
						addToQueue(blEvent, temp.get(index).getIp_add(), frequencyForPow, bandwidth,
								"" + (antennaAngle - 30), "" + (antennaAngle + 30), tilt, selfTimeout, selfValidity,
								LocalDateTime.now(), maxPower);
					//	}
				}
			}
			
			}
			catch (Exception e) {
				
				logger.debug("Exception in bl"+e);
				System.out.println(e);
			}
			finally {
				peakCacheBl.clear();
			}
			// temp = null;
			
		}
			

		List<PeakDetectionInfo> temp1;
		// calculate for other frequency
		logger.warn("inside auto bl event detection5");
		if (peakCacheOther != null && peakCacheOther.size() > 0) {
			try {
			logger.warn("inside auto bl event detection6");
			temp1 = new ArrayList<PeakDetectionInfo>(peakCacheOther);
			logger.warn("inside auto bl event detection7");
			synchronized (temp1) {
				logger.warn("inside auto bl event detection8");
				List<Double> allPow = new ArrayList<Double>();
				// System.out.println("List size: " + temp1.size());
				logger.warn("inside auto bl event detection9");
				if (!temp1.isEmpty()) {
					logger.warn("inside auto other event detection");
					// for(int i=0; i < temp1.size(); i++)
					// {
					Iterator<PeakDetectionInfo> it = temp1.iterator();
					while (it.hasNext()) {
						PeakDetectionInfo p = it.next();
						if (p != null) {
							allPow.add(Double.parseDouble(p.getPower()));
						}
					}
					// allPow.add(Double.parseDouble(((PeakDetectionInfo)temp1.get(i)).getPower()));
					// }
					logger.debug("0");
					double maxPower = Collections.max(allPow);
					int index = allPow.indexOf(maxPower);
					logger.debug("0");
					String frequencyForPow = (temp1.get(index)).getFrequency();
					int antennaAngle = Integer.valueOf((temp1.get(index)).getAngle());
					logger.debug("0");
					int tilt = tiltAngleProperty;

					double offset = ptzOffset;
					if (offset < 0) {
						offset = offset + 360;
					}
					offset = offset + ptz_roll_offset;
					if (offset > 360)
						offset = offset - 360;
					logger.debug("0");
					if (angle_is_between_angles(antennaAngle, offset, offset + 60)) {
						JMDeviceDataHistory jd = jmData.get("11");
						if (jd != null && jmValid) {
							tilt = (int) Float.parseFloat(jd.getTilt());
							logger.info("Tilt value : ----------------------- " + tilt);
						}
					} else if (angle_is_between_angles(antennaAngle, offset + 60, offset + 120)) {
						JMDeviceDataHistory jd = jmData.get("21");
						if (jd != null && jmValid) {
							tilt = (int) Float.parseFloat(jd.getTilt());
							logger.info("Tilt value : ----------------------- " + tilt);
						}
					} else if (angle_is_between_angles(antennaAngle, offset + 120, offset + 180)) {
						JMDeviceDataHistory jd = jmData.get("31");
						if (jd != null && jmValid) {
							tilt = (int) Float.parseFloat(jd.getTilt());
							logger.info("Tilt value : ----------------------- " + tilt);
						}
					}

					System.out.println("DBDataService:--------" + antennaAngle);

					double bandw = calculateRbwBandwidth(smmStopFreq - smmStartFreq);// (Math.round(((smmStopFreq-smmStartFreq)*1000/(maxSamples-1))/100)+1)*100;//changed
																						// here *2;
					// logger.error("#################"+smmStopFreq+"##########"+smmStartFreq+"#####"+rbwMaxSamples);
					// logger.error("#############"+bandw);
					// bandw = bandw/1000;

					double bandwidth = calculateBandwidth(frequencyForPow);
					bandwidth = bandw > bandwidth ? bandw : bandwidth;
					/*
					 * SystemConfigurationData scd = scr.getSystemConfigurationData(defaultProfile);
					 * int range=Integer.parseInt(scd.getRange()); String
					 * distanceEmit=calculateDistance(Double.parseDouble(frequencyForPow),String.
					 * valueOf(maxPower)); String[] parts = distanceEmit.split("-"); Double
					 * distance=Double.parseDouble(parts[0]); Double
					 * emtpow=Double.parseDouble(parts[1]);
					 */
					// jmData.get()
					/*if (!isFreeze && !isCalibrating)
						if(distance>range && distance>0) {
							createAuditlog("Cue", "Terminated" + "\t" + eventData.getEventName() + "\tCueId:"
									+ eventData.getCueId()+"\tfrequency:"+frequencyForPow+"\tEmtPower:"+emtpow+"\tDistance:"+distance+ "\tdistance greater than configured range");
						}else {*/
					if (!isFreeze && !isCalibrating)
								addToQueue(autoEvent, temp1.get(index).getIp_add(), frequencyForPow, bandwidth,
										"" + (antennaAngle - 30), "" + (antennaAngle + 30), tilt, selfTimeout, selfValidity,
										LocalDateTime.now(), maxPower);
							//}
				}
				}
			}catch (Exception e) {
				
				logger.debug("Exception in other"+e);
				System.out.println(e);
			}
			finally {
				peakCacheOther.clear();
			}
			
			
			// temp = null;
		}

		// send unique uplink bl frequency to falcon
		List<String> falcontemp = new ArrayList<String>();
		if (falconData != null && !falconData.isEmpty()) {
			logger.info("Inside send to falcon from auto");
			logger.error("In " + falconData.size());
			synchronized (falconData) {
				// logger.error("#################"+smmStopFreq+"##########"+smmStartFreq+"#####"+rbwMaxSamples);
				double bandw = calculateRbwBandwidth(smmStopFreq - smmStartFreq);// (Math.round(((smmStopFreq-smmStartFreq)*1000/(maxSamples-1))/100)+1)*100;//changed
																					// here *2;
				Map<String, PeakDetectionInfo> tempMap = sortByValue(falconData);
				// logger.error("#############"+bandw);
				// bandw = bandw/1000;
				for (PeakDetectionInfo peak : tempMap.values()) {
					if (!falconDataCache.contains(peak.getFrequency())) {
						String transId = generateTransId();
						
							SystemConfigurationData scd = scr.getSystemConfigurationData(defaultProfile);
							double range=Double.parseDouble(scd.getRange());
							String distanceEmit=calculateDistance(Double.parseDouble(peak.getFrequency()),String.valueOf(peak.getPower()));
							String[] parts = distanceEmit.split("@");
							Double distance=Double.parseDouble(parts[0]);
							Double emtpow=Double.parseDouble(parts[1]);
							
							// jmData.get()
							if (!isFreeze && !isCalibrating)
								if(distance>range && range>0) {
									createAuditlog("Cue", "Terminated" + "\t" + eventData.getEventName() + "\tCueId:"
											+ eventData.getCueId()+"\tFreq:"+peak.getFrequency()+"\tE-Power(dBm):"+emtpow+"\tDistance(m):"+distance+"\tRange(m):"+range+ "\tdistance out of range");
								}
						/*sendToFalcon(transId, transId, peak.getIp_add(), Float.parseFloat(peak.getFrequency()), bandw,
								peak.getAngle(), 5, 0.0, 0.0, timeout, validity);*/
							else {
								sendToFalcon(transId, transId, peak.getIp_add(), Float.parseFloat(peak.getFrequency()), peak.getLat(),
								peak.getAngle(), 5, 0.0, 0.0, timeout, validity);
							}
							falcontemp.add(peak.getFrequency());
					}
					// commented to sent to uniq in alternate window.
					//falcontemp.add(peak.getFrequency());
				}

				falconData.clear();
				falconDataCache.clear();
			}
		
		}
		falconDataCache.clear();
		falconDataCache.addAll(falcontemp);
		logger.error("Out " + falconDataCache.size());

		antenna30Freq.clear();
		antenna30Power.clear();
		antenna90Freq.clear();
		antenna90Power.clear();
		antenna150Freq.clear();
		antenna150Power.clear();

	}

	public double calculateBandwidth(String frequency) {
		double freq = Double.parseDouble(frequency);
		double bandwidth = 0;

		Iterator<ColorStripsData> itr1 = twogUplinkFreq.iterator();
		ColorStripsData colorStripsData;
		double startFreq;
		double stopFreq;
		boolean isUplinkFrequency = false;

		while (itr1.hasNext()) {
			colorStripsData = itr1.next();
			startFreq = colorStripsData.getStartfreq();
			stopFreq = colorStripsData.getStopfreq();
			if (freq >= startFreq && freq <= stopFreq) {
				bandwidth = twogBandwidth;
				isUplinkFrequency = true;
				break;
			}
		}

		itr1 = threegUplinkFreq.iterator();
		while (itr1.hasNext()) {
			colorStripsData = itr1.next();
			startFreq = colorStripsData.getStartfreq();
			stopFreq = colorStripsData.getStopfreq();
			if (freq >= startFreq && freq <= stopFreq) {
				bandwidth = threegBandwidth;
				isUplinkFrequency = true;
				break;
			}
		}

		itr1 = fourgUplinkFreq.iterator();
		while (itr1.hasNext()) {
			colorStripsData = itr1.next();
			startFreq = colorStripsData.getStartfreq();
			stopFreq = colorStripsData.getStopfreq();
			if (freq >= startFreq && freq <= stopFreq) {
				bandwidth = fourgBandwidth;
				isUplinkFrequency = true;
				break;
			}
		}

		itr1 = wifiUplinkFreq.iterator();
		while (itr1.hasNext()) {
			colorStripsData = itr1.next();
			startFreq = colorStripsData.getStartfreq();
			stopFreq = colorStripsData.getStopfreq();
			if (freq >= startFreq && freq <= stopFreq) {
				bandwidth = wifiBandwidth;
				isUplinkFrequency = true;
				break;
			}
		}
		return bandwidth;
	}

	public HashMap<String, PeakDetectionInfo> sortByValue(Map<String, PeakDetectionInfo> peakMap) {
		// Create a list from elements of HashMap
		List<Map.Entry<String, PeakDetectionInfo>> list = new LinkedList<Map.Entry<String, PeakDetectionInfo>>(
				peakMap.entrySet());

		// Sort the list
		Collections.sort(list, new Comparator<Map.Entry<String, PeakDetectionInfo>>() {
			public int compare(Map.Entry<String, PeakDetectionInfo> o1, Map.Entry<String, PeakDetectionInfo> o2) {
				return (new Double(o2.getValue().getPower())).compareTo(new Double(o1.getValue().getPower()));
			}
		});

		// put data from sorted list to hashmap
		HashMap<String, PeakDetectionInfo> temp = new LinkedHashMap<String, PeakDetectionInfo>();
		for (Map.Entry<String, PeakDetectionInfo> aa : list) {
			temp.put(aa.getKey(), aa.getValue());
		}
		return temp;
	}

	// Scheduler to check status of all nodes
	public List<NodeData> checkNodeStatus() {
		List<NodeData> data = new ArrayList<NodeData>();
		String device_ip = "";

		Iterator<Entry<String, String>> it = Common.nodes.entrySet().iterator();
		while (it.hasNext()) {
			Entry<String, String> pair = it.next();
			if (pair.getValue().equals(nodeIdMap.get(3))) {
				String status = "Not Reachable";
				status = dinfo.getDataByIP(pair.getKey()).getState();

				NodeData n = new NodeData();
				n.setIp_add(pair.getKey());
				n.setType(pair.getValue());
				n.setStatus(status);
				data.add(n);
			} else {
				InetAddress ping;
				String status = "Not Reachable";
				try {
					ping = InetAddress.getByName(pair.getKey());

					if (ping.isReachable(1000)) {
						status = "Reachable";
						if (pair.getValue().equals(nodeIdMap.get(2))) {
							ptzReachable = true;
						} else if (pair.getValue().equals(nodeIdMap.get(1))) {
							Response rs = sendGetRequestToUrl(
									"http://" + pair.getKey() + ":" + finleyPort + "/finley/api/GET_STATUS");
							if (rs != null) {
								JSONObject result = new JSONObject(rs);
								status = "Reachable";
							} else {
								status = "Not Reachable";
							}
						} else if (pair.getValue().equals(nodeIdMap.get(4))) {
							Response rs = sendGetRequestToUrl("http://" + pair.getKey() + ":" + falconPort
									+ "/locator/service/common/GET_STATUS");
							if (rs != null) {
								JSONObject result = new JSONObject(rs);
								status = "Reachable";
							} else {
								status = "Not Reachable";
							}
						}
					} else {
						if (pair.getValue().equals(nodeIdMap.get(2))) {
							ptzReachable = false;
						}
						status = "Not Reachable";
					}

				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				NodeData n = new NodeData();
				n.setIp_add(pair.getKey());
				n.setType(pair.getValue());
				n.setStatus(status);
				data.add(n);
			}

		}

		/*
		 * Iterator<NodeData> itr = nr.getNodeInfo().iterator(); while(itr.hasNext()) {
		 * NodeData nodeData = itr.next(); String ip = nodeData.getIp_add();
		 * 
		 * if(nodeData.getType().equals("FINLEY")) { //Response rs =
		 * sendRequestToUrl("http://"+ip+"/finley/api/GET_STATUS", data);
		 * nodeData.setStatus(status); nr.updateNodeStatus(status, ip); } else
		 * if(nodeData.getType().equals("FALCON")) { //Response rs =
		 * sendRequestToUrl("http://"+ip+"/locator/service/common/GET_STATUS", data);
		 * nodeData.setStatus(status); nr.updateNodeStatus(status, ip); } else{
		 * nodeData.setStatus(status); //nr.updateNodeStatus(status, ip); }
		 * data.add(nodeData); }
		 */

		sendStatusForNode(data);
		return data;
	}
	/*
	 * public List<NodeData> checkNodeStatus() { List<NodeData> data = new
	 * ArrayList<NodeData>(); String device_ip = "";
	 * 
	 * Iterator<DeviceInfo> it = dinfo.getDeviceInfo().iterator();
	 * while(it.hasNext()) { DeviceInfo deviceInfo = it.next(); NodeData node = new
	 * NodeData(); device_ip = deviceInfo.getIp_add(); node.setIp_add(device_ip);
	 * node.setType(deviceInfo.getName()); node.setStatus(deviceInfo.getState());
	 * data.add(node); }
	 * 
	 * PTZData pt = ptzr.getPTZInfo(device_ip); String status = "Not Reachable";
	 * if(pt!=null) { InetAddress ping; device_ip = pt.getPtz_ip(); try { ping =
	 * InetAddress.getByName(device_ip);
	 * 
	 * if (ping.isReachable(1000)) status = "Reachable"; else status =
	 * "Not Reachable";
	 * 
	 * } catch (UnknownHostException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated catch
	 * block e.printStackTrace(); }
	 * 
	 * NodeData n = new NodeData(); n.setIp_add(device_ip);
	 * n.setType(nodeIdMap.get(2)); n.setStatus(status); data.add(n); }
	 * 
	 * Iterator<NodeData> itr = nr.getNodeInfo().iterator(); while(itr.hasNext()) {
	 * NodeData nodeData = itr.next(); String ip = nodeData.getIp_add();
	 * 
	 * if(nodeData.getType().equals("FINLEY")) { //Response rs =
	 * sendRequestToUrl("http://"+ip+"/finley/api/GET_STATUS", data);
	 * nodeData.setStatus(status); nr.updateNodeStatus(status, ip); } else
	 * if(nodeData.getType().equals("FALCON")) { //Response rs =
	 * sendRequestToUrl("http://"+ip+"/locator/service/common/GET_STATUS", data);
	 * nodeData.setStatus(status); nr.updateNodeStatus(status, ip); } else{
	 * nodeData.setStatus(status); //nr.updateNodeStatus(status, ip); }
	 * data.add(nodeData); }
	 * 
	 * sendStatusForNode(data); return data; }
	 */

	public void checkDiskSpace() {

		File file = new File(System.getProperty("user.dir"));
		long totalSpace = file.getTotalSpace();
		long usableSpace = file.getUsableSpace();
		usedSpace = 100 - (int) ((usableSpace * 100) / totalSpace);

		boolean oldFreeze = isFreeze;

		isFreeze = usedSpace > freeze ? true : false;

		if (!oldFreeze && isFreeze) {
			createAuditlog("Cue", "Stopped" + "\t" + "Hummer"
					+ "\tDisk Space Usage Exceeded. Please free space to restore functionality.");
			priorityQueue.clear();
		}

	}

	public void updateJMNodeMap() {
		Iterator<JMNodeMapping> itr = jmnmr.getNodeMappingList().iterator();
		JMNodeMapping node;
		while (itr.hasNext()) {
			node = itr.next();
			jmNodeMap.put(node.getDeviceid(), String.valueOf(node.getAntennaid()));
		}
	}

	public List<JMNodeMapping> getJMNodeList() {
		return jmnmr.getNodeMappingList();
	}

	// Scheduler to get data from all JM nodes
	public List<JMDeviceData> jmNodeData() {
		List<JMDeviceData> data = new ArrayList<JMDeviceData>();
		String device_ip = "";

		Iterator<DeviceInfo> it = dinfo.getDeviceInfo().iterator();
		while (it.hasNext()) {
			DeviceInfo deviceInfo = it.next();
			PTZData pt = ptzr.getPTZInfo(deviceInfo.getIp_add());
			if (pt != null)
				device_ip = pt.getPtz_ip();
		}

		if (!device_ip.equals("")) {
			// Iterator<JMNodeMapping> itr = jmnmr.getNodeMappingList().iterator();
			Iterator<Integer> itr = jmNodeMap.keySet().iterator();
			try {
				while (itr.hasNext()) {
					// JMNodeMapping nodeMapping = itr.next();
					int deviceId = itr.next();// nodeMapping.getDeviceId();

					JMDeviceData node = UdpPacketSender.getInstance().sendToJM(device_ip, ptz_udp_port, deviceId,
							jmRetries, jmTimeout);
					Thread.sleep(jmDiff);

					if (node != null) {
						// jmData.put(String.valueOf(nodeMapping.getAntennaid()), node);
						jmdr.save(node);
						/*
						 * try { node.setTime(sdf.parse(sdf.format(node.getTime()))); } catch
						 * (ParseException e) { // TODO Auto-generated catch block e.printStackTrace();
						 * }
						 */
						data.add(node);
					}
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			sendStatusForJMNode(data);
			updateJmList();
		}
		return data;
	}

	public void updateJmList() {
		JMDeviceDataHistory node;
		Iterator<JMDeviceDataHistory> i = jmdhr.getJmDeviceDataHistory().iterator();
		while (i.hasNext()) {
			node = i.next();
			jmData.put(jmNodeMap.get(node.getDeviceid()), node);
		}
		logger.info(jmData.toString());
	}

	public void stopOperations() {
		isCalibrating = true;
	}

	public void startOperations() {
		isCalibrating = false;
	}

	// added notify in case of failure
	public void notifyWait() {
		synchronized (wait) {
			if (currentEventName == null)
				wait.notify();
		}
	}

	// Queue to process all events and trigger recieved
	public void executeQueue() {
		try {
			EventData localEventData;
			boolean valid;
			while ((localEventData = priorityQueue.take()) != null) {
				DBDataService.eventData = localEventData;
				currentEventName = eventData.getEventName();
				valid = false;
				logger.debug("In executeQueue"+currentEventName);
				if (currentEventName.equals(manualEvent)) {
					try {
						frequencyForPTZEvent = Double.parseDouble(eventData.getFrequency());
						startFreqForPTZEvent = frequencyForPTZEvent - eventData.getBandwidth() / 2;
						stopFreqForPTZEvent = frequencyForPTZEvent + eventData.getBandwidth() / 2;
						int startAngle = Integer.parseInt(eventData.getStartAngle()) + ptzOffset + ptz_roll_offset;
						int stopAngle = Integer.parseInt(eventData.getStopAngle()) + ptzOffset + ptz_roll_offset;
                        int temp_stop=stopAngle;
                        int temp_start=startAngle;
                        if(temp_stop>360) {
                        	temp_stop=stopAngle-360;
                        }
                        else if(temp_stop<0) {
                        	temp_stop=360+temp_stop;
                        }
                       
                        if(temp_start>360) {
                        	temp_start=startAngle-360;
                        }
                        else if(temp_start<0) {
                        	temp_start=360+temp_start;
                        }
						createAuditlog("Cue",
								eventData.getEventName() + "\tHUMMER\tCueId:" + eventData.getCueId() + "\tStartAngle:"
										+ temp_start + "\tStopAngle:" + temp_stop + "\tFrequency:"
										+ eventData.getFrequency() + "\tBandwidth:" + eventData.getBandwidth()
										+ "\tTilt:" + eventData.getTiltAngleProperty());
						oprThread = new Thread() {
							public void run() {
								try {
									
									eventValid = rotateDevice(eventData.getDevice_ip(), startAngle + "", stopAngle + "",
											eventData.getTiltAngleProperty(), timeout * 60.0, eventData.getMaxPower());
								} catch (IOException e) {
									// TODO Auto-generated catch block
									eventValid = false;
									e.printStackTrace();
								}
							}
						};
						oprThread.start();
						oprThread.join();
						// rotateDevice(eventData.getDevice_ip(), startAngle+"", stopAngle+"",
						// eventData.getTiltAngleProperty(), timeout);
						valid = eventValid;
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (currentEventName.equals(ugsEvent) && !isManualOverride) {
					try {
						// check validity for event
						synchronized (synch) {
							oxfamEventCount--;

							logger.error("Count" + oxfamEventCount);

							if (oxfamEventCount > 0) {
								stopRotateDeviceInterupted(getDeviceIp());
							} else if (eventData.getValidity() > ChronoUnit.MINUTES.between(eventData.getDate(),
									LocalDateTime.now())) {
								// createAuditlog("Cue",
								// "Processing"+"\t"+eventData.getEventName()+"\t"+eventData.getFrequency()+"\t"+eventData.getBandwidth()+"\t"+eventData.getTimeout());
								oprThread = new Thread() {
									public void run() {
										try {
											eventValid = ugsTriggerInfo(eventData.getTransId(), eventData.getCueId(),
													eventData.getBandwidth(), eventData.getLatitude(),
													eventData.getLongitude(), eventData.getTimeout());
										} catch (IOException e) {
											// TODO Auto-generated catch block
											eventValid = false;
											e.printStackTrace();
										}
									}
								};
								oprThread.start();
								oprThread.join();
								valid = eventValid;
								// valid = ugsTriggerInfo(eventData.getTransId(), eventData.getCueId(),
								// eventData.getBandwidth(), eventData.getLatitude(), eventData.getLongitude(),
								// eventData.getTimeout());
								// valid = true;
							} else {
								createAuditlog("Cue", "Terminated" + "\t" + eventData.getEventName() + "\tCueId:"
										+ eventData.getCueId() + "\tEvent Validity Expired");
							}
						}
					} catch (Exception e) { // IO exception here
						// TODO Auto-generated catch block
						e.printStackTrace();
						valid = false;
					}
				} else if (currentEventName.equals(falconEvent) && !isManualOverride) {
					try {
						// check validity for event
						if (eventData.getValidity() > ChronoUnit.MINUTES.between(eventData.getDate(),
								LocalDateTime.now())) {
							createAuditlog("Cue", "Processing" + "\t" + eventData.getEventName() + "\tCueId:"
									+ eventData.getCueId() + "\tFrequency:" + eventData.getFrequency() + "\tBandwidth:"
									+ eventData.getBandwidth() + "\tSector:" + eventData.getSector() + "\tTimeout:"
									+ eventData.getTimeout() + "\tValidity:" + eventData.getValidity());
							oprThread = new Thread() {
								public void run() {
									try {
										eventValid = falconTriggerInfo(eventData.getBandwidth(),
												eventData.getFrequency(), eventData.getSector(),
												eventData.getTimeout());
									} catch (IOException e) {
										// TODO Auto-generated catch block
										eventValid = false;
										e.printStackTrace();
									}
								}
							};
							oprThread.start();
							oprThread.join();
							// falconTriggerInfo(eventData.getBandwidth(), eventData.getFrequency(),
							// eventData.getSector(), eventData.getTimeout());
							valid = eventValid;
						} else {
							createAuditlog("Cue", "Terminated" + "\t" + eventData.getEventName() + "\tCueId:"
									+ eventData.getCueId() + "\tEvent Validity Expired");
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (currentEventName.equals(blEvent) && isAutoEnabled && !isManualOverride) {
					try {
						// check validity for event
						//
						logger.debug("In blEvent  ");
                        int temp_stop=Integer.parseInt(eventData.getStopAngle());
                        int temp_start=Integer.parseInt(eventData.getStartAngle());
                        if(temp_stop>360) {
                        	temp_stop=temp_stop-360;
                        }
                        else if(temp_stop<0) {
                        	temp_stop=360+temp_stop;
                        }
                       
                        if(temp_start>360) {
                        	temp_start=temp_start-360;
                        }
                        else if(temp_start<0) {
                        	temp_start=360+temp_start;
                        }
						if (eventData.getValidity() > ChronoUnit.MINUTES.between(eventData.getDate(),
								LocalDateTime.now())) {
							SystemConfigurationData scd = scr.getSystemConfigurationData(defaultProfile);
							double range=Double.parseDouble(scd.getRange());
							String distanceEmit=calculateDistance(Double.parseDouble(eventData.getFrequency()),String.valueOf(eventData.getMaxPower()));
							String[] parts = distanceEmit.split("@");
							Double distance=Double.parseDouble(parts[0]);
							Double emtpow=Double.parseDouble(parts[1]);
							
							
								if(distance>range && range>0) {
									createAuditlog("Cue", "Terminated" + "\t" + eventData.getEventName() + "\tCueId:"
											+ eventData.getCueId()+"\tFreq:"+eventData.getFrequency()+"\tE-Power(dBm):"+emtpow+"\tDistance(m):"+distance+"\tRange(m):"+range+ "\tdistance out of range");
								}
							else {
							frequencyForPTZEvent = Double.parseDouble(eventData.getFrequency());
							startFreqForPTZEvent = frequencyForPTZEvent - eventData.getBandwidth() / 2;
							stopFreqForPTZEvent = frequencyForPTZEvent + eventData.getBandwidth() / 2;

							createAuditlog("Cue",
									eventData.getEventName() + "\tHUMMER\tCueId:" + eventData.getCueId()
											+ "\tStartAngle:" + temp_start + "\tStopAngle:"
											+ temp_stop + "\tFrequency:" + eventData.getFrequency()
											+ "\tBandwidth:" + eventData.getBandwidth() + "\tTimeout:"
											+ eventData.getTimeout() + "\tValidity:" + eventData.getValidity());
							oprThread = new Thread() {
								public void run() {
									try {
										logger.debug("In blEvent currentEventName "+currentEventName);
										logger.debug("In blEvent rotateDevice ");
										eventValid = rotateDevice(eventData.getDevice_ip(), eventData.getStartAngle(),
												eventData.getStopAngle(), eventData.getTiltAngleProperty(),
												eventData.getTimeout() * 60, eventData.getMaxPower());
									} catch (IOException e) {
										// TODO Auto-generated catch block
										eventValid = false;
										e.printStackTrace();
									}
								}
							};
							oprThread.start();
							// oprThread.interrupt();
							oprThread.join();
							// rotateDevice(eventData.getDevice_ip(), eventData.getStartAngle(),
							// eventData.getStopAngle(), eventData.getTiltAngleProperty()
							// ,eventData.getTimeout());
							logger.debug("In blEvent sendDataForEvent ");
							sendDataForEvent(blEvent, Double.parseDouble(eventData.getFrequency()),
									Integer.parseInt(eventData.getStartAngle()),
									Integer.parseInt(eventData.getStopAngle()));
							valid = eventValid;
						}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else if (currentEventName.equals(autoEvent) && isAutoEnabled && !isManualOverride) {
					try {
						logger.debug("In autoEvent");
						logger.debug("In autoEvent currentEventName "+currentEventName);
						// check validity for event
						if (eventData.getValidity() > ChronoUnit.MINUTES.between(eventData.getDate(),
								LocalDateTime.now())) {
							
							//muneesh 
							SystemConfigurationData scd = scr.getSystemConfigurationData(defaultProfile);
							double range=Double.parseDouble(scd.getRange());
							String distanceEmit=calculateDistance(Double.parseDouble(eventData.getFrequency()),String.valueOf(eventData.getMaxPower()));
							String[] parts = distanceEmit.split("@");
							Double distance=Double.parseDouble(parts[0]);
							Double emtpow=Double.parseDouble(parts[1]);
							
							
								if(distance>range && range>0) {
									createAuditlog("Cue", "Terminated" + "\t" + eventData.getEventName() + "\tCueId:"
											+ eventData.getCueId()+"\tFreq:"+eventData.getFrequency()+"\tE-Power(dBm):"+emtpow+"\tDistance(m):"+distance+"\tRange(m):"+range+ "\tdistance out of range");
								}
							else {
							
							frequencyForPTZEvent = Double.parseDouble(eventData.getFrequency());
							startFreqForPTZEvent = frequencyForPTZEvent - eventData.getBandwidth() / 2;
							stopFreqForPTZEvent = frequencyForPTZEvent + eventData.getBandwidth() / 2;
							 int temp_stop=Integer.parseInt(eventData.getStopAngle());
		                        int temp_start=Integer.parseInt(eventData.getStartAngle());
		                        if(temp_stop>360) {
		                        	temp_stop=temp_stop-360;
		                        }
		                        else if(temp_stop<0) {
		                        	temp_stop=360+temp_stop;
		                        }
		                       
		                        if(temp_start>360) {
		                        	temp_start=temp_start-360;
		                        }
		                        else if(temp_start<0) {
		                        	temp_start=360+temp_start;
		                        }
							createAuditlog("Cue",
									eventData.getEventName() + "\tHUMMER\tCueId:" + eventData.getCueId()
											+ "\tStartAngle:" + temp_start + "\tStopAngle:"
											+ temp_stop + "\tFrequency:" + eventData.getFrequency()
											+ "\tBandwidth:" + eventData.getBandwidth() + "\tTimeout:"
											+ eventData.getTimeout() + "\tValidity:" + eventData.getValidity());
							oprThread = new Thread() {
								public void run() {
									try {
										logger.debug("In autoEvent call rotateDevice ");
										eventValid = rotateDevice(eventData.getDevice_ip(), eventData.getStartAngle(),
												eventData.getStopAngle(), eventData.getTiltAngleProperty(),
												eventData.getTimeout() * 60, eventData.getMaxPower());
									} catch (IOException e) {
										// TODO Auto-generated catch block
										eventValid = false;
										e.printStackTrace();
									}
								}
							};
							oprThread.start();
							oprThread.join();
							// rotateDevice(eventData.getDevice_ip(), eventData.getStartAngle(),
							// eventData.getStopAngle(), eventData.getTiltAngleProperty()
							// ,eventData.getTimeout());
							logger.debug("In autoEvent sendDataForEvent ");
							sendDataForEvent(autoEvent, Double.parseDouble(eventData.getFrequency()),
									Integer.parseInt(eventData.getStartAngle()),
									Integer.parseInt(eventData.getStopAngle()));
							valid = eventValid;
						}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				if (valid) {
					synchronized (wait) {
						try {
							wait.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				sendCurrentEvent("Idle");
				currentEventName = null;
				DBDataService.eventData = null;
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		} finally {
			synchronized (wait) {
				wait.notify();
			}
			Runnable runnable = new Runnable() {
				public void run() {
					executeQueue();
				}
			};
			Thread executeQueueThread = new Thread(runnable);
			executeQueueThread.start();
		}
	}

	// For adding Manual events and auto events and blacklist events
	public void addToQueue(String eventName, String device_ip, String freq, double bandwidth, String startAngle,
			String stopAngle, int tiltAngleProperty, double timeout, int validity, LocalDateTime date,
			double maxPower) {
		if (eventName.equals(manualEvent)) {
			try {
				String transId = generateTransId();
				String cueId = transId;
				priorityQueue.put(new EventData(transId, cueId, priority.get(eventName), eventName, device_ip, freq,
						bandwidth, startAngle, stopAngle, tiltAngleProperty, timeout, validity, date, maxPower));
				/*
				 * AuditLog auditLog = new AuditLog(); auditLog.setLogType("Cue");
				 * auditLog.setDescription("Manual\tTRGL\t"+transId+"\t"+startAngle+"\t"+
				 * stopAngle+"\t"+freq+"\t"+timeout+"\t"+validity);
				 */
				if (currentEventName != null && priority.get(eventName) < priority.get(currentEventName)) {
					shutdownTimer();
					try {
						if (oprThread != null && oprThread.isAlive()) {
							oprThread.interrupt();
							oprThread = null;
						} else {
							stopRotateDeviceInterupted(device_ip);
						}
						// stopRotateDeviceInterupted(device_ip);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} catch (Exception e) {
				System.out.println(e);
			}
		} else if (eventName.equals(autoEvent) && isAutoEnabled) {
			String transId = generateTransId();
			String cueId = transId;
			// createAuditlog("Cue",
			// eventName+"\tHUMMER\t"+transId+"\t"+startAngle+"\t"+stopAngle+"\t"+freq+"\t"+timeout+"\t"+validity);

			EventData event = new EventData(transId, cueId, priority.get(eventName), eventName, device_ip, freq,
					bandwidth, startAngle, stopAngle, tiltAngleProperty, timeout, validity, date, maxPower);
			if (previousOtherEventData != null)
				priorityQueue.remove(previousOtherEventData);
			previousOtherEventData = event;
			priorityQueue.put(event);
			/*
			 * AuditLog auditLog = new AuditLog(); auditLog.setLogType("Cue");
			 * auditLog.setDescription("Auto\tTRGL\t"+transId+"\t"+startAngle+"\t"+stopAngle
			 * +"\t"+freq+"\t"+timeout+"\t"+validity);
			 */
		} else if (eventName.equals(blEvent) && isAutoEnabled) {
			String transId = generateTransId();
			String cueId = transId;
			// createAuditlog("Cue",
			// eventName+"\tHummer\t"+transId+"\t"+startAngle+"\t"+stopAngle+"\t"+freq+"\t"+timeout+"\t"+validity);

			synchronized (synch) {
				EventData event = new EventData(transId, cueId, priority.get(eventName), eventName, device_ip, freq,
						bandwidth, startAngle, stopAngle, tiltAngleProperty, timeout, validity, date, maxPower);
				if (previousBlEventData != null)
					priorityQueue.remove(previousBlEventData);
				previousBlEventData = event;

				priorityQueue.put(event);

				if (currentEventName != null && priority.get(eventName) < priority.get(currentEventName)) {
					shutdownTimer();
					try {
						if (oprThread != null && oprThread.isAlive()) {
							oprThread.interrupt();
							oprThread = null;
						} else {
							stopRotateDeviceInterupted(device_ip);
						}
						// stopRotateDeviceInterupted(device_ip);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}

	// For adding UGS Trigger events
	public void addToQueue(String cueId, String eventName, String latitude, String longitude, int timeout, int validity,
			LocalDateTime date) {
		if (eventName.equals(ugsEvent) && !isManualOverride) {
			String transId = generateTransId();

			synchronized (synch) {
				oxfamEventCount++;

				logger.error("Count " + oxfamEventCount);

				priorityQueue.put(new EventData(transId, cueId, priority.get(eventName), eventName, bandwidth, latitude,
						longitude, timeout, validity, date, 100.0));

				int count = 0;

				if (currentEventName != null && priority.get(eventName) <= priority.get(currentEventName)
						&& count == oxfamEventCount - 1) {
					shutdownTimer();
					try {
						String device_ip = getDeviceIp();
						if (device_ip != null) {
							if (oprThread != null && oprThread.isAlive()) {
								oprThread.interrupt();
								oprThread = null;
//							while(oprThread.isAlive()) {
//								
//							}
//							if(currentEventName.equals(ugsEvent)) {
//								stopRotateDeviceInterupted(device_ip);
//								createAuditlog("Cue", "Terminated\t"+ugsEvent+"\tCueId:"+eventData.getCueId()+"\tPriority Event");
//							}
//							else
//								stopRotateDeviceInterupted(device_ip);
							} else {
//							if(currentEventName.equals(ugsEvent)) {
//								stopRotateDeviceInterupted(device_ip);
//								createAuditlog("Cue", "Terminated\t"+ugsEvent+"\tCueId:"+eventData.getCueId()+"\tPriority Event");
//							}
//							else
								stopRotateDeviceInterupted(device_ip);
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// String transId = generateTransId();
				// priorityQueue.add(new EventData(transId, cueId, priority.get(eventName),
				// eventName, bandwidth, latitude, longitude, timeout, validity, date));
			}
		}
	}

	// For adding FALCON Trigger events
	public void addToQueue(String cueId, String eventName, String frequency, double bandwidth, String angle,
			String sector, int timeout, int validity, LocalDateTime date) {
		if (eventName.equals(falconEvent) && !isManualOverride) {// && isTmdasEnabled) {

			String transId = generateTransId();
			synchronized (synch) {
				priorityQueue.put(new EventData(transId, cueId, priority.get(eventName), eventName, frequency,
						bandwidth, angle, sector, timeout, validity, date, 100.0));

				if (currentEventName != null && priority.get(eventName) < priority.get(currentEventName)) {
					shutdownTimer();
					try {
						String device_ip = getDeviceIp();
						if (device_ip != null) {
							if (oprThread != null && oprThread.isAlive()) {
								oprThread.interrupt();
								oprThread = null;
							} else {
								stopRotateDeviceInterupted(device_ip);
							}
						}
						// stopRotateDeviceInterupted(device_ip);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// String transId = generateTransId();
				// priorityQueue.add(new EventData(transId, cueId, priority.get(eventName),
				// eventName, frequency, bandwidth, angle, sector, timeout, validity, date));
			}
		}
	}

	public boolean angle_is_between_angles(double N, double a, double b) {
		N = N < 360 ? N : N - 360;
		a = a < 360 ? a : a - 360;
		b = b < 360 ? b : b - 360;

		if (a < b)
			return a <= N && N <= b;
		return a <= N || N <= b;
	}

	@Transactional
	public boolean ugsTriggerInfo(String transId, String cueId, double bandwidth, String latitude, String longitude,
			Double timeout) throws IOException {

		List<DeviceInfo> allDevices = dinfo.getDeviceInfo();
		int timeCount = 20;
		String device_ip = null;
		DeviceInfo device_info = null;
		for (DeviceInfo device : allDevices) {
			device_ip = device.getIp_add();
			device_info = device;
		}

		if (device_ip == null) {
			System.out.println("DBDataService : Unable to retrieve device Info");
			return false;
		}

		double device_lat = java.lang.Math.toRadians(Double.parseDouble(device_info.getLat()));
		double device_lon = java.lang.Math.toRadians(Double.parseDouble(device_info.getLon()));
		double ugs_lat = java.lang.Math.toRadians(Double.parseDouble(latitude));
		double ugs_lon = java.lang.Math.toRadians(Double.parseDouble(longitude));

		double angleInDegree = angleFromCoordinate(device_lat, device_lon, ugs_lat, ugs_lon);
		// double angleInDegree = angle * Math.PI/180;

		PTZData ptData = ptzr.getPTZInfo(device_ip);

		boolean flag = true;

		int tilt = tiltAngleProperty;

		double offset = Double.parseDouble(ptData.getPtz_offset());
		if (offset < 0) {
			offset = offset + 360;
		}
		offset = offset + ptz_roll_offset;
		if (offset > 360)
			offset = offset - 360;

//		if(angleInDegree < offset)
//		{
//			//Not in PT range
//			createAuditlog("Cue", "Terminated\t"+ugsEvent+"\tCueId:"+cueId+"\t"+"Outside Sector");
//			currentEventName = null;
//			return false;
//		}else 
		if (angle_is_between_angles(angleInDegree, offset, offset + 60))// angleInDegree > offset && angleInDegree <=
																		// (offset+60))
		{

			CircularQueue<Double> antenna30PowerTemp;
			CircularQueue<Double> antenna30FreqTemp;
			float count = 0;
			createAuditlog("Cue", "Started\t" + ugsEvent + "\tCueId:" + cueId);
			// looping to find maximized frequency in that particular sector
			do {
				synchronized (antenna30Power) {
					antenna30PowerTemp = (CircularQueue<Double>) antenna30Power.clone();
					antenna30FreqTemp = (CircularQueue<Double>) antenna30Freq.clone();
				}
				if (antenna30PowerTemp.size() > 0 || count > timeCount) {
					flag = false;
				} else {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						stopRotateDeviceInterupted(device_ip);
						e.printStackTrace();
						return false;
					}
				}
				count++;
			} while (flag);

			if (antenna30FreqTemp.size() > 0) {

				Double time = timeout * 60 - Math.round(count / 2);

				// Find Frequency with max power
				double maxPower = Collections.max(antenna30PowerTemp);
				int index = antenna30PowerTemp.indexOf(maxPower);
				double freqToMaximize = antenna30FreqTemp.get(index);

				JMDeviceDataHistory jd = jmData.get("11");
				if (jd != null && jmValid)
					tilt = (int) Float.parseFloat(jd.getTilt());
				;

				// 0 to 60 degree Antenna
				int startAngle = 0 + ptz_roll_offset + ptzOffset;
				int stopAngle = 60 + ptz_roll_offset + ptzOffset;
				DBDataService.frequencyForPTZEvent = freqToMaximize;
				
				createAuditlog("Cue", "Processing\t" + ugsEvent + "\tCueId:" + cueId + "\tFrequency:" + freqToMaximize
						+ "\tSector:1" + "\tTilt:" + tilt + "\tTimeout:" + timeout);

				rotateDevice(device_ip, startAngle + "", stopAngle + "", tilt, time, maxPower);
				sendDataForEvent(ugsEvent, freqToMaximize, startAngle, stopAngle);

				// createAuditlog("Cue",
				// "Processing\t"+ugsEvent+"\tCueId:"+cueId+"\tFrequency:"+freqToMaximize+"\tSector:1"+"\tTilt:"+tilt+"\tTimeout:"+timeout);

				DBDataService.isUGSTriggeredEvent = true;

				startFreqForPTZEvent = frequencyForPTZEvent - bandwidth / 2;
				stopFreqForPTZEvent = frequencyForPTZEvent + bandwidth / 2;
			} else {
				sendToFalcon(transId, cueId, Common.nodeIpMap.get(nodeIdMap.get(3)), -1, 0, angleInDegree + "", 5,
						Double.parseDouble(latitude), Double.parseDouble(longitude), DBDataService.timeout, validity);
				createAuditlog("Cue", "Terminated\t" + ugsEvent + "\tCueId:" + cueId + "\tNo Frequency Found");
				return false;
			}

			// Make flag to start caching peak data
		} else if (angle_is_between_angles(angleInDegree, offset + 60, offset + 120))// angleInDegree > (offset+60) &&
																						// angleInDegree <=
																						// (offset+120))
		{
			CircularQueue<Double> antenna90PowerTemp;
			CircularQueue<Double> antenna90FreqTemp;

			float count = 0;
			createAuditlog("Cue", "Started\t" + ugsEvent + "\tCueId:" + cueId);
			// looping to find maximized frequency in that particular sector
			do {
				synchronized (antenna90Power) {
					antenna90PowerTemp = (CircularQueue<Double>) antenna90Power.clone();
					antenna90FreqTemp = (CircularQueue<Double>) antenna90Freq.clone();
				}
				if (antenna90PowerTemp.size() > 0 || count > timeCount) {
					flag = false;
				} else {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						stopRotateDeviceInterupted(device_ip);
						e.printStackTrace();
						return false;
					}
				}
				count++;
			} while (flag);

			// Find Frequency with max power
			if (antenna90FreqTemp.size() > 0) {

				Double time = timeout * 60 - Math.round(count / 2);

				double maxPower = Collections.max(antenna90PowerTemp);
				int index = antenna90PowerTemp.indexOf(maxPower);
				double freqToMaximize = antenna90FreqTemp.get(index);

				JMDeviceDataHistory jd = jmData.get("21");
				if (jd != null && jmValid)
					tilt = (int) Float.parseFloat(jd.getTilt());

				// 60 to 120 degree Antenna
				int startAngle = 60 + ptz_roll_offset + ptzOffset;
				int stopAngle = 120 + ptz_roll_offset + ptzOffset;
				DBDataService.frequencyForPTZEvent = freqToMaximize;
				
				createAuditlog("Cue", "Processing\t" + ugsEvent + "\tCueId:" + cueId + "\tFrequency:" + freqToMaximize
						+ "\tSector:2" + "\tTilt:" + tilt + "\tTimeout:" + timeout);

				rotateDevice(device_ip, startAngle + "", stopAngle + "", tilt, time, maxPower);
				sendDataForEvent(ugsEvent, freqToMaximize, startAngle, stopAngle);

				// createAuditlog("Cue",
				// "Processing\t"+ugsEvent+"\tCueId:"+cueId+"\tFrequency:"+freqToMaximize+"\tSector:2"+"\tTilt:"+tilt+"\tTimeout:"+timeout);

				DBDataService.isUGSTriggeredEvent = true;

				startFreqForPTZEvent = frequencyForPTZEvent - bandwidth / 2;
				stopFreqForPTZEvent = frequencyForPTZEvent + bandwidth / 2;
			} else {
				sendToFalcon(transId, cueId, Common.nodeIpMap.get(nodeIdMap.get(3)), -1, 0, angleInDegree + "", 5,
						Double.parseDouble(latitude), Double.parseDouble(longitude), DBDataService.timeout, validity);
				createAuditlog("Cue", "Terminated\t" + ugsEvent + "\tCueId:" + cueId + "\tNo Frequency Found");
				return false;
			}

		} else if (angle_is_between_angles(angleInDegree, offset + 120, offset + 180))// angleInDegree > (offset +120)
																						// && angleInDegree <=
																						// (offset+180))
		{
			CircularQueue<Double> antenna150PowerTemp;
			CircularQueue<Double> antenna150FreqTemp;

			// looping to find maximized frequency in that particular sector
			float count = 0;
			createAuditlog("Cue", "Started\t" + ugsEvent + "\tCueId:" + cueId);
			do {
				synchronized (antenna150Power) {
					antenna150PowerTemp = (CircularQueue<Double>) antenna150Power.clone();
					antenna150FreqTemp = (CircularQueue<Double>) antenna150Freq.clone();
				}
				if (antenna150PowerTemp.size() > 1 || count > timeCount) {
					flag = false;
				} else {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						stopRotateDeviceInterupted(device_ip);
						e.printStackTrace();
						return false;
					}
				}
				count++;
			} while (flag);

			// Find Frequency with max power
			if (antenna150FreqTemp.size() > 0) {

				Double time = timeout * 60 - Math.round(count / 2);

				double maxPower = Collections.max(antenna150PowerTemp);
				int index = antenna150PowerTemp.indexOf(maxPower);
				double freqToMaximize = antenna150FreqTemp.get(index);

				JMDeviceDataHistory jd = jmData.get("31");
				if (jd != null && jmValid)
					tilt = (int) Float.parseFloat(jd.getTilt());

				// 120 to 180 degree Antenna
				int startAngle = 120 + ptz_roll_offset + ptzOffset;
				int stopAngle = 180 + ptz_roll_offset + ptzOffset;
				DBDataService.frequencyForPTZEvent = freqToMaximize;
				
				createAuditlog("Cue", "Processing\t" + ugsEvent + "\tCueId:" + cueId + "\tFrequency:" + freqToMaximize
						+ "\tSector:3" + "\tTilt:" + tilt + "\tTimeout:" + timeout);

				rotateDevice(device_ip, startAngle + "", stopAngle + "", tilt, time, maxPower);
				sendDataForEvent(ugsEvent, freqToMaximize, startAngle, stopAngle);

				// createAuditlog("Cue",
				// "Processing\t"+ugsEvent+"\tCueId:"+cueId+"\tFrequency:"+freqToMaximize+"\tSector:3"+"\tTilt:"+tilt+"\tTimeout:"+timeout);

				DBDataService.isUGSTriggeredEvent = true;

				startFreqForPTZEvent = frequencyForPTZEvent - bandwidth / 2;
				stopFreqForPTZEvent = frequencyForPTZEvent + bandwidth / 2;
			} else {
				sendToFalcon(transId, cueId, Common.nodeIpMap.get(nodeIdMap.get(3)), -1, 0, angleInDegree + "", 5,
						Double.parseDouble(latitude), Double.parseDouble(longitude), DBDataService.timeout, validity);
				createAuditlog("Cue", "Terminated\t" + ugsEvent + "\tCueId:" + cueId + "\tNo Frequency Found");
				return false;
			}

		} else {
			// Not in PT range
			createAuditlog("Cue", "Terminated\t" + ugsEvent + "\tCueId:" + cueId + "\t" + "Outside Sector");
			currentEventName = null;
			return false;
		}
		return true;
	}

	@Transactional
	public boolean falconTriggerInfo(double bandwidth, String frequency, String sector, Double timeout)
			throws IOException {
		String device_ip = getDeviceIp();
		int tilt = tiltAngleProperty;

		boolean flag;

		if (device_ip == null) {
			System.out.println("DBDataService : Unable to retrieve device Info");
			return false;
		}

		if (sector.equals("1")) {
			// 0 to 60 degree Antenna
			int startAngle = 0 + ptz_roll_offset + ptzOffset;
			int stopAngle = 60 + ptz_roll_offset + ptzOffset;

			JMDeviceDataHistory jd = jmData.get("11");
			if (jd != null && jmValid)
				tilt = (int) Float.parseFloat(jd.getTilt());

			frequencyForPTZEvent = Double.parseDouble(frequency);
			
			flag = rotateDevice(device_ip, startAngle + "", stopAngle + "", tilt, timeout * 60.0, 100.0);
			sendDataForEvent(falconEvent, frequencyForPTZEvent, startAngle, stopAngle);

			startFreqForPTZEvent = frequencyForPTZEvent - bandwidth / 2;
			stopFreqForPTZEvent = frequencyForPTZEvent + bandwidth / 2;

			return flag;
			// Make flag to start caching peak data
		} else if (sector.equals("2")) {
			// 60 to 120 degree Antenna
			int startAngle = 60 + ptz_roll_offset + ptzOffset;
			int stopAngle = 120 + ptz_roll_offset + ptzOffset;

			JMDeviceDataHistory jd = jmData.get("21");
			if (jd != null && jmValid)
				tilt = (int) Float.parseFloat(jd.getTilt());

			frequencyForPTZEvent = Double.parseDouble(frequency);
			
			flag = rotateDevice(device_ip, startAngle + "", stopAngle + "", tilt, timeout * 60.0, 100.0);
			sendDataForEvent(falconEvent, frequencyForPTZEvent, startAngle, stopAngle);

			startFreqForPTZEvent = frequencyForPTZEvent - bandwidth / 2;
			stopFreqForPTZEvent = frequencyForPTZEvent + bandwidth / 2;

			return flag;
		} else if (sector.equals("3")) {
			// 120 to 180 degree Antenna
			int startAngle = 120 + ptz_roll_offset + ptzOffset;
			int stopAngle = 180 + ptz_roll_offset + ptzOffset;

			JMDeviceDataHistory jd = jmData.get("31");
			if (jd != null && jmValid)
				tilt = (int) Float.parseFloat(jd.getTilt());
			
			frequencyForPTZEvent = Double.parseDouble(frequency);

			flag = rotateDevice(device_ip, startAngle + "", stopAngle + "", tilt, timeout * 60.0, 100.0);
			sendDataForEvent(falconEvent, frequencyForPTZEvent, startAngle, stopAngle);

			startFreqForPTZEvent = frequencyForPTZEvent - bandwidth / 2;
			stopFreqForPTZEvent = frequencyForPTZEvent + bandwidth / 2;

			return flag;
		} else {
			// Not in PT range
			return false;
		}
	}

	public String getDeviceIp() {
		List<DeviceInfo> allDevices = dinfo.getDeviceInfo();
		String device_ip = null;

		for (DeviceInfo device : allDevices) {
			device_ip = device.getIp_add();
		}
		return device_ip;
	}

	public double[] getDeviceLatLon() {
		List<DeviceInfo> allDevices = dinfo.getDeviceInfo();
		double[] value = null;
		DeviceInfo device_info = null;

		for (DeviceInfo device : allDevices) {
			device_info = device;
		}

		if (device_info == null) {
			System.out.println("DBDataService : Unable to retrieve device Info");
			return value;
		} else {
			value = new double[2];
			value[0] = toRadians(Double.parseDouble(device_info.getLat()));
			value[1] = toRadians(Double.parseDouble(device_info.getLon()));

			return value;
		}
	}

	private double[] calculateAngleAndDistanceAndPower(List<List<Double>> distance) {
		double[] value = new double[3];
		double[] temp = new double[2];

		double lat = 0;
		double lon = 0;
		double pow = -130;
		int count = 1;
		int size = distance.size();

		double angle = 0;
		double dis = 0;

		List<DeviceInfo> allDevices = dinfo.getDeviceInfo();
		DeviceInfo device_info = null;
		for (DeviceInfo device : allDevices) {
			device_info = device;
		}

		if (device_info == null) {
			System.out.println("DBDataService : Unable to retrieve device Info");
			return value;
		}

		double device_lat = toRadians(Double.parseDouble(device_info.getLat()));
		double device_lon = toRadians(Double.parseDouble(device_info.getLon()));

		Iterator<List<Double>> i = distance.iterator();
		Iterator<Double> j;
		while (i.hasNext()) {
			j = i.next().iterator();
			count = 1;
			while (j.hasNext()) {
				// if(count==1) {
				// temp = calculatePoint(device_lat, device_lon, j.next(), j.next());
				// lat = lat + temp[0];
				// lon = lon + temp[1];
				temp[0] = j.next();
				temp[1] = j.next();
				// }
				// else {
				// pow = pow + j.next();
				double t = j.next();
				if (t > pow) {
					pow = t;
					dis = temp[0];
					angle = temp[1];
					// lat = temp[0];
					// lon = temp[1];
				}
				// }
				count++;
			}
		}

		value[0] = pow;/// size;
		// lat = lat/size;
		// lon = lon/size;

		value[1] = angle;// angleFromCoordinate(device_lat, device_lon, lat, lon);
		value[2] = dis;// distanceBetweenCoordinate(device_lat, device_lon, lat, lon);

		return value;
	}

	private double[] calculatePoint(double latitude, double longitude, double distanceInMetres, double bearing) {
		double[] val = new double[2];

		double brngRad = toRadians(bearing);
		double latRad = latitude;
		double lonRad = longitude;
		int earthRadiusInMetres = 6371000;
		double distFrac = distanceInMetres / earthRadiusInMetres;

		double latitudeResult = asin(sin(latRad) * cos(distFrac) + cos(latRad) * sin(distFrac) * cos(brngRad));
		double a = atan2(sin(brngRad) * sin(distFrac) * cos(latRad), cos(distFrac) - sin(latRad) * sin(latitudeResult));
		double longitudeResult = (lonRad + a + 3 * PI) % (2 * PI) - PI;

		val[0] = toDegrees(latitudeResult);
		val[1] = toDegrees(longitudeResult);
		// System.out.println("latitude: " + toDegrees(latitudeResult) + ", longitude: "
		// + toDegrees(longitudeResult));
		return val;
	}

	private double angleFromCoordinate(double lat1, double long1, double lat2, double long2) {

		lat1 = Math.toRadians(lat1);
		long1 = Math.toRadians(long1);
		lat2 = Math.toRadians(lat2);
		long2 = Math.toRadians(long2);

		double dLon = (long2 - long1);

		double y = Math.sin(dLon) * Math.cos(lat2);
		double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

		double brng = Math.atan2(y, x);

		brng = Math.toDegrees(brng);
		// brng = (brng + 360) % 360;
		// brng = 360 - brng; // count degrees counter-clockwise - remove to make
		// clockwise

		return brng;
	}

	private double distanceBetweenCoordinate(double lat1, double lon1, double lat2, double lon2) {

		final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		// double latDistance = lat2 - lat1;
		// double lonDistance = lon2 - lon1;

		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = 0;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return Math.sqrt(distance);
	}

	public static int calcBearingBetweenTwoGpsLoc(double lat1, double lon1, double lat2, double lon2) {

		double longitude1 = lon1;
		double longitude2 = lon2;
		double latitude1 = Math.toRadians(lat1);
		double latitude2 = Math.toRadians(lat2);
		double longDiff = Math.toRadians(longitude2 - longitude1);
		double y = Math.sin(longDiff) * Math.cos(latitude2);
		double x = Math.cos(latitude1) * Math.sin(latitude2)
				- Math.sin(latitude1) * Math.cos(latitude2) * Math.cos(longDiff);
		double bearing = Math.toDegrees(Math.atan2(y, x));
		int bearingInInt = (int) Math.round(bearing);
		// return bearingInInt;

		return (bearingInInt + 360) % 360;
	}

	public static int calcNewAngleOffset(int bearing) {

		int angleOffset = bearing - getAntennaToVehicleDiffAngle();

		/*
		 * if(angleOffset<0){ angleOffset=360+angleOffset; } if(angleOffset>360){
		 * angleOffset=angleOffset%360; }
		 */

		return angleOffset;
	}

	@CrossOrigin
	@Transactional
	public void recordPTZPositionData(String ptz_ip, float pan_position, float tilt_position, String date_time,
			String system_time) {
		try {
			PTZPositionData ptz_udp_data = new PTZPositionData();
			ptz_udp_data.setPtz_ip(ptz_ip);
			ptz_udp_data.setPan_position(pan_position);
			ptz_udp_data.setTilt_position(tilt_position);
			ptz_udp_data.setDate_time(date_time);
			ptz_udp_data.setSystem_time(system_time);
			ptzpr.save(ptz_udp_data);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	public void shutdownTimer() {
		if (executorTimerService != null)
			executorTimerService.shutdownNow();
	}
	/*
	 * @Transactional public void sendLEDDataToSocket(String ip_add) { System.out.
	 * println("===============================================SENDING DATA ON WEBSOCKET"
	 * ); messagingTemplate.convertAndSend("/topic/LEDInfo",
	 * lr.getLEDStatusByIP(ip_add)); }
	 */

	// generate unique TranId
	public static String generateTransId() {
		String transId = "H_" + System.currentTimeMillis();

		return transId;
	}

	// send event cue to Finley server
	public Response sendCueToFinley(String date, String cueId, String source, String detail) {
		JSONObject jb = new JSONObject();

		try {
			jb.put("DATE", date);
			jb.put("CUE_ID", cueId);
			jb.put("SOURCE", source);
			jb.put("DETAIL", detail);
		} catch (JSONException e) {
			logger.error(e.toString());
		}

		String finleyIp = Common.nodeIpMap.get(nodeIdMap.get(1));
		String resultData = "";
		if (finleyIp != null) {

			// Response rs =
			// sendPostRequestToUrl("http://"+finleyIp+":"+finleyPort+"/finley/event/cue/HUMMER",
			// jb);

			sendPostRequestToUrl("http://" + finleyIp + ":" + finleyPort + "/finley/event/cue/HUMMER", jb);

			/*
			 * if(rs != null) { resultData = rs.readEntity(String.class); }
			 */

			/*
			 * if(rs.getStatus() == 400 &&
			 * data.get("cmdType").equalsIgnoreCase("STOP_SCAN")) { resultData =
			 * "alreadyStopped"; }
			 */
			if (resultData == null || resultData.equals("")) {
				resultData = "{}";
			}
		}

		return Response.status(201).entity(resultData).build();
	}

	// send event data to Finley server
	public Response sendToFinley(String transId, String cueId, String deviceIp, String freq, String angle, String power,
			double probableDistance, boolean eventType, double latitude, double longitude) {
		// LinkedHashMap<String,String> data = new LinkedHashMap<String,String>();
		JSONObject jb = new JSONObject();
		// String transId = "HUMMER_"+System.currentTimeMillis();

		try {
			jb.put("TIME_STAMP", sdf.format(new Date()));
			jb.put("TRANS_ID", transId);
			jb.put("CUE_ID", cueId);
			jb.put("DEVICE_IP", deviceIp);
			jb.put("FREQ", Double.parseDouble(freq));
			jb.put("ANGLE", Double.parseDouble(angle));
			jb.put("POWER", Double.parseDouble(power));
			jb.put("PROBABLE_DISTANCE", probableDistance);
			jb.put("EVENT_TYPE", eventType ? 7 : 1);
			jb.put("LATITUDE", latitude);
			jb.put("LONGITUDE", longitude);
		} catch (JSONException e) {
			logger.error(e.toString());
		}

		String finleyIp = Common.nodeIpMap.get(nodeIdMap.get(1));
		String resultData = "";
		if (finleyIp != null) {

			// Response rs =
			// sendPostRequestToUrl("http://"+finleyIp+":"+finleyPort+finleyUrl, jb);

			sendPostRequestToUrl("http://" + finleyIp + ":" + finleyPort + finleyUrl, jb);

			/*
			 * if(rs != null) { resultData = rs.readEntity(String.class); }
			 */

			/*
			 * if(rs.getStatus() == 400 &&
			 * data.get("cmdType").equalsIgnoreCase("STOP_SCAN")) { resultData =
			 * "alreadyStopped"; }
			 */
			if (resultData == null || resultData.equals("")) {
				resultData = "{}";
			}
		}

		return Response.status(201).entity(resultData).build();
	}

	// send event data to Falcon server
	public Response sendToFalcon(String transId, String cueId, String deviceIp, double freq, double bandwidth,
			String angle, int eventType, Double latitude, Double longitude, int timeout, int validity) {
		// LinkedHashMap<String,String> data = new LinkedHashMap<String,String>();
		JSONObject jb = new JSONObject();
		double sectorAngle = Double.parseDouble(angle) - ptz_roll_offset - ptzOffset;

		PTZData ptData = ptzr.getPTZInfo(deviceIp);

		double offset = Double.parseDouble(ptData.getPtz_offset());
		if (offset < 0) {
			offset = offset + 360;
		}
		offset = offset + ptz_roll_offset;
		if (offset > 360)
			offset = offset - 360;

		String sector = angle_is_between_angles(Double.parseDouble(angle), offset, offset + 60) ? "S1"
				: angle_is_between_angles(Double.parseDouble(angle), offset + 60, offset + 120) ? "S2" : "S3";

		freq = round(freq, 2);
		// String sector = sectorAngle<60?"S1":sectorAngle<120?"S2":"S3";

		// String transId = "HUMMER_"+System.currentTimeMillis();
		try {
			jb.put("TIME_STAMP", sdf.format(new Date()));
			jb.put("TRANS_ID", transId);
			jb.put("CUE_ID", cueId);
			jb.put("DEVICE_IP", deviceIp);
			jb.put("FREQ", freq);
			jb.put("BANDWIDTH", bandwidth);
			jb.put("ANGLE", Double.parseDouble(angle));
			jb.put("SECTOR", sector);
			jb.put("EVENT_TYPE", eventType);
			jb.put("LATITUDE", latitude);
			jb.put("LONGITUDE", longitude);
			jb.put("TIME_OUT", timeout);
			jb.put("VALIDITY", validity);
		} catch (JSONException e) {
			logger.error(e.toString());
		}

		String falconIp = Common.nodeIpMap.get(nodeIdMap.get(4));
		String resultData = "";
		if (falconIp != null) {

			// Response rs =
			// sendPostRequestToUrl("http://"+falconIp+":"+falconPort+falconUrl, jb);

			sendPostRequestToUrl("http://" + falconIp + ":" + falconPort + falconUrl, jb);
			/*
			 * if(rs != null) { resultData = rs.readEntity(String.class); }
			 */

			/*
			 * AuditLog auditLog = new AuditLog(); auditLog.setLogType("Cue");
			 * auditLog.setDescription("Sent\tFalcon\t"+transId+"\t"+angle+"\t"+freq+"\t"+
			 * timeout+"\t"+validity); saveAuditLog(auditLog);
			 */
			if (freq > -1)
				createAuditlog("Cue", "Sent\tFalcon\tCueId:" + cueId + "\tFrequency:" + freq + "\tBandwidth:"
						+ bandwidth + "\tSector:" + sector + "\tTimeout:" + timeout + "\tValidity:" + validity);
			else
				createAuditlog("Cue", "Sent\tFalcon\tCueId:" + cueId + "\tLatitude:" + latitude + "\tLongitude:"
						+ longitude + "\tSector:" + sector + "\tTimeout:" + timeout + "\tValidity:" + validity);

			if (resultData == null || resultData.equals("")) {
				resultData = "{}";
			}
		}

		logger.info("Sent data to falcon");

		return Response.status(201).entity(resultData).build();
	}

	// send event data to Falcon server
	public Response sendOffset(String deviceIp, String port, String url, int offset, String latitude,
			String longitude) {
		// LinkedHashMap<String,String> data = new LinkedHashMap<String,String>();
		JSONObject jb = new JSONObject();
       if(offset>360) {
    	   offset=offset-360;
       }
       else if(offset<0) {
    	   offset=offset+360;
       }
		try {
			jb.put("OFFSET", offset);
			jb.put("LATITUDE", latitude);
			jb.put("LONGITUDE", longitude);
		} catch (JSONException e) {
			logger.error(e.toString());
		}

		String resultData = "";
		if (deviceIp != null) {

			// Response rs = sendPostRequestToUrl("http://"+deviceIp+":"+port+url, jb);

			sendPostRequestToUrl("http://" + deviceIp + ":" + port + url, jb);
			/*
			 * if(rs != null) { resultData = rs.readEntity(String.class); }
			 */

			if (resultData == null || resultData.equals("")) {
				resultData = "{}";
			}
		}

		logger.info("Sent offset to " + deviceIp);

		return Response.status(201).entity(resultData).build();
	}

	// method to send http post request
	public synchronized void sendPostRequestToUrl(String url, JSONObject jo)// LinkedHashMap<String,String> data)
	{
		Thread th = new Thread() {
			public void run() {
				Response res = null;
				try {
					System.out.println("**********sendRequestToUrl**********");
					System.out.println("url is :" + url);
					System.out.println("**********sendRequestToUrl**********");
					Client client = ClientBuilder.newClient();
					client.property(ClientProperties.CONNECT_TIMEOUT, 10000);
					client.property(ClientProperties.READ_TIMEOUT, 10000);
					String jsonString = null;
					if (jo != null) {
						jsonString = jo.toString();// new JSONObject(data).toString();
					}
					WebTarget webTarget = client.target(url);
					/*
					 * for(String key:queryParam.keySet()) { webTarget=webTarget.queryParam(key,
					 * queryParam.get(key)); }
					 */
					res = webTarget.request(MediaType.APPLICATION_JSON).post(Entity.json(jsonString));
					System.out.println("**********Response Received**********");
					System.out.println("url is :" + url);
					System.out.println("**********Response Received**********");
				} catch (Exception e) {
					System.out.println("Exception while making request : MSG : " + e.getMessage());
					try {
						throw e;
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		};

		th.start();

		// return res;
	}

	// method to send http get request
	public synchronized Response sendGetRequestToUrl(String url)// LinkedHashMap<String,String> data)
	{
		Response res = null;
		try {
			System.out.println("**********sendRequestToUrl**********");
			System.out.println("url is :" + url);
			System.out.println("**********sendRequestToUrl**********");
			Client client = ClientBuilder.newClient();
			client.property(ClientProperties.CONNECT_TIMEOUT, 1000);
			client.property(ClientProperties.READ_TIMEOUT, 1000);

			// String jsonString = jo.toString();//new JSONObject(data).toString();
			WebTarget webTarget = client.target(url);
			/*
			 * for(String key:queryParam.keySet()) { webTarget=webTarget.queryParam(key,
			 * queryParam.get(key)); }
			 */
			res = webTarget.request(MediaType.APPLICATION_JSON).get();
			System.out.println("**********Response Received**********");
			System.out.println("url is :" + url);
			System.out.println("**********Response Received**********");
		} catch (Exception e) {
			System.out.println("Exception while making request : MSG : " + e.getMessage());
			try {
				throw e;
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return res;
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

	// VAIBHAV BELOW

	public int ptzGetAngle(String ip_add) {

		int ptz_angle = -10000;
		try {
			PTZData ptzData = ptzr.getPTZInfo(ip_add);
			if (ptzData != null) {
				ptz_angle = PTZPacketSender.getInstance().getCurrentAngle(ptzData.getPtz_ip(), ptz_tcp_port);
			}
		} catch (Exception ex) {
			System.out.println("Could not get the Angle, try again");
		}

		return ptz_angle;
	}

	public int ptzGetTilt(String ip_add) {

		int tiltAngle = -10000;
		try {
			PTZData ptzData = ptzr.getPTZInfo(ip_add);
			if (ptzData != null) {
				tiltAngle = PTZPacketSender.getInstance().getCurrentTilt(ptzData.getPtz_ip(), ptz_tcp_port)
						- ptz_tilt_offset;
			}
		} catch (Exception ex) {
			System.out.println("Could not get the Tilt, try again");

		}
		return tiltAngle;
	}

	// vaibhav ABOVE
}
