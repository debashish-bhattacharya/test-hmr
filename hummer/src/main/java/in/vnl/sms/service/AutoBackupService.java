package in.vnl.sms.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import in.vnl.sms.report.ReportServer;

@Service
@PropertySource("classpath:application.properties")
public class AutoBackupService {

	@Value("${autoBackupDirectory}")
	String autoDirectory;
	
	@Autowired
	ReportServer report;
	
	static String fileSeperator = System.getProperty("file.separator");
	
	Logger logger = LoggerFactory.getLogger(AutoBackupService.class);
	
	public static long period = 86400000;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public List<String> getReportNames() {
		List<String> reportNames = new ArrayList<String>();
		String absolutePath = System.getProperty("user.dir")+fileSeperator+autoDirectory+fileSeperator;
		
		File folder = new File(absolutePath);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				System.out.println("File " + listOfFiles[i].getName());
				reportNames.add(listOfFiles[i].getName());
			} 
			else if (listOfFiles[i].isDirectory()) {
				
			}
		}
		return reportNames;
	}
	
	public boolean deleteReport(String reportName) {
		String path = System.getProperty("user.dir")+fileSeperator+autoDirectory+fileSeperator+reportName;
		File file = new File(path);
		
		return file.delete();
	}
	
	public String getFilePath(String reportName) {
		String path = System.getProperty("user.dir")+fileSeperator+autoDirectory+fileSeperator+reportName;
		return path;
	}
	
	public void startAutoBackup() {
		
		Runnable runnable = new Runnable()
        {
            @Override
            public void run()
            {
            	try {
	            	String startTime =  sdf.format(getPreviousDate(new Date()))+" 00:00:00";
	            	String endTime = sdf.format(getPreviousDate(new Date()))+" 23:59:59";
	            	report.createAutoReport(autoDirectory, startTime, endTime, ",");
            	}catch(Exception e) {
            		logger.error(e.getMessage());
            	}
            }
        };
        
        final Date midnight = setTime(new Date());
        long initialDelay = new Date(midnight.getTime()-System.currentTimeMillis()).getTime();
        
        System.out.println(initialDelay);
        ScheduledExecutorService autoBackupScheduler = Executors.newScheduledThreadPool(1);
        autoBackupScheduler.scheduleAtFixedRate(runnable, initialDelay, period, TimeUnit.MILLISECONDS);
        
	}
	
	public static Date setTime(Date date) {    
        Calendar cal = Calendar.getInstance();  
        cal.setTime(date);  
        cal.add(Calendar.DAY_OF_YEAR, 1);
        cal.set(Calendar.HOUR_OF_DAY, 2);  
        cal.set(Calendar.MINUTE, 0);  
        cal.set(Calendar.SECOND, 0);  
        cal.set(Calendar.MILLISECOND, 0);  
        return cal.getTime(); 
    }
	
	public static Date getPreviousDate(Date date) {
        Calendar cal = Calendar.getInstance();  
        cal.setTime(date);  
        cal.add(Calendar.DAY_OF_YEAR, -1);  
        return cal.getTime(); 
    }
	
}
