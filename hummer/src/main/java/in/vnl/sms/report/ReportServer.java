package in.vnl.sms.report;

import in.vnl.sms.controller.AlarmController;
import in.vnl.sms.controller.DataController;
import in.vnl.sms.model.AlarmInfo;
import in.vnl.sms.model.AuditLog;
import in.vnl.sms.model.DeviceInfo;
import in.vnl.sms.model.PeakDetectionInfo;
import in.vnl.sms.model.PeakView;
import in.vnl.sms.model.TriggerData;
import in.vnl.sms.repository.AlarmRepository;
import in.vnl.sms.repository.AuditLogRepository;
import in.vnl.sms.repository.DataRepository;
import in.vnl.sms.repository.DeviceInfoRepository;
import in.vnl.sms.repository.PeakInfoRepository;
import in.vnl.sms.repository.PeakViewRepository;
import in.vnl.sms.service.DBDataService;
import in.vnl.sms.service.DatabaseService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.crypto.Data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class ReportServer {

	@Autowired
	private AuditLogRepository alr;
	@Autowired
	private AlarmController alarmCl;
	@Autowired
	private AlarmRepository ar;
	@Autowired
	private DatabaseService dbService;
	@Autowired
	private PeakViewRepository pr;
	@Autowired
	private DeviceInfoRepository dinfo;
	@Autowired
	private DataRepository dr;
	@Autowired
	private SimpMessagingTemplate messagingTemplate;
	
	@Value("${database}")
	String database_name;
	
	@Value("${FaultDatabase}")
	String fault_database;
	
	@Value("${databasePort}")
	String database_port;
	
	@Value("${spring.datasource.url}")
	String database_url;
	
	@Value("${spring.datasource.username}")	
	String database_username;

	@Value("${spring.datasource.password}")
	String database_password;
	
	@Value("${windowsDbPath}")
	String windowsDbPath;
	
	@Value("${linuxDbPath}")
	String linuxDbPath;
	
	@Value("${windowDumpPath}")
	String windowDumpPath;
	
	@Value("${linuxDumpPath}")
	String linuxDumpPath;
	
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	SimpleDateFormat formater = new SimpleDateFormat("yyyy_MM_dd_HH_mm");
	
	SimpleDateFormat formater1 = new SimpleDateFormat("yyyy_MM_dd_HH_mm");
	
	static String fileSeperator = System.getProperty("file.separator");
	
	static String operatingSystem = System.getProperty("os.name").toLowerCase();
	
	Logger logger = LoggerFactory.getLogger(ReportServer.class);

    public JSONArray getLogAuditReport(String startTime,String endTime)
    {
    	List<AuditLog> auditLog = alr.getAuditBetweenTime(startTime, endTime);
    	System.out.println(auditLog.size());
        return getAuditJson(auditLog);
    }

    public JSONArray getEventAuditReport(String startTime, String endTime)
    {

        List<PeakView> peakView = new ArrayList<PeakView>();
		try {
			peakView = pr.getPeaksBetweenTime(sdf.parse(startTime), sdf.parse(endTime), getDeviceInfo());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//getPeakInfoRecordBetweenTime(startTime, endTime, getDeviceInfo());
        return getEventJson(peakView);
    }


    public void writeToFile(String header,JSONArray data ,String reportColumns,String childDirectoryPath)
    {

        File f = new File(childDirectoryPath+fileSeperator+header);
        ArrayList<String> columnHead = new ArrayList<String>(Arrays.asList(reportColumns.split(",")));

        System.out.println("column are"+columnHead.toString());
        PrintWriter writer = null;
        //if(columnHead.size()>=1)
        //{

            try {

                writer = new PrintWriter(f, "UTF-8");
                if(!f.exists())
                {
                    try {
                        f.createNewFile();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                writer.println(header);

                //creating columns head
                StringBuilder colsHead = new StringBuilder();
                for(int i=0;i<columnHead.size();i++)
                {
                    if(i==0)
                    {
                        colsHead.append(columnHead.get(i));
                    }
                    else
                    {
                        colsHead.append("\t");
                        colsHead.append(columnHead.get(i));
                    }
                }
                System.out.println("colsHead is :"+colsHead);

                writer.println(colsHead.toString());


                for(int j=0;j<data.length();j++)
                {
                    StringBuilder bb = new StringBuilder();
                    for(int i=0;i<columnHead.size();i++)
                    {
                        if(i==0)
                        {
                        	bb.append(data.getJSONObject(j).get(columnHead.get(i)));
                        }
                        else
                        {
                            bb.append("\t");
                            bb.append(data.getJSONObject(j).get(columnHead.get(i)));
                        }
                    }
                    writer.println(bb.toString());
                }

            } catch (FileNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            finally
            {
                writer.close();

            }
    }

    public String createAutoReport(String childDir, String startTime, String endTime, String seprator)
    {
        
        String reportFolderName = "HUMMER_C2_"+formater.format(new Date());

        String childDirectoryPath = createChildReportDirectory(childDir, reportFolderName);

        JSONArray auditLogData = getLogAuditReport(startTime, endTime);
        createAuditLogReport(getReportColumnValues("audit"), auditLogData, childDirectoryPath, "Audit Log", seprator);

        //jdbcEventLogReport(startTime, endTime, getReportColumnValues("eventnew"), childDirectoryPath, "Event Log", seprator);
        boolean flag = createReportFromCopy(startTime, endTime, childDirectoryPath, "Event Log");
        if(flag) {
        	splitFile(childDirectoryPath, "Event Log");
        }
        
        createAlarmLogReport(startTime, endTime, getReportColumnValues("alarm"), childDirectoryPath, "Alarm Log", seprator);
        
        String systemLogDirectory = createChildReportDirectory(childDir+System.getProperty("file.separator")+reportFolderName, "system_log");
        copySystemLogs(systemLogDirectory);
        
        dbBackup(childDirectoryPath,"security");
        
        try {
        	zipFolder(childDirectoryPath,childDirectoryPath+".zip");
        	deleteFolder(childDirectoryPath);
        }catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            logger.error(e.getMessage());
            return "{\"result\":\"fail\",\"msg\":\"Exception\"}";
        }
        return childDirectoryPath+".zip";

    }

    public String createReport(String startTime,String endTime,String seprator) //throws Exception
    {
    	boolean flag = false;
    	DeviceInfo device = getDeviceInfo();
    	String unitNo = getUnitNo(device.getIp_add());
    	JSONArray faultData=new JSONArray();
        String reportFolderName = "HUMMER_C2_"+formater.format(new Date())+"_"+device.getLat()+"_"+device.getLon()+"_"+unitNo;

        String childDirectoryPath = createChildReportDirectory("reports", reportFolderName);

        JSONArray auditLogData = getLogAuditReport(startTime, endTime);
        flag = createAuditLogReport(getReportColumnValues("audit"), auditLogData, childDirectoryPath, "Audit Log", seprator);

        //jdbcEventLogReport(startTime, endTime, getReportColumnValues("eventnew"), childDirectoryPath, "Event Log", seprator);
        boolean report = createReportFromCopy(startTime, endTime, childDirectoryPath, "Event Log");
        if(report) {
        	splitFile(childDirectoryPath, "Event Log");
        }
     
        boolean report1 = getFaultReport(startTime, endTime, childDirectoryPath, "fault log");
        
        //JSONArray eventLogData = getEventAuditReport(startTime, endTime);
        //createAuditLogReport(getReportColumnValues("event"), eventLogData, childDirectoryPath, "Event Log", seprator);
        createAlarmLogReport(startTime, endTime, getReportColumnValues("alarm"), childDirectoryPath, "Alarm Log", seprator);
        
        //copySystemLogs(childDirectoryPath);
        
        try {
        	zipFolder(childDirectoryPath,childDirectoryPath+".zip");
        	deleteFolder(childDirectoryPath);
        }catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "{\"result\":\"fail\",\"msg\":\"Exception\"}";
        }
        
//        if(!flag)
//        	throw new Exception("File is empty");
        
        return childDirectoryPath+".zip";

    }
    
    public String createSystemLogs()
    {
    	DeviceInfo device = getDeviceInfo();
    	String unitNo = getUnitNo(device.getIp_add());
    	
        String reportFolderName = "HUMMER_C2_SysLogs_"+formater.format(new Date())+"_"+device.getLat()+"_"+device.getLon()+"_"+unitNo;

        String childDirectoryPath = createChildReportDirectory("reports", reportFolderName);

        copySystemLogs(childDirectoryPath);
        
        try {
        	zipFolder(childDirectoryPath,childDirectoryPath+".zip");
        	deleteFolder(childDirectoryPath);
        }catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "{\"result\":\"fail\",\"msg\":\"Exception\"}";
        }
        return childDirectoryPath+".zip";

    }
    
    public void deleteFolder(String path) throws IOException{
    	Path directory = Paths.get(path);
		Files.walkFileTree(directory, new SimpleFileVisitor<Path>() {
		   @Override
		   public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
		       Files.delete(file); // this will work because it's always a File
		       return FileVisitResult.CONTINUE;
		   }

		   @Override
		   public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
		       Files.delete(dir); //this will work because Files in the directory are already deleted
		       return FileVisitResult.CONTINUE;
		   }
		});
    }
    
    public JSONArray createEventReport(String startTime, String endTime, boolean all) {
    	List<PeakDetectionInfo> peakDetectionInfo = null;
    	List<AlarmInfo> alarmInfo = null;
    	if(all) {
    		//peakDetectionInfo = pr.getPeakInfoRecordBetweenTimeForAlarm(startTime, endTime, getDeviceInfo(), true, new PageRequest(0, 500));
    		alarmInfo = ar.getAlarmInfoRecordByTime(startTime, endTime, "centroid");
    	}
    	else {
    		//peakDetectionInfo = pr.getPeakInfoRecordBetweenTimeForAlarm(startTime, endTime, getDeviceInfo(), !all);
    		alarmInfo = ar.getAlarmInfoRecordByTime(startTime, endTime, "centroid");
    	}
    	
    	JSONArray ja = new JSONArray();
    	int count = 0;
    	
    	if(alarmInfo!=null) {
    		AlarmInfo ar;
	        Iterator<AlarmInfo> itr1 = alarmInfo.iterator();
	        while(itr1.hasNext())
	        {
	            count++;
	            try {
	                if(count >= 100000)
	                {
	                    /*JSONObject jb = new JSONObject();
	                    jb.put("result","fail");
	                    jb.put("msg","Limit exceed");
	                    ja = null;
	                    ja = new JSONArray();
	                    ja.put(jb);*/
	                    break;
	                }
	
	                JSONObject jb = new JSONObject();
	            	ar = itr1.next();
	            	jb.put("TIME_STAMP", ar.getTime());
	            	jb.put("TRANS_ID", ar.getTransid());
	            	jb.put("CUE_ID", ar.getCueid());
	            	jb.put("DEVICE_IP", ar.getIp_addr());
	            	jb.put("FREQ", Double.parseDouble(ar.getFrequency()));
	            	jb.put("ANGLE", Double.parseDouble(ar.getAngle()));
	                jb.put("POWER", Double.parseDouble(ar.getPower()));
	                jb.put("PROBABLE_DISTANCE", ar.getRange());
	            	jb.put("EVENT_TYPE", 7);
	            	jb.put("LATITUDE", ar.getLatitude());
	            	jb.put("LONGITUDE", ar.getLongitude());
	                ja.put(jb);
	            }catch (JSONException e) {
					e.printStackTrace();
	            }
	        }
    	}
    	
//    	if(peakDetectionInfo!=null) {
//	        PeakDetectionInfo peak;
//	        Iterator<PeakDetectionInfo> itr = peakDetectionInfo.iterator();
//	        while(itr.hasNext())
//	        {
//	            count++;
//	            try {
//	                if(count >= 100000)
//	                {
//	                    /*JSONObject jb = new JSONObject();
//	                    jb.put("result","fail");
//	                    jb.put("msg","Limit exceed");
//	                    ja = null;
//	                    ja = new JSONArray();
//	                    ja.put(jb);*/
//	                    break;
//	                }
//	
//	                JSONObject jb = new JSONObject();
//	            	peak = itr.next();
//	            	jb.put("TIME_STAMP", peak.getTime());
//	            	jb.put("TRANS_ID", DBDataService.generateTransId());
//	            	jb.put("DEVICE_IP", peak.getIp_add());
//	            	jb.put("FREQ", Double.parseDouble(peak.getFrequency()));
//	            	jb.put("ANGLE", Double.parseDouble(peak.getAngle()));
//	                jb.put("POWER", Double.parseDouble(peak.getPower()));
//	                jb.put("PROBABLE_DISTANCE", Double.parseDouble("-1"));
//	            	jb.put("EVENT_TYPE", 1);
//	            	jb.put("LATITUDE", Double.parseDouble("-1"));
//	            	jb.put("LONGITUDE", Double.parseDouble("-1"));
//	                ja.put(jb);
//	            }catch (JSONException e) {
//					e.printStackTrace();
//	            }
//	        }
//        }
        return ja;
    }

    public boolean createAuditLogReport(String reportColumns, JSONArray data, String filePath, String fileName, String seprator)
    {
    	boolean flag = false;
    	String sepType = seprator.equals(",")?".csv":".tsv";
    	
        File f = new File(filePath+System.getProperty("file.separator")+fileName+sepType);
        ArrayList<String> columnHead = new ArrayList<String>(Arrays.asList(reportColumns.split(",")));
        int size=0;
        
        PrintWriter writer = null;
        try {

            writer = new PrintWriter(f, "UTF-8");
            if(!f.exists())
            {
                try {
                    f.createNewFile();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            //creating columns head
            StringBuilder colsHead = new StringBuilder();
            size = columnHead.size();
            for(int i=0;i<size;i++)
            {
                if(i==0)
                {
                    colsHead.append(columnHead.get(i));
                }
                else
                {
                    colsHead.append(seprator);
                    colsHead.append(columnHead.get(i));
                }
            }
            writer.println(colsHead.toString());
            
            try {
                for (int j = 0; j < data.length(); j++) {
                	writer.println(data.getJSONObject(j).get("logtime")+seprator+"HUMMER"+seprator
                			+data.getJSONObject(j).get("log_type")+seprator+data.getJSONObject(j).get("description"));
                    flag = true;
                	/*if (j != 0) {
                        bb.append("\n");
                    }

                    bb.append(data.getJSONObject(j).get("logtime"));
                    bb.append(seprator);
                    if(size>3) 
                    	bb.append("HUMMER"+seprator);
                    bb.append(data.getJSONObject(j).get("log_type"));
                    bb.append(seprator);
                    bb.append(data.getJSONObject(j).get("description"));*/

                    /*JSONObject jj = new JSONObject(data.getJSONObject(j).get("description"));

                    Iterator<String> itr = jj.keys();
                    while (itr.hasNext()) {
                        String key = itr.next();
                        bb.append(seprator);
                        bb.append(jj.getString(key));
                    }*/
                }
            } catch (Exception e) {
                // TODO: handle exception
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        finally
        {
        	writer.flush();
            writer.close();
        }
        
        return flag;
    }
    
    public void jdbcEventLogReport(String startTime, String endTime, String reportColumns, String filePath, String fileName, String seprator)
    {
    	Connection c = getConnection();
    	
    	if(c!=null) {
    		try {
	    		c.setAutoCommit(false);
	    		Statement stmt;
	    		ResultSet rs;
	    		
				stmt = c.createStatement();
		        rs = stmt.executeQuery( "SELECT * FROM peak_view where inserttime between '"+startTime+"' and '"+endTime+"';");
				rs.next();
				//System.out.println(rs.getString("time"));
		        //peakDetectionInfo = pr.getPeaksBetweenTime(sdf.parse(startTime), sdf.parse(endTime), getDeviceInfo());
		        //getPeakInfoRecordBetweenTime(startTime, endTime, getDeviceInfo());

		        int peakCount = 1;
		        
		        if(peakCount>0) {
		        	System.out.println("Event Log : "+peakCount);
		        	String sepType = seprator.equals(",")?".csv":".tsv";
		        	
			        File f = new File(filePath+System.getProperty("file.separator")+fileName+sepType);
			        ArrayList<String> columnHead = new ArrayList<String>(Arrays.asList(reportColumns.split(",")));
			        int size=0;
			        
			        PrintWriter writer = null;
			        try {
			
			            writer = new PrintWriter(f, "UTF-8");
			            if(!f.exists())
			            {
			                try {
			                    f.createNewFile();
			                } catch (IOException e) {
			                    // TODO Auto-generated catch block
			                    e.printStackTrace();
			                }
			            }
			
			            //creating columns head
			            StringBuilder colsHead = new StringBuilder();
			            size = columnHead.size();
			            for(int i=0;i<size;i++)
			            {
			                if(i==0)
			                {
			                    colsHead.append(columnHead.get(i));
			                }
			                else
			                {
			                    colsHead.append(seprator);
			                    colsHead.append(columnHead.get(i));
			                }
			            }
			            writer.println(colsHead.toString());
			            
			            try {
			                while(rs.next()) {
			                	writer.println(rs.getString("time")+seprator+rs.getString("type")+seprator+rs.getString("frequency")+seprator
			                			+rs.getString("power")+seprator+rs.getString("antennaid")+seprator+rs.getString("sector")+seprator+rs.getString("angle"));
			                }
			                
			            } catch (Exception e) {
			                System.out.println(e);
			            }
			        } catch (FileNotFoundException e) {
			            // TODO Auto-generated catch block
			            e.printStackTrace();
			        } catch (UnsupportedEncodingException e) {
			            // TODO Auto-generated catch block
			            e.printStackTrace();
			        }
			        finally
			        {
			        	writer.flush();
			            writer.close();
		            	rs.close();
		            	stmt.close();
						c.close();
					}
		        }
    		}catch(Exception e) {
    			System.out.println(e);
    		}
    	}
    }
    
    public void createEventLogReport(String startTime, String endTime, String reportColumns, String filePath, String fileName, String seprator)
    {
        List<PeakView> peakDetectionInfo = new ArrayList<PeakView>();
        
		try {
			Date d = sdf.parse(startTime);
	        System.out.println(d);
			peakDetectionInfo = pr.getPeaksBetweenTime(sdf.parse(startTime), sdf.parse(endTime), getDeviceInfo());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}//getPeakInfoRecordBetweenTime(startTime, endTime, getDeviceInfo());
        int peakCount = peakDetectionInfo.size();
        
        if(peakCount>0) {
        	System.out.println("Event Log : "+peakCount);
        	String sepType = seprator.equals(",")?".csv":".tsv";
        	
	        File f = new File(filePath+System.getProperty("file.separator")+fileName+sepType);
	        ArrayList<String> columnHead = new ArrayList<String>(Arrays.asList(reportColumns.split(",")));
	        int size=0;
	        
	        PrintWriter writer = null;
	        try {
	
	            writer = new PrintWriter(f, "UTF-8");
	            if(!f.exists())
	            {
	                try {
	                    f.createNewFile();
	                } catch (IOException e) {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
	                }
	            }
	
	            //creating columns head
	            StringBuilder colsHead = new StringBuilder();
	            size = columnHead.size();
	            for(int i=0;i<size;i++)
	            {
	                if(i==0)
	                {
	                    colsHead.append(columnHead.get(i));
	                }
	                else
	                {
	                    colsHead.append(seprator);
	                    colsHead.append(columnHead.get(i));
	                }
	            }
	            writer.println(colsHead.toString());
	            
	            try {
	                Iterator<PeakView> itr = peakDetectionInfo.iterator();
	                while(itr.hasNext()) {
	                	PeakView pk = itr.next();
	                	writer.println(pk.getTime()+seprator+pk.getType()+seprator+pk.getFrequency()+seprator
	                			+pk.getPower()+seprator+pk.getAntennaid()+seprator+pk.getSector()+seprator+pk.getAngle());
	                }
	                
	            } catch (Exception e) {
	                // TODO: handle exception
	            }
	        } catch (FileNotFoundException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (UnsupportedEncodingException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        finally
	        {
	        	writer.flush();
	            writer.close();
	        }
        }
    }
    
    public void createAlarmLogReport(String startTime, String endTime, String reportColumns, String filePath, String fileName, String seprator)
    {
        List<AlarmInfo> alarmInfo = new ArrayList<AlarmInfo>();
        
		alarmInfo = ar.getAlarmInfoRecordByTime(startTime, endTime, "centroid");
		//getPeakInfoRecordBetweenTime(startTime, endTime, getDeviceInfo());
        int alarmCount = alarmInfo.size();
        
        if(alarmCount>0) {
        	System.out.println("Alarm Log : "+alarmCount);
        	String sepType = seprator.equals(",")?".csv":".tsv";
        	
	        File f = new File(filePath+System.getProperty("file.separator")+fileName+sepType);
	        ArrayList<String> columnHead = new ArrayList<String>(Arrays.asList(reportColumns.split(",")));
	        int size=0;
	        
	        PrintWriter writer = null;
	        try {
	
	            writer = new PrintWriter(f, "UTF-8");
	            if(!f.exists())
	            {
	                try {
	                    f.createNewFile();
	                } catch (IOException e) {
	                    // TODO Auto-generated catch block
	                    e.printStackTrace();
	                }
	            }
	
	            //creating columns head
	            StringBuilder colsHead = new StringBuilder();
	            size = columnHead.size();
	            for(int i=0;i<size;i++)
	            {
	                if(i==0)
	                {
	                    colsHead.append(columnHead.get(i));
	                }
	                else
	                {
	                    colsHead.append(seprator);
	                    colsHead.append(columnHead.get(i));
	                }
	            }
	            writer.println(colsHead.toString());
	            
	            try {
	                Iterator<AlarmInfo> itr = alarmInfo.iterator();
	                while(itr.hasNext()) {
	                	AlarmInfo ai = itr.next();
	                	writer.println(ai.getTime()+seprator+ai.getTrigger()+seprator+ai.getFrequency()+seprator
	                			+ai.getAngle()+seprator+ai.getRange()+seprator+ai.getPower()+seprator+ai.getEmitterpower()+seprator+ai.getAlarm()+seprator+ai.getLatitude()+seprator+ai.getLongitude());
	                }
	                
	            } catch (Exception e) {
	                // TODO: handle exception
	            }
	        } catch (FileNotFoundException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        } catch (UnsupportedEncodingException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
	        finally
	        {
	        	writer.flush();
	            writer.close();
	        }
        }
    }
    
    public Connection getConnection() {
    	Connection c = null;
        try {
           Class.forName("org.postgresql.Driver");
           c = DriverManager
              .getConnection(database_url,
              database_username, database_password);
        } catch (Exception e) {
           e.printStackTrace();
           System.err.println(e.getClass().getName()+": "+e.getMessage());
           System.exit(0);
        }
        return c;
    }

    public void copySystemLogs(String filePath) {
    	try {
    		//filePath = filePath+System.getProperty("file.separator")+"System Log";
    		if(operatingSystem.contains("win")) {
		        Path from = Paths.get("C:\\Windows\\System32\\winevt\\Logs\\Application.evtx");
		        Path to= Paths.get(filePath+System.getProperty("file.separator")+"Application.evtx");
		
		        Files.copy(from, to);
		        
		        from = Paths.get("C:\\Windows\\System32\\winevt\\Logs\\System.evtx");
		        to= Paths.get(filePath+System.getProperty("file.separator")+"System.evtx");
		
		        Files.copy(from, to);
		        
		        from = Paths.get("C:\\Windows\\System32\\winevt\\Logs\\Security.evtx");
		        to= Paths.get(filePath+System.getProperty("file.separator")+"Security.evtx");
		
		        Files.copy(from, to);
    		}
    		else {
    			
    			Path from = Paths.get("/var/log");
		        Path to= Paths.get(filePath+fileSeperator+"System_logs");//+System.getProperty("file.separator")+"logs");
		        
    			CustomFileVisitor customFileVisitor = new CustomFileVisitor(from,  to);
    			
		        Files.walkFileTree(from, customFileVisitor);
		        
    		}
	        
    	}catch(IOException e) {
    		System.out.println(e);
    		Writer buffer = new StringWriter();
    		PrintWriter pw = new PrintWriter(buffer);
    		e.printStackTrace(pw);
    		logger.error("Error in Sys logs "+buffer.toString());
    	}
    }
    
    public String createChildReportDirectory(String name, String reportFolderName)
    {
        String path=null;
        String absolutePath = System.getProperty("user.dir");//getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
        //absolutePath = absolutePath.substring(0,absolutePath.lastIndexOf("classes"));
        path = absolutePath+ fileSeperator+name+fileSeperator;
        String childDirectoryPath = path + reportFolderName;
        System.out.println("@sajal"+childDirectoryPath);
        checkOrCreateParentChildDirectory(path,reportFolderName);
        return childDirectoryPath;
    }

    public void checkOrCreateParentChildDirectory(String parentPath,String childDirectoryName)
    {

        File file=new File(parentPath);

        if(!file.exists())
        {
            file.mkdir();
        }

        String childPath = parentPath + childDirectoryName;

        File file1 = new File(childPath);

        if (!file1.exists()){
                file1.mkdir();
          }

    }

	static public void zipFolder(String srcFolder, String destZipFile) throws Exception {
	    ZipOutputStream zip = null;
	    FileOutputStream fileWriter = null;
	
	    fileWriter = new FileOutputStream(destZipFile);
	    zip = new ZipOutputStream(fileWriter);
	
	    addFolderToZip("", srcFolder, zip);
	    
	    zip.flush();
	    zip.close();
	    fileWriter.flush();
	    fileWriter.close();
	}
	
	static private void addFileToZip(String path, String srcFile, ZipOutputStream zip) throws Exception {
	
		File folder = new File(srcFile);
		if (folder.isDirectory()) {
		  addFolderToZip(path, srcFile, zip);
		} else {
		  byte[] buf = new byte[1024];
		  int len;
		  FileInputStream in = new FileInputStream(srcFile);
		  zip.putNextEntry(new ZipEntry(path + fileSeperator + folder.getName()));
		  while ((len = in.read(buf)) > 0) {
		    zip.write(buf, 0, len);
		  }
		  in.close();
		}
	}
	
	static private void addFolderToZip(String path, String srcFolder, ZipOutputStream zip) throws Exception {
	    File folder = new File(srcFolder);
	
	    for (String fileName : folder.list()) {
	      if (path.equals("")) {
	    	  addFileToZip(folder.getName(), srcFolder + fileSeperator + fileName, zip);
	      } else {
	    	  addFileToZip(path + fileSeperator + folder.getName(), srcFolder + fileSeperator + fileName, zip);
	      }
	    }
	}

    public String getReportColumnValues(String ReportColumnKey)
    {

    	ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties prop = new Properties();
        try
        {
            prop.load(loader.getResourceAsStream("report_columns.properties"));
        }
        catch(Exception E)
        {
            System.out.println("report_columns.properties not found" + E.getMessage());

        }
        return prop.getProperty(ReportColumnKey);
    }
    
    public JSONArray getAuditJson(List<AuditLog> auditLog)
    {
        JSONArray ja = new JSONArray();
        if(auditLog!=null) {
            AuditLog al;
            int count = 0;
            Iterator<AuditLog> itr = auditLog.iterator();
            while(itr.hasNext())
            {
                count++;
                try {
	                if(count >= 100000)
	                {
	                    JSONObject jb = new JSONObject();
	                    jb.put("result","fail");
	                    jb.put("msg","Limit exceed");
	                    ja = null;
	                    ja = new JSONArray();
	                    ja.put(jb);
	                    break;
	                }
	
	                JSONObject jb = new JSONObject();
	            	al = itr.next();
	        		jb.put("log_type", al.getLogType());
	            	jb.put("logtime", al.getCreatedDateTime().toString());
					jb.put("description", al.getDescription());
					
	                ja.put(jb);
                }catch (JSONException e) {
					e.printStackTrace();
                }
            }
        }
        System.out.println(ja.length());
        return ja;
    }
    
    public JSONArray getEventJson(List<PeakView> peakInfo)
    {
        JSONArray ja = new JSONArray();
        PeakView peak;
        int count = 0;
        Iterator<PeakView> itr = peakInfo.iterator();
        while(itr.hasNext())
        {
            count++;
            try {
                if(count >= 100000)
                {
                    JSONObject jb = new JSONObject();
                    jb.put("result","fail");
                    jb.put("msg","Limit exceed");
                    ja = null;
                    ja = new JSONArray();
                    ja.put(jb);
                    break;
                }

                JSONObject jb = new JSONObject();
            	peak = itr.next();
        		jb.put("log_type", "Event");
            	jb.put("logtime", peak.getTime());
				jb.put("description", "Meas"+","+peak.getType()+","+peak.getFrequency()+","+peak.getPower()+","+peak.getAntennaid()+","+peak.getSector()+","+peak.getAngle());
				
                ja.put(jb);
            }catch (JSONException e) {
				e.printStackTrace();
            }
        }
        return ja;
    }

    public DeviceInfo getDeviceInfo() {
		List<DeviceInfo> allDevices = dinfo.getDeviceInfo();
		DeviceInfo deviceInfo = null;
		
		for(DeviceInfo device: allDevices)
		{
			deviceInfo = device;
		}
		return deviceInfo;
	}
    
    public String getUnitNo(String ip_add) {
    	TriggerData td = dr.getTriggerData(ip_add);
    	return td.getUnit_no();
    }
    
    public String createDbBackup(String childDir)
    {
    	DataController.scheduleDb = true;
    	DeviceInfo device = getDeviceInfo();
    	String unitNo = getUnitNo(device.getIp_add());
    	
        String reportFolderName = "HUMMER_C2_FULL_BACKUP_"+formater.format(new Date())+"_"+device.getLat()+"_"+device.getLon()+"_"+unitNo;

        String childDirectoryPath = createChildReportDirectory(childDir, reportFolderName);
        
        dbBackup(childDirectoryPath,"security");
        
        try {
        	zipFolder(childDirectoryPath,childDirectoryPath+".zip");
        	deleteFolder(childDirectoryPath);
        }catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "{\"result\":\"fail\",\"msg\":\"Exception\"}";
        }
        DataController.scheduleDb = false;
        return childDirectoryPath+".zip";

    }
    
    public void dbBackup(String filePath, String fileName) {
        
    	Process p;
        ProcessBuilder pb;
        
        String dumpPath = operatingSystem.contains("win")?windowDumpPath:linuxDumpPath;
        
        pb = new ProcessBuilder(
                dumpPath,			//"C:\\Program Files\\PostgreSQL\\10\\bin\\pg_dump.exe",
                "--host", "localhost",
                "--port", database_port, //"5432",
                "--username", database_username,
                "--no-password",
                "--format", "plain",
                "--blobs",
                "--verbose", "--file", filePath+System.getProperty("file.separator")+fileName+".dump", database_name);
        try {
            final Map<String, String> env = pb.environment();
            env.put("PGPASSWORD", database_password);
            p = pb.start();
            final BufferedReader r = new BufferedReader(
                    new InputStreamReader(p.getErrorStream()));
            String line = r.readLine();
            while (line != null) {
                //System.err.println(line);
                //logger.error(line);
                line = r.readLine();
            }
            r.close();
            p.waitFor();
            System.out.println(p.exitValue());
            messagingTemplate.convertAndSend("/topic/DbStatus", "Data backup completed successfully");

        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
            logger.error(e.getMessage());
        }
    }
    
    public boolean createReportFromCopy(String startTime, String stopTime, String filePath, String fileName) {
        Process p;
        ProcessBuilder pb;
       
        String dbPath = operatingSystem.contains("win")?windowsDbPath:linuxDbPath;
        
        pb = new ProcessBuilder(
                dbPath,			//"C:/Program Files/PostgreSQL/10/bin/psql",
                "-d",database_name,
                "--username", database_username,
                "--no-password",
                "--command", 
                "\\copy (select time as logtime,type,frequency,power,antennaid,sector,lat as Latitude,lon as longitude,angle from peak_jm_view where time between'"+startTime+"' and '"+stopTime+"' order by id asc) to '"+
                		filePath+System.getProperty("file.separator")+fileName+".csv"+"' with csv header");    //,tilt
                //"--verbose", "--file", filePath+System.getProperty("file.separator")+fileName+".sql", "security_server");
        try {
        	//pb.directory(new File("C:\\Program Files\\PostgreSQL\\10\\bin\\"));
        	//pb.directory(new File(dbPath.substring(0,dbPath.lastIndexOf("/"))));
        	//System.out.println(pb.directory());
            final Map<String, String> env = pb.environment();
            env.put("PGPASSWORD", database_password);
            p = pb.start();
            final BufferedReader r = new BufferedReader(
                    new InputStreamReader(p.getErrorStream()));
            String line = r.readLine();
            while (line != null) {
                //System.err.println(line);
                logger.error(line);
                line = r.readLine();
            }
            r.close();
            p.waitFor();
            System.out.println(p.exitValue());
            
            return true;
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
            logger.error(e.getMessage());
            return false;
        }
    }
    
    public boolean getFaultReport(String startTime, String stopTime, String filePath, String fileName) {
        Process p;
        ProcessBuilder pb;
        int i=0;
        boolean recordExits=false;
        String dbPath = operatingSystem.contains("win")?windowsDbPath:linuxDbPath;
        String qry ="select * from view_history where logtime between'"+startTime+"' and '"+stopTime+"' order by logtime asc limit 1";
         try {
			JSONArray faultData =dbService.faultGetDbData(qry);
			if(faultData.length()>0) {
				recordExits=true;
			}
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			
		}
         if(!recordExits) {
        	 return recordExits;
         }
        pb = new ProcessBuilder(
                dbPath,			//"C:/Program Files/PostgreSQL/10/bin/psql",
                "-d",fault_database,
                "--username", database_username,
                "--no-password",
                "--command", 
                "\\copy (select * from view_history where logtime between'"+startTime+"' and '"+stopTime+"' order by logtime asc) to '"+
                  
                		filePath+System.getProperty("file.separator")+fileName+".csv"+"' with csv header");    //,tilt
                //"--verbose", "--file", filePath+System.getProperty("file.separator")+fileName+".sql", "security_server");
        try {
        	//pb.directory(new File("C:\\Program Files\\PostgreSQL\\10\\bin\\"));
        	//pb.directory(new File(dbPath.substring(0,dbPath.lastIndexOf("/"))));
        	//System.out.println(pb.directory());
            final Map<String, String> env = pb.environment();
            env.put("PGPASSWORD", database_password);
            p = pb.start();
            final BufferedReader r = new BufferedReader(
                    new InputStreamReader(p.getInputStream()));
            String line = r.readLine();
            while (line != null) {
                //System.err.println(line);
                logger.error(line);
                line = r.readLine();
                i++;
            }
            r.close();
            p.waitFor();
            System.out.println(p.exitValue());
            
            return true;
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
            logger.error(e.getMessage());
            return false;
        }
    }
    
    public void splitFile(String path, String fileName) {
    	try{
			String fileType = ".csv";
			String inputfile = path + fileSeperator + fileName; 
			String filePath = inputfile + fileType;
			  
			int nol = 100000;
			int count = 0;
			 
			FileInputStream fstream = new FileInputStream(filePath);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			  
			int j=1;
			FileWriter fstream1 = new FileWriter(inputfile+"_"+j+fileType);
			BufferedWriter out = new BufferedWriter(fstream1); 
			
			String header = br.readLine();
			out.write(header); 
			out.newLine();
			
			String str;
			while((str=br.readLine())!=null)
			{    
				count++;
				if(count==nol) {
					out.close();
					fstream1.close();
					j++;
					fstream1 = new FileWriter(inputfile+"_"+j+fileType); 
					out = new BufferedWriter(fstream1); 
				    count=0;
				}
				
				if(count==0) {
					out.write(header); 
					out.newLine();
				}
				
				out.write(str); 
				out.newLine();  
			} 
			out.close();
			br.close();
			in.close();
			fstream.close();
			fstream1.close();
			Path fileToDeletePath = Paths.get(filePath);
			Files.delete(fileToDeletePath);
  		 
  		}
  		catch (Exception e)
  		{
  			System.err.println("Error: " + e.getMessage());
  			logger.error(e.getMessage());
  		}
    }
    
}




