package in.vnl.sms.model;

public class DeviceDataInfo {
	
	private String lat;
	private String lon;
	private int ptzoffset;
	
	public DeviceDataInfo(String lat, String lon, int ptzoffset) {
		this.lat=lat;
		this.lon=lon;
		this.ptzoffset=ptzoffset;
	}
	
	public String getLat() {
		return lat;
	}
	
	public void setLat(String lat) {
		this.lat = lat;
	}
	
	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public int getPtzoffset() {
		return ptzoffset;
	}

	public void setPtzoffset(int ptzoffset) {
		this.ptzoffset = ptzoffset;
	}

	
}
