package in.vnl.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="events")
public class Event {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	@Column(name="event_date")
	private String eventDate;
	@Column(name="node_type")
	private String nodeType;
	@Column(name="event_Data")
	private String eventData;
	@Column(name="event_type")
	private String eventType;
	@Column(name="event_status")
	private boolean eventStatus;
	
//	public Event() 
//	{
//		
//	}
//
//
//	public Event(Long eventId, Date eventDate, String nodeType, String eventData,String eventType) {
//		super();
//		this.eventId = eventId;
//		this.eventDate = eventDate;
//		this.nodeType = nodeType;
//		this.eventData = eventData;
//		this.eventType = eventType;
//	}


	public Long getId() {
		return id;
	}

	public void setEventId(Long id) {
		this.id = id;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	public String getEventData() {
		return eventData;
	}

	public void setEventData(String eventData) {
		this.eventData = eventData;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	public boolean getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(boolean eventStatus) {
		this.eventStatus = eventStatus;
	}
	
	@Override
	public String toString() 
	{
		return "Event [id=" + id + ", eventDate=" + eventDate + ", nodeType=" + nodeType + ", eventData=" + eventData + ""
				+ ", eventType=" + eventType + ", eventStatus=" + eventStatus +"]";
	}
	
}
