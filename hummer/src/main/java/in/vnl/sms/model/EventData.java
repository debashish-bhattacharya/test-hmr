package in.vnl.sms.model;

import java.time.LocalDateTime;

public class EventData {
	
	private int type;
	private String transId;
	private String cueId;
	private String eventName;
	private String device_ip;
	private String frequency;
	private double bandwidth;
	private String startAngle;
	private String stopAngle;
	private String sector;
	private int tiltAngleProperty;
	private String latitude;
	private String longitude;
	private double timeout;
	private int validity;
	private LocalDateTime date;
    private double maxPower;
    // A parameterized EventData constructor 
    public EventData(String transId, String cueId, int type, String eventName, String device_ip, String frequency, double bandwidth, String startAngle, String stopAngle, int tiltAngleProperty, double timeout, int validity, LocalDateTime date, double maxPower) { 
    	
    	this.transId = transId;
    	this.cueId = cueId;
        this.type = type; 
        this.eventName = eventName;
        this.device_ip = device_ip;
        this.frequency = frequency;
        this.bandwidth = bandwidth;
        this.startAngle = startAngle;
        this.stopAngle = stopAngle;
        this.tiltAngleProperty = tiltAngleProperty;
        this.timeout = timeout;
        this.validity = validity;
        this.date = date;
        this.maxPower = maxPower;
    }
    
    // A parameterized EventData constructor 
    public EventData(String transId, String cueId, int type, String eventName, double bandwidth, String latitude, String longitude, double timeout, int validity, LocalDateTime date, double maxPower) { 
    	
    	this.transId = transId;
    	this.cueId = cueId;
        this.type = type; 
        this.eventName = eventName;
        this.bandwidth = bandwidth;
        this.latitude = latitude;
        this.longitude = longitude;
        this.timeout = timeout;
        this.validity = validity;
        this.date = date;
    }
    
 // A parameterized EventData constructor 
    public EventData(String transId, String cueId, int type, String eventName, String frequency, double bandwidth, String angle, String sector, double timeout, int validity, LocalDateTime date, double maxPower) { 
    	this.transId = transId;
    	this.cueId = cueId;
        this.type = type; 
        this.eventName = eventName;
        this.frequency = frequency;
        this.bandwidth = bandwidth;
        this.startAngle = angle;
        this.sector = sector;
        this.timeout = timeout;
        this.validity = validity;
        this.date = date;
    }
    
    public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}
	
	public String getCueId() {
		return cueId;
	}

	public void setCueId(String cueId) {
		this.cueId = cueId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getDevice_ip() {
		return device_ip;
	}

	public void setDevice_ip(String device_ip) {
		this.device_ip = device_ip;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	public double getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(double bandwidth) {
		this.bandwidth = bandwidth;
	}
	
	public String getStartAngle() {
		return startAngle;
	}

	public void setStartAngle(String startAngle) {
		this.startAngle = startAngle;
	}

	public String getStopAngle() {
		return stopAngle;
	}

	public void setStopAngle(String stopAngle) {
		this.stopAngle = stopAngle;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}
	
	public int getTiltAngleProperty() {
		return tiltAngleProperty;
	}

	public void setTiltAngleProperty(int tiltAngleProperty) {
		this.tiltAngleProperty = tiltAngleProperty;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public double getTimeout() {
		return timeout;
	}

	public void setTimeout(double timeout) {
		this.timeout = timeout;
	}

	public int getValidity() {
		return validity;
	}

	public void setValidity(int validity) {
		this.validity = validity;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public double getMaxPower() {
		return maxPower;
	}

	public void setMaxPower(double maxPower) {
		this.maxPower = maxPower;
	}

	
	
	
	
	
    
}
