package in.vnl.sms.model;

import java.util.Set;

public class RolePojo extends BasePojo {
	private long id;
	private String role;
	private int active;
	private Set<UserPojo> users;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	
	
	public Set<UserPojo> getUsers() {
		return users;
	}
	public void setUsers(Set<UserPojo> users) {
		this.users = users;
	}
	@Override
	public String toString() {
		return "RolePojo [id=" + id + ", role=" + role + ", active=" + active + "]";
	}
	
	
	
	
}
