package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="systemconfiguration_data")
public class SystemConfigurationData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@Column(name="ugs")	
	private Integer ugs;
	
	@Column(name="tmdas")
	private Integer tmdas;
	
	@Column(name="auto")
	private Integer auto;
	
	@Column(name="timeout")
	private Integer timeout;
	
	@Column(name="validity")
	private Integer validity;
	
	@Column(name="selftimeout")
	private Double selftimeout;
	
	@Column(name="selfvalidity")
	private Integer selfvalidity;

	@Column(name="profile")
	private String profile;
	
	@Column(name="range")
	private String range;
	
	public SystemConfigurationData createCopy(SystemConfigurationData scd, String profile) {
		SystemConfigurationData scd1 = new SystemConfigurationData();
		
		scd1.setUgs(scd.getUgs());
		scd1.setTmdas(scd.getTmdas());
		scd1.setAuto(scd.getAuto());
		scd1.setTimeout(scd.getTimeout());
		scd1.setValidity(scd.getValidity());
		scd1.setSelftimeout(scd.getSelftimeout());
		scd1.setSelfvalidity(scd.getSelfvalidity());
		scd1.setProfile(profile);
		return scd1;
	}

	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRange() {
		return range;
	}



	public void setRange(String range) {
		this.range = range;
	}



	public Integer getUgs() {
		return ugs;
	}

	public void setUgs(Integer ugs) {
		this.ugs = ugs;
	}


	public Integer getTmdas() {
		return tmdas;
	}

	public void setTmdas(Integer tmdas) {
		this.tmdas = tmdas;
	}

	public Integer getAuto() {
		return auto;
	}

	public void setAuto(Integer auto) {
		this.auto = auto;
	}
	
	public Integer getTimeout() {
		return timeout;
	}

	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}


	public Integer getValidity() {
		return validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

		
	public Double getSelftimeout() {
		return selftimeout;
	}

	public void setSelftimeout(Double selftimeout) {
		this.selftimeout = selftimeout;
	}
	
	public Integer getSelfvalidity() {
		return selfvalidity;
	}

	public void setSelfvalidity(Integer selfvalidity) {
		this.selfvalidity = selfvalidity;
	}
	
	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}
	
	public String toString() {
		return "SystemConfigurationData [ugs=" + ugs +",tmdas=" +tmdas+",timeout=" +timeout+" ,validity=" +validity+",selftimeout=" +selftimeout+",Range=" +range+"]";
	}
	
		
	
}
