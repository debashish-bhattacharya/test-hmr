package in.vnl.sms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="commands")
public class Commands 
{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private int nodeId;
	private String tag;
	private String cmd;
	private boolean status;
	
	public Commands() 
	{
		super();
		// TODO Auto-generated constructor stub
	}

	public Commands(long id, int nodeId, String tag, String cmd, boolean status) {
		super();
		this.id = id;
		this.nodeId = nodeId;
		this.tag = tag;
		this.cmd = cmd;
		this.status = status;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNodeId() {
		return nodeId;
	}

	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getCmd() {
		return cmd;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}
	
	
	
}
