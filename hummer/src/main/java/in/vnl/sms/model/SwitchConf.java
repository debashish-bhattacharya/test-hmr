package in.vnl.sms.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="switch_conf")
public class SwitchConf {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="sid")
	private int  sid;

	@Column(name="pathid")
	private int pathid;
	
	@Column(name="switchid")
	private int switchid;
	
	@Column(name="val")
	private String val;
	
	@Column(name="seq")
	private int seq;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public int getPathid() {
		return pathid;
	}

	public void setPathid(int pathid) {
		this.pathid = pathid;
	}

	public int getSwitchid() {
		return switchid;
	}

	public void setSwitchid(int switchid) {
		this.switchid = switchid;
	}

	public String getVal() {
		return val;
	}

	public void setVal(String val) {
		this.val = val;
	}

	public int getSeq() {
		return seq;
	}

	public void setSeq(int seq) {
		this.seq = seq;
	}
}


