package in.vnl.sms.model;

import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name="audit_log")
public class AuditLog
{
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
//	@Column(name="opr_id")
//	private int oprId;
	
	@Column(name="log_type")
	private String logType;
	
	@Column(name = "logtime")
	private String createdDateTime;
	
	@Column(name="description")
	private String description;
	
//	@Column(name="users")
//	private String user;
//	
//	@Column(name="result")
//	private String result;
		
	public long getId() 
	{
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}
	
//	public int getOprId() 
//	{
//		return oprId;
//	}
//
//	public void setResult(int oprId) 
//	{
//		this.oprId = oprId;
//	}
	
	public String getLogType() 
	{
		return logType;
	}

	public void setLogType(String logType) 
	{
		this.logType = logType;
	}

	public String getCreatedDateTime() 
	{
		return createdDateTime;
	}

	public void setCreatedDateTime(String createdDateTime) 
	{
		this.createdDateTime = createdDateTime;
	}

	public String getDescription() 
	{
		return description;
	}

	public void setDescription(String description) 
	{
		this.description = description;
	}

	@Override
	public String toString() 
	{
		return "AuditLog [id=" + id + ", logTypeId=" + logType + ", createdDateTime=" + createdDateTime + ""
				+ ", description=" + description + "]";
	}
	
}
