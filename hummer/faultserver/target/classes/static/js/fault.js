var deleteRecord = function(id)
{
	$.ajax(
			{
				url:serviceUrls.deleteRecord,
				data:{"registrationNo":registrationNo},
				success:function(data)
				{
					$('#'+registrationNo).remove();
				}
			});
}

var viewRecord = function(registrationNo)
{
	$.ajax(
			{
				url:serviceUrls.viewRecord,
				data:{"registrationNo":registrationNo},
				success:function(data)
				{
					 $('#regNo').text(data.registrationNo);
					 $('#name').text(data.name);
					 $('#email').text(data.email);
					 $('#mobNo').text(data.mobileNo);
					 $('#course').text(data.course);
					 $('#year').text(data.year);
					 $('#gender').text(data.gender);	 
				}
			});
}
