package in.vnl.repository;

import java.util.Date;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import in.vnl.model.AlarmModel;
import in.vnl.model.HistoryModel;

@Repository
public interface AlarmRepository extends CrudRepository<AlarmModel,Integer>{	
	
	AlarmModel findByComponentIdAndManagedObjectIdAndEventId(String componentId,String managedObjectId,String eventId);
	
	List<HistoryModel> findByLogTimeBetween( Date startDate,  Date endDate);
	
	List<AlarmModel> findAllByOrderByLogTimeDesc();
	
	AlarmModel findById(int id);
	
}