package in.vnl.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import in.vnl.model.HistoryModel;
@Repository
public interface HistoryRepository extends CrudRepository<HistoryModel,Integer>{
	
    List<HistoryModel> findByLogTimeBetween( Date startDate,  Date endDate);
    
    List<HistoryModel> findAllByOrderByLogTimeDesc();
}
