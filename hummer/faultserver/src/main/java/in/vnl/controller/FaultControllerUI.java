package in.vnl.controller;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import in.vnl.common.AlarmService;
import in.vnl.common.RestApiImplementation;
import in.vnl.common.UdpServer;
import in.vnl.model.AckModel;
import in.vnl.model.AlarmModel;
import in.vnl.model.HistoryModel;

@RestController()
@RequestMapping("/api")
public class FaultControllerUI {

	private static boolean status = true;
	
	@Autowired
	private SimpMessagingTemplate template;
	
	@Autowired
	private Environment env;
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Autowired
	AlarmService alarmService;
	
	@Autowired
	ScheduleTask scheduleTask;
	
	@Autowired
	RestApiImplementation rc;
	
	@RequestMapping(value="/alarm")
	public void saveAlarm(@RequestBody AlarmModel alarmModel, HttpServletRequest request)throws Exception
	{
		if(FaultControllerUI.status == true) {
			
			String remoteAddr = "";

	        if (request != null) {
	            remoteAddr = request.getHeader("X-FORWARDED-FOR");
	            if (remoteAddr == null || "".equals(remoteAddr)) {
	                remoteAddr = request.getRemoteAddr();
	            }
	        }
	        alarmModel.setIp(remoteAddr);
	        
			alarmService.saveAlarmData(alarmModel);
			String ip = env.getProperty("application.ip");
			String port = env.getProperty("application.port");
			String postUrl = env.getProperty("application.url");
			System.out.println(ip+"..........."+port+".................."+postUrl);
			
			rc.sendAlarmData(alarmModel, ip, port, postUrl); //Send Alarm to RestAPI
		}
	}
	
	@RequestMapping(value="/acknowledgement")
	public HashMap<String,Boolean> alarmAck(@RequestBody AckModel ackModel)
	{
		HashMap<String,Boolean> hm = alarmService.setAlarmByAck(ackModel);
		return hm;
	}
	
	@RequestMapping(value="/clear")
	public void alarmData(@RequestBody AlarmModel alarmModel)
	{
		alarmService.clearAlarmData(alarmModel);
	}
	
	@RequestMapping(value="/clear/{id}")
	public HashMap<String,Integer> clearAlarmData(@PathVariable("id") int id)
	{
		System.out.println("-----------Clear by Id-----------------......."+id);
		HashMap<String,Integer> hm = alarmService.clearById(id);
		return hm;	
	}

	@RequestMapping(value="/current")
	public List<AlarmModel> currentAlarm()
	{
		return alarmService.getCurrentAlarm();
	}
	
	@RequestMapping(value="/history")
	public List<HistoryModel> historyAlarmData()
	{
		return alarmService.getHistoryAlarmData();
	}
	
	
	@RequestMapping(value="/currentData")
	public List<HistoryModel> currentAlarmData(@RequestParam("start") String start, @RequestParam("end") String end)
	{
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = objSDF.parse(start);
			endDate = objSDF.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return alarmService.getHistoryAlarm(startDate, endDate);
	}
	
	@RequestMapping(value="/historyData")
	public List<HistoryModel> historyAlarm(@RequestParam("start") String start, @RequestParam("end") String end)
	{
		SimpleDateFormat objSDF = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Date startDate = null;
		Date endDate = null;
		try {
			startDate = objSDF.parse(start);
			endDate = objSDF.parse(end);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return alarmService.getHistoryAlarm(startDate, endDate);
	}
	
	@RequestMapping(value="/udp/status")
	public HashMap<String,String> getServerStaus()throws IOException
	{
		HashMap<String, String> hm = new HashMap<String, String>();
		if(UdpServer.status==false) {
			hm.put("STATUS", "DOWN");
		}
		else {
			hm.put("STATUS", "UP");
		}
		return hm;
	}
	
	@RequestMapping(value="/udp/stop")
	public void stopUdpServer()throws IOException
	{
		 DatagramSocket ds = new DatagramSocket(); 
	     InetAddress ip = InetAddress.getLocalHost(); 
	     byte buf[] = null; 
	  
	     // convert the String input into the byte array. 
	     buf = "stop".getBytes(); 
	  
	     // Step 2 : Create the datagramPacket for sending the data. 
	     DatagramPacket DpSend = new DatagramPacket(buf, buf.length, ip, Integer.parseInt(env.getProperty("app.udpport"))); 
	  
	     // Step 3 : invoke the send call to actually send the data.
	     ds.send(DpSend);
	     ds.close();
	}
	
	@RequestMapping(value="/udp/start")
	public void startUdpServer()
	{
		if(UdpServer.status==false) {
			Thread th = new UdpServer(Integer.parseInt(env.getProperty("app.udpport")));
			applicationContext.getAutowireCapableBeanFactory().autowireBean(th);
			th.start();
		}
	}
	
	@RequestMapping(value="/http/status")
	public HashMap<String, String> getHttpServerStaus()
	{
		HashMap<String,String> hm = new HashMap<String, String>();
		if(FaultControllerUI.status==false) {
			hm.put("STATUS", "DOWN");
		}
		else {
			hm.put("STATUS", "UP");
		}
		return hm;
	}
	
	@RequestMapping(value="/http/stop")
	public void stopHttpServer()
	{
		FaultControllerUI.status = false;
	}
	
	@RequestMapping(value="/http/start")
	public void startHttpServer()
	{
		FaultControllerUI.status = true;
	}
}
